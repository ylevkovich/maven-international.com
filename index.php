<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

include('init.php');
$home_content = find('first', STATIC_PAGE_HOME, '*', "WHERE id = 1", array());
$translation = find('first', STATIC_PAGE_HOME_FIRST_BLOCK_ONE, '*', "WHERE id = 1", array());
// $interpretation = find('first', STATIC_PAGE_HOME_FIRST_BLOCK_TWO, '*', "WHERE id = 1", array());
// $transcreation = find('first', STATIC_PAGE_HOME_FIRST_BLOCK_THREE, '*', "WHERE id = 1", array());
$SI = find('first', STATIC_PAGE_HOME_FIRST_BLOCK_TWO, '*', "WHERE id = 1", array());
$CI = find('first', STATIC_PAGE_HOME_FIRST_BLOCK_THREE, '*', "WHERE id = 1", array());
$linguistic_validation = find('first', STATIC_PAGE_HOME_FIRST_BLOCK_FOUR, '*', "WHERE id = 1", array());
$training = find('first', STATIC_PAGE_HOME_SECOND_BLOCK_FOUR, '*', "WHERE id = 1", array());
$transcreation2 = find('first', STATIC_PAGE_HOME_SECOND_BLOCK_ONE, '*', "WHERE id = 1", array());
$equipment = find('first', STATIC_PAGE_HOME_SECOND_BLOCK_THREE, '*', "WHERE id = 1", array());
$lorem = find('first', STATIC_PAGE_HOME_SECOND_BLOCK_TWO, '*', "WHERE id = 1", array());
$translation_agency = find('first', STATIC_PAGE_HOME_TRANSLATION_AGENCY, '*', "WHERE id = 1", array());
if (isset($_POST['btnsubmit'])) {
    $_SESSION['source_language'] = $_POST['source_language'];
    $_SESSION['translate_to'] = $_POST['translate_to'];
    $_SESSION['deadline'] = $_POST['deadline'];
    header("location: /translation.php#" . base64_encode($_POST['servise_title']));
    exit;

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= $home_content['page_title']; ?></title>
    <meta name="description" content="<?= $home_content['meta_description']; ?>">
    <meta name="keywords" content="<?= $home_content['meta_keyward']; ?>">
    <link rel='shortcut icon' type='image/x-icon'
          href='<?php echo(DOMAIN_NAME_PATH); ?>images/misc/<?= $general_settings['favicon']; ?>'/>
    <?php include('meta.php'); ?>

    <!-- <script type="text/javascript" src="/js/jquery-2.2.4.min.js"></script>

        <script type="text/javascript" src="/js/bootstrap.min.js"></script> -->

    <link type="text/css" rel="stylesheet" href="css/maventextblock.css">
    <style type="text/css">
        h1, h2, h3, h4, h5, h6, .myheading {
            margin: 0 0 2px 0;
            font-size: 22px;
            line-height: normal;
            font-weight: normal;
            text-transform: capitalize;
        }

        .maventext {
            border: 2px solid rgba(244, 244, 244, 0.41);
            padding-right: 30px;
            padding-left: 30px;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-bottom: 25px;
            font-family: candara;
        }
    </style>
    <?php include 'inc/recaptcha/add_js.php'; ?>
</head>
<body>
<?php include('mobile_menu.php'); ?>
<?php include('topbar.php'); ?>
<?php include('home_header.php'); ?>
<?php include('slider.php'); ?>
<div class="translation">
    <div class="container">
        <div class="row">

            <!--1. translation -->
            <div class="col-xs-3 col-md-3 col-sm-6 translation-item wow fadeInUp" data-wow-duration="1s"
                 data-wow-delay="0.2s">
                <div class="row">
                    <div class="col-sm-4 text-center">
                        <a href="translation.php?id=NA">
                            <img src="<?php echo(DOMAIN_NAME_PATH); ?>images/home/<?= $translation['image']; ?>"
                                 class="res" style="border-radius:50%" alt=""
                                 onerror="this.src='<?= DOMAIN_NAME_PATH; ?>images/misc/no_image.jpg';">
                        </a>
                    </div>
                    <div class="col-sm-8">
                        <h1><a href="translation.php?id=NA"><?= $translation['title_' . $_SESSION['lan']]; ?></a></h1>
                        <a href="translation.php?id=NA">
                            <p class="hidden-xs"><?= $translation['description_' . $_SESSION['lan']]; ?>

                                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>

                            </p>
                        </a>
                    </div>
                </div>
            </div>
            <!-- interpretation (WHERE TO)-->
            <!-- <div class="col-xs-3 col-md-3 col-sm-6 translation-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">
					<div class="row">
						<div class="col-sm-4 text-center">
							<a  href="translation.php?id=NA">
								<img src="<?php echo(DOMAIN_NAME_PATH); ?>images/home/172095_INTERPRETATION.png" style="border-radius:50%" class="res" alt="" onerror="this.src='<?= DOMAIN_NAME_PATH; ?>images/misc/no_image.jpg';">
							<a  href="translation.php?id=NA">
						</div>
						<div class="col-sm-8">
							<h1><?= $interpretation['title_' . $_SESSION['lan']]; ?></h1>
							<p class="hidden-xs"><?= $interpretation['description_' . $_SESSION['lan']]; ?>
									<img src="images/readmore.png" width="15" height="auto" border="0" alt="">
								</p>
							</div>
						</div>
					</div> -->
            <!--2. SI -->
            <div class="col-xs-3 col-md-3 col-sm-6 translation-item wow fadeInUp" data-wow-duration="1s"
                 data-wow-delay="0.7s">
                <div class="row">
                    <div class="col-sm-4 text-center">
                        <a href="translation.php?id=Ng">
                            <img src="<?php echo(DOMAIN_NAME_PATH); ?>images/home/simultaneous_interpretation_2.png"
                                 style="border-radius:50%" class="res" alt=""
                                 onerror="this.src='<?= DOMAIN_NAME_PATH; ?>images/misc/no_image.jpg';">
                        </a>
                    </div>
                    <div class="col-sm-8">
                        <h1><a href="translation.php?id=Ng"><?= $SI['title_' . $_SESSION['lan']]; ?></a></h1>
                        <a href="translation.php?id=Ng">
                            <p class="hidden-xs"><?= $SI['description_' . $_SESSION['lan']]; ?>

                                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                            </p></a>
                    </div>
                </div>
            </div>
            <!--3. CI -->
            <div class="col-xs-3 col-md-3 col-sm-6 translation-item wow fadeInUp" data-wow-duration="1s"
                 data-wow-delay="0.7s">
                <div class="row">
                    <div class="col-sm-4 text-center">
                        <a href="translation.php?id=Nw">
                            <img src="<?php echo(DOMAIN_NAME_PATH); ?>images/home/17299_maven_icon-14.png"
                                 style="border-radius:50%" class="res" alt=""
                                 onerror="this.src='<?= DOMAIN_NAME_PATH; ?>images/misc/no_image.jpg';">
                        </a>
                    </div>
                    <div class="col-sm-8">
                        <h1><a href="translation.php?id=Nw"><?= $CI['title_' . $_SESSION['lan']]; ?></a></h1>
                        <a href="translation.php?id=Nw">
                            <p class="hidden-xs"><?= $CI['description_' . $_SESSION['lan']]; ?>

                                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                            </p></a>
                    </div>
                </div>
            </div>
            <!-- linguistic validation -->
            <div class="col-xs-3 col-md-3 col-sm-6 translation-item wow fadeInUp" data-wow-duration="1s"
                 data-wow-delay="0.7s">
                <div class="row">
                    <div class="col-sm-4 text-center">
                        <a href="translation.php?id=OQ">
                            <img src="<?php echo(DOMAIN_NAME_PATH); ?>images/home/<?= $linguistic_validation['image']; ?>"
                                 class="res" alt="" style="border-radius:50%"
                                 onerror="this.src='<?= DOMAIN_NAME_PATH; ?>images/misc/no_image.jpg';">
                        </a>
                    </div>
                    <div class="col-sm-8">
                        <h1>
                            <a href="translation.php?id=OQ"><?= $linguistic_validation['title_' . $_SESSION['lan']]; ?></a>
                        </h1><a href="translation.php?id=OQ">
                            <p class="hidden-xs"><?= $linguistic_validation['description_' . $_SESSION['lan']]; ?>

                                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>

                            </p></a>
                    </div>
                </div>
            </div>
            <!-- transcreation -->
            <!-- <div class="col-xs-3 col-md-3 col-sm-6 translation-item no-border pos-res wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">
					<div class="row">
						<div class="col-sm-4 text-center">
							<a  href="translation.php?id=NQ">
								<img src="<?php //echo(DOMAIN_NAME_PATH);?>images/home/<? //=$transcreation['image'];?>" class="res" alt=""  style="border-radius:50%" onerror="this.src='<?= DOMAIN_NAME_PATH; ?>images/misc/no_image.jpg';">
							</a>
						</div>
						<div class="col-sm-8">
							<h1><? //=$transcreation['title_'.$_SESSION['lan']];?></h1>
							<p class="hidden-xs"><? //=$transcreation['description_'.$_SESSION['lan']];?>
							<a  href="translation.php?id=NQ">
							<img src="images/readmore.png" width="15" height="auto" border="0" alt="">
							</a>
							</p>
						</div>
					</div>
				</div> -->

            <div class="clearfix"></div>
            <div class="client-say hidden-sm hidden-md hidden-lg"><?php echo(LANG_74); ?></div>
        </div>
    </div>
</div>
<!-- <div class="Training hidden-xs">
		<div class="container">
			<div class="row">
				<div class="spd">
				<div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">
					<div class=" upper-cs">
						<div class="col-xs-4 text-center">
							<img src="<?php echo(DOMAIN_NAME_PATH); ?>images/home/<?= $transcreation2['image']; ?>" class="res" border="0" alt="" onerror="this.src='<?= DOMAIN_NAME_PATH; ?>images/misc/no_image.jpg';">
						</div>
						<div class="col-xs-8">
							<h1><?= $transcreation2['title_' . $_SESSION['lan']]; ?></h1>
						</div>
						<div class="clearfix"></div>
					</div>
					<div >
						<div class="lower-cs"><?= $transcreation2['description_' . $_SESSION['lan']]; ?> &nbsp;&nbsp;&nbsp;<a href="<?= $transcreation2['link']; ?>"><img src="images/arrow.png" width="auto" height="7" border="0" alt=""></a></div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">
					<div class=" upper-cs">
						<div class="col-xs-4 text-center">
							<img src="<?php echo(DOMAIN_NAME_PATH); ?>images/home/<?= $lorem['image']; ?>" class="res" border="0" alt="" onerror="this.src='<?= DOMAIN_NAME_PATH; ?>images/misc/no_image.jpg';">
						</div>
						<div class="col-xs-8">
							<h1><?= $lorem['title_' . $_SESSION['lan']]; ?></h1>
						</div>
						<div class="clearfix"></div>
					</div>
					<div >
						<div class="lower-cs"><?= $lorem['description_' . $_SESSION['lan']]; ?> &nbsp;&nbsp;&nbsp;<a href="<?= $lorem['link']; ?>"><img src="images/arrow.png" width="auto" height="7" border="0" alt=""></a></div>
					</div>
				</div>

				<div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">
					<div class=" upper-cs">
						<div class="col-xs-4 text-center">
							<img src="<?php echo(DOMAIN_NAME_PATH); ?>images/home/<?= $equipment['image']; ?>" class="res" border="0" alt="" onerror="this.src='<?= DOMAIN_NAME_PATH; ?>images/misc/no_image.jpg';">
						</div>
						<div class="col-xs-8">
							<h1><?= $equipment['title_' . $_SESSION['lan']]; ?></h1>
						</div>
						<div class="clearfix"></div>
					</div>
					<div >
						<div class="lower-cs"><?= $equipment['description_' . $_SESSION['lan']]; ?> &nbsp;&nbsp;&nbsp;<a href="<?= $equipment['link']; ?>"><img src="images/arrow.png" width="auto" height="7" border="0" alt=""></a></div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">
					<div class=" upper-cs">
						<div class="col-xs-4 text-center">
							<img src="<?php echo(DOMAIN_NAME_PATH); ?>images/home/<?= $training['image']; ?>" class="res" border="0" alt="" onerror="this.src='<?= DOMAIN_NAME_PATH; ?>images/misc/no_image.jpg';">
						</div>
						<div class="col-xs-8">
							<h1><?= $training['title_' . $_SESSION['lan']]; ?></h1>
						</div>
						<div class="clearfix"></div>
					</div>
					<div >
						<div class="lower-cs"><?= $training['description_' . $_SESSION['lan']]; ?> &nbsp;&nbsp;&nbsp;<a href="<?= $training['link']; ?>"><img src="images/arrow.png" width="auto" height="7" border="0" alt=""></a></div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div> -->

<?php //include('homepage_testimonials.php');?>
<?php include('home_supporter.php'); ?>
<!-- <div class="agency"> -->
<!-- <div class="container" style="padding:50px 50px; background:#f8f8f8; text-align: center;">
			<div class="row">
				<div class="col-md-12">
					<?= $translation_agency['translation_agency_' . $_SESSION['lan']]; ?>
				</div>
			</div>
		</div>
		<br/> -->
<!-- </div> -->
<!-- maven text -->
<div class="wrapper1 bgded overlay toolight"
     style="background: url('<?php echo(DOMAIN_NAME_PATH); ?>images/slideshow/globus3.jpg') bottom left / cover no-repeat;">
    <section class="hoc container clear" style="color: #fff;">
        <!-- ################################################################################################ -->
        <div class="sectiontitle">

        </div>
        <div class="group mytestimonials">
            <article>
                <div class="maventext" style="border: none;">
                    <span style="text-align: center;"><h6 class="myheading">MAVEN</h6><em>I N T E R N A T I O N A L</em></span>
                    <hr>
                    <blockquote
                            style="text-align: center"><?= $translation_agency['translation_agency_' . $_SESSION['lan']]; ?></blockquote>
                    <hr>
                </div>
            </article>
        </div>
        <!-- ################################################################################################ -->
    </section>
</div>
<!-- end of maven text -->


<section id="cases">
    <div class="container">
        <div class="row">
            <div class="col-md-12 cases-block">
                <div class="col-md-6">

                    <!-- first block -->
                    <div class="col-md-12" style="padding-top: 8px;">
                        <div class="block1">
                            <div class="row" style="padding-bottom: 30px;">
                                <div class="col-md-5" style="padding-right: 0!important;">
                                    <img class="img-responsive block-img-1" src="/images/quality.png" alt="">
                                </div>
                                <div class="col-md-7 text-justify" style="padding-right: 42px; line-height: 1.4">
                                    ISO 17100 is the only international standard that is specific to translation.
                                    Conformity to ISO 17100 means that all translations are required to go through an
                                    editorial process, carried out by translators who meet strict qualification
                                    criteria. This standard requires that all project specifications are clearly
                                    elaborated in a binding agreement with clients.
                                </div>
                            </div>
                            <!-- 	<p style="padding: 10px 25px; text-align: justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit soluta iure aliquid, sint ab architecto sequi facilis nobis voluptatem optio.</p>		 -->


                            <div class="text-in-block text-center">
                                <span class="text-center text-client-title">Compliance with ISO 17100</span>
                            </div>
                            <div class="box-block">
                                <a href="quality.php">
                                    <div class="like-button text-center">More on our quality</div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- second block -->
                    <div class="col-md-12">
                        <div class="block2">
                            <div class="row" style="padding-bottom: 30px;">
                                <div class="col-md-5" style="padding-right: 0!important;">
                                    <img class="img-responsive block-img-2" src="/images/icon2.png" alt=""></div>
                                <div class="col-md-7 text-justify" style="padding-right: 42px;line-height: 1.4">
                                    We are equipped with latest technology in the industry. Our technology allows us to
                                    produce quality, consistent and coherent large volume translation work in the most
                                    efficient and cost-effective manner possible.
                                </div>
                            </div>
                            <!-- 	<p style="padding: 10px 25px; text-align: justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit soluta iure aliquid, sint ab architecto sequi facilis nobis voluptatem optio.</p> -->

                            <div class="text-in-block text-center">
                                <span class="text-center text-client-title">SDL associate partners</span>
                            </div>
                            <div class="box-block">
                                <a href="tecnology.php">
                                    <div class="like-button text-center">More on our technology</div>
                                </a>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-md-6 slider-with-images" style="padding: 0!important;">
                    <h1 class="slider-title">Satisfied Clients – Case Studies</h1>

                    <div id="carousel-example-generic" class="carousel slide" data-interval="false"
                         data-ride="carousel">

                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox" style="line-height: 1.4">
                            <!-- 	<div class="item active">
                                <img style="position: relative;" src="/images/cases_homepage/slide1.jpg" alt="...">
                                <div class="test-slider-text-box">
                                    <h3 class="text-center slider-text-title">The Asian Football Confederation:</h3><br>
                                    <p class="small-text">Solutions provided: <br>
                                        -&nbsp;&nbsp;Simultaneous and consecutive interpretation services <br>
                                        -&nbsp;&nbsp;Translation services <br><br>
                                        The Asian Football Confederation (AFC) is the governing body of association football in Asia and Australia and one of the six confederations making up FIFA.
                                    </p>
                                </div>
                            </div>
                        -->
                            <div class="item active">
                                <img src="/images/cases_homepage/1.jpg" alt="...">
                                <div class="test-slider-text-box">
                                    <h3 class="text-center slider-text-title">Asian Football Confederation</h3><br>
                                    <p class="small-text">
                                        The Asian Football Confederation (AFC) is the governing body of association
                                        football in Asia and Australia and one of the six confederations making up FIFA.
                                        <br> <br>
                                        <b>Solutions provided:</b> <br>
                                        -&nbsp;&nbsp;Simultaneous interpretation <br>
                                        -&nbsp;&nbsp;Consecutive interpretation<br>
                                        -&nbsp;&nbsp;Translation<br>
                                        -&nbsp;&nbsp;Linguistic validation<br>
                                    </p>
                                </div>
                            </div>


                            <div class="item">
                                <img src="/images/cases_homepage/2.jpg" alt="...">
                                <div class="test-slider-text-box">
                                    <p class="small-text"><br>
                                        <b>Challenge:</b> <br><br>
                                        As an international organization in one of the most diverse regions in the
                                        world, AFC requires the following language related services: simultaneous
                                        interpretation, consecutive interpretation, translation, and linguistic
                                        validation. <br> <br>
                                        Maven International has been the one-stop language solutions provider for AFC
                                        since 2008.
                                    </p>
                                </div>
                            </div>


                            <div class="item">
                                <img src="/images/cases_homepage/slide6.jpg" alt="...">
                                <div class="test-slider-text-box">
                                    <h3 class="text-center slider-text-title">Translation</h3><br>
                                    <p class="small-text">
                                        Maven has a long-standing relationship with the AFC. In that time, their
                                        consultants have provided thousands of pages of translations in a number of
                                        different global languages. Maven are entrusted with translating
                                        highly-sensitive and commercially-confident documents, and have done so in a
                                        timely and professional manner. I highly recommend them.
                                    <p class="text-right" style="font-size: 12px;">James Kitching <br>
                                        Head of Sports Legal Services, Disciplinary, and Governance at the Asian
                                        Football Confederation <br>
                                        james.kitching@the-afc.com</p>
                                    </p>
                                </div>
                            </div>

                            <div class="item">
                                <img src="/images/cases_homepage/slide5.jpg" alt="...">
                                <div class="test-slider-text-box">
                                    <h3 class="text-center slider-text-title">Simultaneous Interpretation</h3><br>
                                    <p class="small-text">FIFA-AFC congress October 2014 <br><br>
                                        Maven International has been providing AFC with professional simultaneous
                                        interpretation services for numerous high level events on a yearly basis since
                                        2008.
                                    </p>
                                </div>
                            </div>

                            <div class="item">
                                <img src="/images/cases_homepage/3.jpg" alt="...">
                                <div class="test-slider-text-box">
                                    <h3 class="text-center slider-text-title">Consecutive interpretation</h3><br>
                                    <p class="small-text">
                                        AFC requires a high frequency of top level consecutive interpretation services,
                                        mostly communicated at extremely short notice. The topics are mostly delicate
                                        and confidential, and require a very careful selection of professional
                                        interpreters.
                                    </p>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="btnn">
                        <a href="/causes.php">
                            <div class="btnn-under-slider text-center">read more cases</div>
                        </a>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>


<section id="learn-about-solution-block">
    <div class="container">
        <div class="row">
            <div class="col-md-12 lr-ab-sol-block">
                <h1 class="text-center normal-h1 lr-ab-sol-title">I want to learn more about your solutions <br>Call me
                    back</h1><br><br>
                <form action="/callme.php" class="form-horizontal cform-homepage" id="contactform" method="POST"
                      enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-xs-5"><?php echo(LANG_48); ?></label>
                                <div class="col-xs-7">
                                    <input type="text" name="name"
                                           class="form-control form-hm-page validate[required] ad-color-hm-page validate[required]"
                                           value="<?php echo isset($_POST['name']) && $_POST['name'] != '' ? $_POST['name'] : '' ?>"
                                           placeholder="<?php echo(LANG_64); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-5">Company name</label>
                                <div class="col-xs-7">
                                    <input type="text" name="company"
                                           class="form-control form-hm-page ad-color-hm-page"
                                           value="<?php echo isset($_POST['name']) && $_POST['name'] != '' ? $_POST['name'] : '' ?>"
                                           placeholder="<?php echo(LANG_64); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-xs-5"><?php echo(LANG_50); ?> </label>
                                <div class="col-xs-7">
                                    <input type="text" name="phone"
                                           value="<?php echo isset($_POST['phone']) && $_POST['phone'] != '' ? $_POST['phone'] : '' ?>"
                                           class="form-control form-hm-page ad-color-hm-page validate[required],custom[phone]"
                                           placeholder="<?php echo(LANG_67); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="homepage-form-text control-label col-xs-5">Choose language*</label>
                                <div class="col-xs-7">
                                    <select name="corporate"
                                            class="form-control ad-color-hm-page form-hm-page validate[required]"
                                            dir="ltr" placeholder=""
                                            style="background:#2B91A9 url(images/company-homepage.png)no-repeat 97% center !important">
                                        <option value="Eng" selected>English<?php //echo(LANG_46);?></option>
                                        <option value="Eng">Russian<?php //echo(LANG_46);?></option>
                                        <option value="Ar">Arabic<?php //echo(LANG_46);?></option>
                                        <option value="Ma">Malay<?php //echo(LANG_46);?></option>
                                        <option value="Tu">Turkish<?php //echo(LANG_46);?></option>
                                        <!-- <option value="Corporate"><?php //echo(LANG_47);?></option> -->
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div id="html_element" class="g-recaptcha" data-sitekey="6Le6vTcUAAAAAFK2nL_oKcCap7a7EU4Tu8iUtL_3"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group " style="margin-bottom:0px">
                        <div class="col-md-12">
                            <button id="btn_callMe" class="watch text-b bttm_gp hm-but pull-right" name="btnsubmit" type="submit"
                                    style="background:#fff"><?php echo(LANG_63); ?>&nbsp;&nbsp;&nbsp;&nbsp;<i
                                        class="fa fa-arrow-right fa-contact-sent" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    <script type="text/javascript">
                        var widget = null;
                        var onloadCallback = function() {
                            widget = grecaptcha.render('html_element', {
                                'sitekey' : '6Le6vTcUAAAAAFK2nL_oKcCap7a7EU4Tu8iUtL_3'
                            });
                        };

                        var btn = document.getElementById('btn_callMe');
                        var html_element = document.getElementById('html_element');
                        var div = document.createElement('div');
                        div.innerHTML = "<span>Заполните капчу</span>";

                        btn.onclick = function () {
                            if(!grecaptcha.getResponse(widget)){
                                html_element.appendChild(div);
                                return false;
                            }
                        }
                    </script>
                </form>
            </div>
        </div>
    </div>
</section>


<?php //include('map_home.php');?>
<?php include('footer.php'); ?>
<?php include('script.php'); ?>
</body>
</html>
