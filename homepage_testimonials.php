<?php
$where_clause ="WHERE 1 ORDER BY id ASC LIMIT 3";
$testimonials_list = find('all', MANAGE_TESTIMONIALS, '*', $where_clause, array());
if(!empty($testimonials_list))
{
?>
<div class="client-mob hidden-sm hidden-md hidden-lg wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s" >
	<div class="container" style="padding:0px 7px">
		<div class="swiper-container" id="swiper-container">
			<div class="swiper-wrapper">
				<?php
				foreach($testimonials_list as $testimonials_key => $testimonials_value)
				{
				?>
				<div class="swiper-slide" >
					<div class="col-xs-3" style="padding:0px">
						<img src="<?php echo(DOMAIN_NAME_PATH);?>images/testmonials/<?php echo($testimonials_value['testimonials_icon']);?>" onerror="this.src='<?=DOMAIN_NAME_PATH;?>images/misc/no_image.jpg';" style="width:100%; max-width:80px" alt="">
					</div>
					<div class="col-xs-9">
						<div class="test-describe" style="padding:0px; background:none; text-align:left">
							<p style="color:#fff; margin-bottom:0px"><?php echo($testimonials_value['description_'.$_SESSION['lan']]);?></p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>				
				<?php
				}
				?>
			</div>
			<div class="swiper-pagination" id="swiper-pagination"></div>
			<div class="swiper-button-next" id="swiper-button-next" style="height:auto; background:none; top:100%; right:0px"><img src="images/aro.png" width="25" height="25" border="0" alt=""></div>
		</div>
	</div>
  </div> 
  <div class="testimonials hidden-xs">
	<div class="container">
		<div class="row">
			<div class="manage-lowgap">
			<?php
				foreach($testimonials_list as $testimonials_key => $testimonials_value)
				{
			?>
			<div class="col-md-4 lowgap wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s">
				<div class="test-top" style="background:url(<?php echo(DOMAIN_NAME_PATH);?>images/testmonials/<?php echo($testimonials_value['testimonials_image']);?>)no-repeat center center / cover">
					<img src="<?php echo(DOMAIN_NAME_PATH);?>images/testmonials/<?php echo($testimonials_value['testimonials_icon']);?>" class="client-logo1" alt="" onerror="this.src='<?=DOMAIN_NAME_PATH;?>images/misc/no_image.jpg';">
				</div>
				<div class="test-describe">
					<p><?php echo($testimonials_value['description_'.$_SESSION['lan']]);?></p>
					<div class="clearfix"></div>
					<div class="min-gp"></div>
					<div class="row">
						<div class="col-xs-4">
							<a href="javascript:void(0)" class="testiread"><?php echo(LANG_71);?>&nbsp;&nbsp;&nbsp;<img src="images/readmore.png" width="auto" height="15" border="0" alt=""></a>
						</div>
						<div class="col-md-8 text-right testdetails">
							<div  >
								<strong><?php echo($testimonials_value['client_name_'.$_SESSION['lan']]);?></strong>
							</div>
							<?php echo($testimonials_value['client_description_'.$_SESSION['lan']]);?>
						</div>
					</div>
				</div>
			</div>
			<?php
			}
			?>
			<div class="clearfix"></div>
			<div class="col-md-12 text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s" >
				<button class="watch text-b" onclick="window.location.href='<?php echo(DOMAIN_NAME_PATH);?>clients.php'"><?php echo(LANG_23);?>&nbsp;&nbsp;&nbsp;<img src="images/arrow.png" width="auto" height="7" border="0" alt=""></button>
			</div>
			</div>
		</div>
	</div>
  </div>
<?php } ?>