<div class="col-md-5 lpd">
	<div class="sp-body">
		<div class="col-xs-3 ">
			<img src="<?php echo(DOMAIN_NAME_PATH);?>images/service/<?php echo($active_service_name['service_icon']);?>" onerror="this.src='<?=DOMAIN_NAME_PATH;?>images/misc/no_image.jpg';" style="height:60px; width:60px; border-radius:50%; padding:1px; background:#3a6b93;" alt="" class="mhw">
		</div>
		<div class="col-xs-9 npd">
			<h1 class="con-hd"><?php echo(LANG_25);?> <?=$active_service_name['service_title_'.$_SESSION['lan']];?></h1>
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-12 npd ">
			<div class="con-block text-justify">
				<?=nl2br($active_service_name['service_description_'.$_SESSION['lan']]);?> <br> <br>


				<?php if ($active_service_name['service_title_'.$_SESSION['lan']] == "Translation"): ?>
					<div class="col-md-12">
						<div class="col-md-6">	<div class="box-block">
							<a href="tecnology.php">
								<div class="like-button text-center">Our technology</div>
							</a>
						</div></div>
						<div class="col-md-6">	<div class="box-block">
							<a href="quality.php">
								<div class="like-button text-center">Our quality</div>
							</a>
						</div></div>
					</div>
					
				<?php endif ?>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>