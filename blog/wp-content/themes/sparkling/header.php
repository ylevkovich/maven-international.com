<?php
/* *
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package sparkling
 */

if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) header('X-UA-Compatible: IE=edge,chrome=1'); ?>
<!doctype html>
<!--[if !IE]>
	<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>
	<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>
	<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
	<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->
	<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="<?php echo of_get_option( 'nav_bg_color' ); ?>">
		<link rel="profile" href="http://gmpg.org/xfn/11">

		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>
		<a class="sr-only sr-only-focusable" href="#content">Skip to main content</a>
		<div id="page" class="hfeed site">

			<header id="masthead" class="site-header" role="banner">
				<nav class="navbar navbar-default <?php if( of_get_option( 'sticky_header' ) ) echo 'navbar-fixed-top'; ?>" role="navigation">
					<div class="container">
						<div class="row">
							<div class="site-navigation-inner col-sm-12" style="background-color: #fff;">
								<div class="navbar-header">
									<button type="button" class="btn navbar-toggle" style="margin-top: 35px;" data-toggle="collapse" data-target=".navbar-ex1-collapse">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>

									<a class="navbar-brand" rel="home" href="#" title="Buy Sell Rent Everyting">
										<img style="max-width:130px; margin-top: -17px;"
										src="<?php echo get_template_directory_uri();?>/img/log.png">
									</a>
								</div>
								<div class="row">
									<div class="pull-right hidden-xs hidden-sm" style="margin-right: 40px;">
										<ul class="nav navbar-nav navbar-right" style="padding-top: 5px;">									
											<li style="margin-top: 70px;padding: 0 15px; border-right: 1px solid #cfcfcf;color: #000;">
												<i class="fa fa-envelope" aria-hidden="true"></i> 
												Info@maven-international.com
											</li>

											<li style="margin-top: 70px;padding: 0 15px; border-right: 1px solid #cfcfcf; color: #000;">
												<i class="fa fa-phone" aria-hidden="true"></i> 
												+185550 MAVEN (62836)
												<br>
												<span style="font-size: 75%;
												margin-top: -7px;
												position: absolute;
												color: #666!important;"    
												>24 hour service (Mon-Fri) Toll Free</span>
											</li>
										
											<li class="social-fcb" style="border:0; margin-top: 50px;"> <a href="https://www.facebook.com/MavenInternational/" style="margin-right:0;padding-right: 1px"><i class="fa fa-facebook-square fa-2x" aria-hidden="true" style="color: #000"></i></a> </li>
											<li class="social-fcb" style="border:0; margin-top: 50px;"> <a href="https://twitter.com/MavenTranslate" style="margin-right:0;padding-right: 1px"><i class="fa fa-twitter-square fa-2x" aria-hidden="true" style="color: #000"></i></a> </li>
											<li class="social-fcb" style="border:0; margin-top:50px;"> <a href="https://www.linkedin.com/company-beta/3250231/" style="margin-right:0; padding-right: 0px;"><i class="fa fa-linkedin-square fa-2x" aria-hidden="true" style="color: #000"></i></a> </li>	

										</ul>				
									</div>
								</div>

								<?php sparkling_header_menu(); // main navigation ?>		

							</div>
						</div>
					</div>
				</nav><!-- .site-navigation -->
			</header><!-- #masthead -->

			<div id="content" class="site-content">

				<div class="top-section">
					<?php sparkling_featured_slider(); ?>
					<?php sparkling_call_for_action(); ?>
				</div>

				<div class="container main-content-area">
					<?php $layout_class = get_layout_class(); ?>
					<div class="row <?php echo $layout_class; ?>">
						<div class="main-content-inner <?php echo sparkling_main_content_bootstrap_classes(); ?>">
