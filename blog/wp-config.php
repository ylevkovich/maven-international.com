<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'maven_blog');

/** MySQL database username */
define('DB_USER', 'maven_blog');

/** MySQL database password */
define('DB_PASSWORD', 'kNc3GHoYd2MkVbq');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'w0pGHd(i()wWum?-py:3&Qo&Ggng;!y7z7NF1NbUN;RWjwob0vpF|MxH`Lnge<x_');
define('SECURE_AUTH_KEY',  '(]-TaOP%zf@f8]<aP3U>JX}W55lOOSm+zX_D`;K!Q#7^{astMCX?+.r9N5230co=');
define('LOGGED_IN_KEY',    'jtb3o .: h_x/Ez;&bo0sY([QPUBh;9x)_WWQo/I RM#-! +h*W15sP`Xg*|@nJu');
define('NONCE_KEY',        'qe,*N-AQ6uiYT%@acz[?^c&7n@>U*hkw:oV%LCYs! Z9#9{#!vpgK)Q]<^GLbFYT');
define('AUTH_SALT',        'Rb:sH}V}|_6zm6oNxmK@HG(`fpUN4zgmJ2k}V~X%n~M`z|.V4.Nz]X:EH7pkD9FS');
define('SECURE_AUTH_SALT', 'k+(|H`tvE&m35S<O]/4l$dW8T&6f8S2QdQe-oD3WLGVcJ6Fow_Va3ST_Stuy^Gc/');
define('LOGGED_IN_SALT',   'ias]ql+NqFc}h1,W0@i,3/)hs-GiW4BH)-kRWwJe%:$GJ:_NlH%zo9!3n+UTc)3b');
define('NONCE_SALT',       '.aEJ/D.pV@T(.)Q3>X6Sg0PuRqq[bzF%|R5H4L|_,qC>.&<Avv?&+/&jF6_ 5gHE');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

