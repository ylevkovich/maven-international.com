<?php

include('init.php');

$about_content = find('first', STATIC_PAGE_ABOUT_US, '*', "WHERE id = 1", array());

?>

<!DOCTYPE html>

<html lang="en">

<head>

	<title>Our tecnology</title>

	<meta name="description" content="<?=$about_content['meta_description'];?>">

	<meta name="keywords" content="<?=$about_content['meta_keyward'];?>">

	<link rel='shortcut icon' type='image/x-icon' href='<?php echo(DOMAIN_NAME_PATH);?>images/misc/<?=$general_settings['favicon'];?>' />

	<?php include('meta.php');?>

</head>

<body>		

	<?php include('mobile_menu.php');?>

	<?php include('topbar.php');?>

	<?php include('inner_page_header.php');?>	 	 

	<div class="inner_section">

		<div class="container">

			<div class="row">

				<div class="col-md-12 lpd">

					<div class="">

						<div class="col-xs-12 npd">

							<h1 class="con-hd con-hdbig">Our tecnology</h1>

						</div>

						<div class="clearfix"></div>						

					</div>

					

					<div class="sp-body">

						<div class="col-xs-12 col-md-5 npd">

							<!-- 	 <?=$about_content['page_content_'.$_SESSION['lan']];?>  -->


							<p><strong>Why technology</strong></p>				
							<p class="text-justify">Technology has become an integral part of the translation industry. Not only does it help our translators achieve more consistent translations at faster turnaround rates, it actually adds much more in terms of quality and and cost-effectiveness. At Maven International, we believe that technology and innovation are the way forward.											
							</p> <br> 
							<p><strong>Maven International is an official SDL partner, the largest computer assistance tools producer</strong></p>	

							<p class="text-justify">As part of our strategic growth plans, we partnered with SDL, the global leader in translation technology and tools. It is worth mentioning that we are the first (and so far only) SDL partners in South East Asia, the second in Turkey and fourth in Canada. </p> <br> 

							<p><strong>How technology helps our clients achieve quality translation at faster and cheaper rates</strong></p>

							<p class="text-justify">The technology benefits are realized in large volume translations over certain periods of time. This is achieved by leveraging and reusing previously translated documents (same language pairs, content, context, vocabulary..etc.) to expedite the translation process. This leads to higher efficiency and consistency levels in translation projects in terms of quality and terminology management, enabling us to provide discounts to long term clients and large volume translations. </p> <br> 


							<p class="text-center" style="font-size: 20px;"><strong>&nbsp;&nbsp;What we do</strong></p>

							<p><strong>-&nbsp;&nbsp;Translation memory:</strong></p>

							<p class="text-justify">Our technology allows us to preserve translation memory files, which can only be done with client consent. Translation memory means consistent translations of similar documents over long periods of time.</p> <br>							

							<p><strong>-&nbsp;&nbsp;Terminology management:</strong></p>

							<p class="text-justify">We understand that each organization has its own unique terminology, communication methods, acronyms, preference in choice of words, phrases that define their brands…etc. <br> 
								Terminologies form the building blocks of the translation process. We believe that prioritizing terminology management significantly improves the quality of the end result. Our technology allows us to build segregated term bases for various clients and languages. <br>
								Our terminology allows us to extract data from previously translated documents. Illustrations and notes are added where needed. The file is stored in a central location and can easily be shared with stakeholders. Clients are given access to the file and can update/edit the terminology database. Term bases, just like translation memory, can easily be shared with different translators in order to preserve translation quality and consistency. 
							</p> <br>


							<p><strong>Auto verification stage</strong></p>

							<p class="text-justify">
								To eliminate the possibility of human error, we use a translation specific verification software. Unlike your standard spell checker, our software allows us to take both source and target documents into consideration, as well as subjective and objective aspects of the translated document, and minor nuances that may have been overlooked, such as numbers, dates, and figures, in addition to the overall terminological consistency of the document based on the term base set for the project.
							</p> <br>

							<p><strong>Machine translation</strong></p>

							<p class="text-justify">
								It is Maven International policy that only human translators are engaged in our projects due to their superiority over available machine translation tools. However, the exponential growth of short-lived content on social media has created demand for machine translation. 
								Unlike Google Translate and other online translation engines, our technology enables us to greatly improve the final outcome of the translation by customizing software settings for each project, and by using a predefined term base. 
								It doesn’t stop there; at the final stage, we engage specially trained post-machine human editors to review and correct any possible mistranslations. 
								Machine translation results in an even faster translation process and greater savings.
							</p> 						
						</div>
						
						<div class="col-md-6 col-md-offset-1 npd">

							<img src="<?php echo(DOMAIN_NAME_PATH);?>images/about_us/inftecnh.jpg" onerror="this.src='<?=DOMAIN_NAME_PATH;?>images/misc/no_image.jpg';" width="100%" height="auto" border="0" alt="">

						</div>

						<div class="clearfix"></div>						

					</div>

				</div>

				<!-- <div class="col-md-4 rpd hidden-xs">

					<div class="sp-body" >

						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='<?php echo(DOMAIN_NAME_PATH);?>faq.php'"><?php echo(LANG_7);?></h1>

						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='<?php echo(DOMAIN_NAME_PATH);?>contactos.php'"><?php echo(LANG_6);?></h1>

						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='http://grtechdemo.com/maven/privacy.php'"><?php echo(LANG_77);?></h1>

					</div>

				</div> -->

				<div class="clearfix"></div>

			</div>

		</div>

	</div>

	<?php include('footer.php');?>

	<?php include('script.php');?>

</body>

</html>