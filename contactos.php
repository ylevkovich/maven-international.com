<?php
session_start();
include('init.php');
$contact_content = find('first', STATIC_PAGE_CONTATC_US, '*', "WHERE id = 1", array());
if(isset($_POST['btnsubmit']))
{
    $recaptchaValidator = new RecaptchaValidator();
    if (!$recaptchaValidator->isValid($_POST['g-recaptcha-response'])) {
        exit;
    } else {

        $ref = mt_rand();

        // print_r($_FILES);exit;
        $field='full_name, email_address, mobile_number, message, quote_ref, date, corporate_request, company_name';
        $value=':full_name, :email_address, :mobile_number, :message, :quote_ref, :date, :corporate_request, :company_name';
        $execute=array(':full_name'=>stripcleantohtml($_POST['name']),
                ':email_address'=>stripcleantohtml($_POST['email']),
                ':mobile_number'=>stripcleantohtml($_POST['phone']),
                ':message'=>$_POST['comment'],
                ':quote_ref' => $ref,
                ':date'=>date('d M Y').', '.date('h:i A'),
                ':corporate_request'=>stripcleantohtml($_POST['corporate']),
                ':company_name'=>stripcleantohtml($_POST['company'])
                );
        $new_contact = save(ENQUIRIES, $field, $value, $execute);

        if($new_contact)
        {

        $mail_Body = "Dear Administrator,<br/><br/>A new contact details have been posted through your website. Following describe the submitted details.<br/><br/><b>Quote reference: </b>" .$ref. "<br/> <b>Name: </b>".$_POST['name']."<br/><b>Contact No: </b>".$_POST['phone']."<br/><b>E-mail: </b>".$_POST['email']."<br/> <b>Request Type:</b>".stripcleantohtml($_POST['corporate'])."<br/><b>Message: </b>".stripcleantohtml($_POST['comment'])."".'<br><br><br>'.'Regards,'.'<br>Administrator,<br>MAVEN';

        Telegram::sendLetter($mail_Body);
        (new Email())->sendLetter(SERVICE_EMAIL, "New Contact", $mail_Body);

        }
        /*		$_SESSION['SET_TYPE'] = 'success';
        $_SESSION['SET_FLASH'] = LANG_75 ;*/

    }
if(isset($_FILES['files']['name']) && (!empty($_FILES['files']['name'])))
{
	foreach($_FILES['files']['name'] as $key => $value)
	{
		$explode_data = explode('.', $_FILES['files']['name'][$key]);
		$extension = end($explode_data);
		$v_image=$_FILES['files']['name'][$key];
		$path="images/Enquiries/";
		$v_pic=rand(0,200000)."_".$v_image;
		$flag_status1 = true;
			/*if($extension == 'jpg' OR $extension == 'JPG' OR $extension == 'gif' OR $extension == 'GIF' OR $extension == 'png' OR $extension == 'PNG' OR $extension == 'JPEG' OR $extension == 'jpeg')
			{
				$v_image=$_FILES['files']['name'][$key];
				$path="images/Enquiries/";
				$v_pic=rand(0,200000)."_".$v_image;
				$flag_status1 = true;
			}
			else
			{
				$flag_status1 = false;
				$_SESSION['SET_TYPE'] = 'error';
				$_SESSION['SET_FLASH'] = LANG_76;
			}*/
			if($flag_status1 == true)
			{
				$field = 'image_name, enquery_id';
				$value = ':image_name, :enquery_id';
				$execute = array(
					':image_name'=>$v_pic,
					':enquery_id'=>$new_contact
					);
				$add_enquery = save(MANAGE_ENQUERY_DOCUMENT, $field, $value, $execute);
				if($add_enquery)
				{
					move_uploaded_file($_FILES['files']['tmp_name'][$key],$path.$v_pic);
					// $mail->AddAttachment($path.$v_pic);
				}
			}
		}
	}

	// $_SESSION['SET_TYPE'] = 'success';
	// $_SESSION['SET_FLASH'] = LANG_75 ;
	//unset($_POST);

	header("location: http://www.maven-international.com/quote_ref.php?ref=$ref");
	// exit;

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?=$contact_content['page_title'];?></title>
	<meta name="description" content="<?=$contact_content['meta_description'];?>">
	<meta name="keywords" content="<?=$contact_content['meta_keyward'];?>">
	<link rel='shortcut icon' type='image/x-icon' href='<?php echo(DOMAIN_NAME_PATH);?>images/misc/<?=$general_settings['favicon'];?>' />
	<?php include('meta.php');?>
        <?php include 'inc/recaptcha/add_js.php'; ?>
</head>
<body>
	<?php include('mobile_menu.php');?>
	<?php include('topbar.php');?>
	<?php include('inner_page_header.php');?>
</br>

<div class="inner_section">
	<div class="container">
		<div class="row">
				<!-- <div class="col-md-12 hidden-sm hidden-md hidden-lg">
					<h1 class="con-hd con-hdbig"><img src="images/call.png" width="auto" height="22" border="0" style="margin-right:10px" alt=""><?php echo(LANG_61);?></h1>
				</div> -->
				<div class="col-md-8 lpd hidden-xs">
					<div class="">
						<h1 class="con-hd con-hdbig">Contact Us</h1>
						<!-- <div class="congap"></div> -->
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">

			<div class="col-md-8">
				<div style="" class="nbg">
					<div class="col-xs-4 npd">
						<div class="con-block" style="margin-top: 0px;">
							<img src="images/mail.png" style="height:17px; margin-right:10px" alt=""><?php echo(LANG_69);?><br>
							<a href="mailto:<?=nl2br($general_settings['email_address']);?>"><?=nl2br($general_settings['email_address']);?></a><br>We will respond within 1 hour
						</div>
					</div>
					<div class="col-xs-4">
						<div class="con-block" style="margin-top: 0px;">
							<img src="images/call.png" style="height:17px; margin-right:10px" alt="">Hotline<br>
							<?=$general_settings['phone_number'];?><br>
							24-hour service (Mon-Fri)
							<?//=nl2br($general_settings['opening_time_'.$_SESSION['lan']]);?>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>

			<div class="col-md-10">
				<form action="contactos.php" method="post" class="form-horizontal cform" id="contactform"  enctype="multipart/form-data">
					<div class="col-md-4">
						<div class="form-group">

							<select name="corporate" class="form-control validate[required]"  dir="ltr" placeholder="" style="background:#f5f5f5 url(images/company.png)no-repeat 97% center !important">
								<option value="Private" selected><?php echo(LANG_46);?></option>
								<option value="Corporate"><?php echo(LANG_47);?></option>
							</select>

						</div>
						<div class="form-group">

							<input type="text" name="name" class="form-control validate[required] ad-color validate[required]"  value="<?php echo isset($_POST['name']) && $_POST['name']!='' ? $_POST['name'] :'' ?>" placeholder="Your name*">

						</div>
						<div class="form-group">

							<input type="text" name="email" value="<?php echo isset($_POST['email']) && $_POST['email']!='' ? $_POST['email'] :'' ?>" class="form-control validate[required],custom[email] ad-color"  placeholder="Email">

						</div>
						<div class="form-group">

							<input type="text" name="company" value="<?php echo isset($_POST['company']) && $_POST['company']!='' ? $_POST['company'] :'' ?>" class="form-control ad-color"  placeholder="Your company">

						</div>
						<div class="form-group">

							<input type="text" name="phone" value="<?php echo isset($_POST['phone']) && $_POST['phone']!='' ? $_POST['phone'] :'' ?>" class="form-control validate[required],custom[phone] ad-color"  placeholder="Your phone">

						</div>
							<button type="submit" class="watch text-b bttm_gp" name="btnsubmit"  style="background:#7197b6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo(LANG_63);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/arrow.png" alt="" width="auto" border="0" height="7"></button>
					</div> <!-- end col-md-5 -->


					<div class="col-md-5 col-md-offset-1">
						<div class="form-group">

							<textarea name="comment" rows="" cols="" class="form-control validate[required] theight"><?php echo isset($_POST['comment']) && $_POST['comment']!='' ? $_POST['comment'] :'' ?></textarea>

						</div>
						<div class="form-group">
                                                    <div  class="box has-advanced-upload">
                                                        <div class="box__input text-center">
                                                            <input name="files[]" id="file" class="box__file" data-multiple-caption="{count} files selected" multiple type="file" placeholder="Message" style="opacity:0">
                                                            <label for="file" style="text-align:center !important;">
                                                                <strong><?php echo(LANG_54);?></strong>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="g-recaptcha"></div>
                                                </div>
							<script type="text/javascript">

								$(document).ready(function () {
									$("#captchaImg").trigger("click");
								});

								$(document).on("click", "#captchaImg:visible", function () {
									console.log('img');
									var captchaLink = '/captcha/captcha.php?CAPTCHA=' + Math.random();
									$(this).attr('src', captchaLink);
									$("[name=captcha]").val("");
								});
							</script>

						</div>


						<div class="form-group " style="margin-bottom:0px">
							<div class="col-md-12 text-right">

							</div>
						</div>


					</form>
				</div>

				<div class="col-md-12">
					<?php include('maven-mapindividual.php');?>

				</div>





				<div class="clearfix"></div>

			</div>
		</div>
		<div style="clear:both"></div>

		<?php include('footerContactPage.php');?>
		<?php include('script.php');?>
	</body>
	</html>
