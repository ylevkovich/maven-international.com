

<div class="topbanner topbanner2 "  style="padding:0px;min-height:0px; background-position:center top">
	<div class="container">
		<div class="row">
			<div class="micon hidden-sm hidden-md hidden-lg">
			<!-- <img src="images/globe.png" width="auto" height="22" border="0" alt="">&nbsp;&nbsp;<?php echo('');?>
			<select name="" class="lan mb" onchange = "set_status(this.value)">
				<option value="eng" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'eng' ? 'selected' : '')?>>English</option>
				<option value="rus" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'rus' ? 'selected' : '')?>>русский</option>
				<option value="tur" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'tur' ? 'selected' : '')?>>Türk</option>
				<option value="spa" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'spa' ? 'selected' : '')?>>Español</option>
				<option value="fre" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'fre' ? 'selected' : '')?>>français</option>
				<option value="man" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'man' ? 'selected' : '')?>>Мандарин</option>
				<option value="can" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'can' ? 'selected' : '')?>>Cantonese</option>
				<option value="ger" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'ger' ? 'selected' : '')?>>Deutsche</option>
				<option value="per" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'per' ? 'selected' : '')?>>فارسی</option>
				<option value="kor" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'kor' ? 'selected' : '')?>>한국어</option>
				<option value="mal" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'mal' ? 'selected' : '')?>>Malay</option>
				<option value="ind" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'ind' ? 'selected' : '')?>>Bahasa Indonesia</option>
				<option value="ara" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'ara' ? 'selected' : '')?>>العربية</option>
				<option value="jap" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'jap' ? 'selected' : '')?>>日本語</option>
			</select> -->
			<img src="images/mmenu.png" width="31" height="22" border="0" alt="" style="margin-left:20px; cursor:pointer" id="open-menu">
		</div>
		<div class="col-md-3  col-sm-3 lgp">
			<a href="index.php"><img src="<?php echo(DOMAIN_NAME_PATH);?>images/misc/<?=$general_settings['site_logo'];?>" class="logo" alt="" onerror="this.src='<?=DOMAIN_NAME_PATH;?>images/misc/no_image.jpg';"></a>
		</div>
		<div class="col-md-9 col-sm-9 col-xs-4 hidden-xs">
			<ul class="nav navbar-nav navbar-right">
				<li class="main-menu"><a href="<?=DOMAIN_NAME_PATH;?>index.php"><?php echo(LANG_1);?></a></li>
				<li class="dropdown main-menu">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo(LANG_2)?><span class="caret"></span></a>				
					<ul class ="dropdown-menu">
					<li><a  href="<?=DOMAIN_NAME_PATH?>about.php">Our company</a>
					<li><a  href="<?=DOMAIN_NAME_PATH?>quality.php">Our quality</a>
						<!-- <ul class ="main-submenu">
							<li>
								<a href="#">-&nbsp;Our mission</a><br>
								<a href="#">-&nbsp;Our vision</a><br>
								<a href="#">-&nbsp;Our values</a>
							</li>
						</ul> -->
					</li>
					<li><a href="<?=DOMAIN_NAME_PATH?>tecnology.php">Our technology</a></li>
					<!-- <li><a  href="#">ISO 17100 conformity</a></li>		 -->							
				</ul>
				</li>
				<li class ="main-menu">
					<!-- <li class ="main-menu"> -->
					<li class ="dropdown main-menu">
						<!-- <a href="translation.php"><?php echo(LANG_3);?></a> -->
						<a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo(LANG_3);?><span class="caret"></span></a>
					<!-- <ul class ="dropdown-menu">
						<li><a  href="translation.php#NA"> Translation</a></li>
						<li><a  href="translation.php#NQ"> Transcreation</a></li>
						<li><a  href="translation.php#NG"> Simultaneous Interpretation</a></li>
						<li><a  href="translation.php#Nw"> Continuous Interpretation</a></li>
				    	<li><a  href="translation.php#OA"> Court Interpretation</a></li>
						<li><a  href="translation.php#OQ"> Linguistic Validation</a></li>
					</ul> -->
					<ul class ="dropdown-menu">
						<li><a  href="translation.php?id=NA"> <?php echo(LANG_126);?></a></li>
						<li><a  href="translation.php?id=NQ"> <?php echo(LANG_127);?></a></li>
						<li><a  href="translation.php?id=Ng"> <?php echo(LANG_128);?></a></li>
						<li><a  href="translation.php?id=Nw"> <?php echo(LANG_129);?></a></li>
						<li><a  href="translation.php?id=OA"> <?php echo(LANG_130);?></a></li>
						<li><a  href="translation.php?id=OQ"> <?php echo(LANG_131);?></a></li>
					</ul>

				</li>
				
				<li class ="dropdown main-menu">			
					<a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo(LANG_4)?><span class="caret"></span></a>
					<ul class ="dropdown-menu">
						<li><a  href="<?=DOMAIN_NAME_PATH;?>clients.php">Clientele and Testimonials</a></li>
						<li><a  href="<?=DOMAIN_NAME_PATH;?>causes.php">Cases</a></li>									
					</ul>
				</li>
				<li class="main-menu"><a href="<?=DOMAIN_NAME_PATH;?>our_team.php"><?php echo(LANG_5)?></a></li>						
				<li class="main-menu"><a href="<?=DOMAIN_NAME_PATH;?>contactos.php"><?php echo(LANG_6)?></a></li>
			</ul>
		</div>
	</div>
</div>		  
<?php include('service_menu.php');?>
</div>

<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script>
	$(document).ready(function(){
		$('ul.nav li.dropdown').hover(function(){

			$('.dropdown-menu', this).fadeIn();

		}, function(){
			$('.dropdown-menu', this).fadeOut('fast');

		});


	});
</script>