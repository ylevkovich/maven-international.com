<!-- the key need to be changed -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWrxv4GrKYfiWBEtwlYM0bS0_rq2667nQ"></script> 
<script src="https://cdn.mapkit.io/v1/infobox.js"></script> 
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet"> 
<link href="https://cdn.mapkit.io/v1/infobox.css" rel="stylesheet" > 

<script type="text/javascript">
    // google.maps.event.addDomListener(window, 'load', init);
    var map, markersArray = [];

    function bindInfoWindow(marker, map, location) {
        // google.maps.event.addListener(marker, 'click', function() {
            function close(location) {
                location.ib.close();
                location.infoWindowVisible = false;
                location.ib = null;
            }

            if (location.infoWindowVisible === true) {
                close(location);
            } else {
                markersArray.forEach(function(loc, index){
                    if (loc.ib && loc.ib !== null) {
                        close(loc);
                    }
                });

                var boxText = document.createElement('div');
                boxText.style.cssText = 'background: #fff;';
                boxText.classList.add('md-whiteframe-2dp');

                function buildPieces(location, el, part, icon) {
                    if (location[part] === '') {
                        return '';
                    } else if (location.iw[part]) {
                        switch(el){
                            case 'photo':
                            if (location.photo){
                                return '<div class="iw-photo" style="background-image: url(' + location.photo + ');"></div>';
                            } else {
                                return '';
                            }
                            break;
                            case 'iw-toolbar':
                            return '<div class="iw-toolbar"><h3 class="md-subhead">' + location.title + '</h3></div>';
                            break;
                            case 'div':
                            switch(part){
                                case 'email':
                                return '<div class="iw-details"><i class="material-icons" style="color:#4285f4;"><img src="//cdn.mapkit.io/v1/icons/' + icon + '.svg"/></i><span><a href="mailto:' + location.email + '" target="_blank">' + location.email + '</a></span></div>';
                                break;
                                case 'web':
                                return '<div class="iw-details"><i class="material-icons" style="color:#4285f4;"><img src="//cdn.mapkit.io/v1/icons/' + icon + '.svg"/></i><span><a href="' + location.web + '" target="_blank">' + location.web_formatted + '</a></span></div>';
                                break;
                                case 'desc':
                                return '<label class="iw-desc" for="cb_details"><input type="checkbox" id="cb_details"/><h3 class="iw-x-details">Details</h3><i class="material-icons toggle-open-details"><img src="//cdn.mapkit.io/v1/icons/' + icon + '.svg"/></i><p class="iw-x-details">' + location.desc + '</p></label>';
                                break;
                                default:
                                return '<div class="iw-details"><i class="material-icons"><img src="//cdn.mapkit.io/v1/icons/' + icon + '.svg"/></i><span>' + location[part] + '</span></div>';
                                break;
                            }
                            break;
                            case 'open_hours':
                            var items = '';
                            if (location.open_hours.length > 0){
                                for (var i = 0; i < location.open_hours.length; ++i) {
                                    if (i !== 0){
                                        items += '<li><strong>' + location.open_hours[i].day + '</strong><strong>' + location.open_hours[i].hours +'</strong></li>';
                                    }
                                    var first = '<li><label for="cb_hours"><input type="checkbox" id="cb_hours"/><strong>' + location.open_hours[0].day + '</strong><strong>' + location.open_hours[0].hours +'</strong><i class="material-icons toggle-open-hours"><img src="//cdn.mapkit.io/v1/icons/keyboard_arrow_down.svg"/></i><ul>' + items + '</ul></label></li>';
                                }
                                return '<div class="iw-list"><i class="material-icons first-material-icons" style="color:#4285f4;"><img src="//cdn.mapkit.io/v1/icons/' + icon + '.svg"/></i><ul>' + first + '</ul></div>';
                            } else {
                                return '';
                            }
                            break;
                        }
                    } else {
                        return '';
                    }
                }

                boxText.innerHTML = 
                buildPieces(location, 'photo', 'photo', '') +
                    // buildPieces(location, 'iw-toolbar', 'title', '') +
                    buildPieces(location, 'div', 'address', 'location_on') +
                    buildPieces(location, 'div', 'web', 'public') +
                    buildPieces(location, 'div', 'email', 'email') +
                    buildPieces(location, 'div', 'tel', 'phone') +
                    buildPieces(location, 'div', 'int_tel', 'phone') +
                    buildPieces(location, 'open_hours', 'open_hours', 'access_time') +
                    buildPieces(location, 'div', 'desc', 'keyboard_arrow_down');

                    var myOptions = {
                        alignBottom: true,
                        content: boxText,
                        disableAutoPan: true,
                        maxWidth: 0,
                        pixelOffset: new google.maps.Size(-140, -40),
                        zIndex: null,
                        boxStyle: {
                            opacity:0.8,
                            width: '280px'
                        },
                        closeBoxMargin: '0px 0px 0px 0px',
                        infoBoxClearance: new google.maps.Size(1, 1),
                        isHidden: false,
                        pane: 'floatPane',
                        enableEventPropagation: false
                    };

                    location.ib = new InfoBox(myOptions);
                    location.ib.open(map, marker);
                    location.infoWindowVisible = true;
                }
        // });
    }
</script>
<script src="<?=DOMAIN_NAME_PATH;?>js/kl.js"></script>
<script src="<?=DOMAIN_NAME_PATH;?>js/turkey.js"></script>
<script src="<?=DOMAIN_NAME_PATH;?>js/canada.js"></script>

<style>
    #mapkit-2483 {
        height:300px;
        width:100%;
    }
    #mapkit-4273 {
        height:300px;
        width:100%;
    }
    #mapkit-5525 {
        height:300px;
        width:100%;
    }

    /*  SECTIONS  */
    .section {
        clear: both;
        padding: 0px;
        margin: 0px;
    }

    /*  COLUMN SETUP  */
    .col {
        display: block;
        float:left;
        margin: 1% 0 1% 0.1%;
        /*background-color: red;*/
    }
    .col:first-child { margin-left: 0; }

    /*  GROUPING  */
    .group:before,
    .group:after { content:""; display:table; }
    .group:after { clear:both;}
.group { zoom:1; /* For IE 6/7 */ }
/*  GRID OF THREE  */
.span_3_of_3 { width: 100%; }
.span_2_of_3 { width: 66.63%; }
.span_1_of_3 { width: 33.26%; }

/*  GO FULL WIDTH BELOW 480 PIXELS */
@media only screen and (max-width: 480px) {
    .col {  margin: 1% 0 1% 0%; }
    .span_3_of_3, .span_2_of_3, .span_1_of_3 { width: 100%; }
}
</style>

<!-- <div id='mapkit-2483'></div> -->


<div class="section group">


    <div class="row">
        <div class="col-md-4">
           <div class="col-md-12">
             <div id='mapkit-2483'></div> <br>
         </div>
         <div class="col-md-12 footercontent">
            <h1>KUALA LUMPUR, MALAYSIA, MAVEN INTERNATIONAL SDN BHD (1031478-D)</h1>
            <p>
                35-3A BINJAI 8 TOWER, LORONG BINJAI, 50450 KUALA LUMPUR, MALAYSIA.
                <br>
                info@maven-international.com <br>
                <strong>Call us:</strong> (+6)03-2181-5771 <br>
                9 am - 6 pm Monday to Friday (except public holidays)
            </p> <br>
        </div>
    </div>




    <div class="col-md-4">
        <div class="col-md-12">
            <div id='mapkit-4273'></div>  <br>  
        </div>
        <div class="col-md-12 footercontent">         
            <h1>ISTANBUL, TURKEY, MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)</h1>
            <p>
                MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY 
                <br>
                info@maven-international.com <br>
                <strong>Call us:</strong> (+9)0531-7458425 <br>
                9 am - 6 pm Monday to Friday (except public holidays)

            </p>    <br>      
        </div>




    </div>
    <div class="col-md-4">
        <div class="col-md-12">
        <div id='mapkit-5525'></div><br>
        </div>

        <div class="col-md-12 footercontent">          
            <h1>OTTAWA, CANADA, MAVEN INTERNATIONAL INC (974862-8)</h1>
            <p>
                100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4 OTTAWA, CANADA 
                <br>
                info@maven-international.ca <br>
                <strong>Call us:</strong> (+1)6136997975 <br>
                9 am - 6 pm Monday to Friday (except public holidays)

            </p>    <br>       
        </div>




    </div>
</div>
</div>