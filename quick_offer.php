<div class="container" style="position:relative">

	 <div class="row">

		<div class="clearfix"></div>				

		<div class="col-md-4 col-sm-6 hidden-xs" style="position:absolute; right:0px; z-index:99999999">

			<div class="quick-offer">

				<h1><?php echo(LANG_11);?></h1>

				 <form id="contactform" method = "POST" enctype = "multipart/form-data">

					 <div class="t1">

						  <div class="form-group">

							<label><?php echo(LANG_12);?></label>

							<select name="servise_title" class="form-control  validate[required] home_dp"  dir="ltr" >

								<option value="" selected><?php echo(LANG_13);?></option>

								<?php

									if(!empty($service_name))

									{

										foreach($service_name as $service_key => $swervice_value)

										{

								?>

								<option value="<?=$swervice_value['id'];?>"><?=$swervice_value['service_title_'.$_SESSION['lan']];?></option>

								<?php

										}

									}

									else

									{

								?>

								<option value=""><?php echo(LANG_14);?></option>

								<?php

									}

								?>

							</select>

						  </div>

						  <div class="form-group" >

							<label>Source</label>

							<select name="source_language" class="form-control home_dp validate[required]"  dir="ltr" >

								<option value="" selected><?php echo(LANG_17);?></option>

								<?php

									if(!empty($languages))

									{

										foreach($languages as $lan_key => $lan_value)

										{

								?>

								<option value="<?=$lan_value['name'];?>"><?=$lan_value['name'];?></option>

								<?php

										}

									}

									else

									{

								?>

								<option value=""><?php echo(LANG_14);?></option>

								<?php

									}

								?>

							</select>

						  </div>

						  <div class="form-group" >

							<label>Target</label>

							<select name="translate_to" class="form-control  home_dp validate[required]"  dir="ltr" >

								<option value="" selected><?php echo(LANG_17);?></option>

								<?php

									if(!empty($languages))

									{

										foreach($languages as $lan_key => $lan_value)

										{

								?>

								<option value="<?=$lan_value['name'];?>"><?=$lan_value['name'];?></option>

								<?php

										}

									}

									else

									{

								?>

								<option value=""><?php echo(LANG_14);?></option>

								<?php

									}

								?>

							</select>

						  </div>

						  <div class="form-group" >

							<label><?php echo(LANG_20);?></label>

							<input type="text" name="deadline" class="form-control  datepicker" placeholder="<?php echo(LANG_21);?>">

						  </div>				  

						  <button type="subbmit" name="btnsubmit" class="btn btn-default" ><?php echo(LANG_22);?></button>

					</div>

				</form>

			</div>

		</div>

		<div class="clearfix"></div>

		<div class="need-toquick hidden-sm hidden-md hidden-lg" data-toggle="modal" data-target="#myModal">

			<div class="need-toquick-inner">

				<?php echo(LANG_11);?><br><span style=""><?php echo(LANG_73);?></span>

			</div>

		</div>

		<div class="clearfix"></div>

	 </div>

  </div>