<?php

$where_clause = "WHERE slide_position!='' ORDER BY slide_position";

$supporter_list = find('all', SUPPORTER, '*', $where_clause, array());


if(!empty($supporter_list))

{

?>

<div class="client-mob wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s" style="background:none" >

<div class="container" style="padding:20px 7px">

	<div class="swiper-container" id="swiper-container2" style="padding-bottom:35px">

		<div class="swiper-wrapper">

			<?php

			foreach($supporter_list as $support_key => $support_value)

			{

			?>

			<div class="swiper-slide outline-img" style="" onclick="window.location.href='<?php echo($support_value['website_link'] ? $support_value['website_link'] : 'javascript:void(0)');?>'">

				<img src="<?php echo(DOMAIN_NAME_PATH);?>images/image_support/<?php echo $support_value['image'] ;?>" class="res grayscale-img" alt="" onerror="this.src='<?=DOMAIN_NAME_PATH;?>images/misc/no_image.jpg';">

			</div>			

			<?php

			}

			?>

		</div>
		

		<div class="swiper-pagination hidden-sm hidden-md hidden-lg" id="swiper-pagination2"></div>

		<div class="swiper-button-next hidden-sm hidden-md hidden-lg" id="swiper-button-prev2" style="height:auto; background:none; top:100%; left:0px"><img src="images/aro1.png" width="25" height="25" border="0" alt=""></div>

		<div class="swiper-button-next hidden-sm hidden-md hidden-lg" id="swiper-button-next2" style="height:auto; background:none; top:100%; right:0px"><img src="images/aro2.png" width="25" height="25" border="0" alt=""></div>

	</div>

</div>

</div>

<?php

}

?>

<style type="text/css">

.gry{

	position:relative;

	-webkit-filter: grayscale(90%);

	-moz-filter: grayscale(90%);

	-ms-filter: grayscale(90%);

	-o-filter: grayscale(90%);

	filter: bscale(90%);

}

</style>