<!-- <html>
<head>
<meta name="google-site-verification" content="cdAYdpMl0JNnfZ7b25MF7td5LnUJ2wH6bE2Gx6T69Ds" />
<title> My title </title>
</head> 
<body>
page contents
</body>
</html> -->

<div class="topbanner topbanner3 wow fadeInUp" data-wow-duration="0s" data-wow-delay="0" style="background: #fff;">
	<div class="container">
		<div class="row">
			<div class="micon hidden-sm hidden-md hidden-lg">
				<!-- <img src="images/globe.png" width="auto" height="22" border="0" alt="">&nbsp;&nbsp; -->
			<!-- <select name="" class="lan mb" onchange = "set_status(this.value)">
				<option value="eng" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'eng' ? 'selected' : '')?>>English</option>
				<option value="rus" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'rus' ? 'selected' : '')?>>русский</option>
				<option value="tur" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'tur' ? 'selected' : '')?>>Türk</option>
				<option value="spa" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'spa' ? 'selected' : '')?>>Español</option>
				<option value="fre" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'fre' ? 'selected' : '')?>>français</option>
				<option value="man" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'man' ? 'selected' : '')?>>Мандарин</option>
				<option value="can" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'can' ? 'selected' : '')?>>Cantonese</option>
				<option value="ger" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'ger' ? 'selected' : '')?>>Deutsche</option>
				<option value="per" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'per' ? 'selected' : '')?>>فارسی</option>
				<option value="kor" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'kor' ? 'selected' : '')?>>한국어</option>
				<option value="mal" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'mal' ? 'selected' : '')?>>Malay</option>
				<option value="ind" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'ind' ? 'selected' : '')?>>bahasa Indonesia</option>
				<option value="ara" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'ara' ? 'selected' : '')?>>العربية</option>
				<option value="jap" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'jap' ? 'selected' : '')?>>日本語</option>
			</select>
		-->
		<img src="images/mmenu.png" width="31" height="22" border="0" alt="" style="margin-left:20px; cursor:pointer" id="open-menu">
	</div>
	<div class="col-md-3 col-lg-3 col-sm-2 lgp">
		<a href="index.php"><img src="<?php echo(DOMAIN_NAME_PATH);?>images/misc/<?=$general_settings['site_logo'];?>" onerror="this.src='<?=DOMAIN_NAME_PATH;?>images/misc/no_image.jpg';" class="logo" alt=""></a>
	</div>
	<div class="col-xs-5 hidden-sm hidden-md hidden-lg">
		<a style="cursor: pointer;" href="tel:+185550"><i class="fa fa-phone" aria-hidden="true"></i> +185550 MAVEN</a>
		<br>
		<span style="font-size: 75%;
		margin-top: -7px;
		position: absolute;
		color: #666;">24 hour service (Mon-Fri) Toll Free</span>
	</div>
	<div class="col-md-9 col-sm-10 col-xs-4 hidden-xs">
		<ul class="nav navbar-nav navbar-right">
			<li class="main-menu hidden-md hidden-xs hidden-sm"><a href="<?=DOMAIN_NAME_PATH;?>index.php"><?php echo(LANG_1);?></a></li>
			<li class="dropdown main-menu">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo(LANG_2)?><span class="caret"></span></a>				
				<ul class ="dropdown-menu">
					<li><a  href="<?=DOMAIN_NAME_PATH?>about.php">Our company</a>
					<li><a  href="<?=DOMAIN_NAME_PATH?>quality.php">Our quality</a>
						<!-- <ul class ="main-submenu">
							<li>
								<a href="#">-&nbsp;Our mission</a><br>
								<a href="#">-&nbsp;Our vision</a><br>
								<a href="#">-&nbsp;Our values</a>
							</li>
						</ul> -->
					</li>
					<li><a href="<?=DOMAIN_NAME_PATH?>tecnology.php">Our technology</a></li>
					<!-- <li><a  href="#">ISO 17100 conformity</a></li>		 -->							
				</ul>
			</li>
			<!-- <li class ="main-menu"> -->
			<li class ="dropdown main-menu">
				<!-- <a href="translation.php"><?php echo(LANG_3);?></a> -->
				<a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo(LANG_3);?><span class="caret"></span></a>
					<!-- <ul class ="dropdown-menu">
						<li><a  href="translation.php#NA"> Translation</a></li>
						<li><a  href="translation.php#NQ"> Transcreation</a></li>
						<li><a  href="translation.php#NG"> Simultaneous Interpretation</a></li>
						<li><a  href="translation.php#Nw"> Continuous Interpretation</a></li>
				    	<li><a  href="translation.php#OA"> Court Interpretation</a></li>
						<li><a  href="translation.php#OQ"> Linguistic Validation</a></li>
					</ul> -->
					<ul class ="dropdown-menu">
						<li><a  href="translation.php?id=NA"> <?php echo(LANG_126);?></a></li>
						<li><a  href="translation.php?id=NQ"> <?php echo(LANG_127);?></a></li>
						<li><a  href="translation.php?id=Ng"> <?php echo(LANG_128);?></a></li>
						<li><a  href="translation.php?id=Nw"> <?php echo(LANG_129);?></a></li>
						<li><a  href="translation.php?id=OA"> <?php echo(LANG_130);?></a></li>
						<li><a  href="translation.php?id=OQ"> <?php echo(LANG_131);?></a></li>					
					</ul>

				</li>

				<li class ="dropdown main-menu">			
					<a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo(LANG_4)?><span class="caret"></span></a>
					<ul class ="dropdown-menu">
						<li><a  href="<?=DOMAIN_NAME_PATH;?>clients.php">Clientele and Testimonials</a></li>
						<li><a  href="<?=DOMAIN_NAME_PATH;?>causes.php">Cases</a></li>									
					</ul>
				</li>
				<li class="main-menu"><a href="<?=DOMAIN_NAME_PATH;?>our_team.php"><?php echo(LANG_5)?></a></li>																			
				<li class="main-menu"><a href="<?=DOMAIN_NAME_PATH;?>contactos.php"><?php echo(LANG_6)?></a></li>
			</ul>
		</div>
	</div>
</div>
<?php include('service_menu.php');?>
<div class="clearfix"></div>
  <!-- <div style="height:30px"></div>
-->  <div class="clearfix"></div>
<?php //include('quick_offer.php');?>
</div>

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog" style="margin-top:100px">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><?php echo('Select Service');?></h4>
			</div>
			<div class="modal-body">
				<ul class="msmall" style="margin:8px">
					<?php
					if(!empty($service_name))
					{
						foreach($service_name as $service_key => $service_value)
						{
							?>
							<li><a href="translation.php?id=<?php echo(base64_encode($service_value['id']));?>" style="color:#000"><?=$service_value['service_title_'.$_SESSION['lan']];?></a></li>
							<?php
						}
					}
					else
					{
						?>
						<li><?php echo(LANG_14)?></li>
						<?php
					}
					?>
				</ul>
			</div>

		</div>

	</div>
</div>
<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script>
	$(document).ready(function(){
		$('ul.nav li.dropdown').hover(function(){

			$('.dropdown-menu', this).fadeIn();

		}, function(){
			$('.dropdown-menu', this).fadeOut('fast');

		});


	});
</script>