<?php

$where_clause ="WHERE 1 ORDER BY sl_no ASC";

$slider_list = find('all', HOME_SLIDER, '*', $where_clause, array());

?>
<link type="text/css" rel="stylesheet" href="<?php echo(DOMAIN_NAME_PATH);?>css/maventextblock.css">
<style type="text/css">
	.overlay.toolight{color:rgba(23, 109, 182, 0.13);}
	.overlay.toolight::after{background-color:rgba(23, 109, 182, 0.21);
	}
</style>

<!-- <div class="container-full"> -->
	<!-- <div class="row"> -->
		<div class="swiper-container" id="main_slider" >

	<div class="swiper-wrapper swiper-wrapper22">

		<?php

		if(!empty($slider_list))

		{

			foreach($slider_list as $slider_key => $slider_value)

			{

		?>

		<div class="swiper-slide slheight overlay toolight" style="background:url(<?php echo(DOMAIN_NAME_PATH);?>images/slideshow/<?php echo($slider_value['slider_image']);?>)no-repeat center center / cover">

			<div class="container">

				 <div class="row">

					<div class="clearfix"></div>

					<div class="col-md-8 col-sm-8 col-xs-10 col-xs-offset-1 col-md-offset-0 col-lg-offset-0 we-speak text-left">

						<h1 ><?php echo($slider_value['slider_text_'.$_SESSION['lan']]);?></h1>

						<button class="watch text-b bttm_gp piklan quote" type="button" style="/*background:rgba(160, 17, 15, 0.66);*/background: #2b91a9; height: 47px; width:200px""><b><?php echo strtoupper(LANG_83);?></b></button>

						<div class="clearfix"></div>

						<div class="col-md-8 spl text-center hidden-xs">

							

						</div>

					</div>

				</div>

			</div>			

		</div>

		<?php

			}

		}

		else

		{

		?>



		<div class="swiper-slide slheight" style="background:url(images/misc/no_image.jpg)no-repeat center center / cover">

			<div class="container">

				 <div class="row">

					<div class="clearfix"></div>

					<div class="col-md-8 col-sm-6 we-speak text-left">

						<h1 ><?php echo(LANG_14);?></h1>

						<div class="clearfix"></div>

					</div>

				</div>

			</div>			

		</div>

		<?php

		}

		?>



	</div>        

</div>
	<!-- </div> -->
<!-- </div> -->

<script type="text/javascript">
$('.quote').click(function(){
	// var pathname = window.location.pathname; // Returns path only
	// var url      = window.location;     // Returns full URL
	location.href = "quote.php";
	// console.log(pathname);
	// console.log(url);

});
</script>
