<?php
include('init.php');
require_once('classes/Email.php');
require_once 'classes/recaptcha/RecaptchaValidator.php';
require_once ('classes/Telegram.php');

$service_content = find('first', STATIC_PAGE_OUR_SERVICE, '*', "WHERE id = 1", array());
$active_service = '';

if (!isset($_GET['id']) || (isset($_GET['id']) && $_GET['id'] == '')) {
    $_SESSION['source_language'] = '';
    $_SESSION['translate_to'] = '';
    $_SESSION['deadline'] = '';
    header('location:' . DOMAIN_NAME_PATH . 'index.php');
} else {
    $wherecluse = "WHERE id = :id";
    $active_service_name = find('first', MANAGE_SERVICE, '*', $wherecluse, array(':id' => base64_decode($_GET['id'])));
}

if (isset($_POST['btnsubmit'])) {
    
    $recaptchaValidator = new RecaptchaValidator();
    if (!$recaptchaValidator->isValid($_POST['g-recaptcha-response'])) {
        exit;
    }

    $target_lan = $_POST['target-language'];
    $sourse_lan = $_POST['source-language'];
    $date_lan = $_POST['deadline'];
    $service_title = $_POST['id'];
    $validAttachments = "";
    $subject = "";

    // common field:
    if (empty($_POST['additional_info'])) {
        $add = "-";
    } else {
        $add = $_POST['additional_info'];
    }
    if (empty($_POST['text-subject'])) {
        $text = "-";
    } else {
        $text = $_POST['text-subject'];
    }
    if (empty($_POST['no_word'])) {
        $word = "-";
    } else {
        $word = $_POST['no_word'];
    }
    if (empty($_POST['type_cert'])) {
        $type = "";
    } else {
        $type = "(" . $_POST['type_cert'] . ")";
    }
    if (empty($_POST['company'])) {
        $company = "-";
    } else {
        $company = $_POST['company'];
    }

    // details specification::
    switch ($service_title) {
        case "NA":
            $service_title = 'Translation';
            $subject = "New quote details for Translation Service";
            break;
        case "NQ":
            $service_title = 'Transcreation';
            $subject = "New quote details for Transcreation Service";
            break;
        case "OQ":
            $service_title = 'Linguistic Validation';
            $subject = "New quote details for Linguistic Validation Service";
            break;
    }

    $mail_subject = $subject;
    $mail_Body = "
	Dear Administrator,<br/><br/>A new quote details have been posted through your website. Following describe the submitted details.
	<br/><br/>
	<b>Service Name:" . $service_title . "</b><br/>
	<b>Name: </b>" . $_POST['name'] . "<br/>
	<b>Contact No: </b>" . $_POST['phone_number'] . "<br/>
	<b>E-mail: </b>" . $_POST['email_address'] . "<br/>
	<b>Quote Request : </b>" . ucfirst($_POST['private']) . " Request<br/>
	<b>Company : </b>" . $company . "<br/>
	<b>Text Subject : </b>" . $text . "<br/>
	<b>No. of words/pages : </b>" . $word . "<br/>
	<b>Certification (if any) : </b>" . $_POST['cert'] . " " . $type . "<br/>
	<b>Additional Information : </b>" . $add . "<br>
	<hr><br/>";

    for ($x = 0; $x < count($sourse_lan); $x++) {

        $mail_Body .= "<b>Source Language: </b>" . $sourse_lan[$x] . "<br/>";
        $mail_Body .= "<b>Target Language: </b>" . $target_lan[$x] . "<br/>";
        $mail_Body .= "<b>Deadline: </b>" . $date_lan[$x] . "<br><br><hr><br>";
    }
    $mail_body = "<br/><br/>" . "Regards," . "<br/>Administrator,<br/>MAVEN<br/>";

    Telegram::sendLetter($mail_Body);
    (new Email())->sendLetter(SERVICE_EMAIL, $mail_subject, $mail_Body);

//    $_SESSION['SET_TYPE'] = 'success';
//    $_SESSION['SET_FLASH'] = LANG_75;

    unset($_POST);
    unset($_SESSION['captcha']);

}

if (isset($_POST['btnsubmit_form_two'])) {
    // print_r($_POST);exit;

    if ($_POST['captcha'] !== $_SESSION['captcha']['code']) {
        echo "Captha is wrong or empty. <a href='javascript:window.history.back();'>Try again.</a>";
        exit;
    }

    // $sourse_lan =  implode(':-:', $_POST['source-language']);
    $sourse_lan = $_POST['source-language'];
    $translate_lan = $_POST['translate_to'];
    $date_lan = $_POST['date_required'];

    // $day_lan =  $_POST['select_day'];
    // $hours_lan =  $_POST['select_hours'];

    $service_title2 = $_POST['id'];

    switch ($service_title2) {
        case "Ng":
            $service_title2 = 'Simultaneous Interpretation';
            $the_address = 'Event Address';
            $subject = "New quote details for Simultaneous Interpretation Service";
            break;
        case "Nw":
            $service_title2 = 'Consecutive Interpretation';
            $the_address = 'Event Address';
            $subject = "New quote details for Consecutive Interpretation Service";
            break;
        case "OA":
            $service_title2 = 'Court Interpretation';
            $the_address = 'Court Address';
            $subject = "New quote details for Court Interpretation Service";
            $topic = "-";
            break;
    }
    if (empty($_POST['additional_info'])) {
        $add = "-";
    } else {
        $add = $_POST['additional_info'];
    }
    if (empty($_POST['event_topic'])) {
        $topic = "-";
    } else {
        $topic = $_POST['event_topic'];
    }

    $validAttachments = "";
    $mail_subject = $subject;
    $mail_CC = "";
    $mail_From = "grtech.developers@gmail.com";
    $mail_To = SERVICE_EMAIL;
    // $mail_From="miraimaria.yuki@gmail.com";
    // $mail_To="miraimaria.yuki@gmail.com";
    // $mail_To=$general_settings['email_address'];
    $mail_Body = "
			Dear Administrator,<br/><br/>A new quote details have been posted through your website. Following describe the submitted details.
			<br/><br/>
			<b>Service Name:" . $service_title2 . "</b><br/>
			<b>Name: </b>" . $_POST['name'] . "<br/>
			<b>Contact No: </b>" . $_POST['phone_number'] . "<br/>
			<b>E-mail: </b>" . $_POST['email_address'] . "<br>
			<b>Company: </b>" . $_POST['company'] . "<br>
			<b>Event Topic: </b>" . $topic . "<br>
			<b>" . $the_address . ": </b>" . $_POST['event_address'] . "<br>
			<b>Quote Request : </b>" . ucfirst($_POST['corporate_request']) . " Request<br>
			<b>Additional Information : </b>" . $add . "<br>
			<hr><br>";
    for ($x = 0; $x < count($sourse_lan); $x++) {
        if (@$hours_lan[$x] == "") {
            $hours = "";
        } else {
            $hours = $hours_lan[$x];
        }
        $mail_Body .= "<b>Language A: </b>" . $sourse_lan[$x] . "<br/>";
        $mail_Body .= "<b>Language B: </b>" . $translate_lan[$x] . "<br/>";
        $mail_Body .= "<b>Date Required: </b>" . $date_lan[$x] . "<br><br><hr><br>";
    }
    $mail_Body .= "<br><br>" . "Regards," . "<br>Administrator,<br>MAVEN";

    Telegram::sendLetter($mail_Body);
    (new Email())->sendLetter(SERVICE_EMAIL, $mail_subject, $mail_Body);

    header("Location: " . $_SERVER['REQUEST_URI']);
    unset($_POST);
    unset($_SESSION['captcha']);

}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        // Translation
        if ($_GET['id'] == "NA") {
            $service_content['page_title'] = 'Professional translation services in Malaysia, Canada and Turkey';
            $service_content['meta_description'] = 'We provide quality translation services in over 100 languages. Contact us for a quote today';
            $service_content['meta_keyward'] = 'certified translation, document translation, professional translation, in malaysia, canada, turkey, in kuala lumpur, ottawa, istanbul';
        }

        // Transcreation
        if ($_GET['id'] == "NQ") {
            $service_content['page_title'] = 'Transcreation services in Malaysia, Canada and Turkey';
            $service_content['meta_description'] = 'We assist in promoting your ideas, products, and brands across linguistic divide to establish an international presence';
            $service_content['meta_keyward'] = 'transcreation, creative translation, in malaysia, canada, turkey, in kuala lumpur, ottawa, istanbul';
        }

        // Simultaneous Interpretation
        if ($_GET['id'] == "Ng") {
            $service_content['page_title'] = 'Simultaneous interpretation services and equipment in Malaysia, Singapore Canada and Turkey';
            $service_content['meta_description'] = 'We do an accurate oral translation done concurrently with the source speaker and provides equipment necessary for carrying out';
            $service_content['meta_keyward'] = 'conference interpretation, conference translation , translation equipment, interpretation equipment, simultaneous interpretation, simultaneous translation, simultaneous interpretation equipment, in malaysia, canada, turkey, singapore, in kuala lumpur, ottawa, istanbul';
        }

        // Consecutive Interpretation
        if ($_GET['id'] == "Nw") {
            $service_content['page_title'] = 'Consecutive interpretation services in Malaysia, Canada and Turkey';
            $service_content['meta_description'] = 'We are trained to deliver accurate and meticulous interpretation while preserving the technical jargons or semantics';
            $service_content['meta_keyward'] = 'consecutive interpretation, consecutive translation, oral translation, in malaysia, canada, turkey, in kuala lumpur, ottawa, istanbul';
        }

        // Court Interpretation
        if ($_GET['id'] == "OA") {
            $service_content['page_title'] = 'Professional court interpretation services in Malaysia, Canada and Turkey';
            $service_content['meta_description'] = 'We provide highly qualified, experienced interpreters who are intimately familiar with legal protocols';
            $service_content['meta_keyward'] = 'court translation, court interpretation, court interpretation services, in malaysia, canada, turkey, in kuala lumpur, ottawa, istanbul';
        }

        // Linguistic Validation
        if ($_GET['id'] == "OQ") {
            $service_content['page_title'] = 'Linguistic validation services in Malaysia, Turkey, Canada';
            $service_content['meta_description'] = 'We approach this service by thoroughly reviewing the accuracy of already translated documents and validating a final review using steps such as retranslation and back translation';
            $service_content['meta_keyward'] = 'linguistic validation, in malaysia, canada, turkey, in kuala lumpur, ottawa, istanbul';
        }


        ?>
        <title><?= $service_content['page_title']; ?></title>
        <meta name="description" content="<?= $service_content['meta_description']; ?>">
        <meta name="keywords" content="<?= $service_content['meta_keyward']; ?>">
        <link rel='shortcut icon' type='image/x-icon'
              href='<?php echo(DOMAIN_NAME_PATH); ?>images/misc/<?= $general_settings['favicon']; ?>'/>
        <?php include('meta.php'); ?>
        <?php include 'inc/recaptcha/add_js.php'; ?>
    </head>
    <body>
        <?php include('mobile_menu.php'); ?>
        <?php include('topbar.php'); ?>
        <?php include('inner_page_header.php'); ?>
        <div class="inner_section">
            <div class="container">
                <div id="translation" class="row"
                     style="<?php echo(($_GET['id'] == "NA") ? 'display: block;' : 'display: none;'); ?>">
                    <?php

                    $id = "NA";
                    if (!isset($id) || (isset($id) && $id == '')) {
                        $_SESSION['source_language'] = '';
                        $_SESSION['translate_to'] = '';
                        $_SESSION['deadline'] = '';
                        header('location:' . DOMAIN_NAME_PATH . 'index.php');
                    } else {
                        $wherecluse = "WHERE id = :id";
                        $active_service_name = find('first', MANAGE_SERVICE, '*', $wherecluse, array(':id' => base64_decode($id)));
                        // print_r($active_service_name);
                    }
                    ?>
                    <?php include('service_left_panel.php'); ?>

                    <div class="col-md-7 rpd">
                        <?php
                        if ($active_service_name['attachment_document'] == 'Y') {
                            ?>
                            <div class="sp-body">
                                <?php include('service_form_one.php'); ?>
                            </div>
                            <?php
                        } else if ($active_service_name['attachment_document'] == 'N') {
                            ?>
                            <div class="sp-body">
                                <?php include('service_form_two.php'); ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>

                    <div class="clearfix"></div>
                </div>

                <div id="transcreation" class="row"
                     style="<?php echo(($_GET['id'] === "NQ") ? 'display: block;' : 'display: none;'); ?>">

                    <?php
                    $id = "NQ";
                    if (!isset($id) || (isset($id) && $id == '')) {
                        $_SESSION['source_language'] = '';
                        $_SESSION['translate_to'] = '';
                        $_SESSION['deadline'] = '';
                        header('location:' . DOMAIN_NAME_PATH . 'index.php');
                    } else {
                        $wherecluse = "WHERE id = :id";
                        $active_service_name = find('first', MANAGE_SERVICE, '*', $wherecluse, array(':id' => base64_decode($id)));
                        // print_r($active_service_name);
                    }
                    ?>
                    <?php include('service_left_panel.php'); ?>
                    <div class="col-md-7 rpd">
                        <?php
                        if ($active_service_name['attachment_document'] == 'Y') {
                            ?>
                            <div class="sp-body">
                                <?php include('service_form_one.php'); ?>
                            </div>
                            <?php
                        } else if ($active_service_name['attachment_document'] == 'N') {
                            ?>
                            <div class="sp-body">
                                <?php include('service_form_two.php'); ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div id="simultaneous" class="row"
                     style="<?php echo(($_GET['id'] === "Ng") ? 'display: block;' : 'display: none;'); ?>">

                    <?php
                    $id = "Ng";

                    if (!isset($id) || (isset($id) && $id == '')) {
                        $_SESSION['source_language'] = '';
                        $_SESSION['translate_to'] = '';
                        $_SESSION['deadline'] = '';
                        header('location:' . DOMAIN_NAME_PATH . 'index.php');
                    } else {
                        $wherecluse = "WHERE id = :id";
                        $active_service_name = find('first', MANAGE_SERVICE, '*', $wherecluse, array(':id' => base64_decode($id)));
                        // print_r($active_service_name);
                    }

                    ?>
                    <?php include('service_left_panel.php'); ?>

                    <div class="col-md-7 rpd">

                        <?php
                        if ($active_service_name['attachment_document'] == 'Y') {
                            ?>
                            <div class="sp-body">
                                <?php include('service_form_one.php'); ?>
                            </div>

                            <?php
                        } else if ($active_service_name['attachment_document'] == 'N') {
                            ?>

                            <div class="sp-body">
                                <?php include('service_form_two.php'); ?>
                            </div>

                            <?php

                        }

                        ?>

                    </div>

                    <div class="clearfix"></div>
                </div>

                <div id="consecutive" class="row"
                     style="<?php echo(($_GET['id'] == "Nw") ? 'display: block;' : 'display: none;'); ?>">

                    <?php
                    $id = "Nw";
                    if (!isset($id) || (isset($id) && $id == '')) {
                        $_SESSION['source_language'] = '';
                        $_SESSION['translate_to'] = '';
                        $_SESSION['deadline'] = '';
                        header('location:' . DOMAIN_NAME_PATH . 'index.php');
                    } else {

                        $wherecluse = "WHERE id = :id";

                        $active_service_name = find('first', MANAGE_SERVICE, '*', $wherecluse, array(':id' => base64_decode($id)));
                        // print_r($active_service_name);

                    }

                    ?>
                    <?php include('service_left_panel.php'); ?>

                    <div class="col-md-7 rpd">

                        <?php

                        if ($active_service_name['attachment_document'] == 'Y') {

                            ?>

                            <div class="sp-body">

                                <?php include('service_form_one.php'); ?>

                            </div>

                            <?php

                        } else if ($active_service_name['attachment_document'] == 'N') {

                            ?>

                            <div class="sp-body">

                                <?php include('service_form_two.php'); ?>

                            </div>

                            <?php

                        }

                        ?>

                    </div>

                    <div class="clearfix"></div>
                </div>

                <div id="court" class="row"
                     style="<?php echo(($_GET['id'] == 'OA') ? 'display: block;' : 'display: none;'); ?>">

                    <?php
                    $id = 'OA';

                    if (!isset($id) || (isset($id) && $id == '')) {

                        $_SESSION['source_language'] = '';

                        $_SESSION['translate_to'] = '';

                        $_SESSION['deadline'] = '';

                        header('location:' . DOMAIN_NAME_PATH . 'index.php');

                    } else {

                        $wherecluse = "WHERE id = :id";

                        $active_service_name = find('first', MANAGE_SERVICE, '*', $wherecluse, array(':id' => base64_decode($id)));
                        // print_r($active_service_name);

                    }

                    ?>
                    <?php include('service_left_panel.php'); ?>

                    <div class="col-md-7 rpd">

                        <?php

                        if ($active_service_name['attachment_document'] == 'Y') {

                            ?>

                            <div class="sp-body">

                                <?php $service = 'QA';
                                include('service_form_one.php'); ?>

                            </div>

                            <?php

                        } else if ($active_service_name['attachment_document'] == 'N') {

                            ?>

                            <div class="sp-body">

                                <?php $service = 'QA';
                                include('service_form_two.php'); ?>

                            </div>

                            <?php

                        }

                        ?>

                    </div>

                    <div class="clearfix"></div>
                </div>

                <div id="linguistic" class="row"
                     style="<?php echo(($_GET['id'] == "OQ") ? 'display: block;' : 'display: none;'); ?>">

                    <?php
                    $id = "OQ";

                    if (!isset($id) || (isset($id) && $id == '')) {

                        $_SESSION['source_language'] = '';

                        $_SESSION['translate_to'] = '';

                        $_SESSION['deadline'] = '';

                        header('location:' . DOMAIN_NAME_PATH . 'index.php');

                    } else {

                        $wherecluse = "WHERE id = :id";

                        $active_service_name = find('first', MANAGE_SERVICE, '*', $wherecluse, array(':id' => base64_decode($id)));
                        // print_r($active_service_name);

                    }

                    ?>
                    <?php include('service_left_panel.php'); ?>

                    <div class="col-md-7 rpd">

                        <?php

                        if ($active_service_name['attachment_document'] == 'Y') {

                            ?>

                            <div class="sp-body">

                                <?php $service = 'OQ';
                                include('service_form_one.php'); ?>

                            </div>

                            <?php

                        } else if ($active_service_name['attachment_document'] == 'N') {

                            ?>

                            <div class="sp-body">

                                <?php $service = 'OQ';
                                include('service_form_two.php'); ?>

                            </div>

                            <?php

                        }

                        ?>

                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

        </div>

        <?php 
            include 'footer.php';
            include 'script.php';
        ?>
    </body>

</html>

<script type="text/javascript">

    $('input').filter('.multiDatespicker').multiDatesPicker({minDate: 0});
    $('input').filter('.datepicker').datepicker({minDate: 0});
    $('input').filter('.target').multiselect({includeSelectAllOption: true});

</script>


<script type="text/javascript">

    // if this page was refered by another page. it will add # to its url. get them & open the page automatically.
    $(document).ready(function () {
        var hashValue = location.hash;
        hashValue = hashValue.replace(/^#/, '');
        //alert(hashValue);
        service(hashValue);
    });

    function service(id) {
        // alert(id);
        $('.contactform').trigger("reset");
        $('.contactform1').trigger("reset");
        $('.tab-pane, .pik-lang').removeClass('active');
        $('#open_one, #tabone').addClass('active');
        $(".error, .formErrorContent").hide();
        $('input[type="radio"]').prop('checked', false);
        $('.target option:selected').removeAttr("selected");

        if (id == "NA==") {
            $("#translation").show();
            $("#transcreation, #simultaneous, #consecutive, #court, #linguistic").hide();

            document.getElementById(id).className = "active";
            document.getElementById("NQ==").className = "";
            document.getElementById("Ng==").className = "";
            document.getElementById("Nw==").className = "";
            document.getElementById("OA==").className = "";
            document.getElementById("OQ==").className = "";

            $(".box_NQ, .box_OQ, .box_NA").empty();
            $(".box_NA").append('<div  class="box has-advanced-upload" style="padding:0px"><div class="box__input "><input name="files[]" id="file" class="box__file file" data-multiple-caption="{count} files selected" multiple type="file" style="opacity:0"><label for="file" style="text-align:center !important; max-width: 100% !important; width: 100% !important;"><strong><?php echo(LANG_54);?><br><br></strong><span class="box__dragndrop"><?php echo(LANG_91);?></span></label></div><input type="hidden" name="ajax" value="1"></div><script src="js/upload.js" type="text/javascript" />');

        }
        else if (id == "NQ==") {
            $("#transcreation").show();
            $("#translation, #simultaneous, #consecutive, #court, #linguistic").hide();

            document.getElementById(id).className = "active";
            document.getElementById("NA==").className = "";
            document.getElementById("Ng==").className = "";
            document.getElementById("Nw==").className = "";
            document.getElementById("OA==").className = "";
            document.getElementById("OQ==").className = "";

            $(".box_NA, .box_OQ, .box_NQ").empty();
            $(".box_NQ").append('<div  class="box has-advanced-upload" style="padding:0px"><div class="box__input "><input name="files[]" id="file" class="box__file file" data-multiple-caption="{count} files selected" multiple type="file" style="opacity:0"><label for="file" style="text-align:center !important; max-width: 100% !important; width: 100% !important;"><strong><?php echo(LANG_54);?><br><?php echo(LANG_55);?><br></strong><span class="box__dragndrop"><?php echo(LANG_91);?></span></label></div><input type="hidden" name="ajax" value="1"></div><script src="js/upload.js" type="text/javascript" />');

        }
        else if (id == "Ng==") {
            $("#simultaneous, #min").show();
            $("#translation, #transcreation, #consecutive, #court, #linguistic").hide();

            document.getElementById(id).className = "active";
            document.getElementById("NA==").className = "";
            document.getElementById("NQ==").className = "";
            document.getElementById("Nw==").className = "";
            document.getElementById("OA==").className = "";
            document.getElementById("OQ==").className = "";

        }
        else if (id == "Nw==") {
            $("#consecutive").show();
            $("#translation, #transcreation, #simultaneous, #court, #linguistic, #min").hide();

            document.getElementById(id).className = "active";
            document.getElementById("NA==").className = "";
            document.getElementById("NQ==").className = "";
            document.getElementById("Ng==").className = "";
            document.getElementById("OA==").className = "";
            document.getElementById("OQ==").className = "";

        }
        else if (id == "OA==") {
            $("#court, #min").show();
            $("#translation, #transcreation, #simultaneous, #consecutive, #linguistic").hide();

            document.getElementById(id).className = "active";
            document.getElementById("NA==").className = "";
            document.getElementById("NQ==").className = "";
            document.getElementById("Ng==").className = "";
            document.getElementById("Nw==").className = "";
            document.getElementById("OQ==").className = "";

        }
        else if (id == "OQ==") {
            $("#linguistic").show();
            $("#translation, #transcreation, #simultaneous, #consecutive, #court").hide();

            document.getElementById(id).className = "active";
            document.getElementById("NA==").className = "";
            document.getElementById("NQ==").className = "";
            document.getElementById("Ng==").className = "";
            document.getElementById("Nw==").className = "";
            document.getElementById("OA==").className = "";

            $(".box_NA, .box_NQ, .box_OQ").empty();
            $(".box_OQ").append('<div  class="box has-advanced-upload" style="padding:0px"><div class="box__input "><input name="files[]" id="file" class="box__file file" data-multiple-caption="{count} files selected" multiple type="file" style="opacity:0"><label for="file" style="text-align:center !important; max-width: 100% !important; width: 100% !important;"><strong><?php echo(LANG_54);?><br><?php echo(LANG_55);?><br></strong><span class="box__dragndrop"><?php echo(LANG_91);?></span></label></div><input type="hidden" name="ajax" value="1"></div><script src="js/upload.js" type="text/javascript" />');

        }

    }


</script>

<script>

    $("#open_two", "#translation, #transcreation, #linguistic").on("click", "#next_one", function () {
        var captchaLink = '/captcha/captcha.php?CAPTCHA=' + Math.random();
        $(this).parents(".tab-content").find("#captchaImg").attr('src', captchaLink);
        $("[name=captcha]").val("");
    });

    $("#open_one", "#simultaneous, #consecutive, #court").on("click", "#add_text2", function () {
        var captchaLink = '/captcha/captcha.php?CAPTCHA=' + Math.random();
        $(this).parents(".tab-content").find("#captchaImg").attr('src', captchaLink);
        $("[name=captcha]").val("");
    });

    $(document).on("click", "#captchaImg:visible", function () {
        console.log('img');
        var captchaLink = '/captcha/captcha.php?CAPTCHA=' + Math.random();
        $(this).attr('src', captchaLink);
        $("[name=captcha]").val("");
    });

</script>
