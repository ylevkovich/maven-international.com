<?php

require_once ADMIN_INCLUDE_PATH . '/config.php';

class RecaptchaValidator
{
    const API_URL = 'https://www.google.com/recaptcha/api/siteverify';
    
    public function __construct()
    {
        
    }
    
    private function sendRequest($userInput)
    {
        $ch = curl_init(self::API_URL);
        $postValues = "secret=" . RECAPTCHA_SECRET_KEY . "&response={$userInput}";
        
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postValues);
        
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response['success'];
        
    }

    public function isValid($userInput)
    {
        return $this->sendRequest($userInput);
    }
}