<?php

/**
 * Created by PhpStorm.
 * User: yurii.l
 * Date: 10/20/2017
 * Time: 22:55
 */

use PHPMailer\PHPMailer\PHPMailer;

require __DIR__ . '/../vendor/autoload.php';

class Email
{
    public function sendLetter($to, $subject, $body)
    {
        $reference = $this->generateString(8);

        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "maveninternationalforms@gmail.com";
        $mail->Password = "As@&1);isg8@1$0";
        $mail->setFrom('from@example.com', 'Maven International');
        $mail->addAddress($to);
        $mail->Subject = $subject . ' #' . $reference;
        $mail->msgHTML($body);

        if( !empty($_FILES['files']['name'][0]) ){
            foreach( $_FILES['files']['name'] as $key => $file ){
                $uploadfile = 'tmp/' . basename($file);
                copy($_FILES['files']['tmp_name'][$key], $uploadfile);
                $mail->addAttachment( $uploadfile );
            }
        }

        if (!$mail->send()) {
            mail(SERVICE_EMAIL, 'Mail did not send : #' . $reference, 'Error : ' . $mail->ErrorInfo);
            header('Location: mail_sent.php?error=1&reference=' . $reference);
        } else {
            header('Location: mail_sent.php?reference=' . $reference);
        }
    }

    function save_mail($mail)
    {
        //You can change 'Sent Mail' to any other folder or tag
        $path = "{imap.gmail.com:993/imap/ssl}[Gmail]/Sent Mail";
        //Tell your server to open an IMAP connection using the same username and password as you used for SMTP
        $imapStream = imap_open($path, $mail->Username, $mail->Password);
        $result = imap_append($imapStream, $path, $mail->getSentMIMEMessage());
        imap_close($imapStream);
        return $result;
    }

    public function generateString($length = 8)
    {
        $chars = '123456789';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }
}