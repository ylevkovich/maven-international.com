<?php include('init.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Quote reference</title>
	<meta name="description" content="<?=$contact_content['meta_description'];?>">
	<meta name="keywords" content="<?=$contact_content['meta_keyward'];?>">
	<link rel='shortcut icon' type='image/x-icon' href='<?php echo(DOMAIN_NAME_PATH);?>images/misc/<?=$general_settings['favicon'];?>' />
	<?php include('meta.php');?>
</head>
<body>
	<?php include('mobile_menu.php');?>
	<?php include('topbar.php');?>
	<?php include('inner_page_header.php');?>

	<div class="container" style="margin-top: 50px; margin-bottom: 200px;">
		<div class="col-md-10 col-md-offset-1">
			<h1 style="color: #23527C; font-weight: 600;">Thank you for submitting your quote request.</h1>
			<p>
				Our team is currently reviewing your request and will send a quote within 1 hour, <br>
				if your assignment requires more time to price, our sales team will notify you. <br>
				If you have any questions regarding your submission please use the following reference: <br>
				<strong>Your quote reference:  <?php echo $_GET['ref'];?>.</strong>
			</p>
		</div>
	</div>
	<?php include('footer.php');?>
	<?php include('script.php');?>
</body>
</html>
