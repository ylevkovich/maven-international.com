 <?php
	$where_clause = "WHERE 1";
	$address_list = find('all', MANAGE_ADDRESS, '*', $where_clause, array());
	if(!empty($address_list))
	{	
 ?>

 <div class="contakt hidden-sm hidden-md hidden-lg ">
	<div class="container">
		<div class="row">			
			<div class="col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">
				<div class="client-say hidden-sm hidden-md hidden-lg" style="margin-bottom:15px"><?php echo(LANG_72);?></div>
			</div>
			<?php
			foreach($address_list as $address_key => $address_value)
			{
			$formattedAddr = str_replace(' ','+',$address_value['address_eng']);
			$geocodeFromAddr = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false'); 
			$output = json_decode($geocodeFromAddr);		
			$arr=$output->results[0]->address_components;
			//echo '<pre>';print_r($arr);echo '</pre>';
			$address ='';
			foreach($arr as $adrkey => $adr_value)
			{
				if($adr_value->types[0] =='country')
				{
					$address = $adr_value->long_name;
				}
			}			
			?>
			<div class="col-xs-4 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s" >
				<button type="button" class="btn btn-default footer_btn" id="f<?php echo($address_value['id']);?>"><?php echo($address)?></button>
			</div>			
			<?php
			}
			?>
		</div>
	</div>
  </div>
  <?php
	} 
 ?>
 <footer>
	<div class="container">
		<div class="row">
			
			<div class="col-md-12 footercontent">
				<div class="row">
				<?php
				if(!empty($address_list))
				{
					foreach($address_list as $address_key => $address_value)
					{
				?>		
					<div class="col-md-4 col-sm-4 res-foot" id="footerf<?php echo($address_value['id']);?>">
						<img src="images/Close-161.png" width="14" height="14" border="0" alt="" class="clf hidden-sm hidden-md hidden-lg">
						<h1><?php echo($address_value['company_name_'.$_SESSION['lan']]);?></h1>
						<p><?php echo($address_value['address_'.$_SESSION['lan']]);?><br> <strong><?php echo(LANG_68);?></strong>: <?php echo($address_value['phone_number']);?></p>
					</div>											
				<?php
					}				
				}
				else
				{
				?>
					<div class="col-md-12 col-sm-12 res-foot text_center" >
						<?php echo(LANG_14);?>
					</div>
				<?php
				}
				?>
				</div>
			</div>
		</div>
	</div>
</footer>