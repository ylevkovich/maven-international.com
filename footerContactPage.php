 <?php
 $where_clause = "WHERE 1";
 $address_list = find('all', MANAGE_ADDRESS, '*', $where_clause, array()); 	?>


 	<footer>
 		


 		<!-- added04082017 -->
 		
 		<div class="container">
 			<div class="row">
 				<div class="col-md-2 col-xs-0"></div>
 				<div class="col-md-10 col-xs-12 footercontent bot-block">
 					<div class="col-md-3">
 						<ul class="footer-menu">
 							<li><a href="<?=DOMAIN_NAME_PATH;?>about.php" >our company</a></li>
 							<!-- <li><a href="translation.php"><?php echo(LANG_3);?></a></li> -->
 							<li><a href="<?=DOMAIN_NAME_PATH;?>clients.php">clientele</a></li>
 							<li><a href="<?=DOMAIN_NAME_PATH;?>our_team.php"><?php echo(LANG_5)?></a></li>			
 							<li><a href="<?=DOMAIN_NAME_PATH;?>contactos.php"><?php echo(LANG_6)?></a></li>
 						</ul>

 					</div>
 					<div class="col-md-3">
 						<span class="footer-small-block-title">Resources:</span>
 						<ul class="footer-resources">
 							<li><a href="<?=DOMAIN_NAME_PATH;?>blog/index.php">blog</a></li>
 							<li><a href="<?=DOMAIN_NAME_PATH;?>causes.php">cases studies</a></li>
 							<li><a href="<?=DOMAIN_NAME_PATH;?>glossary.php">glossary</a></li>
 							<li style="font-weight: 600;">Memberships:</li>
 							<li><a class="proz" href="http://www.proz.com/interpreter/1831324">proz</a></li>
 						</ul>
 						<span class="footer-small-block-title"><a href="<?=DOMAIN_NAME_PATH;?>privacy.php">legal/terms of service</a></span>
 					</div>
 					<div class="col-md-3">
 						<span class="footer-small-block-title">Payment Methods:</span>
 						<ul class="footer-resources">
 							<li>visa</li>
 							<li>master card</li>
 							<li>paypal</li>
 							<li>payoneer</li>
 						</ul>
 						<div style="line-height: 1.4;">
 								<!-- <span class="footer-small-block-title">Payment Security: <br></span>
 								<span style="text-transform: uppercase;">view our certificate</span> -->
 							</div>
 						</div>
 						<div class="col-md-3">
 							<div style="line-height: 2;">
 								<span class="footer-small-block-title">Subscribe To Our Newsletter<br></span>

 								<form action="subscribe.php" method="POST">
 									<input type="text" class="form-control" name="sub_name" id="exampleInputName1" placeholder="Your name">
 									<input type="email" class="form-control" name="sub_email" id="exampleInputEmail1" placeholder="Email">
 									<button class="watch text-b bttm_gp" type="submit" name="subscribe" style="background:#2B91A9; color: #fff; font-weight: 400;">Subscribe&nbsp;&nbsp;&nbsp;&nbsp;<!-- <i class="fa fa-arrow-right fa-contact-sent-footer" aria-hidden="true"></i> --></button>
 								</form>

 								<span class="footer-small-block-title">stay connected:</span>
 							</div>
 							<li style="border:0; margin-top: 7px; display: inline-block;"> <a href="https://www.facebook.com/MavenInternational/" style="margin-right:0;padding-right: 5px"><i class="fa fa-facebook-square fa-2x" aria-hidden="true" style="color: #001641"></i></a> </li>
 							<li style="border:0; margin-top: 7px; display: inline-block;"> <a href="https://twitter.com/MavenTranslate" style="margin-right:0;padding-right: 5px"><i class="fa fa-twitter-square fa-2x" aria-hidden="true" style="color: #001641"></i></a> </li>
 							<li style="border:0; margin-top: 7px; display: inline-block;"> <a href="https://www.linkedin.com/company-beta/3250231/" style="margin-right:0; padding-right: 5px;"><i class="fa fa-linkedin-square fa-2x" aria-hidden="true" style="color: #001641"></i></a> </li>	
 							
 							
 						</div>
 					</div>
 				</div>
 			</div>




 		</footer>
 		