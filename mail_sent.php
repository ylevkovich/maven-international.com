<?php

include('init.php');
$about_content = find('first', STATIC_PAGE_ABOUT_US, '*', "WHERE id = 1", array());

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?=$about_content['page_title'];?></title>
        <meta name="description" content="<?=$about_content['meta_description'];?>">
        <meta name="keywords" content="<?=$about_content['meta_keyward'];?>">
        <link rel='shortcut icon' type='image/x-icon' href='<?php echo(DOMAIN_NAME_PATH);?>images/misc/<?=$general_settings['favicon'];?>' />
        <?php include('meta.php');?>
    </head>
    <body>
        <?php include('mobile_menu.php');?>
        <?php include('topbar.php');?>
        <?php include('inner_page_header.php');?>
        <div class="inner_section">
            <div class="container">
                <div class="row">
                    <?php if(!empty($_GET['error'])): ?>
                        <div class="col-xs-12 npd">
                            <h1 class="con-hd con-hdbig">Email error..</h1>
                            <p>Your reference:<strong><?=$_GET['reference']?></strong></p>
                        </div>
                    <?php else: ?>
                        <div class="col-md-12 lpd">
                            <div class="">
                                <div class="col-xs-12 npd">
                                    <h1 class="con-hd con-hdbig">Thank you for submitting your quote request.</h1>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="sp-body">
                                <div class="col-xs-12 col-md-5 npd">
                                    <p class="text-justify">Our team is currently reviewing your request and will send a quote within 1 hour,
                                        if your assignment requires more time to price, our sales team will notify you.</p> <br>
                                    <p class="text-center">If you have any questions regarding your submission please use the following reference:
                                        Your quote reference:<strong><?=$_GET['reference']?></strong></p>
                                </div>
                                <div class="col-md-6 col-md-offset-1 npd"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <?php include('footer.php');?>
        <?php include('script.php');?>
    </body>
</html>