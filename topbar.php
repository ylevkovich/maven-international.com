<style type="text/css">
	.me {
		border-top: 0px;
		border-top-style: solid;
		border-top-color: rgb(191, 26, 38);
	}
	
</style>

<nav class="navbar navbar-inverse  hidden-xs me" >

 <div class="container">            

	<div class="collapse navbar-collapse pull-right">

	   <ul class="nav navbar-nav navbar-right" style="margin-bottom: 5px;">
		
		  <li>
			<!-- 
			<select id="selectlang" name="" class="lan mytopbar" onchange = "set_status(this.value)" style="margin-right: 15px">

				<option value="eng" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'eng' ? 'selected' : '')?>>English</option>

				<option value="rus" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'rus' ? 'selected' : '')?>>Русский</option>

				<option value="tur" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'tur' ? 'selected' : '')?>>Türkçe</option>

				<option value="spa" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'spa' ? 'selected' : '')?>>Español</option>

				<option value="fre" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'fre' ? 'selected' : '')?>>Français</option>

				<option value="man" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'man' ? 'selected' : '')?>>普通話</option>

				<option value="can" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'can' ? 'selected' : '')?>>Cantonese</option>

				<option value="ger" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'ger' ? 'selected' : '')?>>Deutsche</option>

				<option value="per" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'per' ? 'selected' : '')?>>فارسی</option>

				<option value="kor" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'kor' ? 'selected' : '')?>>한국어</option>

				<option value="mal" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'mal' ? 'selected' : '')?>>Malay</option>

				<option value="ind" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'ind' ? 'selected' : '')?>>Bahasa Indonesia</option>

				<option value="ara" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'ara' ? 'selected' : '')?>>العربية</option>

				<option value="jap" <?php echo(isset($_SESSION['lan']) && $_SESSION['lan'] == 'jap' ? 'selected' : '')?>>日本語</option>

			</select>			
 -->
		  </li>

		  <!-- <li><a href="#"><?php echo(LANG_8);?></a></li> -->
			
			<li style="padding: 0px 15px 0px 15px;">
				<i class="fa fa-envelope" aria-hidden="true"></i> 
				Info@maven-international.com
			</li>
		 

		  <li style="padding: 0px 15px 0px 15px;">
		  <i class="fa fa-phone" aria-hidden="true"></i> 
		  	<?php echo(LANG_112);?>
		  	<br>
		  	<span style="font-size: 75%;
    margin-top: -7px;
    position: absolute;
    color: #666;">24 hour service (Mon-Fri) Toll Free</span>
		  </li>

		  <!-- <li><a href="<?php //echo($general_settings['facebook_link']);?>"><?php //echo(LANG_10);?></a></li> -->		 	
		  <li style="border:0; margin-top: 7px;"> <a href="https://www.facebook.com/MavenInternational/" style="margin-right:0;padding-right: 1px"><i class="fa fa-facebook-square fa-2x" aria-hidden="true" style="color: #000"></i></a> </li>
		  <li style="border:0; margin-top: 7px;"> <a href="https://twitter.com/MavenTranslate" style="margin-right:0;padding-right: 1px"><i class="fa fa-twitter-square fa-2x" aria-hidden="true" style="color: #000"></i></a> </li>
		  <li style="border:0; margin-top: 7px;"> <a href="https://www.linkedin.com/company-beta/3250231/" style="margin-right:0; padding-right: 0px;"><i class="fa fa-linkedin-square fa-2x" aria-hidden="true" style="color: #000"></i></a> </li>	


	   </ul>

	   <!-- <input type="text" name="" class="top-search navbar-right"> -->

	</div>

 </div>

</nav>