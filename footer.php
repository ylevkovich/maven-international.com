<?php
    $where_clause = "WHERE 1";
    $address_list = find('all', MANAGE_ADDRESS, '*', $where_clause, array());
    if (!empty($address_list)) {	
?>
    <div class="contakt hidden-sm hidden-md hidden-lg ">
        <div class="container">
            <div class="row">			
                <div class="col-xs-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">
                    <div class="client-say hidden-sm hidden-md hidden-lg" style="margin-bottom:15px"><?php echo(LANG_72);?></div>
                </div>
                <?php
                    foreach($address_list as $address_key => $address_value) {
                        $formattedAddr = str_replace(' ','+',$address_value['address_eng']);
                        $geocodeFromAddr = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false'); 
                        $output = json_decode($geocodeFromAddr);		
                        $arr=$output->results[0]->address_components;
                        //echo '<pre>';print_r($arr);echo '</pre>';
                        $address ='';
                        foreach($arr as $adrkey => $adr_value) {
                            if($adr_value->types[0] =='country') {
                                $address = $adr_value->long_name;
                            }
                        }			
                ?>
                    <div class="col-xs-4 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s" >
                        <button type="button" class="btn btn-default footer_btn" id="f<?php echo($address_value['id']);?>"><?php echo($address)?></button>
                    </div>			
                <?php
                    }
                ?>
            </div>
        </div>
    </div>
<?php   
    } 
?>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-2 text-center wow fadeInUp hidden-xs" data-wow-duration="1s" data-wow-delay="0.6s">
                    <a href="<?php echo(DOMAIN_NAME_PATH);?>index.php" ><img src="<?php echo(DOMAIN_NAME_PATH);?>images/misc/<?=$general_settings['footer_logo'];?>" onerror="this.src='<?=DOMAIN_NAME_PATH;?>images/misc/no_image.jpg';" class="logo" alt=""></a>
                </div>
                <div class="col-md-10 footercontent">
                    <div class="row">
                    <?php
                        if(!empty($address_list)) {
                            foreach($address_list as $address_key => $address_value) {
                    ?>		
                                <div class="col-md-4 col-sm-4 res-foot" id="footerf<?php echo($address_value['id']);?>">
                                    <img src="images/Close-161.png" width="14" height="14" border="0" alt="" class="clf hidden-sm hidden-md hidden-lg">
                                    <!-- added270217 -->
                                    <h1><?php echo($address_value['country_'.$_SESSION['lan']]);?></h1> 
                                    <p><?php echo($address_value['company_name_'.$_SESSION['lan']]);?><br> 
                                    <p><?php echo($address_value['address_'.$_SESSION['lan']]);?><br> 
                                        <strong><?php echo(LANG_68);?></strong>: 
                                        <a href="tel:<?php echo(preg_replace("/[^0-9\+]+/ism", "", $address_value['phone_number'])); ?>">
                                            <?php echo($address_value['phone_number']); ?>
                                        </a>
                                    </p>
                                </div>											
                    <?php
                            }				
                        } else {
                    ?>
                    <div class="col-md-12 col-sm-12 res-foot text_center">
                        <?php echo(LANG_14);?>
                    </div>
                    <?php
                        }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- added270217 -->
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-12 footercontent" style="text-align: right">
                    <!-- <p> <a href="/privacy.php"><strong><?php echo(LANG_77);?></strong></a></p> -->
                </div>
            </div> 
        </div>
        <br /><br />

        <!-- added04082017 -->
        <br />
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-xs-0"></div>
                <div class="col-md-10 col-xs-12 footercontent line bot-block">
                    <div class="col-md-3">
                        <ul class="footer-menu">
                            <li><a href="<?=DOMAIN_NAME_PATH;?>about.php" >our company</a></li>
                            <!-- <li><a href="translation.php"><?php echo(LANG_3);?></a></li> -->
                            <li><a href="<?=DOMAIN_NAME_PATH;?>clients.php">clientele</a></li>
                            <li><a href="<?=DOMAIN_NAME_PATH;?>our_team.php"><?php echo(LANG_5)?></a></li>			
                            <li><a href="<?=DOMAIN_NAME_PATH;?>contactos.php"><?php echo(LANG_6)?></a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <span class="footer-small-block-title">Resources:</span>
                        <ul class="footer-resources">
                            <li><a href="<?=DOMAIN_NAME_PATH;?>blog/index.php">blog</a></li>
                            <li><a href="<?=DOMAIN_NAME_PATH;?>causes.php">cases studies</a></li>
                            <li><a href="<?=DOMAIN_NAME_PATH;?>glossary.php">glossary</a></li>
                            <li style="font-weight: 600;">Memberships:</li>
                            <li><a class="proz" href="http://www.proz.com/interpreter/1831324">proz</a></li>
                        </ul>
                        <span class="footer-small-block-title"><a href="<?=DOMAIN_NAME_PATH;?>privacy.php">legal/terms of service</a></span>
                    </div>
                    <div class="col-md-3">
                        <span class="footer-small-block-title">Payment Methods:</span>
                        <ul class="footer-resources">
                            <li>visa</li>
                            <li>master card</li>
                            <li>paypal</li>
                            <li>payoneer</li>
                        </ul>
                        <div style="line-height: 1.4;">
                            <!-- <span class="footer-small-block-title">Payment Security: <br></span>
                            <span style="text-transform: uppercase;">view our certificate</span> -->
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div style="line-height: 2;">
                            <span class="footer-small-block-title">Subscribe To Our Newsletter<br></span>

                            <form action="subscribe.php" method="POST">
                                <input type="text" class="form-control" name="sub_name" id="exampleInputName1" placeholder="Your name">
                                <input type="email" class="form-control" name="sub_email" id="exampleInputEmail1" placeholder="Email">
                                <div class="g-recaptcha" data-callback="recaptchaEnabled" data-expired-callback="recaptchaDisabled"></div>
                                <button disabled="disabled" class="watch text-b bttm_gp" type="submit" name="subscribe" style="background:#2B91A9; color: #fff; font-weight: 400;">Subscribe&nbsp;&nbsp;&nbsp;&nbsp;<!-- <i class="fa fa-arrow-right fa-contact-sent-footer" aria-hidden="true"></i> --></button>
                            </form>

                            <span class="footer-small-block-title">stay connected:</span>
                        </div>
                <li style="border:0; margin-top: 7px; display: inline-block;"> <a href="https://www.facebook.com/MavenInternational/" style="margin-right:0;padding-right: 5px"><i class="fa fa-facebook-square fa-2x" aria-hidden="true" style="color: #001641"></i></a> </li>
                <li style="border:0; margin-top: 7px; display: inline-block;"> <a href="https://twitter.com/MavenTranslate" style="margin-right:0;padding-right: 5px"><i class="fa fa-twitter-square fa-2x" aria-hidden="true" style="color: #001641"></i></a> </li>
                <li style="border:0; margin-top: 7px; display: inline-block;"> <a href="https://www.linkedin.com/company-beta/3250231/" style="margin-right:0; padding-right: 5px;"><i class="fa fa-linkedin-square fa-2x" aria-hidden="true" style="color: #001641"></i></a> </li>	


        </div>
</div>
</div>
</div>




</footer>



<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
        w.yaCounter44327491 = new Ya.Metrika({
                id:44327491,
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
        });
} catch(e) { }
});

var n = d.getElementsByTagName("script")[0],
s = d.createElement("script"),
f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = "https://mc.yandex.ru/metrika/watch.js";

if (w.opera == "[object Opera]") {
d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44327491" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-97699231-1', 'auto');
ga('send', 'pageview');

</script>
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'xXX2rIPQTk';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->