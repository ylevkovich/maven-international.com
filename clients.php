<?php

include('init.php');

$client_content = find('first', STATIC_PAGE_OUR_CLIENT, '*', "WHERE id = 1", array());

$testimonials_list = find('all', MANAGE_TESTIMONIALS, '*', "WHERE 1", array());

$support_list = find('all', SUPPORTER, '*', "WHERE 1", array());

?>

<!DOCTYPE html>

<html lang="en">

<head>

	<title><?=$client_content['page_title'];?></title>

	<meta name="description" content="<?=$client_content['meta_description'];?>">

	<meta name="keywords" content="<?=$client_content['meta_keyward'];?>">

	<link rel='shortcut icon' type='image/x-icon' href='<?php echo(DOMAIN_NAME_PATH);?>images/misc/<?=$general_settings['favicon'];?>' />

	<?php include('meta.php');?>
	<!-- update 7 mar17 -->
	<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script> -->

	<style type="text/css">
		a {
			color: #2e6492
		}
		a:visited {
			color: #2e6492
		}
		#header h2 {
			color: white;
			background-color: #00A1E6;
			margin:0px;
			padding: 5px;
		}
		.comment {
			width: auto;

			margin: 10px;
		}
		a.morelink {
			text-decoration:none;
			outline: none;
		}
		.morecontent span {
			display: none;
		}
		h4 .hcenter{
			text-align:center;
		}
	</style>

</head>

<body>

	<?php include('mobile_menu.php');?>

	<?php include('topbar.php');?>

	<?php include('inner_page_header.php');?>

	<?php

	if(!empty($testimonials_list))

	{

		?>

		<div class="testimonials inner_section ">

			<div class="container">

				<div class="row">

					<div class="manage-lowgap">

						<div class="col-md-12">

							<h1 class="con-hd con-hdbig"><img src="images/star.png" width="auto" height="25" border="0" alt="" class="hidden-sm hidden-md hidden-lg"><?php echo(LANG_60);?></h1>

							<div style="margin-bottom:20px"></div>

						</div>



					</div>

				</div>

			</div>

		</div>

		<!-- script to "show more text" -->
	<!-- 	<script type="text/javascript">
		$(document).ready(function() {
			var showChar = 303;
			var ellipsestext = "...";
			var moretext = "show more";
			var lesstext = "less";
			$('.more').each(function() {
				var content = $(this).html();

				if(content.length > showChar) {

					var c = content.substr(0, showChar);
					var h = content.substr(showChar-1, content.length - showChar);

					var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

					$(this).html(html);
				}

			});

			$(".morelink").click(function(){
				if($(this).hasClass("less")) {
					$(this).removeClass("less");
					$(this).html(moretext);
				} else {
					$(this).addClass("less");
					$(this).html(lesstext);
				}
				$(this).parent().prev().toggle();
				$(this).prev().toggle();
				return false;
			});
		});
	</script> -->

		<?php

	}

	if(!empty($support_list))
	{
		?>

		<div class="container cl-logo">
			<div class="row">
				<div class="col-md-8">
			<!-- <div class="row">
				<ul class="cl-logo-list">
					<?php
					foreach($support_list as $support_key => $support_value)
					{
					?>
					<li onclick="window.location.href='<?php echo($support_value['website_link'] ? $support_value['website_link'] : 'javascript:void(0)');?>'">
						<div class="pheight" style="background:#fff url(<?php echo(DOMAIN_NAME_PATH);?>images/image_support/<?php echo $support_value['image'] ;?>)no-repeat center center / contain;">
						</div>
					</li>
					<?php
					}
					?>
				</ul>
			</div> -->
			<!-- 1 -->
			<h4 class="hcenter">INTERNATIONAL AND NATIONAL SPORT ORGANIZATIONS </h4>
			<div class="row">
				<ul class="cl-logo-list">
					<?php
					foreach($support_list as $support_key => $support_value1)
					{
						if($support_value1['description']=='sport'){
							?>


							<li onclick="window.location.href='<?php echo($support_value1['website_link'] ? $support_value1['website_link'] : 'javascript:void(0)');?>'">
								<div class="bord">
									<div class="pheight" style="background:#fff url(<?php echo(DOMAIN_NAME_PATH);?>images/image_support/<?php echo $support_value1['image'] ;?>)no-repeat center center / contain;">
									</div>
								</div>
								<div class="text-center supp_name"><?php echo $support_value1['supp_name']; ?></div>
							</li>
							<?php	}	}?>
						</ul>
					</div>
					<!-- 2 -->
					<h4 class="hcenter">NON-PROFIT INTERNATIONAL ORGANIZATIONS (NGOs)</h4>
					<div class="row">
						<ul class="cl-logo-list">
							<?php
							foreach($support_list as $support_key => $support_value2)
							{
								if($support_value2['description']=='non-profit'){
									?>
									<li onclick="window.location.href='<?php echo($support_value2['website_link'] ? $support_value2['website_link'] : 'javascript:void(0)');?>'">
										<div class="bord">
											<div class="pheight" style="background:#fff url(<?php echo(DOMAIN_NAME_PATH);?>images/image_support/<?php echo $support_value2['image'] ;?>)no-repeat center center / contain;">
											</div>
										</div>
										<div class="text-center supp_name"><?php echo $support_value2['supp_name']; ?></div>
									</li>
									<?php
								}

							}?>
						</ul>
					</div>
					<!-- 3 -->
					<h4 class="hcenter">BANKS & FINANCIAL INSTITUTIONS </h4>
					<div class="row">
						<ul class="cl-logo-list">
							<?php
							foreach($support_list as $support_key => $support_value3)
							{
								if($support_value3['description']=='bank-fin'){
									?>
									<li onclick="window.location.href='<?php echo($support_value3['website_link'] ? $support_value3['website_link'] : 'javascript:void(0)');?>'">
										<div class="bord">
											<div class="pheight" style="background:#fff url(<?php echo(DOMAIN_NAME_PATH);?>images/image_support/<?php echo $support_value3['image'] ;?>)no-repeat center center / contain;">
											</div>
										</div>
										<div class="text-center supp_name"><?php echo $support_value3['supp_name']; ?></div>
									</li>

									<?php
								}

							}?>
						</ul>
					</div>
					<!-- 4 -->
					<h4 class="hcenter">EMBASSIES & CHAMBERS OF COMMERCE</h4>
					<div class="row">
						<ul class="cl-logo-list">
							<?php
							foreach($support_list as $support_key => $support_value4)
							{
								if($support_value4['description']=='embassie'){
									?>


									<li onclick="window.location.href='<?php echo($support_value4['website_link'] ? $support_value4['website_link'] : 'javascript:void(0)');?>'">
										<div class="bord">
											<div class="pheight" style="background:#fff url(<?php echo(DOMAIN_NAME_PATH);?>images/image_support/<?php echo $support_value4['image'] ;?>)no-repeat center center / contain;">
											</div>
										</div>

										<div class="text-center supp_name"><?php echo $support_value4['supp_name']; ?></div>
									</li>
									<?php	} }?>
								</ul>
							</div>
							<!-- 5 -->
							<h4 class="hcenter">OIL & GAS</h4>
							<div class="row">
								<ul class="cl-logo-list">
									<?php
									foreach($support_list as $support_key => $support_value5)
									{
										if($support_value5['description']=='oil-gas'){
											?>
											<li onclick="window.location.href='<?php echo($support_value5['website_link'] ? $support_value5['website_link'] : 'javascript:void(0)');?>'">
												<div class="bord">
													<div class="pheight" style="background:#fff url(<?php echo(DOMAIN_NAME_PATH);?>images/image_support/<?php echo $support_value5['image'] ;?>)no-repeat center center / contain;">
													</div>
												</div>
												<div class="text-center supp_name"><?php echo $support_value5['supp_name']; ?></div>
											</li>
											<?php	}

										}?>
									</ul>
								</div>

								<!-- 6 -->
								<h4 class="hcenter">OTHERS</h4>
								<div class="row">
									<ul class="cl-logo-list">
										<?php
										foreach($support_list as $support_key => $support_value6)
										{
											if($support_value6['description']=='others'){
												?>
												<li onclick="window.location.href='<?php echo($support_value6['website_link'] ? $support_value6['website_link'] : 'javascript:void(0)');?>'">
													<div class="bord">
														<div class="pheight" style="background:#fff url(<?php echo(DOMAIN_NAME_PATH);?>images/image_support/<?php echo $support_value6['image'] ;?>)no-repeat center center / contain;">
														</div>
													</div>
													<div class="text-center supp_name"><?php echo $support_value6['supp_name']; ?></div>
												</li>
												<?php
											}

										}?>
									</ul>
								</div>

								<!-- 4 -->
			<!-- <h4 class="hcenter">CONSTRUCTION & DEVELOPMENT</h4>
			<div class="row">
				<ul class="cl-logo-list">
					<?php
					foreach($support_list as $support_key => $support_value4)
					{
						if($support_value4['description']=='const-dev'){
							?>
							<li onclick="window.location.href='<?php echo($support_value4['website_link'] ? $support_value4['website_link'] : 'javascript:void(0)');?>'">
								<div class="pheight" style="background:#fff url(<?php echo(DOMAIN_NAME_PATH);?>images/image_support/<?php echo $support_value4['image'] ;?>)no-repeat center center / contain;">
								</div>
							</li>
							<?php
						}

					}?>
				</ul>
			</div> -->
			<!-- 5 -->
		<!-- 	<h4 class="hcenter">MANUFACTURING & PRODUCTION</h4>
		<div class="row">
			<ul class="cl-logo-list">
				<?php
				foreach($support_list as $support_key => $support_value5)
				{
					if($support_value5['description']=='manu-prod'){
						?>
						<li onclick="window.location.href='<?php echo($support_value5['website_link'] ? $support_value5['website_link'] : 'javascript:void(0)');?>'">
							<div class="pheight" style="background:#fff url(<?php echo(DOMAIN_NAME_PATH);?>images/image_support/<?php echo $support_value5['image'] ;?>)no-repeat center center / contain;">
							</div>
						</li>
						<?php
					}

				}?>
			</ul>
		</div> -->
		<!-- 6 -->
		<!-- 	<h4 class="hcenter">PUBLICATION/NEWS & EDUCATION</h4>
		<div class="row">
			<ul class="cl-logo-list">
				<?php
				foreach($support_list as $support_key => $support_value6)
				{
					if($support_value6['description']=='pub-edu'){
						?>
						<li onclick="window.location.href='<?php echo($support_value6['website_link'] ? $support_value6['website_link'] : 'javascript:void(0)');?>'">
							<div class="pheight" style="background:#fff url(<?php echo(DOMAIN_NAME_PATH);?>images/image_support/<?php echo $support_value6['image'] ;?>)no-repeat center center / contain;">
							</div>
						</li>
						<?php
					}

				}?>
			</ul>
		</div> -->


		<!-- 9 -->
			<!-- <h4>ECONOMICS AND FINANCE</h4>
			<div class="row">
				<ul class="cl-logo-list">
					<?php
					foreach($support_list as $support_key => $support_value9)
					{
						if($support_value9['description']=='econ-fin'){
							?>
							<li onclick="window.location.href='<?php echo($support_value9['website_link'] ? $support_value9['website_link'] : 'javascript:void(0)');?>'">
								<div class="pheight" style="background:#fff url(<?php echo(DOMAIN_NAME_PATH);?>images/image_support/<?php echo $support_value9['image'] ;?>)no-repeat center center / contain;">
								</div>
							</li>
							<?php
						}

					}?>
				</ul>
			</div> -->


		</div>

		<div class="col-md-4">
			<?php

			foreach($testimonials_list as $client_key => $client_value)

			{

				?>

				<div class="col-md-12 lowgap wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s">

					<!-- <div class="test-top" style="background:url(<?php echo(DOMAIN_NAME_PATH);?>images/testmonials/<?php echo($client_value['testimonials_image']);?>)no-repeat center center / cover">

						<img src="<?php echo(DOMAIN_NAME_PATH);?>images/testmonials/<?php echo($client_value['testimonials_icon']);?>" onerror="this.src='<?=DOMAIN_NAME_PATH;?>images/misc/no_image.jpg';" class="client-logo1" alt="">

					</div> -->

					<div class="test-describe">

						<p  class="comment more"><?php echo($client_value['description_'.$_SESSION['lan']]);?></p>

						<div class="clearfix"></div>

						<div class="min-gp"></div>

						<div class="row">

							<div class="col-xs-4">
								<!-- update 7mar17 -->
								<!-- <a href="javascript:void(0)" class="testiread"><?php echo(LANG_71);?>&nbsp;&nbsp;&nbsp;<img src="images/readmore.png" width="auto" height="15" border="0" alt=""></a> -->

							</div>

							<div class="col-md-8 text-right testdetails">

								<div  >

									<strong><?php echo nl2br($client_value['client_name_'.$_SESSION['lan']]);?></strong>

								</div>

								<?php echo($client_value['client_description_'.$_SESSION['lan']]);?>

							</div>

						</div>

					</div>

				</div>

				<?php

			}

			?>
		</div>
	</div>
</div>
<?php
}
?>
<?php include('footer.php');?>
<?php include('script.php');?>
</body>
</html>
