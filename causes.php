<?php include('init.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?=$contact_content['page_title'];?></title>
	<meta name="description" content="<?=$contact_content['meta_description'];?>">
	<meta name="keywords" content="<?=$contact_content['meta_keyward'];?>">
	<link rel='shortcut icon' type='image/x-icon' href='/images/misc/<?=$general_settings['favicon'];?>' />
	<?php include('meta.php');?>
</head>
<body>
	<?php include('mobile_menu.php');?>
	<?php include('topbar.php');?>
	<?php include('inner_page_header.php');?>



	<div class="container" style="margin-bottom: 20px;">
		<h1 class="text-center case-pos-title">Satisfied Clients – Case Studies</h1> <br>
		<div class="row">
			<div class="col-md-6">
				<div id="carousel-example-generic1" class="carousel slide" data-interval="false" data-ride="carousel">
					<ol class="carousel-indicators case-page">
						<li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-example-generic1" data-slide-to="1"></li>
						<li data-target="#carousel-example-generic1" data-slide-to="2"></li>
						<li data-target="#carousel-example-generic1" data-slide-to="3"></li>
						<li data-target="#carousel-example-generic1" data-slide-to="4"></li>

					</ol>

					<!-- Wrapper for slides -->

					<div class="carousel-inner" role="listbox">
						<!-- <div class="item active">
							<img src="/images/cases_homepage/slide1.jpg" alt="...">
							<div class="test-slider-text-box">
								<h3 class="text-center slider-text-title">The Asian Football Confederation:</h3><br>
								<p class="small-text">
									The Asian Football Confederation (AFC) is the governing body of association football in Asia and Australia and one of the six confederations making up FIFA. <br> <br>
									<b>Solutions provided:</b><br>
									-&nbsp;&nbsp;Simultaneous and consecutive interpr<br><br>
								</p>
							</div>
						</div> -->

						<div class="item active">
							<img src="/images/cases_homepage/1.jpg" alt="...">
							<div class="test-slider-text-box">
								<h3 class="text-center slider-text-title">Asian Football Confederation</h3><br>
								<p class="small-text">
									The Asian Football Confederation (AFC) is the governing body of association football in Asia and Australia and one of the six confederations making up FIFA. <br> <br>
									<b>Solutions provided:</b> <br>
									-&nbsp;&nbsp;Simultaneous interpretation <br>
									-&nbsp;&nbsp;Consecutive interpretation<br>
									-&nbsp;&nbsp;Translation<br>
									-&nbsp;&nbsp;Linguistic validation<br>
								</p>
							</div>
						</div>

						<div class="item">
							<img src="/images/cases_homepage/2.jpg" alt="...">
							<div class="test-slider-text-box">
								<p class="small-text"><br>
									<b>Challenge:</b> <br><br>
									As an international organization in one of the most diverse regions in the world, AFC requires the following language related services: simultaneous interpretation, consecutive interpretation, translation, and linguistic validation.  <br> <br>
									Maven International has been the one-stop language solutions provider for AFC since 2008.
								</p>
							</div>
						</div>

						<!-- <div class="item">
							<img src="/images/cases_homepage/slide2.jpg" alt="...">
							<div class="test-slider-text-box">
								<h3 class="text-center slider-text-title">Asian Football Confederation</h3><br>
								<p class="small-text">
									<em>Needs and Solutions.</em> As an international organization in one of the most diverse regions in the world, AFC requires the following language related services: simultaneous interpretation, consecutive interpretation, translation, and linguistic validation. <br> <br>Maven International has been the one-stop language solutions provider for AFC since 2008.
								</p>
							</div>
						</div> 	 -->


						<div class="item">
							<img src="/images/cases_homepage/slide6.jpg" alt="...">
							<div class="test-slider-text-box">
								<h3 class="text-center slider-text-title">Translation</h3><br>
								<p class="small-text">
									Maven has a long-standing relationship with the AFC. In that time, their consultants have provided thousands of pages of translations in a number of different global languages. Maven are entrusted with translating highly-sensitive and commercially-confident documents, and have done so in a timely and professional manner. I highly recommend them.
									<p class="text-right" style="font-size: 12px;">James Kitching <br>
										Head of Sports Legal Services, Disciplinary, and Governance at the Asian Football Confederation <br>
										james.kitching@the-afc.com</p>
									</p>
								</div>
							</div>

							<div class="item">
								<img src="/images/cases_homepage/slide5.jpg" alt="...">
								<div class="test-slider-text-box">
									<h3 class="text-center slider-text-title">Simultaneous Interpretation</h3><br>
									<p class="small-text">FIFA-AFC congress  October 2014  <br><br>
										Maven International has been providing AFC with professional simultaneous interpretation services for numerous high level events on a yearly basis since 2008.
									</p>
								</div>
							</div>

							<div class="item">
								<img src="/images/cases_homepage/3.jpg" alt="...">
								<div class="test-slider-text-box">
									<h3 class="text-center slider-text-title">Consecutive interpretation</h3><br>
									<p class="small-text">
										AFC requires a high frequency of top level consecutive interpretation services, mostly communicated at extremely short notice. The topics are mostly delicate and confidential, and require a very careful selection of professional interpreters.
									</p>
								</div>
							</div>
						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic1" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left case-page-control" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic1" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right case-page-control" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
				<!-- end of first slider -->

				<!-- second slider -->
				<div class="col-md-6">
					<div id="carousel-example-generic" class="carousel slide" data-interval="false" data-ride="carousel">
						<ol class="carousel-indicators case-page">
							<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
							<li data-target="#carousel-example-generic" data-slide-to="1"></li>
							<li data-target="#carousel-example-generic" data-slide-to="2"></li>
						</ol>
						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img src="/images/cases_homepage/4.jpg" alt="...">
								<div class="test-slider-text-box">
									<h3 class="text-center slider-text-title">Chief Registrar's Office Federal Court of Malaysia</h3><br>
									<p class="small-text">
										The office of the Chief Registrar is in charge of administrating the courts, personnel of the courts, and procuring the courts’ budget as it is considered the administrative arm of the Malaysian Judiciary. <br> <br>
										<b>Solutions provided:</b> <br>
										-&nbsp;&nbsp;Court interpretation  <br>
									</p>
								</div>
							</div>

							<div class="item">
								<img src="/images/cases_homepage/5.jpg" alt="...">
								<div class="test-slider-text-box">
									<p class="small-text"><br>
										<b>Challenge:</b>			 <br> <br>
										Court interpretation to and from over 40 languages, which include dialects as rare as Twi, Ewe, Afrikaans and many others. These services are mostly requested on a very short notice in courts across all Malaysian states.  <br> <br>

										Up to 800 hours of interpretation monthly.

									</p>
								</div>
							</div>

						<!-- <div class="item">
							<img src="/images/cases_homepage/slide14.jpg" alt="...">
							<div class="test-slider-text-box">
								<h3 class="text-center slider-text-title">Court interpretation </h3><br>
								<p>
									-	&nbsp;&nbsp;Court interpretation to and from over 40 languages, which include dialects as rare as Twi, Ewe, Afrikaans and many others. These services are mostly requested on a very short notice in courts across all Malaysian states. <br><br>
									-	&nbsp;&nbsp;Up to 800 hours of interpretation monthly
								</p>
							</div>
						</div> -->

						<div class="item">
							<img src="/images/cases_homepage/6.jpg" alt="...">
							<div class="test-slider-text-box">
								<p><br>
									Maven International has been an official court interpretation provider to the Chief Registrar over the past years. <br> <br>

									-&nbsp;&nbsp; Maven International has been able to cope with pressure and has always managed to deliver on the highly demanding expectations for several years.  <br> <br>

									-&nbsp;&nbsp; Due to our extensive database of trained and experienced court interpreters, a shortage of interpreters was hardly ever an occurrence.


								</p>
							</div>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

		</div><!-- end row	 -->

		<div class="row">
			<div class="col-md-6 case-page-carousel">
				<div id="carousel-example-generic2" class="carousel slide" data-interval="false" data-ride="carousel">
					<ol class="carousel-indicators case-page">
						<li data-target="#carousel-example-generic2" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-example-generic2" data-slide-to="1"></li>
						<li data-target="#carousel-example-generic2" data-slide-to="2"></li>
						<li data-target="#carousel-example-generic2" data-slide-to="3"></li>
					</ol>
					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<img src="/images/cases_homepage/7.jpg" alt="...">
							<div class="test-slider-text-box">
								<h3 class="text-center slider-text-title">Alliance for Financial Inclusion</h3><br>
								<p class="small-text">
									The Alliance for Financial Inclusion (AFI) is the world’s leading organization on financial inclusion policy and regulation. A member-owned network, AFI promotes and develops evidence-based policy solutions that help to improve the lives of the poor. <br> <br>
									<b>Solutions provided:</b> <br>
									-&nbsp;&nbsp;Simultaneous interpretation <br>
									-&nbsp;&nbsp;Translation<br>
								</p>
							</div>
						</div>

						<div class="item">
							<img src="/images/cases_homepage/slide15.jpg" alt="...">
							<div class="test-slider-text-box">
							<h3 class="text-center slider-text-title">Translation</h3>
								<p class="small-text"><br>
									<em>
										“Business service team (e.g. Abdul Aziz, Rasil etc) provides swift response on our requests and prompt delivery on all translation works.”
									</em>
									<p class="text-right" style="font-size: 12px;">David Lee PMO Lead<br>
										David.Lee@afi-global.org</p>
									</p>
								</div>
							</div>


					<!-- 	<div class="item">
						<img src="/images/cases_homepage/slide11.jpg" alt="...">
						<div class="test-slider-text-box">
							<p class="small-text"><br>
								The Alliance for Financial Inclusion (AFI) is the world’s leading organization on financial inclusion policy and regulation. A member-owned network, AFI promotes and develops evidence-based policy solutions that help to improve the lives of the poor.
							</p>
						</div>
					</div> -->


					<div class="item">
						<img src="/images/cases_homepage/8.jpg" alt="...">
						<div class="test-slider-text-box">
							<h3 class="text-center slider-text-title">Simultaneous interpretation</h3>
							<h4 class="text-center slider-text-title">2016 Global Policy Forum in Fiji</h4>
							<p class="small-text">
								<b>Challenge: </b> <br> <br>
								-&nbsp;&nbsp; 16 professional simultaneous (conference) interpreters required <br>
								-&nbsp;&nbsp;	3 parallel sessions  <br>
								-&nbsp;&nbsp;	Three-day event<br>
								-&nbsp;&nbsp;	Remote location (Fiji)
							</p>
						</div>
					</div>

							<!-- <div class="item">
								<img src="/images/cases_homepage/slide12.jpg" alt="...">
								<div class="test-slider-text-box">
									<h3 class="text-center slider-text-title">Simultaneous interpretation</h3>
									<h4 class="text-center slider-text-title">2016 Global Policy Forum in Fiji</h4>
									<p class="small-text">
										<b>Needs:</b> <br>
										- &nbsp;&nbsp;16 professional simultaneous (conference) interpreters <br>
										- &nbsp;&nbsp;3 parallel sessions <br>
										- &nbsp;&nbsp;Three-day event
									</p>
								</div>
							</div> -->
							<div class="item">
								<img src="/images/cases_homepage/slide12.jpg" alt="...">
								<div class="test-slider-text-box">
									<p class="small-text"><br>
										<b>Feedback</b><br><br>
										<em>“The working relationships between AFI & Maven had so far been pleasant. With the biggest project involving bringing a group of interpreters to our flagship event in Fiji in 2016. Rasil being the main point of contact had been extremely helpful in meeting our needs.”</em>
										<p class="text-right" style="font-size: 10px;">David Lee PMO Lead<br>
											David.Lee@afi-global.org</p>
										</p>
									</div>
								</div>

							</div>
							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-example-generic2" role="button" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic2" role="button" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
					<!-- end of first slider -->

					<!-- second slider -->
					<div class="col-md-6 case-page-carousel">
						<div id="carousel-example-generic3" class="carousel slide" data-interval="false" data-ride="carousel">
							<ol class="carousel-indicators case-page">
								<li data-target="#carousel-example-generic3" data-slide-to="0" class="active"></li>
								<li data-target="#carousel-example-generic3" data-slide-to="1"></li>
								<li data-target="#carousel-example-generic3" data-slide-to="2"></li>
								<li data-target="#carousel-example-generic3" data-slide-to="3"></li>
								<li data-target="#carousel-example-generic3" data-slide-to="4"></li>
							</ol>
							<!-- Wrapper for slides -->
							<div class="carousel-inner" role="listbox">
								<div class="item active">
									<img src="/images/cases_homepage/9.jpg" alt="...">
									<div class="test-slider-text-box">
										<h3 class="text-center slider-text-title">Тhe World Islamic Economic Forum Foundation</h3><br>
										<p class="small-text" style="font-size: 13.5px;">
											WIEF Foundation, a not-for-profit organization based in Kuala Lumpur, organizes the annual World Islamic Economic Forum (WIEF), a world-class business platform showcasing business opportunities in the Muslim world, and runs programs of the various initiatives of the Foundation that strengthen people partnership and knowledge exchange between Muslim and non-Muslim communities across the globe. <br> <br>
											<b>Solutions provided: </b> <br>
											-&nbsp;&nbsp;Simultaneous interpretation <br>
											-&nbsp;&nbsp;Consecutive interpretation <br>
											-&nbsp;&nbsp;Translation
										</p>
									</div>
								</div>


								<div class="item">
									<img src="/images/cases_homepage/10.jpg" alt="...">
									<div class="test-slider-text-box">
										<h3 class="text-center slider-text-title">Simultaneous Interpretation</h3><br>
										<p class="small-text">
											<b>Challenge:</b> <br> <br>
											WIEF requires highly experienced simultaneous interpreters to few global languages for its annually held flagship event: World Islamic Economic Forum.
										</p>
									</div>
								</div>


								<div class="item">
									<img src="/images/cases_homepage/slide9.jpg" alt="...">
									<div class="test-slider-text-box">
										<p class="small-text"><br>
											<b>Feedback:</b> <br> <br>
											<em>“On behalf of the organizers of the World Islamic Economic Forum (WIEF) we would like to extend to Maven International our sincere gratefulness for the quality services you provided following your employment for professional interpretation services for the World Islamic Economic Forum of 2010 and 2012; held in Kuala Lumpur and Johor Bahru respectively.”</em>
											<p class="text-right" style="font-size: 10px;">Fazil Irwan Executive Director</p>
										</p>
									</div>
								</div>

								<div class="item">
									<img src="/images/cases_homepage/slide10.jpg" alt="...">
									<div class="test-slider-text-box">
										<p class="small-text"><br>
											<b>Feedback:</b> <br> <br>
											<em>“Our forum attendees and participants have given us highly positive feedback on smooth and proficient simultaneous translation you provided from English to Russian, French and Arabic.”</em>
											<p class="text-right" style="font-size: 10px;">Fazil Irwan Executive Director</p>
										</p>
									</div>
								</div>

								<div class="item">
									<img src="/images/cases_homepage/slide13.jpg" alt="...">
									<div class="test-slider-text-box">
										<p class="small-text"><br>
											<b>Feedback:</b> <br> <br>
											<em>“Aside from your unparalleled interpretation services, we are highly grateful for your efficient stress management of the event, and the excellent displays of professionalism and teamwork that you exhibited as a whole which made WIEF the success it is today.”</em>
											<p class="text-right" style="font-size: 10px;">Fazil Irwan Executive Director</p>
										</p>
									</div>
								</div>


							</div>
							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-example-generic3" role="button" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic3" role="button" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>


				</div><!-- end row -->

			</div>

			<?php include('footer.php');?>
			<?php include('script.php');?>
		</body>
		</html>
