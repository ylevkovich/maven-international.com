<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
if(isset($_POST['btn_submit']))
{
	if(isset($_POST['page_content_en']) && $_POST['page_content_en'] !='')
	{
		update(STATIC_PAGE_CONTATC_US, 'page_content_en=:page_content_en, page_content_tu=:page_content_tu, page_content_tk=:page_content_tk', 'WHERE id=1', array(':page_content_en'=>($_POST['page_content_en']), ':page_content_tu'=>($_POST['page_content_tu']), ':page_content_tk'=>($_POST['page_content_tk'])));

		$_SESSION['SET_TYPE'] = 'success';
		$_SESSION['SET_FLASH'] = 'Contact us page content updated successfully.';
	}
	else
	{
		$_SESSION['SET_TYPE'] = 'error';
		$_SESSION['SET_FLASH'] = 'Error occurred. Something is wrong.';

	}
}

$contact_us_page_content = find('first', STATIC_PAGE_CONTATC_US, '*', "WHERE id = 1", array());
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Static Pages | Manage Contact Us</title>
	<?php include_once('includes/scripts.php')?>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Static Pages</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
						<li>&nbsp;Static Pages&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Contact Us</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
										<div class="panel panel-grey">
											<div class="panel-heading">Edit Contact Us</div>
											<div class="panel-body">
												<div class="panel-body pan">
													<form name = "edit-form" id = "edit-form" action="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_contact_us.php" method = "POST">
														<div class="row">
															<div class="col-md-12">															
																<div class="form-group">
																	<label for="inputSubject" class="control-label">
																		Page Content (<span style="color:#0000FF">English</span>)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "page_content_en" id = "page_content_en" rows="5" class="form-control"><?php echo($contact_us_page_content['page_content_en']);?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputMessage" class="control-label">
																		Page Content (<span style="color:#fe1a1a">Türkçe</span>)
																	</label>
																	<textarea name = "page_content_tu" id = "page_content_tu" rows="5" class="form-control"><?php echo($contact_us_page_content['page_content_tu']);?></textarea>
																</div>
																<div class="form-group">
																	<label for="inputMessage" class="control-label">
																		Page Content (<span style="color:#107257">Türkmençe</span>)
																	</label>
																	<textarea name = "page_content_tk" id = "page_content_tk" rows="5" class="form-control"><?php echo($contact_us_page_content['page_content_tk']);?></textarea>
																</div>
																<div class="form-actions text-right pal">
																	<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Update</button>
																</div>															
															</div>
														</div>
												</div>
											</div>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<script>
	CKEDITOR.replace('page_content_en',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script//ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script//ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script//ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script//ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});

	CKEDITOR.replace('page_content_tu',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script//ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script//ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script//ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script//ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		height: '450px'
	});
	CKEDITOR.replace('page_content_tk',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script//ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script//ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script//ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script//ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		height: '450px'
	});
</script>

<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>