<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
if(isset($_GET['mode']) && $_GET['mode'] == 'delete')
{
	$_SESSION['SET_TYPE'] = 'success';
	$_SESSION['SET_FLASH'] = 'Address has been deleted successfully';
}
if(isset($_GET['mode']) && $_GET['mode'] == 'add')
{
	$_SESSION['SET_TYPE'] = 'success';
	$_SESSION['SET_FLASH'] = 'Address has been uploaded successfully';
}
if(isset($_GET['mode']) && $_GET['mode'] == 'update')
{
	$_SESSION['SET_TYPE'] = 'success';
	$_SESSION['SET_FLASH'] = 'Address has been updated successfully';
}

if(isset($_GET['id']) && $_GET['id']!='')
{
	delete(MANAGE_ADDRESS, 'WHERE id=:id', array(':id'=>$_GET['id']));
	header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_address.php?mode=delete');
}

$where_clause = "WHERE 1";

$count_result = find("first", MANAGE_ADDRESS, "count(id) as total_count", $where_clause, array());

$total_result = $count_result['total_count'];

$record_no = PAGELIMIT;
$no_of_page = ceil($total_result / $record_no);

if(isset($_GET['page']))
{
	$page = $_GET['page'];
	if(($page > $no_of_page) && $no_of_page!=0)
	{
		$page=$no_of_page;
	}
	else
	{
		$page=$_GET['page'];
	}
}
else
{
	$page=1;
}

$offset = ($page-1) * $record_no;

$slider_list = find('all', MANAGE_ADDRESS, '*', "".$where_clause." ORDER BY id ASC LIMIT ".$offset.",".$record_no, array());

$total_search_result_main = count($slider_list);

$page_link = 5;
$mid_link = ceil($page_link/2);
if($page_link%2==0)
{
	$st_link = $mid_link;
	$end_link = $mid_link-1;
}
else
{
	$st_link = $mid_link-1;
	$end_link = $mid_link-1;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Manage Addresses</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		function delete_record(id)
		{
			if(confirm('Are you sure you wish to delete this record?'))
			{
				window.location.href = '<?php echo(DOMAIN_NAME_PATH_ADMIN)?>list_address.php?id='+id;
			}
		}
	//-->
	</script>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Manage Addresses</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">List of Addresses</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
				<div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
								<div class="col-md-12">
									<div id="area-chart-spline" style="width: 100%; height: 300px; display: none;"></div>
								</div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-grey">
											<div class="panel-heading">List of Addresses <span style="float:right;margin-top:-5px;"><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/add_address.php" class="label-success" style="padding:10px 15px; color:#ffffff;font-size:13px;">+ Add New Address</a></div>
											<div class="panel-body">
												<table class="table table-hover">
													<thead>
													<tr>
														<th>Sl.&nbsp;No.</th>
														<th>Company Name</th>
														<th>Address</th>
														<th>Phone Number</th>
														<th width="80">Action</th>
													</tr>
													</thead>
													<tbody>
													<?php
													if($slider_list)
													{
														foreach($slider_list AS $photo)
														{
															$offset++;
													?>
													<tr>
														<td><?=$offset?></td>
														<td><?php echo($photo['company_name_eng']);?></td>
														<td><?php echo($photo['address_eng']);?></td>														
														<td><?php echo($photo['phone_number']);?></td>														
														<td><a href = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_address.php?id=<?php echo(base64_encode($photo['id']));?>" title = "Edit Address" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/edit.png" border = "0" alt = "Edit Address" /></a>&nbsp;&nbsp;&nbsp;<a href = "javascript:void(0);" onclick = "delete_record('<?php echo($photo['id']);?>');" title = "Delete Address" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/delete.png" border = "0" alt = "Delete Address" /></a></td>
													</tr>
													<?php
														}
													}
													else
													{
													?>
													<tr>
														<td colspan = "7" align = "center"><font color = "red"><b>No Record Found!</b></td>
													</tr>
													<?php
													}
													?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

								<?php
								if($total_result>1)
								{
									if($total_search_result_main == $total_result)
									{
										//DO NOTHING
									} 
									else
									{
								?>
								<div class="row">
									<div class="col-lg-12">
										<ul data-hover="" class="pagination mtm mbm">
											<?php 
											if($page > 1)
											{
											?>
											<li><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_slider.php?page=<?php echo($page-1);?>">«</a></li>
											<?php
											}
											if($no_of_page < $page_link)
											{
												for($l=1; $l<=$no_of_page; $l++)
												{
											?>
											<li <?php echo($page==$l ? 'class="active"' : '');?> ><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_slider.php?page=<?php echo $l;?>"><?php echo $l;?></a></li>
											<?php
													if($l==$no_of_page)
													{
														break;
													}
												}
											}
											else if($page>$no_of_page-$mid_link)
											{
												for($l=$no_of_page-$page_link+1;$l<=$no_of_page;$l++)
												{
											?>
											<li <?php echo($page==$l ? 'class="active"' : '');?> ><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_slider.php?page=<?php echo $l;?>"><?php echo $l;?></a></li>
											<?php
													if($l==$no_of_page)
													{
														break;
													} 
												}
											}
											else if($page>$mid_link)
											{ 
												for($l=$page-$st_link;$l<=$page+$end_link;$l++)
												{
											?>
											<li <?php echo($page==$l ? 'class="active"' : '');?> ><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_slider.php?page=<?php echo $l;?>"><?php echo $l;?></a></li>
											<?php
													if($l==$no_of_page)
													{
														break;
													} 
												}
											}
											else
											{
												for($l=1;$l<=$page_link;$l++)
												{
											?>
											<li <?php echo($page==$l ? 'class="active"' : '');?> ><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_slider.php?page=<?php echo $l;?>"><?php echo $l;?></a></li>
											<?php
													if($l==$no_of_page)
													{
														break;
													}
												}
											}
											if($page!=$no_of_page) 
											{
											?>
											<li><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_slider.php?page=<?php echo($page+1);?>">»</a></li>
											<?php
											}
											?>
										</ul>
									 </div>
								</div>
								<?php
									}
								}
								?>
                            </div>
                        </div>
                    </div>
                </div>

                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>