
<form name = "edit-form5" id = "edit-form5" action="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/static_home_page.php" method = "POST" enctype = "multipart/form-data">
<div class="col-md-6" >
	<div style="border:1px solid #ccc; padding:15px; margin-top:15px">
		<h4 style="margin-top: -24px;"><strong style="background:#fff;">&nbsp;Block Four&nbsp;</strong></h4>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Image
			</label>
			<input name = "image" id = "image8" type="file" placeholder="" onChange="ValidateFileUpload8()"/>
		<div><small style="color:#ff0000">Image dimension: 70px by 70px</small></div>
		</div>
		<div class="form-group">
			<?php
				$exist_file = "../images/home/".$second_block_four_four['image'];
					if (file_exists($exist_file) && $second_block_four_four['image']!='')
						{
			?>
							<img src="../images/home/<?php echo $second_block_four_four['image'] ;?>" style="width:70px; height:70px" id="blah8">
			<?php
						}
						else
						{
			?>
							<img src="../images/misc/no_image.jpg" style="width:70px; height:70px" id="blah8">
			<?php
						}
			?>	
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (English)
			</label>
			<input type="text" name = "title_eng" id = "title_eng" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_eng']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (English)
			</label>
			<textarea   name = "description_eng" id = "description_eng" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_eng']);?></textarea>
		</div>

		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (Russian)
			</label>
			<input type="text" name = "title_rus" id = "title_rus" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_rus']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (Russian)
			</label>
			<textarea   name = "description_rus" id = "description_rus" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_rus']);?></textarea>
		</div>

		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (Turkish)
			</label>
			<input type="text" name = "title_tur" id = "title_tur" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_tur']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (Turkish)
			</label>
			<textarea   name = "description_tur" id = "description_tur" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_tur']);?></textarea>
		</div>

		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (Spanish)
			</label>
			<input type="text" name = "title_spa" id = "title_spa" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_spa']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (Spanish)
			</label>
			<textarea   name = "description_spa" id = "description_spa" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_spa']);?></textarea>
		</div>

		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (French)
			</label>
			<input type="text" name = "title_fre" id = "title_fre" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_fre']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (French)
			</label>
			<textarea   name = "description_fre" id = "description_fre" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_fre']);?></textarea>
		</div>

		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (Mandarin)
			</label>
			<input type="text" name = "title_man" id = "title_man" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_man']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (Mandarin)
			</label>
			<textarea   name = "description_man" id = "description_man" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_man']);?></textarea>
		</div>

		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (Cantonese)
			</label>
			<input type="text" name = "title_can" id = "title_can" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_can']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (Cantonese)
			</label>
			<textarea   name = "description_can" id = "description_can" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_can']);?></textarea>
		</div>

		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (German)
			</label>
			<input type="text" name = "title_ger" id = "title_ger" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_ger']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (German)
			</label>
			<textarea   name = "description_ger" id = "description_ger" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_ger']);?></textarea>
		</div>

		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (Persian)
			</label>
			<input type="text" name = "title_per" id = "title_per" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_per']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (Persian)
			</label>
			<textarea   name = "description_per" id = "description_per" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_per']);?></textarea>
		</div>

		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (Korean)
			</label>
			<input type="text" name = "title_kor" id = "title_kor" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_kor']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (Korean)
			</label>
			<textarea   name = "description_kor" id = "description_kor" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_kor']);?></textarea>
		</div>

		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (Malay)
			</label>
			<input type="text" name = "title_mal" id = "title_mal" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_mal']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (Malay)
			</label>
			<textarea   name = "description_mal" id = "description_mal" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_mal']);?></textarea>
		</div>

		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (Indonesian)
			</label>
			<input type="text" name = "title_ind" id = "title_ind" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_ind']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (Indonesian)
			</label>
			<textarea   name = "description_ind" id = "description_ind" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_ind']);?></textarea>
		</div>

		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (Arabic)
			</label>
			<input type="text" name = "title_ara" id = "title_ara" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_ara']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (Arabic)
			</label>
			<textarea   name = "description_ara" id = "description_ara" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_ara']);?></textarea>
		</div>

		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Title (Japanese)
			</label>
			<input type="text" name = "title_jap" id = "title_jap" rows="5" class="form-control" value="<?php echo($second_block_four_four['title_jap']);?>">
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Description (Japanese)
			</label>
			<textarea   name = "description_jap" id = "description_jap" rows="5" class="form-control" style="height: 86px;"><?php echo($second_block_four_four['description_jap']);?></textarea>
		</div>
		<div class="form-group">
			<label for="inputMessage" class="control-label">
				Link
			</label>
			<input type="text" name = "link" id = "link" rows="5" class="form-control" value="<?php echo($second_block_four_four['link']);?>">
		</div>
			<button type="submit" class="btn btn-primary" name = "btn_submit5" id = "btn_submit5">Update</button>
	</div>
</div>
</form>