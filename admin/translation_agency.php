<style type="text\css">

	.form-group control-label h2{
	text-align:center;
	font-size: 25px;
	}
</style>
<form name = "edit-form9" id = "edit-form9" action="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/static_home_page.php" method = "POST" enctype = "multipart/form-data">
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label" style ="text-align:center;">
			<h2 >TRANSLATION AGENCY (English)</h2>
		</label>
		<textarea name = "translation_agency_eng"  id = "translation_agency_eng" rows="15" class="form-control agency_t" > <?php echo($translation	['translation_agency_eng']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label">
			<h2>TRANSLATION AGENCY (Russian)</h2>
		</label>
		<textarea name = "translation_agency_rus" id = "translation_agency_rus" rows="15" class="form-control" > <?php echo($translation['translation_agency_rus']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label">
			<h2>TRANSLATION AGENCY (Turkish)</h2>
		</label>
		<textarea name = "translation_agency_tur" id = "translation_agency_tur" rows="15" class="form-control" > <?php echo($translation['translation_agency_tur']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label">
			<h2>TRANSLATION AGENCY (Spanish)</h2>
		</label>
		<textarea name = "translation_agency_spa" id = "translation_agency_spa" rows="15" class="form-control" > <?php echo($translation['translation_agency_spa']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label">
			<h2>TRANSLATION AGENCY (French)</h2>
		</label>
		<textarea name = "translation_agency_fre" id = "translation_agency_fre" rows="15" class="form-control" > <?php echo($translation['translation_agency_fre']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label">
			<h2>TRANSLATION AGENCY (Mandarin)</h2>
		</label>
		<textarea name = "translation_agency_man" id = "translation_agency_man" rows="15" class="form-control" > <?php echo($translation['translation_agency_man']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label">
			<h2>TRANSLATION AGENCY (Cantonese)</h2>
		</label>
		<textarea name = "translation_agency_can" id = "translation_agency_can" rows="15" class="form-control" > <?php echo($translation['translation_agency_can']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label">
			<h2>TRANSLATION AGENCY (German)</h2>
		</label>
		<textarea name = "translation_agency_ger" id = "translation_agency_ger" rows="15" class="form-control" > <?php echo($translation['translation_agency_ger']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label">
			<h2>TRANSLATION AGENCY (Persian)</h2>
		</label>
		<textarea name = "translation_agency_per" id = "translation_agency_per" rows="15" class="form-control" > <?php echo($translation['translation_agency_per']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label">
			<h2>TRANSLATION AGENCY (Korean)</h2>
		</label>
		<textarea name = "translation_agency_kor" id = "translation_agency_kor" rows="15" class="form-control" > <?php echo($translation['translation_agency_kor']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label">
			<h2>TRANSLATION AGENCY (Malay)</h2>
		</label>
		<textarea name = "translation_agency_mal" id = "translation_agency_mal" rows="15" class="form-control" > <?php echo($translation['translation_agency_mal']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label">
			<h2>TRANSLATION AGENCY (Indonesian)</h2>
		</label>
		<textarea name = "translation_agency_ind" id = "translation_agency_ind" rows="15" class="form-control" > <?php echo($translation['translation_agency_ind']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label">
			<h2>TRANSLATION AGENCY (Arabic)</h2>
		</label>
		<textarea name = "translation_agency_ara" id = "translation_agency_ara" rows="15" class="form-control" > <?php echo($translation['translation_agency_ara']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12" style="margin-top:25px">
	<div class="form-group">
		<label for="inputMessage" class="control-label">
			<h2>TRANSLATION AGENCY (Japanese)</h2>
		</label>
		<textarea name = "translation_agency_jap" id = "translation_agency_jap" rows="15" class="form-control" > <?php echo($translation['translation_agency_jap']); ?> </textarea>
	</div>																
</div>
<div class="col-md-12 text-right pal">
	<button type="submit" class="btn btn-primary" name = "btn_submit9" id = "btn_submit9">Update</button>
</div>
</form>