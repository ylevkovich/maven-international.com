<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
if(!isset($_GET['id']) || (isset($_GET['id']) && $_GET['id'] ==''))
{
	header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_service.php');
}
$flag_status = true;
if(isset($_POST['btn_submit']))
{
	if($_FILES['service_icon']['name']!='')
	{
		$explode_data = explode('.', $_FILES['service_icon']['name']);
		$extension = end($explode_data);
		if($extension == 'jpg' OR $extension == 'JPG' OR $extension == 'gif' OR $extension == 'GIF' OR $extension == 'png' OR $extension == 'PNG' OR $extension == 'JPEG' OR $extension == 'jpeg')
		{
			$gallery_photo = $_FILES['service_icon']['name'];
		}
		else
		{
			$flag_status = false;
			$_SESSION['SET_TYPE'] = 'error';
			$_SESSION['SET_FLASH'] = 'Invalid extension. Please uplaod .jpg or .jpeg or .gif or .png image';
		}
	}

	if($flag_status)
	{	

		$field='service_title_eng=:service_title_eng, service_title_rus=:service_title_rus, service_title_tur=:service_title_tur, service_title_spa=:service_title_spa, service_title_fra=:service_title_fra, service_title_man=:service_title_man, service_title_can=:service_title_can, service_title_ger=:service_title_ger, service_title_per=:service_title_per, service_title_kor=:service_title_kor, service_title_mal=:service_title_mal, service_title_ind=:service_title_ind, service_title_ara=:service_title_ara, service_title_jap=:service_title_jap, service_description_eng=:service_description_eng, service_description_rus=:service_description_rus, service_description_tur=:service_description_tur, service_description_spa=:service_description_spa, service_description_fre=:service_description_fre, service_description_man=:service_description_man, service_description_can=:service_description_can, service_description_ger=:service_description_ger, service_description_per=:service_description_per, service_description_kor=:service_description_kor, service_description_mal=:service_description_mal, service_description_ind=:service_description_ind, service_description_ara=:service_description_ara, service_description_jap=:service_description_jap';
		$execute=array(
						':service_title_eng'=>stripcleantohtml($_POST['service_title_eng']),
						':service_title_rus'=>stripcleantohtml($_POST['service_title_rus']),
						':service_title_tur'=>stripcleantohtml($_POST['service_title_tur']),
						':service_title_spa'=>stripcleantohtml($_POST['service_title_spa']),
						':service_title_fra'=>stripcleantohtml($_POST['service_title_fra']),
						':service_title_man'=>stripcleantohtml($_POST['service_title_man']),
						':service_title_can'=>stripcleantohtml($_POST['service_title_can']),
						':service_title_ger'=>stripcleantohtml($_POST['service_title_ger']),
						':service_title_per'=>stripcleantohtml($_POST['service_title_per']),
						':service_title_kor'=>stripcleantohtml($_POST['service_title_kor']),
						':service_title_mal'=>stripcleantohtml($_POST['service_title_mal']),
						':service_title_ind'=>stripcleantohtml($_POST['service_title_ind']),
						':service_title_ara'=>stripcleantohtml($_POST['service_title_ara']),
						':service_title_jap'=>stripcleantohtml($_POST['service_title_jap']),
						':service_description_eng'=>stripcleantohtml($_POST['service_description_eng']),
						':service_description_rus'=>stripcleantohtml($_POST['service_description_rus']),
						':service_description_tur'=>stripcleantohtml($_POST['service_description_tur']),
						':service_description_spa'=>stripcleantohtml($_POST['service_description_spa']),
						':service_description_fre'=>stripcleantohtml($_POST['service_description_fre']),
						':service_description_man'=>stripcleantohtml($_POST['service_description_man']),
						':service_description_can'=>stripcleantohtml($_POST['service_description_can']),
						':service_description_ger'=>stripcleantohtml($_POST['service_description_ger']),
						':service_description_per'=>stripcleantohtml($_POST['service_description_per']),
						':service_description_kor'=>stripcleantohtml($_POST['service_description_kor']),
						':service_description_mal'=>stripcleantohtml($_POST['service_description_mal']),
						':service_description_ind'=>stripcleantohtml($_POST['service_description_ind']),
						':service_description_ara'=>stripcleantohtml($_POST['service_description_ara']),
						':service_description_jap'=>stripcleantohtml($_POST['service_description_jap'])
						);
		$where='WHERE id='.base64_decode($_GET['id']).'';
		update(MANAGE_SERVICE, $field, $where, $execute);

		if($gallery_photo!='')
		{
			if($manage_service_details = find('first', MANAGE_SERVICE, 'service_icon', "WHERE id = ".base64_decode($_GET['id'])."", array()))
			{
				if($manage_service_details['service_icon']!='' && file_exists('../images/service/'.$manage_service_details['service_icon']))
				{
					unlink('../images/service/'.$manage_service_details['service_icon']);
				}
			}

			$image_name = base64_decode($_GET['id']).'_'.$gallery_photo;
			move_uploaded_file($_FILES['service_icon']['tmp_name'], '../images/service/'.$image_name);
			update(MANAGE_SERVICE, 'service_icon=:service_icon', 'WHERE id='.base64_decode($_GET['id']).'', array(':service_icon'=>$image_name));
		}
		
		header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_service.php?mode=update');
	}
}

$manage_service = find('first', MANAGE_SERVICE, '*', "WHERE id = ".base64_decode($_GET['id'])."", array());
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Manage Services</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#add-form").validate({
						rules: {
							service_description_eng: "required", 
							service_description_rus: "required", 
							service_description_tur: "required", 
							service_description_spa: "required", 
							service_description_fre: "required", 
							service_description_man: "required", 
							service_description_can: "required", 
							service_description_ger: "required", 
							service_description_per: "required", 
							service_description_kor: "required", 
							service_description_mal: "required", 
							service_description_ind: "required", 
							service_description_ara: "required", 
							service_description_jap: "required",
							service_title_eng: "required",
							service_title_rus: "required", 
							service_title_tur: "required", 
							service_title_spa: "required", 
							service_title_fre: "required", 
							service_title_man: "required", 
							service_title_can: "required", 
							service_title_ger: "required", 
							service_title_per: "required", 
							service_title_kor: "required", 
							service_title_mal: "required", 
							service_title_ind: "required", 
							service_title_ara: "required", 
							service_title_jap: "required",
						},
						messages: {
							service_description_eng: "<font color = 'red'><b>Please enteer service description (English)</b></font>",
							service_description_rus: "<font color = 'red'><b>Please enteer service description (Russian)</b></font>", 
							service_description_tur: "<font color = 'red'><b>Please enteer service description (Turkish)</b></font>", 
							service_description_spa: "<font color = 'red'><b>Please enteer service description (Spanish)</b></font>", 
							service_description_fre: "<font color = 'red'><b>Please enteer service description (French)</b></font>", 
							service_description_man: "<font color = 'red'><b>Please enteer service description (Mandarin)</b></font>", 
							service_description_can: "<font color = 'red'><b>Please enteer service description (Cantonese)</b></font>", 
							service_description_ger: "<font color = 'red'><b>Please enteer service description (German)</b></font>", 
							service_description_per: "<font color = 'red'><b>Please enteer service description (Persian)</b></font>", 
							service_description_kor: "<font color = 'red'><b>Please enteer service description (Korean)</b></font>", 
							service_description_mal: "<font color = 'red'><b>Please enteer service description (Malay)</b></font>", 
							service_description_ind: "<font color = 'red'><b>Please enteer service description (Indonesian)</b></font>", 
							service_description_ara: "<font color = 'red'><b>Please enteer service description (Arabic)</b></font>", 
							service_description_jap: "<font color = 'red'><b>Please enteer service description (Japanese)</b></font>",
							service_title_eng: "<font color = 'red'><b>Please enteer service title (English)</b></font>",
							service_title_rus: "<font color = 'red'><b>Please enteer service title (Russian)</b></font>", 
							service_title_tur: "<font color = 'red'><b>Please enteer service title (Turkish)</b></font>", 
							service_title_spa: "<font color = 'red'><b>Please enteer service title (Spanish)</b></font>", 
							service_title_fre: "<font color = 'red'><b>Please enteer service title (French)</b></font>", 
							service_title_man: "<font color = 'red'><b>Please enteer service title (Mandarin)</b></font>", 
							service_title_can: "<font color = 'red'><b>Please enteer service title (Cantonese)</b></font>", 
							service_title_ger: "<font color = 'red'><b>Please enteer service title (German)</b></font>", 
							service_title_per: "<font color = 'red'><b>Please enteer service title (Persian)</b></font>", 
							service_title_kor: "<font color = 'red'><b>Please enteer service title (Korean)</b></font>", 
							service_title_mal: "<font color = 'red'><b>Please enteer service title (Malay)</b></font>", 
							service_title_ind: "<font color = 'red'><b>Please enteer service title (Indonesian)</b></font>", 
							service_title_ara: "<font color = 'red'><b>Please enteer service title (Arabic)</b></font>", 
							service_title_jap: "<font color = 'red'><b>Please enteer service title (Japanese)</b></font>",
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
	<style>.pad_mar_bod {background-color: #eee;padding: 5px 15px;margin: 0px 0px 10px 0px;} .bod_right {border-right: 5px solid white;} .bod_left {border-left: 5px solid white;}</style>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Manage Services</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active"><a href="list_service.php">Manage Services</a></li>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Edit Service</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
										<div class="panel panel-grey">
										<div class="panel-heading">Edit Service</div>
											<div class="panel-body">
												<div class="panel-body pan">
													<form name = "add-form" id = "add-form" action = "" method = "POST" enctype = "multipart/form-data">
														<div class="row">
															<div class="col-md-12" style="padding:0px;">	
																<div class="col-md-6 pad_mar_bod bod_right">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (English)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_eng" id = "service_title_eng" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_eng']) && $_POST['service_title_eng']!='' ? $_POST['service_title_eng'] : $manage_service['service_title_eng'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (English)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_eng" id = "service_description_eng" class="form-control"><?php echo isset($_POST['service_description_eng']) && $_POST['service_description_eng']!='' ? $_POST['service_description_eng'] : $manage_service['service_description_eng'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6 pad_mar_bod pull-right bod_left">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (Russian)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_rus" id = "service_title_rus" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_rus']) && $_POST['service_title_rus']!='' ? $_POST['service_title_rus'] : $manage_service['service_title_rus'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (Russian)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_rus" id = "service_description_rus" class="form-control"><?php echo isset($_POST['service_description_rus']) && $_POST['service_description_rus']!='' ? $_POST['service_description_rus'] : $manage_service['service_description_rus'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div style="clear:both"></div>
															</div>
															<div class="col-md-12" style="padding:0px;">	
																<div class="col-md-6 pad_mar_bod bod_right">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (Turkish)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_tur" id = "service_title_tur" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_tur']) && $_POST['service_title_tur']!='' ? $_POST['service_title_tur'] : $manage_service['service_title_tur'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (Turkish)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_tur" id = "service_description_tur" class="form-control"><?php echo isset($_POST['service_description_tur']) && $_POST['service_description_tur']!='' ? $_POST['service_description_tur'] : $manage_service['service_description_tur'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6 pad_mar_bod pull-right bod_left">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (Spanish)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_spa" id = "service_title_spa" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_spa']) && $_POST['service_title_spa']!='' ? $_POST['service_title_spa'] : $manage_service['service_title_spa'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (Spanish)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_spa" id = "service_description_spa" class="form-control"><?php echo isset($_POST['service_description_spa']) && $_POST['service_description_spa']!='' ? $_POST['service_description_spa'] : $manage_service['service_description_spa'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div style="clear:both"></div>
															</div>
															<div class="col-md-12" style="padding:0px;">	
																<div class="col-md-6 pad_mar_bod bod_right">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (French)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_fra" id = "service_title_fra" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_fra']) && $_POST['service_title_fra']!='' ? $_POST['service_title_fra'] : $manage_service['service_title_fra'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (French)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_fre" id = "service_description_fre" class="form-control"><?php echo isset($_POST['service_description_fre']) && $_POST['service_description_fre']!='' ? $_POST['service_description_fre'] : $manage_service['service_description_fre'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6 pad_mar_bod pull-right bod_left">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (Mandarin)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_man" id = "service_title_man" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_man']) && $_POST['service_title_man']!='' ? $_POST['service_title_man'] : $manage_service['service_title_man'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (Mandarin)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_man" id = "service_description_man" class="form-control"><?php echo isset($_POST['service_description_man']) && $_POST['service_description_man']!='' ? $_POST['service_description_man'] : $manage_service['service_description_man'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div style="clear:both"></div>
															</div>
															<div class="col-md-12" style="padding:0px;">	
																<div class="col-md-6 pad_mar_bod bod_right">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (Cantonese)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_can" id = "service_title_can" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_can']) && $_POST['service_title_can']!='' ? $_POST['service_title_can'] : $manage_service['service_title_can'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (Cantonese)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_can" id = "service_description_can" class="form-control"><?php echo isset($_POST['service_description_can']) && $_POST['service_description_can']!='' ? $_POST['service_description_can'] : $manage_service['service_description_can'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6 pad_mar_bod pull-right bod_left">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (German)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_ger" id = "service_title_ger" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_ger']) && $_POST['service_title_ger']!='' ? $_POST['service_title_ger'] : $manage_service['service_title_ger'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (German)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_ger" id = "service_description_ger" class="form-control"><?php echo isset($_POST['service_description_ger']) && $_POST['service_description_ger']!='' ? $_POST['service_description_ger'] : $manage_service['service_description_ger'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div style="clear:both"></div>
															</div>
															<div class="col-md-12" style="padding:0px;">	
																<div class="col-md-6 pad_mar_bod bod_right">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (Persian)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_per" id = "service_title_per" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_per']) && $_POST['service_title_per']!='' ? $_POST['service_title_per'] : $manage_service['service_title_per'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (Persian)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_per" id = "service_description_per" class="form-control"><?php echo isset($_POST['service_description_per']) && $_POST['service_description_per']!='' ? $_POST['service_description_per'] : $manage_service['service_description_per'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6 pad_mar_bod pull-right bod_left">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (Korean)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_kor" id = "service_title_kor" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_kor']) && $_POST['service_title_kor']!='' ? $_POST['service_title_kor'] : $manage_service['service_title_kor'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (Korean)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_kor" id = "service_description_kor" class="form-control"><?php echo isset($_POST['service_description_kor']) && $_POST['service_description_kor']!='' ? $_POST['service_description_kor'] : $manage_service['service_description_kor'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div style="clear:both"></div>
															</div>
															<div class="col-md-12" style="padding:0px;">	
																<div class="col-md-6 pad_mar_bod bod_right">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (Malay)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_mal" id = "service_title_mal" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_mal']) && $_POST['service_title_mal']!='' ? $_POST['service_title_mal'] : $manage_service['service_title_mal'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (Malay)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_mal" id = "service_description_mal" class="form-control"><?php echo isset($_POST['service_description_mal']) && $_POST['service_description_mal']!='' ? $_POST['service_description_mal'] : $manage_service['service_description_mal'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6 pad_mar_bod pull-right bod_left">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (Indonesian)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_ind" id = "service_title_ind" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_ind']) && $_POST['service_title_ind']!='' ? $_POST['service_title_ind'] : $manage_service['service_title_ind'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (Indonesian)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_ind" id = "service_description_ind" class="form-control"><?php echo isset($_POST['service_description_ind']) && $_POST['service_description_ind']!='' ? $_POST['service_description_ind'] : $manage_service['service_description_ind'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div style="clear:both"></div>
															</div>
															<div class="col-md-12" style="padding:0px;">	
																<div class="col-md-6 pad_mar_bod bod_right">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (Arabic)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_ara" id = "service_title_ara" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_ara']) && $_POST['service_title_ara']!='' ? $_POST['service_title_ara'] : $manage_service['service_title_ara'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (Arabic)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_ara" id = "service_description_ara" class="form-control"><?php echo isset($_POST['service_description_ara']) && $_POST['service_description_ara']!='' ? $_POST['service_description_ara'] : $manage_service['service_description_ara'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6 pad_mar_bod pull-right bod_left">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Service Title (Japanese)
																		</label>
																		<div class="input-icon right">
																			<input name = "service_title_jap" id = "service_title_jap" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title_jap']) && $_POST['service_title_jap']!='' ? $_POST['service_title_jap'] : $manage_service['service_title_jap'] ?>"/>
																		</div>
																	</div>	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Short Description (Japanese)
																		</label>
																		<div class="input-icon right">
																			
																			<textarea name = "service_description_jap" id = "service_description_jap" class="form-control"><?php echo isset($_POST['service_description_jap']) && $_POST['service_description_jap']!='' ? $_POST['service_description_jap'] : $manage_service['service_description_jap'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div style="clear:both"></div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Service Icon
																	</label>
																	<div class="input-icon right">
																		<input name = "service_icon" id = "service_icon" placeholder="Inlcude some file" type="file" onChange="ValidateFileUpload()"/>
																		<div><small style="color:#ff0000">Image dimension: 70px by 70px</small></div>
																	</div>
																</div>
																<div class="form-group">
																	<?php
																		$exist_file = "../images/service/".$manage_service['service_icon'];
																			if (file_exists($exist_file))
																				{
																	?>
																					<img src="../images/service/<?php echo $manage_service['service_icon'] ;?>" style="width:100px;" id="blah"><br><br>
																	<?php
																				}
																				else
																				{
																	?>
																					<img src="../images/misc/no_image.jpg" style="width:auto; height:100px;" id="blah"><br><br>
																	<?php
																				}
																	?>	
																</div>
																
															</div>															
															
														</div>
													
														<div class="form-actions text-right pal">
															<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Update</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>
	<script>
     	function ValidateFileUpload() {
			var fuData = document.getElementById('service_icon');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('service_icon').value = '';
            }
    }
</script>
