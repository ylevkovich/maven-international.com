<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);

$flag_status = true;
if(isset($_POST['btn_submit']))
{
	if($_FILES['service_icon']['name']!='')
	{
		$explode_data = explode('.', $_FILES['service_icon']['name']);
		$extension = end($explode_data);
		if($extension == 'jpg' OR $extension == 'JPG' OR $extension == 'gif' OR $extension == 'GIF' OR $extension == 'png' OR $extension == 'PNG' OR $extension == 'JPEG' OR $extension == 'jpeg')
		{
			$photo = $_FILES['service_icon']['name'];
		}
		else
		{
			$flag_status = false;
			$_SESSION['SET_TYPE'] = 'error';
			$_SESSION['SET_FLASH'] = 'Invalid extension. Please uplaod .jpg or .jpeg or .gif or .png image';
		}
	}
	if($flag_status)
	{
		$v_image=$_FILES['service_icon']['name'];
		$path="../images/service/";
		$v_pic=rand(0,200000)."_".$v_image;
		
		$field='service_icon, service_title, service_description, attachment_document';
		$value=':service_icon, :service_title, :service_description, :attachment_document';
		$execute=array(':service_icon'=>$v_pic,
						':service_title'=>stripcleantohtml($_POST['service_title']),
						':service_description'=>($_POST['service_description']),
						':attachment_document'=>($_POST['attachment_document'])
						);
		$add_service = save(MANAGE_SERVICE, $field, $value, $execute);
		if($add_service)
		{
			move_uploaded_file($_FILES['service_icon']['tmp_name'],$path.$v_pic);
			header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_service.php?mode=add');
		}
	}
	
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Manage Serviceses</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#add-form").validate({
						rules: {
							service_icon: "required",
							service_title: "required",
							service_description: "required"
						},
						messages: {
							service_icon: "<font color = 'red'><b>Please select service icon</b></font>",
							service_title: "<font color = 'red'><b>Please enter service title</b></font>",
							service_description: "<font color = 'red'><b>Please enter service description</b></font>",
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Manage Serviceses</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="admin/dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active"><a href="admin/list_service.php">Manage Serviceses</a></li>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Add New Service</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
										<div class="panel panel-grey">
										<div class="panel-heading">Add New Service</div>
											<div class="panel-body">
												<div class="panel-body pan">
													<form name = "add-form" id = "add-form" action = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/add_service.php" method = "POST" enctype = "multipart/form-data">
														<div class="row">
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Service Title
																	</label>
																	<div class="input-icon right">
																		<input name = "service_title" id = "service_title" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['service_title']) && $_POST['service_title']!='' ? $_POST['service_title'] :'' ?>"/>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Attachment Document?
																	</label>
																	<div class="input-icon right">
																		<select name = "attachment_document" id = "attachment_document" class="form-control">
																			<option value="" selected>Select</option>
																			<option value="Y" <?php echo isset($_POST['attachment_document']) && $_POST['attachment_document']='Y' ? 'selected' :'' ?>>YES</option>
																			<option value="N" <?php echo isset($_POST['attachment_document']) && $_POST['attachment_document']='N' ? 'selected' :'' ?>>NO</option>
																		</select>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Short Description 
																	</label>
																	<div class="input-icon right">																		
																		<textarea name = "service_description" id = "service_description" class="form-control" style="resize:none;"><?php echo isset($_POST['service_description']) && $_POST['service_description']!='' ? $_POST['service_description'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Service Icon
																	</label>
																	<div class="input-icon right">
																		<input name = "service_icon" id = "service_icon" placeholder="Inlcude some file" type="file" />
																		<div><small style="color:#ff0000">Image dimension: 70px by 70px</small></div>
																	</div>
																</div>
															</div>
														</div>
													
														<div class="form-actions text-right pal">
															<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Create</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>