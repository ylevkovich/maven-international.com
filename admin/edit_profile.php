<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
if(isset($_POST['btn_submit']))
{
	if($category_exists = find('first', MASTER_ADMIN, 'id', "WHERE email_address = '".stripcleantohtml($_POST['email_address'])."' AND id<>".$_SESSION['admin_id']."", array()))
	{
		$_SESSION['SET_TYPE'] = 'error';
		$_SESSION['SET_FLASH'] = 'This Email Address Already Exists';
	}
	else
	{
		if(isset($_POST['password']) && $_POST['password']!='')
		{
			update(MASTER_ADMIN, 'full_name=:full_name, email_address=:email_address, password=:password', 'WHERE id='.$_SESSION['admin_id'].'', array(':full_name'=>stripcleantohtml($_POST['full_name']), ':email_address'=>stripcleantohtml($_POST['email_address']), ':password'=>stripcleantohtml(md5($_POST['password']))));
			$mail_Body = "Dear ".$_POST['full_name'].",<br/><br/>You have updated your account access details.<br/><br/>Please login at: xltsystems.com/cmsadmin with the following details below:<br/><br/>Email: ".$_POST['email_address']."<br/>Password: ".$_POST['password']."<br/><br/>Thank you.<br/>Best Regards,<br/>XLT Systems CMS Admin.<br/>Powered by SkyCRM";
			Send_HTML_Mail($_POST['email_address'], $admin_details['email_address'], '', 'Your Admin Member Accont Details', $mail_Body);
		}
		else
		{
			update(MASTER_ADMIN, 'full_name=:full_name, email_address=:email_address', 'WHERE id='.$_SESSION['admin_id'].'', array(':full_name'=>stripcleantohtml($_POST['full_name']), ':email_address'=>stripcleantohtml($_POST['email_address'])));
		}
		$_SESSION['SET_TYPE'] = 'success';
		$_SESSION['SET_FLASH'] = 'Account Details Have Been Updated Successfully';
	}
}

$admin_users = find('first', MASTER_ADMIN, '*', "WHERE id = ".$_SESSION['admin_id']."", array());
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Edit Profile</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#edit-form").validate({
						rules: {
							full_name: "required",
							email_address: {
								required: true,
								email: true
							}
						},
						messages: {
							full_name: "<font color = 'red'><b>Please enter full name</b></font>",
							email_address: "<font color = 'red'><b>Please enter valid email address</b></font>",
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">My Account</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Edit Profile</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
										<div class="panel panel-grey">
											<div class="panel-heading">Edit Profile</div>
											<div class="panel-body">
												<div class="panel-body pan">
													<form name = "edit-form" id = "edit-form" action = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_profile.php" method = "POST">
													<div class="form-body pal">
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Full Name
																	</label>
																	<div class="input-icon right">
																		<input name="full_name" id="full_name" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['full_name']) && $_POST['full_name']!='') ? $_POST['full_name'] : $admin_users['full_name']);?>" />
																	</div>
																</div>
															
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Email Address
																	</label>
																	<div class="input-icon right">
																		<input name="email_address" id="email_address" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['email_address']) && $_POST['email_address']!='') ? $_POST['email_address'] : $admin_users['email_address']);?>" />
																	</div>
																</div>
															
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		New Password
																	</label>
																	<div class="input-icon right">
																		<input name="password" id="password" type="password" placeholder="" class="form-control" value = "" />
																	</div>
																</div>
																<div style = "margin-bottom:10px;color:#ff0000">Note: Leave blank if you do not wish to change existing password.</div>
															</div>								
														</div>

														<div class="row">
															<div class="col-md-2">
																<div class="form-group">
																	<div class="form-actions text-right pal">
																		<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Update</button>
																	</div>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>