<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
$page_serial = 13;
include_once('includes/permission.php');

if(isset($_GET['mode']) && $_GET['mode'] == 'update')
{
	$_SESSION['SET_TYPE'] = 'success';
	$_SESSION['SET_FLASH'] = 'Page SEO updated successfully';
}

$home = find('first', STATIC_PAGE_HOME, '*', "WHERE id = 1", array());
$about_us = find('first', STATIC_PAGE_ABOUT_US, '*', "WHERE id = 1", array());
$supporters = find('first', STATIC_PAGE_OUR_CLIENT, '*', "WHERE id = 1", array());
$our_service = find('first', STATIC_PAGE_OUR_SERVICE, '*', "WHERE id = 1", array());
$contact_us = find('first', STATIC_PAGE_CONTATC_US, '*', "WHERE id = 1", array());
$jointeam = find('first', STATIC_PAGE_JOIN_TEAM, '*', "WHERE id = 1", array());
$faq = find('first', STATIC_PAGE_FAQ, '*', "WHERE id = 1", array());
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | SEO | Lists Of SEO</title>
    <?php include_once('includes/scripts.php')?>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>  
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">SEO</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Lists of SEO</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
				<div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
								<div class="col-md-12">
									<div id="area-chart-spline" style="width: 100%; height: 300px; display: none;"></div>
								</div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>

                            <div class="col-lg-12">
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-grey">
											<div class="panel-heading">Lists Of SEO <span style="float:right;"><!-- <a href="add_seo.php" class="label-success" style="padding:0 10px; color:#ffffff;">+ Add New SEO</a> --></div>
											<div class="panel-body">
												<table class="table table-hover">
													<thead>
													<tr>
														<th style="width:110px;">Page Name </th>
														<th>Page Title </th>
														<th>Page Meta Keywords </th>
														<th>Page Meta Description </th>
														<th style="width:50px">Action</th>
													</tr>
													</thead>
													<tbody>
													<tr>
														<td>Home</td>
														<td><?php echo($home['page_title']);?></td>
														<td><?php echo($home['meta_keyward']);?></td>
														<td><?php echo($home['meta_description']);?></td>
														<td><a href = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_seo.php?type=home" title = "Edit SEO" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/edit.png" border = "0" alt = "Edit SEO" /></a><!-- &nbsp;&nbsp;&nbsp;<a href = "#" title = "Delete SEO" data-toggle="tooltip"><img src = "images/icons/delete.png" border = "0" alt = "Delete SEO" /></a> --></td>
													</tr>
													<tr>
														<td>Our Service</td>
														<td><?php echo($our_service['page_title']);?></td>
														<td><?php echo($our_service['meta_keyward']);?></td>
														<td><?php echo($our_service['meta_description']);?></td>
														<td><a href = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>edit_seo.php?type=our_service" title = "Edit SEO" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/edit.png" border = "0" alt = "Edit SEO" /></a><!-- &nbsp;&nbsp;&nbsp;<a href = "#" title = "Delete SEO" data-toggle="tooltip"><img src = "images/icons/delete.png" border = "0" alt = "Delete SEO" /></a> --></td>
													</tr>
													
													<tr>
														<td>Our Clients</td>
														<td><?php echo($supporters['page_title']);?></td>
														<td><?php echo($supporters['meta_keyward']);?></td>
														<td><?php echo($supporters['meta_description']);?></td>
														<td><a href = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_seo.php?type=supporters" title = "Edit SEO" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/edit.png" border = "0" alt = "Edit SEO" /></a><!-- &nbsp;&nbsp;&nbsp;<a href = "#" title = "Delete SEO" data-toggle="tooltip"><img src = "images/icons/delete.png" border = "0" alt = "Delete SEO" /></a> --></td>
													</tr>
													<tr>
														<td>FAQ</td>
														<td><?php echo($faq['page_title']);?></td>
														<td><?php echo($faq['meta_keyward']);?></td>
														<td><?php echo($faq['meta_description']);?></td>
														<td><a href = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_seo.php?type=faq" title = "Edit SEO" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/edit.png" border = "0" alt = "Edit SEO" /></a><!-- &nbsp;&nbsp;&nbsp;<a href = "#" title = "Delete SEO" data-toggle="tooltip"><img src = "images/icons/delete.png" border = "0" alt = "Delete SEO" /></a> --></td>
													</tr>
													<tr>
														<td>About Us</td>
														<td><?php echo($about_us['page_title']);?></td>
														<td><?php echo($about_us['meta_keyward']);?></td>
														<td><?php echo($about_us['meta_description']);?></td>
														<td><a href = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_seo.php?type=about_us" title = "Edit SEO" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/edit.png" border = "0" alt = "Edit SEO" /></a><!-- &nbsp;&nbsp;&nbsp;<a href = "#" title = "Delete SEO" data-toggle="tooltip"><img src = "images/icons/delete.png" border = "0" alt = "Delete SEO" /></a> --></td>
													</tr>
													
													<tr>
														<td>Join Our Team</td>
														<td><?php echo($jointeam['page_title']);?></td>
														<td><?php echo($jointeam['meta_keyward']);?></td>
														<td><?php echo($jointeam['meta_description']);?></td>
														<td><a href = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_seo.php?type=jointeam" title = "Edit SEO" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/edit.png" border = "0" alt = "Edit SEO" /></a><!-- &nbsp;&nbsp;&nbsp;<a href = "#" title = "Delete SEO" data-toggle="tooltip"><img src = "images/icons/delete.png" border = "0" alt = "Delete SEO" /></a> --></td>
													</tr>
													<tr>
														<td>Contact Us</td>
														<td><?php echo($contact_us['page_title']);?></td>
														<td><?php echo($contact_us['meta_keyward']);?></td>
														<td><?php echo($contact_us['meta_description']);?></td>
														<td><a href = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_seo.php?type=contact_us" title = "Edit SEO" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/edit.png" border = "0" alt = "Edit SEO" /></a><!-- &nbsp;&nbsp;&nbsp;<a href = "#" title = "Delete SEO" data-toggle="tooltip"><img src = "images/icons/delete.png" border = "0" alt = "Delete SEO" /></a> --></td>
													</tr>
													
													
													
													
													
													
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>