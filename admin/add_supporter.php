<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);

if(isset($_POST['btn_submit']))
{
	/*$menu_exists = find('first', SUPPORTER, 'id', "WHERE name_en = :name_en", array(':name_en'=>stripcleantohtml($_POST['name_en'])));
	if(!empty($menu_exists))
	{
		$_SESSION['SET_TYPE'] = 'error';
		$_SESSION['SET_FLASH'] = 'This Name Already Exists';
	}
	else
	{*/
	
		if($_FILES['image']['name']!='')
		{
			$explode_data = explode('.', $_FILES['image']['name']);
			$extension = end($explode_data);
			if($extension == 'jpg' OR $extension == 'JPG' OR $extension == 'gif' OR $extension == 'GIF' OR $extension == 'png' OR $extension == 'PNG' OR $extension == 'JPEG' OR $extension == 'jpeg')
			{
				$v_image=$_FILES['image']['name'];
				$path="../images/image_support/";
				$v_pic=rand(0,200000)."_".$v_image;
				$flag_status1 = true;
			}
			else
			{
				$flag_status1 = false;
				$_SESSION['SET_TYPE'] = 'error';
				$_SESSION['SET_FLASH'] = 'Invalid extension. Please uplaod .jpg or .jpeg or .gif or .png image';
			}
		}
		
		if($flag_status1 == true)
		{
			$field = 'image, website_link';
			$value = ':image, :website_link';
			$execute = array(
							':image'=>$v_pic,
							':website_link'=>stripcleantohtml($_POST['website_link'])
							);
			$add_supporter = save(SUPPORTER, $field, $value, $execute);
			if($add_supporter)
			{
				move_uploaded_file($_FILES['image']['tmp_name'],$path.$v_pic);
				header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_supporters.php?mode=add');
			}
		}
	/*}*/
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Manage Supporter | Add New Supporter</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#add-form").validate({
						rules: {
							image: "required"
						},
						messages: {
							image: "<font color = 'red'><b>Please upload supporter image</b></font>",
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Manage Supporter</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="admin/dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active"><a href="admin/list_supporters.php">Manage Supporter</a></li>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Add New Supporter</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
										<div class="panel panel-grey">
										<div class="panel-heading">Add New Supporter</div>
											<div class="panel-body">
												<div class="panel-body pan">
													<form name = "add-form" id = "add-form" action = "" method = "POST" enctype = "multipart/form-data">
														<div class="row">
															<!-- <div class="col-md-4">
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Name (<span style="color:#0000FF">English</span>)
																	</label>
																	<div class="input-icon right">
																		<input name = "name_en" id = "name_en" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['name_en']) && $_POST['name_en']!='' ? $_POST['name_en'] :'' ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Name (<span style="color:#fe1a1a">Türkçe</span>)
																	</label>
																	<div class="input-icon right">
																		<input name = "name_tu" id = "name_tu" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['name_tu']) && $_POST['name_tu']!='' ? $_POST['name_tu'] :'' ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Name (<span style="color:#107257">Türkmençe</span>)
																	</label>
																	<div class="input-icon right">
																		<input name = "name_tk" id = "name_tk" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['name_tk']) && $_POST['name_tk']!='' ? $_POST['name_tk'] :'' ?>"/>
																	</div>
																</div>
															</div> -->
															<div class="col-md-6">
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Upload Image
																	</label>
																	<div class="input-icon right">
																		<input name = "image" id = "image" placeholder="Inlcude some file" type="file" />
																		<div><small style="color:#ff0000">Image dimension: 170px by auto</small></div>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Website Link	
																	</label>
																	<div class="input-icon right">
																		<input name = "website_link" id = "website_link" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['website_link']) && $_POST['website_link']!='' ? $_POST['website_link'] :'' ?>"/>
																	</div>
																</div>
															</div>
														</div>
													
														<div class="form-actions text-right pal">
															<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Create</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>