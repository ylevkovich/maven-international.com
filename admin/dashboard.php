<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');



/* End graph */
$recent_enquries = find('all', ENQUIRIES, '*', "WHERE 1 ORDER BY id DESC LIMIT 0,5", array());
$recent_quotes = find('all', MANAGE_QUOTES, '*', "WHERE 1 ORDER BY id DESC LIMIT 0,5", array());
$total_enquries1 = find('first', ENQUIRIES, 'count(*) as cnt', "WHERE 1", array());
$total_quotes1 = find('first', MANAGE_QUOTES, 'count(*) as cnt_quotes', "WHERE 1", array());
$total_service = find('first', MANAGE_SERVICE, 'count(*) as srv', "WHERE 1", array());
/*$total_news = find('first', NEWS, 'count(*) as cnt_news', "WHERE 1", array());
$total_editions = find('first', EDITION, 'count(*) as cnt_edition', "WHERE 1", array());*/
$total_supporter = find('first', SUPPORTER, 'count(*) as cnt_supporter', "WHERE 1", array());
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Dashboard</title>
    <?php include_once('includes/scripts.php')?>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
		<?php include_once('includes/admin_header.php')?>        
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Dashboard</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="hidden"><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>#">Dashboard</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Dashboard</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="page-content">
                    <div id="tab-general">
                        <div id="sum_box" class="row mbl">
                            <div class="col-sm-6 col-md-3" style="cursor:pointer" onclick="window.location.href='admin/list_enquiries.php'">
                                <div class="panel profit db mbm dip1">
                                    <div class="panel-body">
                                        <p class="icon"><i class="icon fa fa-desktop"></i></p>
                                        <h4 class="value"><span><?php echo($total_enquries1['cnt']); ?></span></h4>
                                      </div>
									  <div class="panel-body">
                                        <p class="description">Total Enquiries</p>
                                      </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3" style="cursor:pointer" onclick="window.location.href='admin/list_service.php'">
                                <div class="panel db mbm dip2">
                                    <div class="panel-body">
                                        <p class="icon"><i class="icon fa fa-file-text-o"></i></p>
                                        <!-- <h4 class="value"><span><?php echo($total_news['cnt_news']); ?></span></h4> -->
                                        <h4 class="value"><span><?php echo($total_service['srv']); ?></span></h4>
                                    </div>
									 <div class="panel-body">
                                        <p class="description">Total Services</p>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-sm-6 col-md-3" style="cursor:pointer" onclick="window.location.href='admin/quote_list.php'">
                                <div class="panel visit db mbm dip4">
                                    <div class="panel-body">
                                        <p class="icon"><i class="icon fa fa-quote-right"></i></p>
                                        <h4 class="value"><span><?php echo($total_quotes1['cnt_quotes']); ?></span></h4>
                                    </div>
									<div class="panel-body">
                                        <p class="description">Total Quotes</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3" style="cursor:pointer" onclick="window.location.href='admin/list_supporters.php'">
                                <div class="panel visit db mbm dip5">
                                    <div class="panel-body">
                                        <p class="icon"><i class="icon fa fa-user"></i></p>
                                        <h4 class="value"><span><?php echo($total_supporter['cnt_supporter']); ?></span></h4>
                                    </div>
									<div class="panel-body">
                                        <p class="description">Total Supporters</p>
                                    </div>
                                </div>
                            </div>
                        </div>

						<?php
/*						if($flag_enquiries)
						{
*/						?>
                        <div class="row mbl">
                            <div class="col-lg-12">
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-grey">
											<div class="panel-heading">Latest five quotes for services<span style="float:right;"><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/quote_list.php" class="label-success" style="padding:0 10px; color:#ffffff;">View All</a></span></div>
											<div class="panel-body">
												<table class="table table-hover">
													<thead>
													<tr>
														<th>#</th>
														<th>Service Name</th>
														<th>Full Name</th>
														<th>Email</th>
														<th>Mobile</th>
														<!-- <th>Date</th >-->
														<th>Source Language</th>
														<th>Translate Language</th>
														<th>Status</th>
														<th>Action</th>
													</tr>
													</thead>
													<tbody>
													<?php
													if($recent_quotes)
													{
														$sl_number = 1;
														foreach($recent_quotes AS $quotes)
														{
															$temp_date = explode(':-:', $quotes['date']);
															//$display_date = end($temp_date);
															$temp = explode(':-:', $quotes['message']);
															$translate_lang = explode(':-:', $quotes['translate_language']);
															$translate_language = implode(', ', $translate_lang);
															$source_lang = explode(':-:', $quotes['source_language']);
															$source_language = implode(', ', $source_lang);
															$message = $temp[0];
															$service_name = find("first", MANAGE_SERVICE, '*', "WHERE id = ".$quotes['service_id']."", array());
													?>
													<tr>
														<td><?php echo($sl_number);?></td>
														<td><?php echo($service_name['service_title_eng']);?></td>
														<td><?php echo($quotes['full_name']);?></td>
														<td><?php echo($quotes['email_address']);?></td>
														<td><?php echo($quotes['mobile_number']);?></td>
														<!-- <td><?php echo($display_date);?></td> -->
														<td><?php echo($source_language);?></td>
														<td><?php echo($translate_language);?></td>
														<td>
														<?php
														if($quotes['status'] == 'Y')
														{
														?>
														<span class="label label-sm label-success">Read</span>
														<?php
														}
														else
														{
														?>
														<span class="label label-sm label-warning">Unread</span>
														<?php
														}
														?>
														</td>
														<td><a href = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/manage_quote.php?id=<?php echo(base64_encode($quotes['id']));?>" title = "Reply Quotes" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/reply.png" border = "0" alt = "Reply Quotes" /></a></td>
													</tr>
													<?php
														$sl_number++;
														}
													}
													else
													{
													?>
													<tr>
														<td colspan = "7" align = "center"><font color = "red"><b>No Record Found!</b></td>
													</tr>
													<?php
													}
													?>
													</tbody>
												</table>
											</div>
										</div>
									</div>

									<div class="col-lg-12">
										<div class="panel panel-grey">
											<div class="panel-heading">Latest five enquiries from contact us<span style="float:right;"><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_enquiries.php" class="label-success" style="padding:0 10px; color:#ffffff;">View All</a></span></div>
											<div class="panel-body">
												<table class="table table-hover">
													<thead>
													<tr>
														<th>#</th>
														<th>Full Name</th>
														<th>Email</th>
														<th>Mobile</th>
														<th>Date</th>
														<th>Message</th>
														<th>Status</th>
														<th>Action</th>
													</tr>
													</thead>
													<tbody>
													<?php
													if($recent_enquries)
													{
														$sl_number = 1;
														foreach($recent_enquries AS $enquiry)
														{
															$temp_date = explode(':-:', $enquiry['date']);
															$display_date = end($temp_date);
															$temp = explode(':-:', $enquiry['message']);
															$message = $temp[0];
													?>
													<tr>
														<td><?php echo($sl_number);?></td>
														<td><?php echo($enquiry['full_name']);?></td>
														<td><?php echo($enquiry['email_address']);?></td>
														<td><?php echo($enquiry['mobile_number']);?></td>
														<td><?php echo($display_date);?></td>
														<td><?php echo($message);?></td>
														<td>
														<?php
														if($enquiry['status'] == 'Y')
														{
														?>
														<span class="label label-sm label-success">Read</span>
														<?php
														}
														else
														{
														?>
														<span class="label label-sm label-warning">Unread</span>
														<?php
														}
														?>
														</td>
														<td><a href = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/reply.php?id=<?php echo(base64_encode($enquiry['id']));?>" title = "Send Reply" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/reply.png" border = "0" alt = "reply" /></a></td>
													</tr>
													<?php
														$sl_number++;
														}
													}
													else
													{
													?>
													<tr>
														<td colspan = "7" align = "center"><font color = "red"><b>No Record Found!</b></td>
													</tr>
													<?php
													}
													?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
                            </div>
                        </div>
						<?php
						//}
						?>
                    </div>
                </div>
                <?php include_once('includes/admin_footer.php');?>
            </div>
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>
