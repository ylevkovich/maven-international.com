<?php
	include_once('../init.php');
	validation_check($_SESSION['admin_id'], 'admin/index.php');
	permission_allowed($_SESSION['role']);
	if(isset($_POST['video_name']) && $_POST['video_name']!="" &&  file_exists('../video/'.$_POST['video_name']))
	{
		unlink('../video/'.$_POST['video_name']);
	}
?>