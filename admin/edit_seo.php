<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
$page_serial = 13;
include_once('includes/permission.php');

if(isset($_POST['btn_submit']))
{
		
	$value='page_title=:page_title, meta_keyward=:meta_keyward, meta_description=:meta_description';
	$execute=array(':page_title'=>stripcleantohtml($_POST['page_title']), 
						':meta_keyward'=>stripcleantohtml($_POST['meta_keyward']),
						':meta_description'=>stripcleantohtml($_POST['meta_description'])
						);
	if(isset($_GET['type']) && $_GET['type'] == 'home')
	{
		update(STATIC_PAGE_HOME, $value, 'WHERE id=1', $execute);
	}
	
	if(isset($_GET['type']) && $_GET['type'] == 'our_service')
	{
		update(STATIC_PAGE_OUR_SERVICE, $value, 'WHERE id=1', $execute);
	}
	if(isset($_GET['type']) && $_GET['type'] == 'about_us')
	{
		update(STATIC_PAGE_ABOUT_US, $value, 'WHERE id=1', $execute);
	}
	if(isset($_GET['type']) && $_GET['type'] == 'supporters')
	{
		update(STATIC_PAGE_OUR_CLIENT, $value, 'WHERE id=1', $execute);
	}
	
	if(isset($_GET['type']) && $_GET['type'] == 'contact_us')
	{
		update(STATIC_PAGE_CONTATC_US, $value, 'WHERE id=1', $execute);
	}	
	if(isset($_GET['type']) && $_GET['type'] == 'faq')
	{
		update(STATIC_PAGE_FAQ, $value, 'WHERE id=1', $execute);
	}	
	if(isset($_GET['type']) && $_GET['type'] == 'jointeam')
	{
		update(STATIC_PAGE_JOIN_TEAM, $value, 'WHERE id=1', $execute);
	}	
	header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_seo.php?mode=update');
}

if(isset($_GET['type']) && $_GET['type'] == 'home')
{
	$page_content = find('first', STATIC_PAGE_HOME, '*', "WHERE id = 1", array());
}
if(isset($_GET['type']) && $_GET['type'] == 'our_service')
{
	$page_content = find('first', STATIC_PAGE_OUR_SERVICE, '*', "WHERE id = 1", array());
}
if(isset($_GET['type']) && $_GET['type'] == 'contact_us')
{
	$page_content = find('first', STATIC_PAGE_CONTATC_US, '*', "WHERE id = 1", array());
}
if(isset($_GET['type']) && $_GET['type'] == 'about_us')
{
	$page_content = find('first', STATIC_PAGE_ABOUT_US, '*', "WHERE id = 1", array());
}
if(isset($_GET['type']) && $_GET['type'] == 'supporters')
{
	$page_content = find('first', STATIC_PAGE_OUR_CLIENT, '*', "WHERE id = 1", array());
}
if(isset($_GET['type']) && $_GET['type'] == 'faq')
{
	$page_content = find('first', STATIC_PAGE_FAQ, '*', "WHERE id = 1", array());
}
if(isset($_GET['type']) && $_GET['type'] == 'jointeam')
{
	$page_content = find('first', STATIC_PAGE_JOIN_TEAM, '*', "WHERE id = 1", array());
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | SEO | Edit SEO</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#edit-form").validate({
						rules: {
							page_title_tu: "required",
							meta_keyward_tu: "required",
							meta_description_tu: "required"
						},
						messages: {
							page_title_tu: "<font color = 'red'><b>Please enter page title</b></font>",
							meta_keyward_tu: "<font color = 'red'><b>Please enter page meta keyword</b></font>",
							meta_description_tu: "<font color = 'red'><b>Please enter page meta description</b></font>"
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">SEO</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
						<li>&nbsp;<a href="admin/list_seo.php">SEO</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Edit SEO</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="panel panel-grey">
									<div class="panel-heading">Edit SEO</div>
									<div class="panel-body">
										<form name = "edit-form" id = "edit-form" action="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_seo.php?type=<?php echo($_GET['type'])?>" method = "POST">
										<div class="row">
											<div class="col-md-4">
												<div class="panel-body pan">
																						
														<div class="form-group">
															<label for="inputSubject" class="control-label">
																Page Title 
															</label>
															<div class="input-icon right">
																<input name = "page_title" id = "page_title" type="text" placeholder="" class="form-control"  value="<?php echo($page_content['page_title']);?>"/>
															</div>
														</div>
														<div class="form-group">
															<label for="inputSubject" class="control-label">
																Page Meta Keywords 
															</label>
															<div class="input-icon right">
																<textarea name = "meta_keyward" id = "meta_keyward" rows="3" class="form-control"><?php echo($page_content['meta_keyward']);?></textarea>
															</div>
														</div>
														<div class="form-group">
															<label for="inputSubject" class="control-label">
																Page Meta Description 
															</label>
															<div class="input-icon right">
																<textarea name = "meta_description" id = "meta_description" rows="3" class="form-control"><?php echo($page_content['meta_description']);?></textarea>
															</div>
														</div>
													
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-actions text-right pal">
													<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Update</button>
												</div>
											</div>
										</div>
										</form>

									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
</body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>
