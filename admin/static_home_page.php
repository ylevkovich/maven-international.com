<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
$first_block_four_one = find('first', STATIC_PAGE_HOME_FIRST_BLOCK_ONE, '*', "WHERE id = 1", array());
if(isset($_POST['btn_submit1']))	
{
		if(isset($_FILES['image']['name']) && $_FILES['image']['name']!="")
		{
			$file1 = "../images/home/".$first_block_four_one['image'];
	
			$v_image1=$_FILES['image']['name'];
			$path1="../images/home/";
			$v_pic1=rand(0,200000)."_".$v_image1;
			if (file_exists($file1))
				{
					@unlink("../images/home/".$first_block_four_one["image"]);
				}
			move_uploaded_file($_FILES['image']['tmp_name'],$path1.$v_pic1);
		}
		else
		{
			$v_pic1=$first_block_four_one['image'];
		}
			$value = 'image=:image, link=:link, title_eng=:title_eng, description_eng=:description_eng, title_rus=:title_rus, description_rus=:description_rus, title_tur=:title_tur, description_tur=:description_tur, title_spa=:title_spa, description_spa=:description_spa, title_fre=:title_fre, description_fre=:description_fre, title_man=:title_man, description_man=:description_man, title_can=:title_can, description_can=:description_can, title_ger=:title_ger, description_ger=:description_ger, title_per=:title_per, description_per=:description_per, title_kor=:title_kor, description_kor=:description_kor, title_mal=:title_mal, description_mal=:description_mal, title_ind=:title_ind, description_ind=:description_ind, title_ara=:title_ara, description_ara=:description_ara, title_jap=:title_jap, description_jap=:description_jap';
			$execute = array(
							':image'=>$v_pic1,
							':link'=>($_POST['link']),
							':title_eng'=>$_POST['title_eng'],
							':description_eng'=>$_POST['description_eng'],
							':title_rus'=>$_POST['title_rus'],
							':description_rus'=>$_POST['description_rus'],
							':title_tur'=>$_POST['title_tur'],
							':description_tur'=>$_POST['description_tur'],
							':description_spa'=>$_POST['description_spa'],
							':title_spa'=>$_POST['title_spa'],
							':description_fre'=>$_POST['description_fre'],
							':title_fre'=>$_POST['title_fre'],
							':description_man'=>$_POST['description_man'],
							':title_man'=>$_POST['title_man'],
							':description_can'=>$_POST['description_can'],
							':title_can'=>$_POST['title_can'],
							':description_ger'=>$_POST['description_ger'],
							':title_ger'=>$_POST['title_ger'],
							':description_per'=>$_POST['description_per'],
							':title_per'=>$_POST['title_per'],
							':description_kor'=>$_POST['description_kor'],
							':title_kor'=>$_POST['title_kor'],
							':description_mal'=>$_POST['description_mal'],
							':title_mal'=>$_POST['title_mal'],
							':description_ind'=>$_POST['description_ind'],
							':title_ind'=>$_POST['title_ind'],
							':description_ara'=>$_POST['description_ara'],
							':title_ara'=>$_POST['title_ara'],
							':description_jap'=>$_POST['description_jap'],
							':title_jap'=>$_POST['title_jap']
							);
				
			$update=update(STATIC_PAGE_HOME_FIRST_BLOCK_ONE, $value, 'WHERE id=1',$execute);
			if($update)
			{
				$_SESSION['SET_TYPE'] = 'success';
				$_SESSION['SET_FLASH'] = 'Static Home Page content successfully updated';
				header('location:admin/static_home_page.php');
				exit;
			}
		
}
$first_block_four_four = find('first', STATIC_PAGE_HOME_FIRST_BLOCK_FOUR, '*', "WHERE id = 1", array());
if(isset($_POST['btn_submit4']))	
{
		if(isset($_FILES['image']['name']) && $_FILES['image']['name']!="")
		{
			$file1 = "../images/home/".$first_block_four_four['image'];
	
			$v_image1=$_FILES['image']['name'];
			$path1="../images/home/";
			$v_pic4=rand(0,200000)."_".$v_image1;
			if (file_exists($file1))
				{
					@unlink("../images/home/".$first_block_four_four["image"]);
				}
			move_uploaded_file($_FILES['image']['tmp_name'],$path1.$v_pic4);
		}
		else
		{
			$v_pic4=$first_block_four_four['image'];
		}
			$value = 'image=:image, link=:link, title_eng=:title_eng, description_eng=:description_eng, title_rus=:title_rus, description_rus=:description_rus, title_tur=:title_tur, description_tur=:description_tur, title_spa=:title_spa, description_spa=:description_spa, title_fre=:title_fre, description_fre=:description_fre, title_man=:title_man, description_man=:description_man, title_can=:title_can, description_can=:description_can, title_ger=:title_ger, description_ger=:description_ger, title_per=:title_per, description_per=:description_per, title_kor=:title_kor, description_kor=:description_kor, title_mal=:title_mal, description_mal=:description_mal, title_ind=:title_ind, description_ind=:description_ind, title_ara=:title_ara, description_ara=:description_ara, title_jap=:title_jap, description_jap=:description_jap';
			$execute = array(
							':image'=>$v_pic4,
							':link'=>($_POST['link']),
							':title_eng'=>$_POST['title_eng'],
							':description_eng'=>$_POST['description_eng'],
							':title_rus'=>$_POST['title_rus'],
							':description_rus'=>$_POST['description_rus'],
							':title_tur'=>$_POST['title_tur'],
							':description_tur'=>$_POST['description_tur'],
							':description_spa'=>$_POST['description_spa'],
							':title_spa'=>$_POST['title_spa'],
							':description_fre'=>$_POST['description_fre'],
							':title_fre'=>$_POST['title_fre'],
							':description_man'=>$_POST['description_man'],
							':title_man'=>$_POST['title_man'],
							':description_can'=>$_POST['description_can'],
							':title_can'=>$_POST['title_can'],
							':description_ger'=>$_POST['description_ger'],
							':title_ger'=>$_POST['title_ger'],
							':description_per'=>$_POST['description_per'],
							':title_per'=>$_POST['title_per'],
							':description_kor'=>$_POST['description_kor'],
							':title_kor'=>$_POST['title_kor'],
							':description_mal'=>$_POST['description_mal'],
							':title_mal'=>$_POST['title_mal'],
							':description_ind'=>$_POST['description_ind'],
							':title_ind'=>$_POST['title_ind'],
							':description_ara'=>$_POST['description_ara'],
							':title_ara'=>$_POST['title_ara'],
							':description_jap'=>$_POST['description_jap'],
							':title_jap'=>$_POST['title_jap']
							);
				
			$update=update(STATIC_PAGE_HOME_FIRST_BLOCK_FOUR, $value, 'WHERE id=1',$execute);
			if($update)
			{	
				$_SESSION['SET_TYPE'] = 'success';
				$_SESSION['SET_FLASH'] = 'Static Home Page content successfully updated';
				header('location:admin/static_home_page.php');
				exit;
			}
}
$first_block_four_three = find('first', STATIC_PAGE_HOME_FIRST_BLOCK_THREE, '*', "WHERE id = 1", array());
if(isset($_POST['btn_submit3']))	
{
		if(isset($_FILES['image']['name']) && $_FILES['image']['name']!="")
		{
			$file1 = "../images/home/".$first_block_four_three['image'];
	
			$v_image1=$_FILES['image']['name'];
			$path1="../images/home/";
			$v_pic3=rand(0,200000)."_".$v_image1;
			if (file_exists($file1))
				{
					@unlink("../images/home/".$first_block_four_three["image"]);
				}
			move_uploaded_file($_FILES['image']['tmp_name'],$path1.$v_pic3);
		}
		else
		{
			$v_pic3=$first_block_four_three['image'];
		}
			$value = 'image=:image, link=:link, title_eng=:title_eng, description_eng=:description_eng, title_rus=:title_rus, description_rus=:description_rus, title_tur=:title_tur, description_tur=:description_tur, title_spa=:title_spa, description_spa=:description_spa, title_fre=:title_fre, description_fre=:description_fre, title_man=:title_man, description_man=:description_man, title_can=:title_can, description_can=:description_can, title_ger=:title_ger, description_ger=:description_ger, title_per=:title_per, description_per=:description_per, title_kor=:title_kor, description_kor=:description_kor, title_mal=:title_mal, description_mal=:description_mal, title_ind=:title_ind, description_ind=:description_ind, title_ara=:title_ara, description_ara=:description_ara, title_jap=:title_jap, description_jap=:description_jap';
			$execute = array(
							':image'=>$v_pic3,
							':link'=>($_POST['link']),
							':title_eng'=>$_POST['title_eng'],
							':description_eng'=>$_POST['description_eng'],
							':title_rus'=>$_POST['title_rus'],
							':description_rus'=>$_POST['description_rus'],
							':title_tur'=>$_POST['title_tur'],
							':description_tur'=>$_POST['description_tur'],
							':description_spa'=>$_POST['description_spa'],
							':title_spa'=>$_POST['title_spa'],
							':description_fre'=>$_POST['description_fre'],
							':title_fre'=>$_POST['title_fre'],
							':description_man'=>$_POST['description_man'],
							':title_man'=>$_POST['title_man'],
							':description_can'=>$_POST['description_can'],
							':title_can'=>$_POST['title_can'],
							':description_ger'=>$_POST['description_ger'],
							':title_ger'=>$_POST['title_ger'],
							':description_per'=>$_POST['description_per'],
							':title_per'=>$_POST['title_per'],
							':description_kor'=>$_POST['description_kor'],
							':title_kor'=>$_POST['title_kor'],
							':description_mal'=>$_POST['description_mal'],
							':title_mal'=>$_POST['title_mal'],
							':description_ind'=>$_POST['description_ind'],
							':title_ind'=>$_POST['title_ind'],
							':description_ara'=>$_POST['description_ara'],
							':title_ara'=>$_POST['title_ara'],
							':description_jap'=>$_POST['description_jap'],
							':title_jap'=>$_POST['title_jap']
							);
				
			$update=update(STATIC_PAGE_HOME_FIRST_BLOCK_THREE, $value, 'WHERE id=1',$execute);
			if($update)
			{	
			$_SESSION['SET_TYPE'] = 'success';
			$_SESSION['SET_FLASH'] = 'Static Home Page content successfully updated';
			header('location:admin/static_home_page.php');
			exit;
			}
		
}

$first_block_four_two = find('first', STATIC_PAGE_HOME_FIRST_BLOCK_TWO, '*', "WHERE id = 1", array());
if(isset($_POST['btn_submit2']))	
{
		if(isset($_FILES['image']['name']) && $_FILES['image']['name']!="")
		{
			$file1 = "../images/home/".$first_block_four_two['image'];
	
			$v_image1=$_FILES['image']['name'];
			$path1="../images/home/";
			$v_pic2=rand(0,200000)."_".$v_image1;
			if (file_exists($file1))
				{
					@unlink("../images/home/".$first_block_four_two["image"]);
				}
			move_uploaded_file($_FILES['image']['tmp_name'],$path1.$v_pic2);
		}
		else
		{
			$v_pic2=$first_block_four_two['image'];
		}
			$value = 'image=:image, link=:link, title_eng=:title_eng, description_eng=:description_eng, title_rus=:title_rus, description_rus=:description_rus, title_tur=:title_tur, description_tur=:description_tur, title_spa=:title_spa, description_spa=:description_spa, title_fre=:title_fre, description_fre=:description_fre, title_man=:title_man, description_man=:description_man, title_can=:title_can, description_can=:description_can, title_ger=:title_ger, description_ger=:description_ger, title_per=:title_per, description_per=:description_per, title_kor=:title_kor, description_kor=:description_kor, title_mal=:title_mal, description_mal=:description_mal, title_ind=:title_ind, description_ind=:description_ind, title_ara=:title_ara, description_ara=:description_ara, title_jap=:title_jap, description_jap=:description_jap';
			$execute = array(
							':image'=>$v_pic2,
							':link'=>($_POST['link']),
							':title_eng'=>$_POST['title_eng'],
							':description_eng'=>$_POST['description_eng'],
							':title_rus'=>$_POST['title_rus'],
							':description_rus'=>$_POST['description_rus'],
							':title_tur'=>$_POST['title_tur'],
							':description_tur'=>$_POST['description_tur'],
							':description_spa'=>$_POST['description_spa'],
							':title_spa'=>$_POST['title_spa'],
							':description_fre'=>$_POST['description_fre'],
							':title_fre'=>$_POST['title_fre'],
							':description_man'=>$_POST['description_man'],
							':title_man'=>$_POST['title_man'],
							':description_can'=>$_POST['description_can'],
							':title_can'=>$_POST['title_can'],
							':description_ger'=>$_POST['description_ger'],
							':title_ger'=>$_POST['title_ger'],
							':description_per'=>$_POST['description_per'],
							':title_per'=>$_POST['title_per'],
							':description_kor'=>$_POST['description_kor'],
							':title_kor'=>$_POST['title_kor'],
							':description_mal'=>$_POST['description_mal'],
							':title_mal'=>$_POST['title_mal'],
							':description_ind'=>$_POST['description_ind'],
							':title_ind'=>$_POST['title_ind'],
							':description_ara'=>$_POST['description_ara'],
							':title_ara'=>$_POST['title_ara'],
							':description_jap'=>$_POST['description_jap'],
							':title_jap'=>$_POST['title_jap']
							);
				
			$update=update(STATIC_PAGE_HOME_FIRST_BLOCK_TWO, $value, 'WHERE id=1',$execute);
			if($update)
			{	
				$_SESSION['SET_TYPE'] = 'success';
				$_SESSION['SET_FLASH'] = 'Static Home Page content successfully updated';
				header('location:admin/static_home_page.php');
				exit;
			}

}
$second_block_four_four = find('first', STATIC_PAGE_HOME_SECOND_BLOCK_FOUR, '*', "WHERE id = 1", array());
if(isset($_POST['btn_submit5']))	
{
		if(isset($_FILES['image']['name']) && $_FILES['image']['name']!="")
		{
			$file1 = "../images/home/".$second_block_four_four['image'];
	
			$v_image1=$_FILES['image']['name'];
			$path1="../images/home/";
			$v_pic5=rand(0,200000)."_".$v_image1;
			if (file_exists($file1))
				{
					@unlink("../images/home/".$second_block_four_four["image"]);
				}
			move_uploaded_file($_FILES['image']['tmp_name'],$path1.$v_pic5);
		}
		else
		{
			$v_pic5=$second_block_four_four['image'];
		}
			$value = 'image=:image, link=:link, title_eng=:title_eng, description_eng=:description_eng, title_rus=:title_rus, description_rus=:description_rus, title_tur=:title_tur, description_tur=:description_tur, title_spa=:title_spa, description_spa=:description_spa, title_fre=:title_fre, description_fre=:description_fre, title_man=:title_man, description_man=:description_man, title_can=:title_can, description_can=:description_can, title_ger=:title_ger, description_ger=:description_ger, title_per=:title_per, description_per=:description_per, title_kor=:title_kor, description_kor=:description_kor, title_mal=:title_mal, description_mal=:description_mal, title_ind=:title_ind, description_ind=:description_ind, title_ara=:title_ara, description_ara=:description_ara, title_jap=:title_jap, description_jap=:description_jap';
			$execute = array(
							':image'=>$v_pic5,
							':link'=>($_POST['link']),
							':title_eng'=>$_POST['title_eng'],
							':description_eng'=>$_POST['description_eng'],
							':title_rus'=>$_POST['title_rus'],
							':description_rus'=>$_POST['description_rus'],
							':title_tur'=>$_POST['title_tur'],
							':description_tur'=>$_POST['description_tur'],
							':description_spa'=>$_POST['description_spa'],
							':title_spa'=>$_POST['title_spa'],
							':description_fre'=>$_POST['description_fre'],
							':title_fre'=>$_POST['title_fre'],
							':description_man'=>$_POST['description_man'],
							':title_man'=>$_POST['title_man'],
							':description_can'=>$_POST['description_can'],
							':title_can'=>$_POST['title_can'],
							':description_ger'=>$_POST['description_ger'],
							':title_ger'=>$_POST['title_ger'],
							':description_per'=>$_POST['description_per'],
							':title_per'=>$_POST['title_per'],
							':description_kor'=>$_POST['description_kor'],
							':title_kor'=>$_POST['title_kor'],
							':description_mal'=>$_POST['description_mal'],
							':title_mal'=>$_POST['title_mal'],
							':description_ind'=>$_POST['description_ind'],
							':title_ind'=>$_POST['title_ind'],
							':description_ara'=>$_POST['description_ara'],
							':title_ara'=>$_POST['title_ara'],
							':description_jap'=>$_POST['description_jap'],
							':title_jap'=>$_POST['title_jap']
							);
				
			$update=update(STATIC_PAGE_HOME_SECOND_BLOCK_FOUR, $value, 'WHERE id=1',$execute);
			if($update)
			{	
				$_SESSION['SET_TYPE'] = 'success';
				$_SESSION['SET_FLASH'] = 'Static Home Page content successfully updated';
				header('location:admin/static_home_page.php');
				exit;
			}
}
$second_block_four_one = find('first', STATIC_PAGE_HOME_SECOND_BLOCK_ONE, '*', "WHERE id = 1", array());
if(isset($_POST['btn_submit6']))	
{
		if(isset($_FILES['image']['name']) && $_FILES['image']['name']!="")
		{
			$file1 = "../images/home/".$second_block_four_one['image'];
	
			$v_image1=$_FILES['image']['name'];
			$path1="../images/home/";
			$v_pic6=rand(0,200000)."_".$v_image1;
			if (file_exists($file1))
				{
					@unlink("../images/home/".$second_block_four_one["image"]);
				}
			move_uploaded_file($_FILES['image']['tmp_name'],$path1.$v_pic6);
		}
		else
		{
			$v_pic6=$second_block_four_one['image'];
		}
			$value = 'image=:image, link=:link, title_eng=:title_eng, description_eng=:description_eng, title_rus=:title_rus, description_rus=:description_rus, title_tur=:title_tur, description_tur=:description_tur, title_spa=:title_spa, description_spa=:description_spa, title_fre=:title_fre, description_fre=:description_fre, title_man=:title_man, description_man=:description_man, title_can=:title_can, description_can=:description_can, title_ger=:title_ger, description_ger=:description_ger, title_per=:title_per, description_per=:description_per, title_kor=:title_kor, description_kor=:description_kor, title_mal=:title_mal, description_mal=:description_mal, title_ind=:title_ind, description_ind=:description_ind, title_ara=:title_ara, description_ara=:description_ara, title_jap=:title_jap, description_jap=:description_jap';
			$execute = array(
							':image'=>$v_pic6,
							':link'=>($_POST['link']),
							':title_eng'=>$_POST['title_eng'],
							':description_eng'=>$_POST['description_eng'],
							':title_rus'=>$_POST['title_rus'],
							':description_rus'=>$_POST['description_rus'],
							':title_tur'=>$_POST['title_tur'],
							':description_tur'=>$_POST['description_tur'],
							':description_spa'=>$_POST['description_spa'],
							':title_spa'=>$_POST['title_spa'],
							':description_fre'=>$_POST['description_fre'],
							':title_fre'=>$_POST['title_fre'],
							':description_man'=>$_POST['description_man'],
							':title_man'=>$_POST['title_man'],
							':description_can'=>$_POST['description_can'],
							':title_can'=>$_POST['title_can'],
							':description_ger'=>$_POST['description_ger'],
							':title_ger'=>$_POST['title_ger'],
							':description_per'=>$_POST['description_per'],
							':title_per'=>$_POST['title_per'],
							':description_kor'=>$_POST['description_kor'],
							':title_kor'=>$_POST['title_kor'],
							':description_mal'=>$_POST['description_mal'],
							':title_mal'=>$_POST['title_mal'],
							':description_ind'=>$_POST['description_ind'],
							':title_ind'=>$_POST['title_ind'],
							':description_ara'=>$_POST['description_ara'],
							':title_ara'=>$_POST['title_ara'],
							':description_jap'=>$_POST['description_jap'],
							':title_jap'=>$_POST['title_jap']
							);
				
			$update=update(STATIC_PAGE_HOME_SECOND_BLOCK_ONE, $value, 'WHERE id=1',$execute);
			if($update)
			{
				$_SESSION['SET_TYPE'] = 'success';
				$_SESSION['SET_FLASH'] = 'Static Home Page content successfully updated';
				header('location:admin/static_home_page.php');
				exit;
			}

}

$second_block_four_three = find('first', STATIC_PAGE_HOME_SECOND_BLOCK_THREE, '*', "WHERE id = 1", array());
if(isset($_POST['btn_submit7']))	
{
		if(isset($_FILES['image']['name']) && $_FILES['image']['name']!="")
		{
			$file1 = "../images/home/".$second_block_four_three['image'];
	
			$v_image1=$_FILES['image']['name'];
			$path1="../images/home/";
			$v_pic7=rand(0,200000)."_".$v_image1;
			if (file_exists($file1))
				{
					@unlink("../images/home/".$second_block_four_three["image"]);
				}
			move_uploaded_file($_FILES['image']['tmp_name'],$path1.$v_pic7);
		}
		else
		{
			$v_pic7=$second_block_four_three['image'];
		}
			$value = 'image=:image, link=:link, title_eng=:title_eng, description_eng=:description_eng, title_rus=:title_rus, description_rus=:description_rus, title_tur=:title_tur, description_tur=:description_tur, title_spa=:title_spa, description_spa=:description_spa, title_fre=:title_fre, description_fre=:description_fre, title_man=:title_man, description_man=:description_man, title_can=:title_can, description_can=:description_can, title_ger=:title_ger, description_ger=:description_ger, title_per=:title_per, description_per=:description_per, title_kor=:title_kor, description_kor=:description_kor, title_mal=:title_mal, description_mal=:description_mal, title_ind=:title_ind, description_ind=:description_ind, title_ara=:title_ara, description_ara=:description_ara, title_jap=:title_jap, description_jap=:description_jap';
			$execute = array(
							':image'=>$v_pic7,
							':link'=>($_POST['link']),
							':title_eng'=>$_POST['title_eng'],
							':description_eng'=>$_POST['description_eng'],
							':title_rus'=>$_POST['title_rus'],
							':description_rus'=>$_POST['description_rus'],
							':title_tur'=>$_POST['title_tur'],
							':description_tur'=>$_POST['description_tur'],
							':description_spa'=>$_POST['description_spa'],
							':title_spa'=>$_POST['title_spa'],
							':description_fre'=>$_POST['description_fre'],
							':title_fre'=>$_POST['title_fre'],
							':description_man'=>$_POST['description_man'],
							':title_man'=>$_POST['title_man'],
							':description_can'=>$_POST['description_can'],
							':title_can'=>$_POST['title_can'],
							':description_ger'=>$_POST['description_ger'],
							':title_ger'=>$_POST['title_ger'],
							':description_per'=>$_POST['description_per'],
							':title_per'=>$_POST['title_per'],
							':description_kor'=>$_POST['description_kor'],
							':title_kor'=>$_POST['title_kor'],
							':description_mal'=>$_POST['description_mal'],
							':title_mal'=>$_POST['title_mal'],
							':description_ind'=>$_POST['description_ind'],
							':title_ind'=>$_POST['title_ind'],
							':description_ara'=>$_POST['description_ara'],
							':title_ara'=>$_POST['title_ara'],
							':description_jap'=>$_POST['description_jap'],
							':title_jap'=>$_POST['title_jap']
							);
				
			$update=update(STATIC_PAGE_HOME_SECOND_BLOCK_THREE, $value, 'WHERE id=1',$execute);
			if($update)
			{	
				$_SESSION['SET_TYPE'] = 'success';
				$_SESSION['SET_FLASH'] = 'Static Home Page content successfully updated';
				header('location:admin/static_home_page.php');
				exit;
			}
}
$second_block_four_two = find('first', STATIC_PAGE_HOME_SECOND_BLOCK_TWO, '*', "WHERE id = 1", array());
if(isset($_POST['btn_submit8']))	
{
		if(isset($_FILES['image']['name']) && $_FILES['image']['name']!="")
		{
			$file1 = "../images/home/".$first_block_four_two['image'];
	
			$v_image1=$_FILES['image']['name'];
			$path1="../images/home/";
			$v_pic8=rand(0,200000)."_".$v_image1;
			if (file_exists($file1))
				{
					@unlink("../images/home/".$first_block_four_two["image"]);
				}
			move_uploaded_file($_FILES['image']['tmp_name'],$path1.$v_pic8);
		}
		else
		{
			$v_pic8=$first_block_four_two['image'];
		}
			$value = 'image=:image, link=:link, title_eng=:title_eng, description_eng=:description_eng, title_rus=:title_rus, description_rus=:description_rus, title_tur=:title_tur, description_tur=:description_tur, title_spa=:title_spa, description_spa=:description_spa, title_fre=:title_fre, description_fre=:description_fre, title_man=:title_man, description_man=:description_man, title_can=:title_can, description_can=:description_can, title_ger=:title_ger, description_ger=:description_ger, title_per=:title_per, description_per=:description_per, title_kor=:title_kor, description_kor=:description_kor, title_mal=:title_mal, description_mal=:description_mal, title_ind=:title_ind, description_ind=:description_ind, title_ara=:title_ara, description_ara=:description_ara, title_jap=:title_jap, description_jap=:description_jap';
			$execute = array(
							':image'=>$v_pic8,
							':link'=>($_POST['link']),
							':title_eng'=>$_POST['title_eng'],
							':description_eng'=>$_POST['description_eng'],
							':title_rus'=>$_POST['title_rus'],
							':description_rus'=>$_POST['description_rus'],
							':title_tur'=>$_POST['title_tur'],
							':description_tur'=>$_POST['description_tur'],
							':description_spa'=>$_POST['description_spa'],
							':title_spa'=>$_POST['title_spa'],
							':description_fre'=>$_POST['description_fre'],
							':title_fre'=>$_POST['title_fre'],
							':description_man'=>$_POST['description_man'],
							':title_man'=>$_POST['title_man'],
							':description_can'=>$_POST['description_can'],
							':title_can'=>$_POST['title_can'],
							':description_ger'=>$_POST['description_ger'],
							':title_ger'=>$_POST['title_ger'],
							':description_per'=>$_POST['description_per'],
							':title_per'=>$_POST['title_per'],
							':description_kor'=>$_POST['description_kor'],
							':title_kor'=>$_POST['title_kor'],
							':description_mal'=>$_POST['description_mal'],
							':title_mal'=>$_POST['title_mal'],
							':description_ind'=>$_POST['description_ind'],
							':title_ind'=>$_POST['title_ind'],
							':description_ara'=>$_POST['description_ara'],
							':title_ara'=>$_POST['title_ara'],
							':description_jap'=>$_POST['description_jap'],
							':title_jap'=>$_POST['title_jap']
							);
				
			$update=update(STATIC_PAGE_HOME_SECOND_BLOCK_TWO, $value, 'WHERE id=1',$execute);
			if($update)
			{				
				$_SESSION['SET_TYPE'] = 'success';
				$_SESSION['SET_FLASH'] = 'Static Home Page content successfully updated';
				header('location:admin/static_home_page.php');
				exit;
			}		
}
$translation = find('first', STATIC_PAGE_HOME_TRANSLATION_AGENCY, '*', "WHERE id = 1", array());
if(isset($_POST['btn_submit9']))	
{
			$value = 'translation_agency_eng=:translation_agency_eng, translation_agency_rus=:translation_agency_rus, translation_agency_tur=:translation_agency_tur, translation_agency_spa=:translation_agency_spa, translation_agency_fre=:translation_agency_fre, translation_agency_man=:translation_agency_man, translation_agency_can=:translation_agency_can, translation_agency_ger=:translation_agency_ger, translation_agency_per=:translation_agency_per, translation_agency_kor=:translation_agency_kor, translation_agency_mal=:translation_agency_mal, translation_agency_ind=:translation_agency_ind, translation_agency_ara=:translation_agency_ara, translation_agency_jap=:translation_agency_jap';
			$execute = array(
							':translation_agency_eng'=>$_POST['translation_agency_eng'],
							':translation_agency_rus'=>$_POST['translation_agency_rus'],
							':translation_agency_tur'=>$_POST['translation_agency_tur'],
							':translation_agency_spa'=>$_POST['translation_agency_spa'],
							':translation_agency_fre'=>$_POST['translation_agency_fre'],
							':translation_agency_man'=>$_POST['translation_agency_man'],
							':translation_agency_can'=>$_POST['translation_agency_can'],
							':translation_agency_ger'=>$_POST['translation_agency_ger'],
							':translation_agency_per'=>$_POST['translation_agency_per'],
							':translation_agency_kor'=>$_POST['translation_agency_kor'],
							':translation_agency_mal'=>$_POST['translation_agency_mal'],
							':translation_agency_ind'=>$_POST['translation_agency_ind'],
							':translation_agency_ara'=>$_POST['translation_agency_ara'],
							':translation_agency_jap'=>$_POST['translation_agency_jap']
							);
				
			update(STATIC_PAGE_HOME_TRANSLATION_AGENCY, $value, 'WHERE id=1',$execute);
			$_SESSION['SET_TYPE'] = 'success';
			$_SESSION['SET_FLASH'] = 'Static Home Page content successfully updated';
			header('location:admin/static_home_page.php');	
			exit;
		
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Settings | General Settings</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#edit-form").validate({
						rules: {
							email_address: {
								required: true,
								email: true
							},
							translation_title: "required",
							translation_description: "required",
							interpretation_title: "required",							
							interpretation_description: "required",
							proofreading_title: "required",
							proofreading_description: "required",
							certification_title: "required",
							certification_description: "required",
							training_title: "required",
							training_description: "required",
							transcreation_title: "required",
							transcreation_description: "required",
							equipment_title: "required",
							equipment_description: "required",
							setting_title: "required",
							setting_description: "required",
							translation_agency_eng: "required",
							translation_link: "required",
							interpretation_link: "required",
							proofreading_link: "required",
							certification_link: "required",
							training_link: "required",
							transcreation_link: "required",
							equipment_link: "required",
							setting_link: "required"
						},
						messages: {
							translation_title: "<font color = 'red'><b>This field is required</b></font>",
							translation_description: "<font color = 'red'><b>This field is required</b></font>",
							interpretation_title: "<font color = 'red'><b>This field is required</b></font>",
							interpretation_description: "<font color = 'red'><b>This field is required</b></font>",
							proofreading_title: "<font color = 'red'><b>This field is required</b></font>",
							proofreading_description: "<font color = 'red'><b>This field is required</b></font>",
							certification_title: "<font color = 'red'><b>This field is required</b></font>",
							certification_description: "<font color = 'red'><b>This field is required</b></font>",
							training_title: "<font color = 'red'><b>This field is required</b></font>",
							training_description: "<font color = 'red'><b>This field is required</b></font>",
							transcreation_title: "<font color = 'red'><b>This field is required</b></font>",
							transcreation_description: "<font color = 'red'><b>This field is required</b></font>",
							equipment_title: "<font color = 'red'><b>This field is required</b></font>",
							equipment_description: "<font color = 'red'><b>This field is required</b></font>",
							setting_description: "<font color = 'red'><b>This field is required</b></font>",
							translation_agency_eng: "<font color = 'red'><b>This field is required</b></font>",
							translation_link: "<font color = 'red'><b>This field is required</b></font>",
							interpretation_link: "<font color = 'red'><b>This field is required</b></font>",
							proofreading_link: "<font color = 'red'><b>This field is required</b></font>",
							certification_link: "<font color = 'red'><b>This field is required</b></font>",
							training_link: "<font color = 'red'><b>This field is required</b></font>",
							transcreation_link: "<font color = 'red'><b>This field is required</b></font>",
							equipment_link: "<font color = 'red'><b>This field is required</b></font>",
							setting_link: "<font color = 'red'><b>This field is required</b></font>",
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Manage Home Content</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Manage Homepage</li>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Manage Home Content</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
								 <div class="panel panel-grey">
									<div class="panel-heading">Manage Home Content</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-lg-12">
												<div class="panel-body pan">
													<div class="form-body pal">
														<div class="row">
														<div class="col-md-12">
															<h2 style="margin-top:0px">Manage first four block</h2>
														</div>
														<?php include_once('manage_first_block_one.php');?>
														<?php include_once('manage_first_block_two.php');?>
														<?php include_once('manage_first_block_three.php');?>
														<?php include_once('manage_first_block_four.php');?>
															<div class="col-md-12" style="margin-top:30px">
																<h2 style="margin-top:0px">Manage second four block</h2>
															</div>
														<?php include_once('manage_second_block_one.php');?>
														<?php include_once('manage_second_block_two.php');?>
														<?php include_once('manage_second_block_three.php');?>
														<?php include_once('manage_second_block_four.php');?>
														<?php include_once('translation_agency.php');?>
												</div>
											</div>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
    
    <script src="script/main.js"></script>
	<script>
	CKEDITOR.replace('translation_agency_eng',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
	
	CKEDITOR.replace('translation_agency_rus',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
	CKEDITOR.replace('translation_agency_tur',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
		CKEDITOR.replace('translation_agency_spa',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
		CKEDITOR.replace('translation_agency_fre',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
		CKEDITOR.replace('translation_agency_man',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
		CKEDITOR.replace('translation_agency_can',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
		CKEDITOR.replace('translation_agency_ger',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
		CKEDITOR.replace('translation_agency_per',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
		CKEDITOR.replace('translation_agency_kor',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
		CKEDITOR.replace('translation_agency_mal',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
		CKEDITOR.replace('translation_agency_ind',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
		CKEDITOR.replace('translation_agency_ara',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
		CKEDITOR.replace('translation_agency_jap',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '350px'
	});
		
	
</script>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>
	<script>
     	function ValidateFileUpload() {
			var fuData = document.getElementById('image');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('image').value = '';
            }
    }

		function ValidateFileUpload2() {
			var fuData = document.getElementById('image2');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah2').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('image2').value = '';
            }
    }
		function ValidateFileUpload3() {
			var fuData = document.getElementById('image3');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah3').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('image4').value = '';
            }
    }
		function ValidateFileUpload4() {
			var fuData = document.getElementById('image4');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah4').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('image4').value = '';
            }
    }
		function ValidateFileUpload5() {
			var fuData = document.getElementById('image5');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah5').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('image5').value = '';
            }
    }
		function ValidateFileUpload6() {
			var fuData = document.getElementById('image6');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah6').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('image6').value = '';
            }
    }
		function ValidateFileUpload7() {
			var fuData = document.getElementById('image7');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah7').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('image7').value = '';
            }
    }
		function ValidateFileUpload8() {
			var fuData = document.getElementById('image8');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah8').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('image8').value = '';
            }
    }
</script>
