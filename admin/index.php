<?php
include_once('../init.php');
if(isset($_POST['btn_login']))
{
	if((isset($_POST['email_address']) && $_POST['email_address']!='') && (isset($_POST['password']) && $_POST['password']!=''))
	{
		if($check_exists = find('first', MASTER_ADMIN, 'id, email_address, role, full_name', "WHERE email_address = :email_address and password = :password", array(':email_address' => stripcleantohtml($_POST['email_address']),':password' => stripcleantohtml(md5($_POST['password'])))))
		{
			if(isset($_POST['remember_me']) && $_POST['remember_me'] == '1')
			{
				if(@$_COOKIE['xlt_email_address'] == '' && @$_COOKIE['xlt_password'] == '')
				{
					$year = time() + 31536000;
					setcookie('xlt_email_address', $_POST['email_address'], $year);
					setcookie('xlt_password', $_POST['password'], $year);
				}
			}
			else
			{
				if(isset($_COOKIE['xlt_email_address'])){
					unset($_COOKIE['xlt_email_address']);
				}
				@setcookie('xlt_email_address', null, -1, '/');
				
				if(isset($_COOKIE['xlt_password'])){
					unset($_COOKIE['xlt_password']);
				}
				@setcookie('xlt_password', null, -1, '/');
			}
			$_SESSION['role'] = $check_exists['role'];
			$_SESSION['admin_id'] = $check_exists['id'];
			$_SESSION['email_address'] = $check_exists['email_address'];
			$_SESSION['full_name'] = $check_exists['full_name'];
			
			header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/dashboard.php');
			exit;
		}
		else
		{
			$_SESSION['SET_TYPE'] = 'error';
			$_SESSION['SET_FLASH'] = 'Invalid Email Address or Password';
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Maven | Login</title>
    <?php include_once('includes/scripts.php') ?>
	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#login_form").validate({
						rules: {
							email_address: {
								required: true,
								email: true
							},
							password: "required"
						},
						messages: {
							email_address: "<font color = 'red'><b>Please enter vaild email address</b></font>",
							password: "<font color = 'red'><b>Please enter your password</b></font>"
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
</head>
<body style="background-image: url(<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/green-walls-bike.jpg); background-position: center top; background-size: cover;">
	<div style="clear:both;"></div>
	<div class="col-md-12 text-center">
		<h1 style="margin-top: 20px; font-size: 24px;">
			<img src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/logo.png" border="0"alt="" style="width:150px" align="absmiddle" />
		</h1>
	</div>

	<div style="clear:both;"></div>

	<div align="center">
		<h4 style=" margin-bottom:25px"><strong style="color: #2e6492; font-size:20px;">Login to Maven Admin Panel</strong></h4>
		<div class="clearfix"></div>
		<!-- <div style = "margin-bottom:10px;">
			<img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/lock.png" border = "0" alt = "" width="90"/>
		</div> -->
		<div class="page-form">
			<div class="panel ">
				
				<div class="col-lg-12">
					<div id="notify_msg_div"></div>
				</div>

				<div class="panel-body pan">
					<form  name = "login_form" id = "login_form" method = "POST" class="form-horizontal">
						<div class="form-body pal">
							<div class="form-group">
								<div class="col-md-12">
									<div class="input-icon right">
										<i class="fa fa-user"></i>
										<input name = "email_address" id = "email_address" type="text" placeholder="Email Address" tabindex = "1" class="form-control" value = "<?php echo((isset($_POST['email_address']) && $_POST['email_address']!='') ? $_POST['email_address'] : @$_COOKIE['xlt_email_address']);?>"/>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<div class="input-icon right">
										<i class="fa fa-lock"></i>
										<input name = "password" id = "password" type="password" placeholder="Password" tabindex = "2" class="form-control" value = "<?php echo(@$_COOKIE['xlt_password']);?>"/>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12" align="left">
									<div align="left">
										<button type="submit" name = "btn_login" id = "btn_login" class="btn btn-primary" style="width:100% !important">Login</button></a>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12" align="left">
									<div align="left"><input type="checkbox" name = "remember_me" id = "remember_me" value = "1" tabindex="5" <?php echo(@$_COOKIE['xlt_email_address']!='' ? 'checked' : '');?>/>&nbsp; Remember My Details</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12" align="left">
									<div align="center"> <a href="forgot_password.php" class = "forgot">Forgot Your Password ?</a></div>
								</div>
							</div>							
						</div>
					</form>
				</div>
			</div>				   
		</div>
	</div>

	<!--BEGIN FOOTER-->
	<?php include_once('includes/admin_footer.php');?>
	<!--END FOOTER-->

 </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>
