<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
$page_serial = 11;
include_once('includes/permission.php');
if(!isset($_GET['id']) || (isset($_GET['id']) && $_GET['id'] ==''))
{
	header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_service.php');
}
if(isset($_GET['mode']) && $_GET['mode'] == 'delete')
{
	$_SESSION['SET_TYPE'] = 'success';
	$_SESSION['SET_FLASH'] = 'Document has been deleted successfully';
}

//print_r($service_name);exit;
update(MANAGE_QUOTES, 'status=:status', 'WHERE id='.base64_decode($_GET['id']).'', array(':status'=>'Y'));

if(isset($_POST['btn_send']))
{
	$enquiry1 = find('first', MANAGE_QUOTES, '*', "WHERE id = ".base64_decode($_GET['id'])."", array());
	$message = $enquiry1['message'].':-:'.$_POST['msg'];
	$date = $enquiry1['date'].':-:'.date('d M Y').', '.date('h:i A');
	$subject = ($enquiry1['subject']!='') ? $enquiry1['subject'].':-:'.$_POST['subject'] : $_POST['subject'];
	update(MANAGE_QUOTES, 'message=:message, date=:date, subject=:subject', 'WHERE id='.base64_decode($_GET['id']).'', array(':message'=>stripcleantohtml($message), ':date'=>stripcleantohtml($date), ':subject'=>stripcleantohtml($subject)));
	$mail_Body = "Dear ".$enquiry1['full_name'].",<br/><br/>Here is the reply of your enquiry.<br/><br/>".$_POST['msg']."<br/><br/><br/>Thank you.<br/>Best Regards,<br/>Maven CMS Admin.<br/>Powered by Maven";
	@Send_HTML_Mail($enquiry1['email_address'], 'service@maven-international.com', '', $_POST['subject'], $mail_Body);

	$_SESSION['SET_TYPE'] = 'success';
	$_SESSION['SET_FLASH'] = 'Reply has been sent successfully.';
}

$enquiry = find('first', MANAGE_QUOTES, '*', "WHERE id = ".base64_decode($_GET['id'])."", array());
//print_r($enquiry);
$translate_lang = explode(':-:', $enquiry['translate_language']);
$translate_language = implode(', ', $translate_lang);
$service_name = find("first", MANAGE_SERVICE, '*', "WHERE id = ".$enquiry['service_id']."", array());
if(isset($_GET['del_id']))
{
	$image_path = find("first", MANAGE_QUOTE_DOCUMENT, "image_name", "WHERE id = ".$_GET['del_id']."", array());
	//print_r($image_path);exit;
	if($image_path['image_name']!='' && file_exists('../images/quotes/'.$image_path['image_name']))
	{
		unlink('../images/quotes/'.$image_path['image_name']);
	}
	delete(MANAGE_QUOTE_DOCUMENT, 'WHERE id=:id', array(':id'=>$_GET['del_id']));
	header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/manage_quote.php?id='.$_GET['id'].'&mode=delete');
}
//print_r($document_array= explode(':-:', $enquiry['add_document']));

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Manage Quotes</title>
    <?php include_once('includes/scripts.php')?>
	<script type="text/javascript">
	<!--
		function delete_record(id,delid)
		{
			if(confirm('Are you sure you wish to delete this record?'))
			{
				window.location.href = '<?php echo(DOMAIN_NAME_PATH_ADMIN)?>admin/manage_quote.php?id='+id+'&del_id='+delid;
			}
		}
	//-->
	</script>
	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#edit-form").validate({
						rules: {
							subject: "required",
							msg: "required"
						},
						messages: {
							subject: "<font color = 'red'><b>Please enter your subject</b></font>",
							msg: "<font color = 'red'><b>Please enter your message</b></font>"
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Manage Quotes</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
						<li>&nbsp;<a href="javascript:void(0)">Manage Quotes</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Reply</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
								 <div class="panel panel-grey">
									<div class="panel-heading">Quotes for "<?=$service_name['service_title_eng']?>" </div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-6">
												<form id="edit-form" name="edit-form" action="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/manage_quote.php?id=<?php echo($_GET['id']);?>" method = "POST">
												<div class="panel-body pan">
													
													<div class="form-group">
														<label for="inputSubject" class="control-label">
															Subject
														</label>
														<div class="input-icon right">
															<input name = "subject" id = "subject" type="text" placeholder="" class="form-control" />
														</div>
													</div>
													<div class="form-group">
														<label for="inputMessage" class="control-label">
															Reply
														</label>
														<textarea name = "msg" id = "msg" rows="5" class="form-control"></textarea>
													</div>
													<div class="form-actions text-right pal">
														<button type="submit" class="btn btn-primary" name = "btn_send" id = "btn_send">Send</button>
													</div>
												</div>
												</form>
											</div>
											<div class="col-md-6" style = "float:right;">
												<p><u><strong>Quote Details:</strong></u></p>
												<p><b>Full Name:</b> <?php echo($enquiry['full_name']);?></p>
												<p><b>Email Address:</b> <?php echo($enquiry['email_address']);?></p>
												<p><b>Mobile Number:</b> <?php echo($enquiry['mobile_number']);?></p>												
												<p><b>Source Language:</b> <?php echo($enquiry['source_language']);?></p>												
												<p><b>Translate - Language:</b> <?php echo($translate_language);?></p>												
												<p>
													<?php
													$temp2 = explode(':-:', $enquiry['date']);
													$tmessage2 = $temp2[0];
													?>
													<b>Date of Quote and time:</b> <?php echo($tmessage2);?>
												</p>
												<p>
													<?php
													$temp = explode(':-:', $enquiry['message']);
													$tmessage = $temp[0];
													?>
													<b>Message:</b><br/><?php echo($tmessage);?>
												</p>
												<?php
												$quote_image = find("all", MANAGE_QUOTE_DOCUMENT, '*', "WHERE quote_id = ".$enquiry['id']."", array());
												if(!empty($quote_image))
												{
												?>
												<h3><strong>Document List</strong></h3>
												<div class="row">
													<?php
														foreach($quote_image as $key => $quote)
													{
													?>
													<div class="col-md-3 text-center">
														<div style="border:1px solid #ccc; margin-bottom:10px">
															<a href="../images/quotes/<?php echo($quote['image_name']);?> " download>
																<img src="../images/quotes/folder_256.png" width="70" height="aoto" border="0" alt=""><br>
																Download Document
															</a>
															<a style="width:100%; display:block; background:#f2f2f2; padding:5px 0px; border-top:1px solid #ccc; color:#000" href = "javascript:void(0);" onclick = "delete_record('<?=$_GET['id']?>','<?php echo($quote['id']);?>');" title = "Delete File" data-toggle="tooltip">Delete&nbsp;<img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/delete.png" border = "0" alt = "Delete File" /></a>
														</div>
													</div>
													<?php
														
														}
													}
													?>
												</div>												
											</div>
										</div>
										<div class="">
											<div class="col-md-12">												
												<div class="form-group">
													<label for="inputMessage" class="control-label">
														<br/>
														<b>Previous Message(s):</b>
														<br/><br/>
														<font style = "font-weight:normal;">
														<?php
														if($enquiry['message']!='')
														{
															$message_array = explode(':-:', $enquiry['message']);
															$date_array = explode(':-:', $enquiry['date']);
															$subject_array = explode(':-:', $enquiry['subject']);
															$sl_no = 1;
															if(count($message_array) > 1)
															{
															for($i=1; $i<count($message_array); $i++)
															{
															?>
															
															<p style="background:#a3a3a3 !important; height:1px;"></p>
															<p style="font-weight:bold; color:#444444;">#<?php echo($sl_no);?>. Date of response by superadmin: <?php echo($date_array[$i]);?></p>
															<p><strong>Subject: </strong><strong style="color:#535353;"><?php echo($subject_array[$i-1]);?></strong></p>
															<p><?php echo(nl2br($message_array[$i]));?></p>
															<?php
																$sl_no++;
															}
															}
															else
															{
															?>
															<p><font color = "red">No previous message(s) available.</font></p>
															<?php
															}
														}
														?>
														</font>
													</label>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>