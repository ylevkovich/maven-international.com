<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);

$flag_status = true;
if(isset($_POST['btn_submit']))
{
	$field='company_name_eng, company_name_rus, company_name_tur, company_name_spa, company_name_fre, company_name_man, company_name_can, company_name_ger, company_name_per, company_name_kor, company_name_mal, company_name_ind, company_name_ara, company_name_jap, address_eng, address_rus, address_tur, address_spa, address_fre, address_man, address_can, address_ger, address_per, address_kor, address_mal, address_ind, address_ara, address_jap, phone_number';
	$value=':company_name_eng, :company_name_rus, :company_name_tur, :company_name_spa, :company_name_fre, :company_name_man, :company_name_can, :company_name_ger, :company_name_per, :company_name_kor, :company_name_mal, :company_name_ind, :company_name_ara, :company_name_jap, :address_eng, :address_rus, :address_tur, :address_spa, :address_fre, :address_man, :address_can, :address_ger, :address_per, :address_kor, :address_mal, :address_ind, :address_ara, :address_jap, :phone_number';
	$execute=array(':company_name_eng'=>stripcleantohtml($_POST['company_name_eng']),
					':company_name_rus'=>stripcleantohtml($_POST['company_name_rus']),
					':company_name_tur'=>stripcleantohtml($_POST['company_name_tur']),
					':company_name_spa'=>stripcleantohtml($_POST['company_name_spa']),
					':company_name_fre'=>stripcleantohtml($_POST['company_name_fre']),
					':company_name_man'=>stripcleantohtml($_POST['company_name_man']),
					':company_name_can'=>stripcleantohtml($_POST['company_name_can']),
					':company_name_ger'=>stripcleantohtml($_POST['company_name_ger']),
					':company_name_per'=>stripcleantohtml($_POST['company_name_per']),
					':company_name_kor'=>stripcleantohtml($_POST['company_name_kor']),
					':company_name_mal'=>stripcleantohtml($_POST['company_name_mal']),
					':company_name_ind'=>stripcleantohtml($_POST['company_name_ind']),
					':company_name_ara'=>stripcleantohtml($_POST['company_name_ara']),
					':company_name_jap'=>stripcleantohtml($_POST['company_name_jap']),
					':address_eng'=>($_POST['address_eng']),
					':address_rus'=>($_POST['address_rus']),
					':address_tur'=>($_POST['address_tur']),
					':address_spa'=>($_POST['address_spa']),
					':address_fre'=>($_POST['address_fre']),
					':address_man'=>($_POST['address_man']),
					':address_can'=>($_POST['address_can']),
					':address_ger'=>($_POST['address_ger']),
					':address_per'=>($_POST['address_per']),
					':address_kor'=>($_POST['address_kor']),
					':address_mal'=>($_POST['address_mal']),
					':address_ind'=>($_POST['address_ind']),
					':address_ara'=>($_POST['address_ara']),
					':address_jap'=>($_POST['address_jap']),
					':phone_number'=>stripcleantohtml($_POST['phone_number'])
					);
	$photo_id = save(MANAGE_ADDRESS, $field, $value, $execute);
	if($photo_id)
	{
		header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_address.php?mode=add');
	}
	
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Manage Address</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#add-form").validate({
						rules: {
							company_name_eng: "required",
							company_name_rus: "required",
							company_name_tur: "required",
							company_name_spa: "required",
							company_name_fre: "required",
							company_name_man: "required",
							company_name_can: "required",
							company_name_ger: "required",
							company_name_per: "required",
							company_name_kor: "required",
							company_name_mal: "required",
							company_name_ind: "required",
							company_name_ara: "required",
							company_name_jap: "required",
							address_eng: "required",
							address_rus: "required",
							address_tur: "required",
							address_spa: "required",
							address_fre: "required",
							address_man: "required",
							address_can: "required",
							address_ger: "required",
							address_per: "required",
							address_kor: "required",
							address_mal: "required",
							address_ind: "required",
							address_ara: "required",
							address_jap: "required",
							phone_number: "required"
						},
						messages: {
							company_name_eng: "<font color = 'red'><b>Please enter company name</b></font>",
							company_name_rus: "<font color = 'red'><b>Please enter company name</b></font>",
							company_name_tur: "<font color = 'red'><b>Please enter company name</b></font>",
							company_name_spa: "<font color = 'red'><b>Please enter company name</b></font>",
							company_name_fre: "<font color = 'red'><b>Please enter company name</b></font>",
							company_name_man: "<font color = 'red'><b>Please enter company name</b></font>",
							company_name_can: "<font color = 'red'><b>Please enter company name</b></font>",
							company_name_ger: "<font color = 'red'><b>Please enter company name</b></font>",
							company_name_per: "<font color = 'red'><b>Please enter company name</b></font>",
							company_name_kor: "<font color = 'red'><b>Please enter company name</b></font>",
							company_name_mal: "<font color = 'red'><b>Please enter company name</b></font>",
							company_name_ind: "<font color = 'red'><b>Please enter company name</b></font>",
							company_name_ara: "<font color = 'red'><b>Please enter company name</b></font>",
							company_name_jap: "<font color = 'red'><b>Please enter company name</b></font>",
							address_eng: "<font color = 'red'><b>Please enter address</b></font>",
							address_rus: "<font color = 'red'><b>Please enter address</b></font>",
							address_tur: "<font color = 'red'><b>Please enter address</b></font>",
							address_spa: "<font color = 'red'><b>Please enter address</b></font>",
							address_fre: "<font color = 'red'><b>Please enter address</b></font>",
							address_man: "<font color = 'red'><b>Please enter address</b></font>",
							address_can: "<font color = 'red'><b>Please enter address</b></font>",
							address_ger: "<font color = 'red'><b>Please enter address</b></font>",
							address_per: "<font color = 'red'><b>Please enter address</b></font>",
							address_kor: "<font color = 'red'><b>Please enter address</b></font>",
							address_mal: "<font color = 'red'><b>Please enter address</b></font>",
							address_ind: "<font color = 'red'><b>Please enter address</b></font>",
							address_ara: "<font color = 'red'><b>Please enter address</b></font>",
							address_jap: "<font color = 'red'><b>Please enter address</b></font>",
							phone_number: "<font color = 'red'><b>Please enter phone number</b></font>",
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
<style>.pad_mar_bod {background-color: #eee;padding: 5px 15px;margin: 0px 0px 10px 0px;} .bod_right {border-right: 5px solid white;} .bod_left {border-left: 5px solid white;}</style>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Manage Addresses</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active"><a href="list_address.php">Manage Addresses</a></li>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Add New Address</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
										<div class="panel panel-grey">
										<div class="panel-heading">Add New Address</div>
											<div class="panel-body">
												<div class="panel-body pan">
													<form name = "add-form" id = "add-form" action = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/add_address.php" method = "POST" enctype = "multipart/form-data">
														<div class="row">
															<div class="col-md-6 pad_mar_bod bod_right">											
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (Eng)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_eng" id = "company_name_eng" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_eng']) && $_POST['company_name_eng']!='' ? $_POST['company_name_eng'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (Eng)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "address_eng" id = "address_eng" class="form-control"><?php echo isset($_POST['address_eng']) && $_POST['address_eng']!='' ? $_POST['address_eng'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_left">
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (Russian)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_rus" id = "company_name_rus" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_rus']) && $_POST['company_name_rus']!='' ? $_POST['company_name_rus'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (Russian)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "address_rus" id = "address_rus" class="form-control"><?php echo isset($_POST['address_rus']) && $_POST['address_rus']!='' ? $_POST['address_rus'] :'' ?></textarea>
																	</div>
																</div>
															</div>

															<div class="col-md-6 pad_mar_bod bod_right">
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (Turkish)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_tur" id = "company_name_tur" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_tur']) && $_POST['company_name_tur']!='' ? $_POST['company_name_tur'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (Turkish)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "address_tur" id = "address_tur" class="form-control"><?php echo isset($_POST['address_tur']) && $_POST['address_tur']!='' ? $_POST['address_tur'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_left">
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (Spanish)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_spa" id = "company_name_spa" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_spa']) && $_POST['company_name_spa']!='' ? $_POST['company_name_spa'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (Spanish)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "address_spa" id = "address_spa" class="form-control"><?php echo isset($_POST['address_spa']) && $_POST['address_spa']!='' ? $_POST['address_spa'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (French)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_fre" id = "company_name_fre" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_fre']) && $_POST['company_name_fre']!='' ? $_POST['company_name_fre'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (French)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "address_fre" id = "address_fre" class="form-control"><?php echo isset($_POST['address_fre']) && $_POST['address_fre']!='' ? $_POST['address_fre'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_left">											
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (Mandarin)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_man" id = "company_name_man" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_man']) && $_POST['company_name_man']!='' ? $_POST['company_name_man'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (Mandarin)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "address_man" id = "address_man" class="form-control"><?php echo isset($_POST['address_man']) && $_POST['address_man']!='' ? $_POST['address_man'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (Cantonese)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_can" id = "company_name_can" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_can']) && $_POST['company_name_can']!='' ? $_POST['company_name_can'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (Cantonese)
																	</label> 
																	<div class="input-icon right">
																		<textarea name = "address_can" id = "address_can" class="form-control"><?php echo isset($_POST['address_can']) && $_POST['address_can']!='' ? $_POST['address_can'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_left">											
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (German)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_ger" id = "company_name_ger" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_ger']) && $_POST['company_name_ger']!='' ? $_POST['company_name_ger'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (German)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "address_ger" id = "address_ger" class="form-control"><?php echo isset($_POST['address_ger']) && $_POST['address_ger']!='' ? $_POST['address_ger'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (Persian)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_per" id = "company_name_per" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_per']) && $_POST['company_name_per']!='' ? $_POST['company_name_per'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (Persian)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "address_per" id = "address_per" class="form-control"><?php echo isset($_POST['address_per']) && $_POST['address_per']!='' ? $_POST['address_per'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_left">											
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (Korean)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_kor" id = "company_name_kor" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_kor']) && $_POST['company_name_kor']!='' ? $_POST['company_name_kor'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (Korean)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "address_kor" id = "address_kor" class="form-control"><?php echo isset($_POST['address_kor']) && $_POST['address_kor']!='' ? $_POST['address_kor'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (Malay)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_mal" id = "company_name_mal" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_mal']) && $_POST['company_name_mal']!='' ? $_POST['company_name_mal'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (Malay)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "address_mal" id = "address_mal" class="form-control"><?php echo isset($_POST['address_mal']) && $_POST['address_mal']!='' ? $_POST['address_mal'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_left">											
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (Indonesian)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_ind" id = "company_name_ind" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_ind']) && $_POST['company_name_ind']!='' ? $_POST['company_name_ind'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (Indonesian)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "address_ind" id = "address_ind" class="form-control"><?php echo isset($_POST['address_ind']) && $_POST['address_ind']!='' ? $_POST['address_ind'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (Arabic)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_ara" id = "company_name_ara" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_ara']) && $_POST['company_name_ara']!='' ? $_POST['company_name_ara'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (Arabic)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "address_ara" id = "address_ara" class="form-control"><?php echo isset($_POST['address_ara']) && $_POST['address_ara']!='' ? $_POST['address_ara'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Company Name (Japanese)
																	</label>
																	<div class="input-icon right">
																		<input name = "company_name_jap" id = "company_name_jap" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['company_name_jap']) && $_POST['company_name_jap']!='' ? $_POST['company_name_jap'] :'' ?>"/>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Address (Japanese)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "address_jap" id = "address_jap" class="form-control"><?php echo isset($_POST['address_jap']) && $_POST['address_jap']!='' ? $_POST['address_jap'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">
																<div class="form-group">
																<label for="inputName" class="control-label">
																	Phone Number
																</label>
																<div class="input-icon right">
																	<input name = "phone_number" id = "phone_number" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['phone_number']) && $_POST['phone_number']!='' ? $_POST['phone_number'] :'' ?>"/>
																</div>
															</div>
															</div>
															</div>													
														<div class="form-actions text-right pal">
															<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Create</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>