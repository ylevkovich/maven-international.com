<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');

$flag_status = true;
if(isset($_POST['btn_submit']))
{
	$field='question_eng, question_rus, question_tur, question_spa, question_fre, question_man, question_can, question_ger, question_per, question_kor, question_mal, question_ind, question_ara, question_jap, answer_eng, answer_rus, answer_tur, answer_spa, answer_fre, answer_man, answer_can, answer_ger, answer_per, answer_kor, answer_mal, answer_ind, answer_ara, answer_jap';
	$value=':question_eng, :question_rus, :question_tur, :question_spa, :question_fre, :question_man, :question_can, :question_ger, :question_per, :question_kor, :question_mal, :question_ind, :question_ara, :question_jap, :answer_eng, :answer_rus, :answer_tur, :answer_spa, :answer_fre, :answer_man, :answer_can, :answer_ger, :answer_per, :answer_kor, :answer_mal, :answer_ind, :answer_ara, :answer_jap';
	$execute=array( 
					':question_eng'=>$_POST['question_eng'],
					':question_rus'=>$_POST['question_rus'],
					':question_tur'=>$_POST['question_tur'],
					':question_spa'=>$_POST['question_spa'],
					':question_fre'=>$_POST['question_fre'],
					':question_man'=>$_POST['question_man'],
					':question_can'=>$_POST['question_can'],
					':question_ger'=>$_POST['question_ger'],
					':question_per'=>$_POST['question_per'],
					':question_kor'=>$_POST['question_kor'],
					':question_mal'=>$_POST['question_mal'],
					':question_ind'=>$_POST['question_ind'],
					':question_ara'=>$_POST['question_ara'],
					':question_jap'=>$_POST['question_jap'],
					':answer_eng'=>$_POST['answer_eng'],
					':answer_rus'=>$_POST['answer_rus'],
					':answer_tur'=>$_POST['answer_tur'],
					':answer_spa'=>$_POST['answer_spa'],
					':answer_fre'=>$_POST['answer_fre'],
					':answer_man'=>$_POST['answer_man'],
					':answer_can'=>$_POST['answer_can'],
					':answer_ger'=>$_POST['answer_ger'],
					':answer_per'=>$_POST['answer_per'],
					':answer_kor'=>$_POST['answer_kor'],
					':answer_mal'=>$_POST['answer_mal'],
					':answer_ind'=>$_POST['answer_ind'],
					':answer_ara'=>$_POST['answer_ara'],
					':answer_jap'=>$_POST['answer_jap']
					);
	$faq = save(MANAGE_FAQ, $field, $value, $execute);
	if($faq)
	{
		header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_faq.php?mode=add');
	}
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Testimonials</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#add-form").validate({
						rules: {
							question_eng: "required",
							question_rus: "required", 
							question_tur: "required", 
							question_spa: "required", 
							question_fre: "required", 
							question_man: "required", 
							question_can: "required", 
							question_ger: "required", 
							question_per: "required", 
							question_kor: "required", 
							question_mal: "required", 
							question_ind: "required", 
							question_ara: "required", 
							question_jap: "required", 
							answer_eng: "required", 
							answer_rus: "required", 
							answer_tur: "required", 
							answer_spa: "required", 
							answer_fre: "required", 
							answer_man: "required", 
							answer_can: "required", 
							answer_ger: "required", 
							answer_per: "required", 
							answer_kor: "required", 
							answer_mal: "required", 
							answer_ind: "required", 
							answer_ara: "required", 
							answer_jap: "required",

						},
						messages: {
							question_eng: "<font color = 'red'><b>Please enteer question (English)</b></font>",
							question_rus: "<font color = 'red'><b>Please enteer question (Russian)</b></font>", 
							question_tur: "<font color = 'red'><b>Please enteer question (Turkish)</b></font>", 
							question_spa: "<font color = 'red'><b>Please enteer question (Spanish)</b></font>", 
							question_fre: "<font color = 'red'><b>Please enteer question (French)</b></font>", 
							question_man: "<font color = 'red'><b>Please enteer question (Mandarin)</b></font>", 
							question_can: "<font color = 'red'><b>Please enteer question (Cantonese)</b></font>", 
							question_ger: "<font color = 'red'><b>Please enteer question (German)</b></font>", 
							question_per: "<font color = 'red'><b>Please enteer question (Persian)</b></font>", 
							question_kor: "<font color = 'red'><b>Please enteer question (Korean)</b></font>", 
							question_mal: "<font color = 'red'><b>Please enteer question (Malay)</b></font>", 
							question_ind: "<font color = 'red'><b>Please enteer question (Indonesian)</b></font>", 
							question_ara: "<font color = 'red'><b>Please enteer question (Arabic)</b></font>", 
							question_jap: "<font color = 'red'><b>Please enteer question (Japanese)</b></font>",
							answer_eng: "<font color = 'red'><b>Please enter answer (English)</b></font>",
							answer_rus: "<font color = 'red'><b>Please enter answer (Russian)</b></font>", 
							answer_tur: "<font color = 'red'><b>Please enter answer (Turkish)</b></font>", 
							answer_spa: "<font color = 'red'><b>Please enter answer (Spanish)</b></font>", 
							answer_fre: "<font color = 'red'><b>Please enter answer (French)</b></font>", 
							answer_man: "<font color = 'red'><b>Please enter answer (Mandarin)</b></font>", 
							answer_can: "<font color = 'red'><b>Please enter answer (Cantonese)</b></font>", 
							answer_ger: "<font color = 'red'><b>Please enter answer (German)</b></font>", 
							answer_per: "<font color = 'red'><b>Please enter answer (Persian)</b></font>", 
							answer_kor: "<font color = 'red'><b>Please enter answer (Korean)</b></font>", 
							answer_mal: "<font color = 'red'><b>Please enter answer (Malay)</b></font>", 
							answer_ind: "<font color = 'red'><b>Please enter answer (Indonesian)</b></font>", 
							answer_ara: "<font color = 'red'><b>Please enter answer (Arabic)</b></font>", 
							answer_jap: "<font color = 'red'><b>Please enter answer (Japanese)</b></font>",
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
	<style>.pad_mar_bod {background-color: #eee;padding: 5px 15px;margin: 0px 0px 10px 0px;} .bod_right {border-right: 5px solid white;} .bod_left {border-left: 5px solid white;}</style>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">FAQ</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="admin/dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active"><a href="admin/list_faq.php">FAQ</a></li>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Add New FAQ</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
										<div class="panel panel-grey">
										<div class="panel-heading">Add New FAQ</div>
											<div class="panel-body">
												<div class="panel-body pan">
													<form name = "add-form" id = "add-form" action = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/add_faq.php" method = "POST" enctype = "multipart/form-data">
														<div class="row"> 
															<div class="col-md-6 pad_mar_bod bod_right">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question (English)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_eng" id = "question_eng" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_eng']) && $_POST['question_eng']!='' ? $_POST['question_eng'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer (English)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_eng" id = "answer_eng" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_eng']) && $_POST['answer_eng']!='' ? $_POST['answer_eng'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_left">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question (Russian)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_rus" id = "question_rus" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_rus']) && $_POST['question_rus']!='' ? $_POST['question_rus'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer (Russian)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_rus" id = "answer_rus" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_rus']) && $_POST['answer_rus']!='' ? $_POST['answer_rus'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question (Turkish)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_tur" id = "question_tur" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_tur']) && $_POST['question_tur']!='' ? $_POST['question_tur'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer (Turkish)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_tur" id = "answer_tur" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_tur']) && $_POST['answer_tur']!='' ? $_POST['answer_tur'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_left">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question (Spanish)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_spa" id = "question_spa" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_spa']) && $_POST['question_spa']!='' ? $_POST['question_spa'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer (Spanish)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_spa" id = "answer_spa" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_spa']) && $_POST['answer_spa']!='' ? $_POST['answer_spa'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question (French)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_fre" id = "question_fre" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_fre']) && $_POST['question_fre']!='' ? $_POST['question_fre'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer (French)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_fre" id = "answer_fre" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_fre']) && $_POST['answer_fre']!='' ? $_POST['answer_fre'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_left">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question (Mandarin)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_man" id = "question_man" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_man']) && $_POST['question_man']!='' ? $_POST['question_man'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer (Mandarin)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_man" id = "answer_man" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_man']) && $_POST['answer_man']!='' ? $_POST['answer_man'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question ( Cantonese)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_can" id = "question_can" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_can']) && $_POST['question_can']!='' ? $_POST['question_can'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer ( Cantonese)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_can" id = "answer_can" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_can']) && $_POST['answer_can']!='' ? $_POST['answer_can'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_left">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question (German)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_ger" id = "question_ger" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_ger']) && $_POST['question_ger']!='' ? $_POST['question_ger'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer (German)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_ger" id = "answer_ger" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_ger']) && $_POST['answer_ger']!='' ? $_POST['answer_ger'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question (Persian)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_per" id = "question_per" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_per']) && $_POST['question_per']!='' ? $_POST['question_per'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer (Persian)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_per" id = "answer_per" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_per']) && $_POST['answer_per']!='' ? $_POST['answer_per'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_left">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question (Korean)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_kor" id = "question_kor" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_kor']) && $_POST['question_kor']!='' ? $_POST['question_kor'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer (Korean)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_kor" id = "answer_kor" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_kor']) && $_POST['answer_kor']!='' ? $_POST['answer_kor'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question (Malay)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_mal" id = "question_mal" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_mal']) && $_POST['question_mal']!='' ? $_POST['question_mal'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer (Malay)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_mal" id = "answer_mal" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_mal']) && $_POST['answer_mal']!='' ? $_POST['answer_mal'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_left">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question (Indonesian)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_ind" id = "question_ind" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_ind']) && $_POST['question_ind']!='' ? $_POST['question_ind'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer (Indonesian)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_ind" id = "answer_ind" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_ind']) && $_POST['answer_ind']!='' ? $_POST['answer_ind'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_right">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question (Arabic)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_ara" id = "question_ara" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_ara']) && $_POST['question_ara']!='' ? $_POST['question_ara'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer (Arabic)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_ara" id = "answer_ara" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_ara']) && $_POST['answer_ara']!='' ? $_POST['answer_ara'] :'' ?></textarea>
																	</div>
																</div>
															</div>
															<div class="col-md-6 pad_mar_bod bod_left">	
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Question (Japanese)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "question_jap" id = "question_jap" class="form-control" style="resize:none;height: 70px;"><?php echo isset($_POST['question_jap']) && $_POST['question_jap']!='' ? $_POST['question_jap'] :'' ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Answer (Japanese)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "answer_jap" id = "answer_jap" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['answer_jap']) && $_POST['answer_jap']!='' ? $_POST['answer_jap'] :'' ?></textarea>
																	</div>
																</div>
															</div>
														</div>
													
														<div class="form-actions text-right pal">
															<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Create</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>