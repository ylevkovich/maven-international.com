<?php

define( "MASTER_ADMIN", "admin_master" );

define( "ENQUIRIES", "enquiries" );

define( "SETTINGS", "settings" );

define( "HOME_SLIDER", "home_slider" );

define( "SUPPORTER", "supporters" );

define( "STATIC_PAGE_OUR_CLIENT", "static_page_our_client" );

define( "STATIC_PAGE_OUR_SERVICE", "static_page_our_service" );

define( "STATIC_PAGE_FAQ", "static_page_faq" );

define( "STATIC_PAGE_ABOUT_US", "static_page_about_us" );

define( "STATIC_PAGE_CONTATC_US", "static_page_contact_us" );

define( "STATIC_PAGE_HOME", "static_page_home" );

define( "MANAGE_TESTIMONIALS", "manage_testimonials" );

define( "MANAGE_FAQ", "manage_faq" );

define( "MANAGE_ADDRESS", "manage_address" );

define( "MANAGE_SERVICE", "manage_service" );

define( "MANAGE_QUOTES", "manage_quotes" );

define( "MANAGE_QUOTE_DOCUMENT", "manage_quote_document" );

define( "MANAGE_ENQUERY_DOCUMENT", "manage_enquery_document" );

define( "STATIC_PAGE_JOIN_TEAM", "static_page_join_team" );

define( "LANGUAGES", "languages" );

define( "STATIC_LANG", "static_lang" );

define( "STATIC_PAGE_HOME_FIRST_BLOCK_FOUR", "static_page_home_first_block_four" );

define( "STATIC_PAGE_HOME_FIRST_BLOCK_ONE", "static_page_home_first_block_one" );

define( "STATIC_PAGE_HOME_FIRST_BLOCK_THREE", "static_page_home_first_block_three" );

define( "STATIC_PAGE_HOME_FIRST_BLOCK_TWO", "static_page_home_first_block_two" );

define( "STATIC_PAGE_HOME_SECOND_BLOCK_FOUR", "static_page_home_second_block_four" );

define( "STATIC_PAGE_HOME_SECOND_BLOCK_ONE", "static_page_home_second_block_one" );

define( "STATIC_PAGE_HOME_SECOND_BLOCK_THREE", "static_page_home_second_block_three" );

define( "STATIC_PAGE_HOME_SECOND_BLOCK_TWO", "static_page_home_second_block_two" );

define( "STATIC_PAGE_HOME_TRANSLATION_AGENCY", "static_page_home_translation_agency" );

define( "STATIC_PAGE_PRIVACY", "static_page_privacy" );

define( "EMAIL_TEMPLATE", "email_template" );

?>