<?php
$urlmenu=basename($_SERVER['PHP_SELF']); 
?>
<nav id="sidebar" role="navigation" data-step="2" data-intro="Template has &lt;b&gt;many navigation styles&lt;/b&gt;" data-position="right" class="navbar-default navbar-static-side">
	<div class="sidebar-collapse menu-scroll">
		<ul id="side-menu" class="nav">                    
			<div class="clearfix"></div>
			<li <?=(($urlmenu == 'admin/dashboard.php') ? 'class="active"' : '')?>>
				<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/dashboard.php">
					<i class="fa fa-tachometer fa-fw">
						<div class="icon-bg bg-orange"></div>
					</i>
					<span class="menu-title">Dashboard</span>
				</a>
			</li> 

			<li <?=(($urlmenu == 'admin/add_service.php' || $urlmenu == 'admin/list_service.php' || $urlmenu == 'admin/edit_service.php') ? 'class="active"' : '')?>>
				<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_service.php">
					<i class="fa fa-wrench">
						<div class="icon-bg bg-violet"></div>
					</i>
					<span class="menu-title">Manage Services</span>
				</a>
				<ul class="nav">
					<div class="clearfix"></div>
					<!-- <li <?=(($urlmenu == 'admin/add_service.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/add_service.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Add New Service</span></a>
					</li> -->
					<li <?=(($urlmenu == 'admin/list_service.php' || $urlmenu == 'admin/edit_service.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_service.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">List of Services</span></a>
					</li>
				</ul>
			</li>
			<li <?=(($urlmenu == 'admin/add_supporter.php' || $urlmenu == 'admin/list_supporters.php' || $urlmenu == 'admin/edit_supporter.php') ? 'class="active"' : '')?>>
				<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_Supporters.php">
					<i class="fa fa-edit fa-fw">
						<div class="icon-bg bg-violet"></div>
					</i>
					<span class="menu-title">Manage Supporters</span>
				</a>
				<ul class="nav">
					<div class="clearfix"></div>
					<li <?=(($urlmenu == 'admin/add_supporter.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/add_supporter.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Add New Supporter</span></a>
					</li>
					<li <?=(($urlmenu == 'admin/list_supporters.php' || $urlmenu == 'admin/edit_supporter.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_supporters.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">List Supporters</span></a>
					</li>
				</ul>
			</li>

			<li <?=(($urlmenu == 'admin/add_testimonials.php' || $urlmenu == 'admin/list_testimonials.php' || $urlmenu == 'admin/edit_testimonials.php') ? 'class="active"' : '')?>>
				<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_testimonials.php">
					<i class="fa fa-comment-o fa-fw">
						<div class="icon-bg bg-violet"></div>
					</i>
					<span class="menu-title">Manage Testimonials</span>
				</a>
				<ul class="nav">
					<div class="clearfix"></div>
					<li <?=(($urlmenu == 'admin/add_testimonials.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/add_testimonials.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Add New Testimonial</span></a>
					</li>
					<li <?=(($urlmenu == 'admin/list_testimonials.php' || $urlmenu == 'admin/edit_testimonials.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_testimonials.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">List Testimonials</span></a>
					</li>
				</ul>
			</li>
			<li <?=(($urlmenu == 'admin/add_faq.php' || $urlmenu == 'admin/list_faq.php' || $urlmenu == 'admin/edit_faq.php') ? 'class="active"' : '')?>>
				<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_testimonials.php">
					<i class="fa fa-question-circle">
						<div class="icon-bg bg-violet"></div>
					</i>
					<span class="menu-title">Manage FAQ</span>
				</a>
				<ul class="nav">
					<div class="clearfix"></div>
					<li <?=(($urlmenu == 'admin/add_faq.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/add_faq.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Add New FAQ</span></a>
					</li>
					<li <?=(($urlmenu == 'admin/list_faq.php' || $urlmenu == 'admin/edit_faq.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_faq.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">List FAQ</span></a>
					</li>
				</ul>
			</li>

			<li  <?=(($urlmenu == 'admin/add_slider.php' || $urlmenu == 'admin/list_slider.php' || $urlmenu == 'admin/edit_slider.php' || $urlmenu == 'admin/static_home_page.php') ? 'class="active"' : '')?>>
				<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_slider.php">
					<i class="fa fa-sitemap fa-fw">
						<div class="icon-bg bg-dark"></div>
					</i>
					<span class="menu-title">Manage Homepage</span>
				</a>
				<ul class="nav">
					<div class="clearfix"></div>
					<li  <?=(($urlmenu == 'admin/add_slider.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/add_slider.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Add New Slider</span></a>
					</li>
					<li  <?=(($urlmenu == 'admin/list_slider.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_slider.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">List Homepage Sliders</span></a>
					</li>					
					<li  <?=(($urlmenu == 'admin/static_home_page.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/static_home_page.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Homepage Content</span></a>
					</li>					
				</ul>
			</li>
			<li <?=(($urlmenu == 'admin/edit_about_us.php' || $urlmenu == 'admin/edit_join_team.php' || $urlmenu == 'admin/edit_advertisement.php' || $urlmenu == 'admin/edit_static_page_supporters.php' || $urlmenu == 'admin/_edit_video.php' || $urlmenu == 'admin/who_we_are.php') ? 'class="active"' : '')?>>
				<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_about_us.php">
					<i class="fa fa-file-o fa-fw">
						<div class="icon-bg bg-yellow"></div>
					</i>
					<span class="menu-title">Manage Static Pages</span>
				</a>
				<ul class="nav">
					<div class="clearfix"></div>
					<li <?=(($urlmenu == 'admin/edit_about_us.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_about_us.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">About Us</span></a>
					</li>					
					<li <?=(($urlmenu == 'admin/edit_join_team.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_join_team.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Join Our Team</span></a>
					</li>					
				</ul>
			</li>			
			<li <?=(($urlmenu == 'admin/list_enquiries.php' || $urlmenu == 'admin/reply.php') ? 'class="active"' : '')?>>
				<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_enquiries.php">
					<i class="fa fa-envelope-o">
						<div class="icon-bg bg-primary"></div>
					</i>
					<span class="menu-title">Manage Enquiries</span>
				</a>
			</li>
			<li <?=(($urlmenu == 'admin/general_settings.php' || $urlmenu == 'admin/google_analytic_settings.php' || $urlmenu == 'admin/edit_social_link.php' || $urlmenu == 'admin/list_seo.php' || $urlmenu == 'admin/edit_seo.php' || $urlmenu == 'admin/language_conversion.php') ? 'class="active"' : '')?>>
				<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/general_settings.php">
					<i class="fa fa-sitemap fa-fw">
						<div class="icon-bg bg-dark"></div>
					</i>
					<span class="menu-title">SEO & Settings</span>
				</a>
				<ul class="nav">
					<div class="clearfix"></div>
					<li <?=(($urlmenu == 'admin/general_settings.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/general_settings.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">General Settings</span></a>
					</li>
					<li  <?=(($urlmenu == 'admin/google_analytic_settings.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/google_analytic_settings.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Google Analytics</span></a>
					</li>	
					<li  <?=(($urlmenu == 'admin/list_seo.php' || $urlmenu == 'admin/edit_seo.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_seo.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Manage SEO</span></a>
					</li>
					<li  <?=(($urlmenu == 'admin/edit_social_link.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_social_link.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Manage Social Link</span></a>
					</li>
				</ul>
			</li>
			<li  <?=(($urlmenu == 'admin/add_address.php' || $urlmenu == 'admin/list_address.php' || $urlmenu == 'admin/edit_address.php') ? 'class="active"' : '')?>>
				<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_slider.php">
					<i class="fa fa-map-marker">
						<div class="icon-bg bg-dark"></div>
					</i>
					<span class="menu-title">Manage Addresses</span>
				</a>
				<ul class="nav">
					<div class="clearfix"></div>
					<li  <?=(($urlmenu == 'admin/add_address.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/add_address.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Add New Address</span></a>
					</li>
					<li  <?=(($urlmenu == 'admin/list_address.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_address.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">List of Addresses</span></a>
					</li>			
				</ul>
			</li>
			<li <?=(($urlmenu == 'admin/edit_static_language.php' || $urlmenu == 'admin/list_static_lang.php') ? 'class="active"' : '')?>>
				<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_static_lang.php">
					<i class="fa fa-language" aria-hidden="true">
						<div class="icon-bg bg-violet"></div>
					</i>
					<span class="menu-title">Manage Static Language</span>
				</a>
				<ul class="nav">
					<div class="clearfix"></div>
					<li <?=(($urlmenu == 'admin/list_static_lang.php' || $urlmenu == 'admin/edit_static_language.php') ? 'class="active"' : '')?>>
						<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_static_lang.php"><i class="fa fa-chevron-right fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">List Static Language</span></a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</nav>