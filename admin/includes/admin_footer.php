<?php 
	$urlmenu_footer=basename($_SERVER['PHP_SELF']); 
	if($urlmenu_footer == 'index.php' || $urlmenu_footer == 'admin/forgot_password.php')
	{
?>
<div id="footer" <?=(($urlmenu_footer == 'index.php' || $urlmenu_footer == 'admin/forgot_password.php') ? 'align="center"' : '')?>>
	<div class="copyright">
		<div class="copy">&copy; <?php echo date('Y'); ?> Maven | Developed by: <a href="http://www.neocoderztechnologies.com" target="_blank">Neo Coderz Technologies</a></div>
	</div>
</div>
<?php
	}
?>