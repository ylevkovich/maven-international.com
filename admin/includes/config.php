<?php
//d1f0d9f9fb06c8e34dc9ba0eacde3fb1
$no_ssl_addr = [
			'127.0.0.1',
			'::1'
			];

$protocol = "http://";
$host = $_SERVER['HTTP_HOST'];

if (in_array($_SERVER['REMOTE_ADDR'], $no_ssl_addr)) {
	$protocol = "http://";
}

if (!isset($_SERVER['HTTPS']) && !in_array($_SERVER['REMOTE_ADDR'], $no_ssl_addr)) {
	//header("Location: {$protocol}{$host}", TRUE, 301);
}

if (isset($_SERVER['HTTPS'])) {
	$protocol = "https://";
}

if (isset($_SERVER['HTTPS']) or $host == 'maven-international.com') {
	header("Location: http://www.maven-international.com", TRUE, 301);
}


//ENTER THE NAME OF THE DATABASE SERVER YOU ARE CONNECTING TO. NORMALLY SET TO "localhost"
define("DATABASE_SERVER", "localhost");

//ENTER THE NAME OF YOUR DATABASE
define("DATABASE_NAME", "maven2");

//ENTER THE USERNAME THAT CONNECTS TO YOUR DATABASE
define("DATABASE_USERNAME", "root");

//ENTER THE PASSWORD FOR YOUR DATABASE USER
define("DATABASE_PASSWORD", "root");

//ENTER THE DOMAIN NAME FOR YOUR APPLICATION
define("DOMAIN_NAME_PATH", "{$protocol}{$host}/");
//ENTER THE ADMIN PATH FOR YOUR APPLICATION
define("DOMAIN_NAME_PATH_ADMIN", "{$protocol}{$host}/");

//DEFINE PAGINATION LIMIT OF LISTING PAGE
define("PAGELIMIT", "25");

define("ADMIN_PAGE_TITLE", "Maven");

define('ROOT_PATH', realpath(dirname(dirname(dirname(__FILE__)))));
define('ADMIN_INCLUDE_PATH', realpath(dirname(__FILE__)));
define('RECAPTCHA_KEY', '6Leo8DYUAAAAAIUe1Idm2jGZeZf8AstgJYMM73BJ');
define('RECAPTCHA_SECRET_KEY', '6Leo8DYUAAAAAEEFV4bZLpZBgtMw_CEZhnFqDwFb');
