<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- <link rel="shortcut icon" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/favicon.ico">

<link rel="apple-touch-icon" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/favicon.png">

<link rel="apple-touch-icon" sizes="72x72" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/favicon-72x72.png">

<link rel="apple-touch-icon" sizes="114x114" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/favicon-114x114.png"> -->

<!--Loading bootstrap css-->

<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">

<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">

<link type="text/css" rel="stylesheet" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/styles/jquery-ui.min.css">

<link type="text/css" rel="stylesheet" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/styles/font-awesome.min.css">

<link type="text/css" rel="stylesheet" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/styles/bootstrap.min.css">

<link type="text/css" rel="stylesheet" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/styles/animate.css">

<link type="text/css" rel="stylesheet" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/styles/all.css">

<link type="text/css" rel="stylesheet" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/styles/main.css">

<link type="text/css" rel="stylesheet" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/styles/style-responsive.css">

<link type="text/css" rel="stylesheet" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/styles/zabuto_calendar.min.css">

<link type="text/css" rel="stylesheet" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/styles/pace.css">

<link type="text/css" rel="stylesheet" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/styles/jquery.news-ticker.css">	

<link rel="stylesheet" type="text/css" href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/styles/notifyBar.css" />



<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery-1.11.0.js"></script> 

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.validate.min.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.notifyBar.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery-migrate-1.2.1.min.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery-ui.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/bootstrap.min.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/bootstrap-hover-dropdown.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/html5shiv.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/respond.min.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.metisMenu.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.slimscroll.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.cookie.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/icheck.min.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/custom.min.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.news-ticker.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.menu.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/pace.min.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/holder.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/responsive-tabs.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.flot.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.flot.categories.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.flot.pie.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.flot.tooltip.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.flot.resize.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.flot.fillbetween.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.flot.stack.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/jquery.flot.spline.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/zabuto_calendar.min.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/index.js"></script>

<!--LOADING SCRIPTS FOR CHARTS-->

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/highcharts.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/data.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/drilldown.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/exporting.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/highcharts-more.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/charts-highchart-pie.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/charts-highchart-more.js"></script>

<!--CORE JAVASCRIPT-->

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/main.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/ckeditor/ckeditor.js"></script>

<script src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/script/ckfinder/ckfinder.js"></script>

<script type="text/javascript">

<!--

	var digitsOnly = /[1234567890]/g;

	var integerOnly = /[0-9\.]/g;

	var alphaOnly = /[A-Za-z 1234567890_-]/g;



	function restrictCharacters(myfield, e, restrictionType) 

	{

		if (!e) var e = window.event

		if (e.keyCode) code = e.keyCode;

		else if (e.which) code = e.which;

		var character = String.fromCharCode(code);

		

		// if they pressed esc... remove focus from field...

		if (code==27) { this.blur(); return false; }

		

		// ignore if they are press other keys

		// strange because code: 39 is the down key AND ' key...

		// and DEL also equals .

		if (!e.ctrlKey && code!=9 && code!=36 && code!=37 && code!=38 && (code!=39 || (code==39 && character=="'")) && code!=40) 

		{

			if (character.match(restrictionType) || code == 8 || code == 46) 

			{

				return true;

			} 

			else

			{

				return false;

			}

		}

		else

		{

			return false;

		}

	}

//-->

</script>