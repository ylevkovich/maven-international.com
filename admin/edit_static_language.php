<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
if(isset($_POST['btn_submit']))
{
			$value='title_eng=:title_eng, title_rus=:title_rus, title_tur=:title_tur, title_spa=:title_spa, title_fre=:title_fre, title_man=:title_man, title_can=:title_can, title_ger=:title_ger, title_per=:title_per, title_kor=:title_kor, title_mal=:title_mal, title_ind=:title_ind, title_ara=:title_ara, title_jap=:title_jap';
			$execution=array(':title_eng'=>stripcleantohtml($_POST['title_eng']), 
								':title_rus'=>stripcleantohtml($_POST['title_rus']), 
								':title_tur'=>stripcleantohtml($_POST['title_tur']), 
								':title_spa'=>stripcleantohtml($_POST['title_spa']),
								':title_fre'=>stripcleantohtml($_POST['title_fre']),
								':title_man'=>stripcleantohtml($_POST['title_man']),
								':title_can'=>stripcleantohtml($_POST['title_can']),
								':title_ger'=>stripcleantohtml($_POST['title_ger']),
								':title_per'=>stripcleantohtml($_POST['title_per']),
								':title_kor'=>stripcleantohtml($_POST['title_kor']),
								':title_mal'=>stripcleantohtml($_POST['title_mal']),
								':title_ind'=>stripcleantohtml($_POST['title_ind']),
								':title_ara'=>stripcleantohtml($_POST['title_ara']),
								':title_jap'=>stripcleantohtml($_POST['title_jap'])
								);
			update(STATIC_LANG, $value, 'WHERE id='.base64_decode($_GET['id']).'', $execution);
			header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_static_lang.php');
			$_SESSION['SET_TYPE'] = 'success';
			$_SESSION['SET_FLASH'] = 'Static language updated successfully.';
			exit;
}

$static_page = find('first', STATIC_LANG, '*', 'WHERE id='.base64_decode($_GET['id']).'', array());
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Edit Static Language</title>
    <?php include_once('includes/scripts.php')?>

	  <script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#edit-form").validate({
						rules: {
							title_eng: "required",
							title_rus: "required",
							title_tur: "required",
							title_spa: "required",
							title_fre: "required",
							title_man: "required",
							title_can: "required",
							title_ger: "required",
							title_per: "required",
							title_kor: "required",
							title_mal: "required",
							title_ind: "required",
							title_ara: "required",
							title_jap: "required"

							
						},
						messages: {
							title_eng: "<font color = 'red'><b>Please enter English Title</b></font>",
							title_rus: "<font color = 'red'><b>Please enter Russian Title</b></font>",
							title_tur: "<font color = 'red'><b>Please enter Turkish Title</b></font>",
							title_spa: "<font color = 'red'><b>Please enter Spanish Title</b></font>",
							title_fre: "<font color = 'red'><b>Please enter French Title</b></font>",
							title_man: "<font color = 'red'><b>Please enter Mandarin Title</b></font>",
							title_can: "<font color = 'red'><b>Please enter Cantonese Title</b></font>",
							title_ger: "<font color = 'red'><b>Please enter German Title</b></font>",
							title_per: "<font color = 'red'><b>Please enter Persian Title</b></font>",
							title_kor: "<font color = 'red'><b>Please enter Korean Title</b></font>",
							title_mal: "<font color = 'red'><b>Please enter Malay Title</b></font>",
							title_ind: "<font color = 'red'><b>Please enter Indonesian Title</b></font>",
							title_ara: "<font color = 'red'><b>Please enter Arabic Title</b></font>",
							title_jap: "<font color = 'red'><b>Please enter Japanese Title</b></font>"

						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	
	</script>

</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Edit Static Language</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Edit Static Language</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
										<div class="panel panel-grey">
											<div class="panel-heading">Edit Static Language</div>
											<div class="panel-body">
												<div class="panel-body pan">
													<form name = "edit-form" id = "edit-form" action = "" method = "POST">
													<div class="form-body pal">
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (English)
																	</label>
																	<div class="input-icon right">
																		<input name="title_eng" id="title_eng" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_eng']) && $_POST['title_eng']!='') ? $_POST['title_eng'] : $static_page['title_eng']);?>" />
																	</div>
																</div>
															</div>
															
															<div class="col-md-6">
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (Russian)
																	</label>
																	<div class="input-icon right">
																		<input name="title_rus" id="title_rus" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_rus']) && $_POST['title_rus']!='') ? $_POST['title_rus'] : $static_page['title_rus']);?>" />
																	</div>
																</div>
															</div>

															<div class="col-md-6">
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (Turkish)
																	</label>
																	<div class="input-icon right">
																		<input name="title_tur" id="title_tur" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_tur']) && $_POST['title_tur']!='') ? $_POST['title_tur'] : $static_page['title_tur']);?>" />
																	</div>
																</div>
															</div>

															<div class="col-md-6">	
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (Spanish)
																	</label>
																	<div class="input-icon right">
																		<input name="title_spa" id="title_spa" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_spa']) && $_POST['title_spa']!='') ? $_POST['title_spa'] : $static_page['title_spa']);?>" />
																	</div>
																</div>
															</div>

															<div class="col-md-6">	
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (French)
																	</label>
																	<div class="input-icon right">
																		<input name="title_fre" id="title_fre" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_fre']) && $_POST['title_fre']!='') ? $_POST['title_fre'] : $static_page['title_fre']);?>" />
																	</div>
																</div>
															</div>

															<div class="col-md-6">	
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (Mandarin)
																	</label>
																	<div class="input-icon right">
																		<input name="title_man" id="title_man" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_man']) && $_POST['title_man']!='') ? $_POST['title_man'] : $static_page['title_man']);?>" />
																	</div>
																</div>
															</div>

															<div class="col-md-6">	
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (Cantonese)
																	</label>
																	<div class="input-icon right">
																		<input name="title_can" id="title_can" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_can']) && $_POST['title_can']!='') ? $_POST['title_can'] : $static_page['title_can']);?>" />
																	</div>
																</div>
															</div>

															<div class="col-md-6">	
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (German)
																	</label>
																	<div class="input-icon right">
																		<input name="title_ger" id="title_ger" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_ger']) && $_POST['title_ger']!='') ? $_POST['title_ger'] : $static_page['title_ger']);?>" />
																	</div>
																</div>
															</div>

															<div class="col-md-6">	
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (Persian)
																	</label>
																	<div class="input-icon right">
																		<input name="title_per" id="title_per" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_per']) && $_POST['title_per']!='') ? $_POST['title_per'] : $static_page['title_per']);?>" />
																	</div>
																</div>
															</div>

															<div class="col-md-6">	
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (Korean)
																	</label>
																	<div class="input-icon right">
																		<input name="title_kor" id="title_kor" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_kor']) && $_POST['title_kor']!='') ? $_POST['title_kor'] : $static_page['title_kor']);?>" />
																	</div>
																</div>
															</div>

															<div class="col-md-6">	
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (Malay)
																	</label>
																	<div class="input-icon right">
																		<input name="title_mal" id="title_mal" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_mal']) && $_POST['title_mal']!='') ? $_POST['title_mal'] : $static_page['title_mal']);?>" />
																	</div>
																</div>
															</div>

															<div class="col-md-6">	
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (Indonesian)
																	</label>
																	<div class="input-icon right">
																		<input name="title_ind" id="title_ind" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_ind']) && $_POST['title_ind']!='') ? $_POST['title_ind'] : $static_page['title_ind']);?>" />
																	</div>
																</div>
															</div>

															<div class="col-md-6">	
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (Arabic)
																	</label>
																	<div class="input-icon right">
																		<input name="title_ara" id="title_ara" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_ara']) && $_POST['title_ara']!='') ? $_POST['title_ara'] : $static_page['title_ara']);?>" />
																	</div>
																</div>
															</div>

															<div class="col-md-6">	
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Title (Japanese)
																	</label>
																	<div class="input-icon right">
																		<input name="title_jap" id="title_jap" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['title_jap']) && $_POST['title_jap']!='') ? $_POST['title_jap'] : $static_page['title_jap']);?>" />
																	</div>
																</div>
															</div>
																
																<!-- <div class="form-group">
																	<label for="inputName" class="control-label">
																		Pinterest Link
																	</label>
																	<div class="input-icon right">
																		<input name="pinterest_link" id="pinterest_link" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['pinterest_link']) && $_POST['pinterest_link']!='') ? $_POST['pinterest_link'] : $settings['pinterest_link']);?>" />
																	</div>
																</div>

																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Vimeo Link
																	</label>
																	<div class="input-icon right">
																		<input name="vimeo_link" id="vimeo_link" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['vimeo_link']) && $_POST['vimeo_link']!='') ? $_POST['vimeo_link'] : $settings['vimeo_link']);?>" />
																	</div>
																</div>

																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Instagram
																	</label>
																	<div class="input-icon right">
																		<input name="instagram" id="instagram" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['instagram']) && $_POST['instagram']!='') ? $_POST['instagram'] : $settings['instagram']);?>" />
																	</div>
																</div> -->

																<div class="form-actions text-right pal">
																	<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Update</button>
																</div>
															</div>	
														</div>
													</form>
												</div>
											</div>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
    
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>