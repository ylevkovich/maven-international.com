<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
if(isset($_POST['btn_submit']))
{
			$value='facebook_link=:facebook_link, twitter_link=:twitter_link, linkedin_link=:linkedin_link, google_plus_link=:google_plus_link';
			$execution=array(':facebook_link'=>stripcleantohtml($_POST['facebook_link']), 
								':twitter_link'=>stripcleantohtml($_POST['twitter_link']), 
								':linkedin_link'=>stripcleantohtml($_POST['linkedin_link']), 
								':google_plus_link'=>stripcleantohtml($_POST['google_plus_link'])
								);
			update(SETTINGS, $value, 'WHERE id=1', $execution);
			header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/edit_social_link.php');
			$_SESSION['SET_TYPE'] = 'success';
			$_SESSION['SET_FLASH'] = 'Social link updated successfully.';
			exit;
}

$settings = find('first', SETTINGS, '*', "WHERE id = 1", array());
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Edit Social Link</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#edit-form").validate({
						rules: {
							facebook_link: "required",
							linkedin_link: "required",
							google_plus_link: "required",
							pinterest_link: "required",
							vimeo_link: "required",
							instagram: "required",
							twitter_link: "required"
							
						},
						messages: {
							facebook_link: "<font color = 'red'><b>Please enter facebook link</b></font>",
							linkedin_link: "<font color = 'red'><b>Please enter linkedin link</b></font>",
							google_plus_link: "<font color = 'red'><b>Please enter google plus link</b></font>",
							pinterest_link: "<font color = 'red'><b>Please enter pinterest link</b></font>",
							vimeo_link: "<font color = 'red'><b>Please enter vimeo link</b></font>",
							instagram: "<font color = 'red'><b>Please enter instagram link</b></font>",
							twitter_link: "<font color = 'red'><b>Please enter twitter link</b></font>"

						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Social Link</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Edit Social Link</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
										<div class="panel panel-grey">
											<div class="panel-heading">Edit Social Link</div>
											<div class="panel-body">
												<div class="panel-body pan">
													<form name = "edit-form" id = "edit-form" action = "" method = "POST">
													<div class="form-body pal">
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Facebook Link
																	</label>
																	<div class="input-icon right">
																		<input name="facebook_link" id="facebook_link" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['facebook_link']) && $_POST['facebook_link']!='') ? $_POST['facebook_link'] : $settings['facebook_link']);?>" />
																	</div>
																</div>
															
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Twitter Link
																	</label>
																	<div class="input-icon right">
																		<input name="twitter_link" id="twitter_link" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['twitter_link']) && $_POST['twitter_link']!='') ? $_POST['twitter_link'] : $settings['twitter_link']);?>" />
																	</div>
																</div>
															
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Linkedin Link
																	</label>
																	<div class="input-icon right">
																		<input name="linkedin_link" id="linkedin_link" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['linkedin_link']) && $_POST['linkedin_link']!='') ? $_POST['linkedin_link'] : $settings['linkedin_link']);?>" />
																	</div>
																</div>
																
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Google Link
																	</label>
																	<div class="input-icon right">
																		<input name="google_plus_link" id="google_plus_link" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['google_plus_link']) && $_POST['google_plus_link']!='') ? $_POST['google_plus_link'] : $settings['google_plus_link']);?>" />
																	</div>
																</div>
																
																<!-- <div class="form-group">
																	<label for="inputName" class="control-label">
																		Pinterest Link
																	</label>
																	<div class="input-icon right">
																		<input name="pinterest_link" id="pinterest_link" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['pinterest_link']) && $_POST['pinterest_link']!='') ? $_POST['pinterest_link'] : $settings['pinterest_link']);?>" />
																	</div>
																</div>

																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Vimeo Link
																	</label>
																	<div class="input-icon right">
																		<input name="vimeo_link" id="vimeo_link" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['vimeo_link']) && $_POST['vimeo_link']!='') ? $_POST['vimeo_link'] : $settings['vimeo_link']);?>" />
																	</div>
																</div>

																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Instagram
																	</label>
																	<div class="input-icon right">
																		<input name="instagram" id="instagram" type="text" placeholder="" class="form-control" value = "<?php echo((isset($_POST['instagram']) && $_POST['instagram']!='') ? $_POST['instagram'] : $settings['instagram']);?>" />
																	</div>
																</div> -->

																<div class="form-actions text-right pal">
																	<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Update</button>
																</div>
															</div>	
														</div>
													</form>
												</div>
											</div>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
    
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>