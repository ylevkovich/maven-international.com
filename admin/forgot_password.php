<?php
include_once('../init.php');

if(isset($_POST['btn_submit']))
{
	if($check_exists = find('first', MASTER_ADMIN, '*', "WHERE email_address = '".stripcleantohtml($_POST['email_address'])."'", array()))
	{
		$password = create_password(4);
		update(MASTER_ADMIN, 'password=:password', 'WHERE id='.$check_exists['id'].'', array(':password'=>stripcleantohtml(md5($password))));

		$mail_Body = "Dear ".$check_exists['full_name'].",<br/><br/>Your admin user account password is been updated successfully. Here is your updated account access details.<br/><br/>Email Address: ".$check_exists['email_address']."<br/>Password: ".$password."<br/><br/>You can change your account access details once you login to your profile.<br/>Regards,<br/>Administrator.";
		Send_HTML_Mail($_POST['email_address'], $admin_details['email_address'], '', 'Your Updated Admin Panel Access Details', $mail_Body);

		$_SESSION['SET_TYPE'] = 'success';
		$_SESSION['SET_FLASH'] = 'Account password updated successfully. Please check your email to find updates.';
	}
	else
	{
		$_SESSION['SET_TYPE'] = 'error';
		$_SESSION['SET_FLASH'] = 'This email address does not exists in our database.';
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Forgot Password</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#edit-form").validate({
						rules: {
							email_address: {
								required: true,
								email: true
							}
						},
						messages: {
							email_address: "<font color = 'red'><b>Please enter valid email address</b></font>"
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
</head>
<body style="background-image: url(<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/green-walls-bike.jpg); background-position: center top; background-size: cover;">
	<div class="col-md-12 text-center">
		<h1 style="margin-top: 20px; font-size: 24px;">
			<img src="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/logo.png" border="0"alt="" align="absmiddle" />
		</h1>
		<br />
	</div>
	<div style="clear:both;"></div>
	<div align="center">
		<h4><strong style="color:#fff; font-size:20px;">Forgot Password?</strong></h4>
		<div class="page-form">
			<div class="panel ">
				
				<div class="col-lg-12">
					<div id="notify_msg_div"></div>
				</div>

				<div class="panel-body pan">
					<form name = "edit-form" id = "edit-form" action="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/forgot_password.php" class="form-horizontal" method = "POST">
					<div class="form-body pal">
						<div class="form-group">
							<div class="col-md-12">
								<div class="input-icon right">
									<i class="fa fa-user"></i>
									<input name = "email_address" id = "email_address" value = "<?php echo((isset($_POST['email_address']) && $_POST['email_address']!='') ? $_POST['email_address'] : '');?>" type="text" placeholder="Enter Email Address" class="form-control" />
								</div>
							</div>
						</div>		

						<div class="form-group">
							<div class="col-md-12" align="left">
								<div align="left">
									<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Submit</button>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12" align="left">
								<div align="left"> <a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/index.php">Back To Login</a></div>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>				   
		</div>
	</div>

	<!--BEGIN FOOTER-->
	<?php include_once('includes/admin_footer.php');?>
	<!--END FOOTER-->

</body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>