<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
$general_settings = find('first', SETTINGS, '*', "WHERE id = 1", array());	

	if(isset($_POST['btn_submit']))
	{
		if(isset($_FILES['footer_logo']['name']) && $_FILES['footer_logo']['name']!="")
		{
			$file1 = "../images/misc/".$general_settings['footer_logo'];
	
			$v_image1=$_FILES['footer_logo']['name'];
			$path1="../images/misc/";
			$v_pic1=rand(0,200000)."_".$v_image1;
			if (file_exists($file1))
				{
					@unlink("../images/misc/".$general_settings["footer_logo"]);
				}
			move_uploaded_file($_FILES['footer_logo']['tmp_name'],$path1.$v_pic1);
		}
		else
		{
			$v_pic1=$general_settings['footer_logo'];
		}
		
		if(isset($_FILES['site_logo']['name']) && $_FILES['site_logo']['name']!="")
		{
			$file2 = "../images/misc/".$general_settings['site_logo'];
	
			$v_image2=$_FILES['site_logo']['name'];
			$path2="../images/misc/";
			$v_pic2=rand(0,200000)."_".$v_image2;
			if (file_exists($file2))
				{
					@unlink("../images/misc/".$general_settings["site_logo"]);
				}
			move_uploaded_file($_FILES['site_logo']['tmp_name'],$path2.$v_pic2);
		}
		else
		{
			$v_pic2=$general_settings['site_logo'];
		}

		if(isset($_FILES['favicon']['name']) && $_FILES['favicon']['name']!="")
		{
			$file55 = "../images/misc/".$general_settings['favicon'];
	
			$v_image55=$_FILES['favicon']['name'];
			$path55="../images/misc/";
			$v_pic55=rand(0,200000)."_".$v_image55;
			if (file_exists($file55))
				{
					@unlink("../images/misc/".$general_settings["favicon"]);
				}
			move_uploaded_file($_FILES['favicon']['tmp_name'],$path55.$v_pic55);
		}
		else
		{
			$v_pic55=$general_settings['favicon'];
		}		
			$value = 'footer_logo=:footer_logo, footer_logo=:footer_logo, phone_number=:phone_number,  site_logo=:site_logo, favicon=:favicon, opening_time_eng=:opening_time_eng, opening_time_rus=:opening_time_rus, opening_time_tur=:opening_time_tur, opening_time_spa=:opening_time_spa, opening_time_fre=:opening_time_fre, opening_time_man=:opening_time_man, opening_time_can=:opening_time_can, opening_time_ger=:opening_time_ger, opening_time_per=:opening_time_per, opening_time_kor=:opening_time_kor, opening_time_mal=:opening_time_mal, opening_time_ind=:opening_time_ind, opening_time_ara=:opening_time_ara, opening_time_jap=:opening_time_jap, email_address=:email_address';
			$execute = array(
							':footer_logo'=>$v_pic1,
							':phone_number'=>stripcleantohtml($_POST['phone_number']),
							':site_logo'=>$v_pic2,
							':favicon'=>$v_pic55,
							':opening_time_eng'=> stripslashes($_POST['opening_time_eng']),
							':opening_time_rus'=> stripslashes($_POST['opening_time_rus']),
							':opening_time_tur'=> stripslashes($_POST['opening_time_tur']),
							':opening_time_spa'=> stripslashes($_POST['opening_time_spa']),
							':opening_time_fre'=> stripslashes($_POST['opening_time_fre']),
							':opening_time_man'=> stripslashes($_POST['opening_time_man']),
							':opening_time_can'=> stripslashes($_POST['opening_time_can']),
							':opening_time_ger'=> stripslashes($_POST['opening_time_ger']),
							':opening_time_per'=> stripslashes($_POST['opening_time_per']),
							':opening_time_kor'=> stripslashes($_POST['opening_time_kor']),
							':opening_time_mal'=> stripslashes($_POST['opening_time_mal']),
							':opening_time_ind'=> stripslashes($_POST['opening_time_ind']),
							':opening_time_ara'=> stripslashes($_POST['opening_time_ara']),
							':opening_time_jap'=> stripslashes($_POST['opening_time_jap']),
							':email_address'=>stripcleantohtml($_POST['email_address'])
							/*':footer_address1'=>$_POST['footer_address1'],
							':footer_address2'=>$_POST['footer_address2'],
							':footer_address3'=>$_POST['footer_address3'],*/
							);
				
			update(SETTINGS, $value, 'WHERE id=1',$execute);
			$_SESSION['SET_TYPE'] = 'success';
			$_SESSION['SET_FLASH'] = 'Settings successfully updated';
			header('location:general_settings.php');
			exit;
	}
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Settings | General Settings</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#edit-form").validate({
						rules: {
							email_address: {
								required: true,
								email: true
							},
							email_address: "required",
							opening_time: "required",
							phone_number: "required",
							footer_address1: "required",
							footer_address2: "required",
							footer_address3: "required",
						},
						messages: {
							opening_time: "<font color = 'red'><b>Please enter opening time</b></font>",
							phone_number: "<font color = 'red'><b>Please enter phone number</b></font>",
							site_logo: "<font color = 'red'><b>Please select site logo</b></font>",
							favicon: "<font color = 'red'><b>Please select site logo</b></font>",
							footer_address3: "<font color = 'red'><b>Please enter footer address three</b></font>",
							email_address: "<font color = 'red'><b>Please enter email address</b></font>",
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
<style>.pad_mar_bod {background-color: #eee;padding: 5px 15px;margin: 0px 0px 10px 0px;} .bod_right {border-right: 5px solid white;} .bod_left {border-left: 5px solid white;} .pad_0 {padding:0px;}</style>

</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Settings</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?><?=DOMAIN_NAME_PATH_ADMIN?>admin/script/dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Settings</li>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">General Settings</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
								 <div class="panel panel-grey">
									<div class="panel-heading">General Settings</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-lg-12">
												<div class="panel-body pan">
													<form name = "edit-form" id = "edit-form" action="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/general_settings.php" method = "POST" enctype = "multipart/form-data">
													<div class="form-body pal">
														<div class="row">
															<div class="col-md-6">
																
																<div class="form-group">
																	<label for="inputMessage" class="control-label">
																		Email Address
																	</label>
																	<input type="text" name = "email_address" id = "email_address" rows="5" class="form-control" value="<?php echo($general_settings['email_address']);?>">
																</div>
																<div class="form-group">
																	<label for="inputMessage" class="control-label">
																		Phone Number
																	</label>
																	<input type="text" name = "phone_number" id = "phone_number" rows="5" class="form-control" value="<?php echo($general_settings['phone_number']);?>">
																</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (English)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_eng" id = "opening_time_eng" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_eng']);?></textarea>
																		</div>	
																	</div>
																</div>															
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (Russian)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_rus" id = "opening_time_rus" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_rus']);?></textarea>
																		</div>	
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (Turkish)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_tur" id = "opening_time_tur" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_tur']);?></textarea>
																		</div>	
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (Spanish)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_spa" id = "opening_time_spa" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_spa']);?></textarea>
																		</div>	
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (French)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_fre" id = "opening_time_fre" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_fre']);?></textarea>
																		</div>	
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (Mandarin)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_man" id = "opening_time_man" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_man']);?></textarea>
																		</div>	
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (Cantonese)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_can" id = "opening_time_can" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_can']);?></textarea>
																		</div>	
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (German)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_ger" id = "opening_time_ger" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_ger']);?></textarea>
																		</div>	
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (Persian)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_per" id = "opening_time_per" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_per']);?></textarea>
																		</div>	
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (Korean)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_kor" id = "opening_time_kor" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_kor']);?></textarea>
																		</div>	
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (Mal)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_mal" id = "opening_time_mal" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_mal']);?></textarea>
																		</div>	
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (Indonesian)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_ind" id = "opening_time_ind" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_ind']);?></textarea>
																		</div>	
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (Arabic)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_ara" id = "opening_time_ara" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_ara']);?></textarea>
																		</div>	
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Opening Time (Japanese)
																		</label>
																		<div class="input-icon right">
																			<textarea type="text" name = "opening_time_jap" id = "opening_time_jap" rows="10" class="form-control" style="height: 109px;"><?php echo($general_settings['opening_time_jap']);?></textarea>
																		</div>	
																	</div>
																</div>

																	<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputMessage" class="control-label">
																			Footer Site Logo
																		</label>
																		<input name = "footer_logo" id = "footer_logo" type="file" placeholder="" onChange="ValidateFileUpload1()"/>
																		<div><small style="color:#ff0000">Image dimension: 150px by 85px</small></div>
																	</div>
																	<div class="form-group">
																		<?php
																			$exist_file = "../images/misc/".$general_settings['footer_logo'];
																				if (file_exists($exist_file))
																					{
																		?>
																						<img src="../images/misc/<?php echo $general_settings['footer_logo'] ;?>" style="width:150px;" id="blah1">
																		<?php
																					}
																					else
																					{
																		?>
																						<img src="../images/misc/no_image.jpg" style="width:auto; height:150px;" id="blah1">
																		<?php
																					}
																		?>	
																</div>
															</div>
														</div>

															<div id="" class="col-md-12 pad_0">
																<div id="" class="col-md-6 pad_0">
																<div class="form-group">
																	<label for="inputMessage" class="control-label">
																		Site Logo
																	</label>
																	<input name = "site_logo" id = "site_logo" type="file" placeholder="" onChange="ValidateFileUpload2()"/>
																<div><small style="color:#ff0000">Image dimension: 150px by 85px</small></div>
																</div>
																<div class="form-group">
																	<?php
																		$exist_file = "../images/misc/".$general_settings['site_logo'];
																			if (file_exists($exist_file) && $general_settings['site_logo']!='')
																				{
																	?>
																					<img src="../images/misc/<?php echo $general_settings['site_logo'] ;?>" style="width:150px;" id="blah2">
																	<?php
																				}
																				else
																				{
																	?>
																					<img src="../images/misc/no_image.jpg" style="width:150px;" id="blah2">
																	<?php
																				}
																	?>	
																</div>
																</div>	
															<div id="" class="col-md-6">																
																<div class="form-group">
																	<label for="inputMessage" class="control-label">
																		Site Favicon
																	</label>
																	<input name = "favicon" id = "favicon" type="file" placeholder="" onChange="ValidateFileUpload55()"/><br>
																	<div><small style="color:#ff0000">Image dimension: 32px by 32px</small></div>
																
																<div class="form-group">
																	<?php
																		$exist_file = "../images/misc/".$general_settings['favicon'];
																			if (file_exists($exist_file) && $general_settings['favicon']!='')
																				{
																	?>
																					<img src="../images/misc/<?php echo $general_settings['favicon'] ;?>" style="width:32px;" id="blah55">
																	<?php
																				}
																				else
																				{
																	?>
																					<img src="../images/misc/no_image.jpg" style="width:16px;" id="blah55">
																	<?php
																				}
																	?>	
																</div>
															</div>														
														</div>														
															</div>
															<div class="col-md-12 text-right pal">
															<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Update</button>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
    
    <script src="script/main.js"></script>
	<script>
	CKEDITOR.replace('footer_address1',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '150px'
	});
	CKEDITOR.replace('footer_address2',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '150px'
	});
	CKEDITOR.replace('footer_address3',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '150px'
	});

	
</script>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>
	<script>
     	function ValidateFileUpload1() {
			var fuData = document.getElementById('footer_logo');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah1').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('footer_logo').value = '';
            }
    }
</script>
	<script>
     	function ValidateFileUpload2() {
			var fuData = document.getElementById('site_logo');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah2').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('site_logo').value = '';
            }
    }
	function ValidateFileUpload55() {
			var fuData = document.getElementById('favicon');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg" || Extension == "ico") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah55').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('favicon').value = '';
            }
    }

</script>
	<script>
     	function ValidateFileUpload3() {
			var fuData = document.getElementById('sidebar_banner_image');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah3').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('sidebar_banner_image').value = '';
            }
    }
     	function ValidateFileUpload4() {
			var fuData = document.getElementById('sidebar_banner_image_2');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah4').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('sidebar_banner_image_2').value = '';
            }
    }
</script>
