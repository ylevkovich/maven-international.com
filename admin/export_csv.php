<?php
include_once('../init.php');

if(isset($_GET['role']) && $_GET['role'] == '1')
{
	$output = '';
	$header = "Sl.,Full Name,Email Address,Role,Status,";
	$header.= "\n";
	$body = '';
	$permission_final_string = '';
	if($user_list = find('all', MASTER_ADMIN, '*', "WHERE status = 'Y' ORDER BY id ASC", array()))
	{
		$sl='';
		foreach($user_list AS $users)
		{
			
			$permission_string = '';
			$temp_permission = '';
			if($users['role']!='')
			{
			if($users['role']==1)
			{
				$role="Admin";
			}
			elseif($users['role']==2)
			{
				$role="Editor";
			}
			elseif($users['role']==3)
			{
				$role="Author";
			}
			elseif($users['role']==4)
			{
				$role="Contributor";
			}
					$sl++;
				/*$permission_string = trim($users['permission'], ',');
				$temp_permission = explode(',', $permission_string);
				$permission_final_string = '';*/
				/*foreach($temp_permission AS $permission)
				{
					/*if($permission == 1)
					{
						$permission_final_string.= ':Manage Category';
					}
					if($permission == 2)
					{
						$permission_final_string.= ':Manage Online Shop';
					}
					if($permission == 3)
					{
						$permission_final_string.= ':Manage Catalogue Products';
					}
					if($permission == 4)
					{
						$permission_final_string.= ':Manage Discount Codes';
					}
					if($permission == 5)
					{
						$permission_final_string.= ':Manage Orders';
					}
					if($permission == 6)
					{
						$permission_final_string.= ':Manage Delivery Fee';
					}
					if($permission == 7)
					{
						$permission_final_string.= ':Manage Members';
					}
					if($permission == 8)
					{
						$permission_final_string.= ':Manage Photo Gallery';
					}
					if($permission == 9)
					{
						$permission_final_string.= ':Manage Static Pages';
					}
					if($permission == 10)
					{
						$permission_final_string.= ':Manage Faq';
					}
					if($permission == 11)
					{
						$permission_final_string.= ':Manage Enquiries';
					}
					if($permission == 12)
					{
						$permission_final_string.= ':Manage Settings';
					}
					if($permission == 13)
					{
						$permission_final_string.= ':Manage SEO';
					}
					if($permission == 14)
					{
						$permission_final_string.= ':Home Slider';
					}
					if($permission == 15)
					{
						$permission_final_string.= ':Home Block';
					}
					if($permission == 16)
					{
						$permission_final_string.= ':Newsletter';
					}
					
				}*/
				$permission_final_string = ltrim($permission_final_string, ':');

				$body.= $sl.",".$users['full_name'].",".$users['email_address'].",".$role.",".($users['status']=='Y' ? 'Active' : 'Inactive').",";
				$body.= "\n";
			}
			else
			{
				$body.= $sl.",".$users['full_name'].",".$users['email_address'].",All,".($users['status']=='Y' ? 'Active' : 'Inactive').",";
				$body.= "\n";
			}
		}
		$output = $header.$body;

		$filename = "admin_user.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);

		echo $output;
		exit;
	}
}

if(isset($_GET['type']) && $_GET['type'] == 'enquiries')
{
	$output = '';
	$header = "id,Full Name,Email Address,Mobile Number,Message,Date,";
	$header.= "\n";
	$body = '';
	if($enquiry_list = find('all', ENQUIRIES, '*', "WHERE 1 ORDER BY id ASC", array()))
	{
		foreach($enquiry_list AS $enquiry)
		{
			$temp1 = explode(':-:', $enquiry['message']);
			$temp2 = explode(':-:', $enquiry['date']);
			$body.= $enquiry['id'].','.$enquiry['full_name'].','.$enquiry['email_address'].','.$enquiry['mobile_number'].',"'.$temp1[0].'",'.$temp2[0].',';
			$body.= "\n";
		}
		$output = $header.$body;

		$filename = "enquiries.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);

		echo $output;
		exit;
	}
}

if(isset($_GET['type']) && $_GET['type'] == 'members')
{
	$output = '';
	$header = "id,First Name,Last Name,Email Address,Mobile Number,Country,State,Zip Code,Address,Account Status,Price Display Status,";
	$header.= "\n";
	$body = '';
	if($user_list = find('all', USERS, '*', "WHERE 1 ORDER BY id ASC", array()))
	{
		foreach($user_list AS $user)
		{
			$body.= $user['id'].','.$user['first_name'].','.$user['last_name'].','.$user['email_address'].','.$user['mobile_number'].','.$user['country'].','.$user['state'].','.$user['zip_code'].',"'.$user['address'].'",'.($user['account_status'] == 'Y' ? "Active" : "Inactive").','.($user['price_display_status'] == 'Y' ? "Can See" : "Cannot See").',';
			$body.= "\n";
		}
		$output = $header.$body;

		$filename = "members.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);

		echo $output;
		exit;
	}
}

if(isset($_GET['type']) && $_GET['type'] == 'newsletter')
{
	$output = '';
	$header = "id,Name,Email Address,Company / Organization,";
	$header.= "\n";
	$body = '';
	if($newsletter_list = find('all', NEWSLETTER, '*', "WHERE 1 ORDER BY id ASC", array()))
	{
		foreach($newsletter_list AS $newsletter)
		{
			$body.= $newsletter['id'].','.$newsletter['name'].','.$newsletter['email_address'].',"'.($newsletter['company_organization']!='' ? $newsletter['company_organization'] : 'N/A').'",';
			$body.= "\n";
		}
		$output = $header.$body;

		$filename = "newsletter.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);

		echo $output;
		exit;
	}
}
?>