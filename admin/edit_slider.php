<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
if(!isset($_GET['id']) || (isset($_GET['id']) && $_GET['id'] ==''))
{
	header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_slider.php');
}
$flag_status = true;
if(isset($_POST['btn_submit']))
{
	if($_FILES['slider_image']['name']!='')
	{
		$explode_data = explode('.', $_FILES['slider_image']['name']);
		$extension = end($explode_data);
		if($extension == 'jpg' OR $extension == 'JPG' OR $extension == 'gif' OR $extension == 'GIF' OR $extension == 'png' OR $extension == 'PNG' OR $extension == 'JPEG' OR $extension == 'jpeg')
		{
			$gallery_photo = $_FILES['slider_image']['name'];
		}
		else
		{
			$flag_status = false;
			$_SESSION['SET_TYPE'] = 'error';
			$_SESSION['SET_FLASH'] = 'Invalid extension. Please uplaod .jpg or .jpeg or .gif or .png image';
		}
	}

	if($flag_status)
	{	

		$field='slider_text_eng=:slider_text_eng, button_text_eng=:button_text_eng, slider_text_rus=:slider_text_rus, button_text_rus=:button_text_rus, slider_text_tur=:slider_text_tur, button_text_tur=:button_text_tur, slider_text_spa=:slider_text_spa, button_text_spa=:button_text_spa, slider_text_fre=:slider_text_fre, button_text_fre=:button_text_fre, slider_text_man=:slider_text_man, button_text_man=:button_text_man, slider_text_can=:slider_text_can, button_text_can=:button_text_can, slider_text_ger=:slider_text_ger, button_text_ger=:button_text_ger, slider_text_per=:slider_text_per, button_text_per=:button_text_per, slider_text_kor=:slider_text_kor, button_text_kor=:button_text_kor, slider_text_mal=:slider_text_mal, button_text_mal=:button_text_mal, slider_text_ind=:slider_text_ind, button_text_ind=:button_text_ind, slider_text_ara=:slider_text_ara, button_text_ara=:button_text_ara, slider_text_jap=:slider_text_jap, button_text_jap=:button_text_jap, button_link=:button_link, sl_no=:sl_no';
		$execute=array(
						':slider_text_eng'=>stripcleantohtml($_POST['slider_text_eng']),
						':button_text_eng'=>stripcleantohtml($_POST['button_text_eng']),
						':slider_text_rus'=>stripcleantohtml($_POST['slider_text_rus']),
						':button_text_rus'=>stripcleantohtml($_POST['button_text_rus']),
						':slider_text_tur'=>stripcleantohtml($_POST['slider_text_tur']),
						':button_text_tur'=>stripcleantohtml($_POST['button_text_tur']),
						':slider_text_spa'=>stripcleantohtml($_POST['slider_text_spa']),
						':button_text_spa'=>stripcleantohtml($_POST['button_text_spa']),
						':slider_text_fre'=>stripcleantohtml($_POST['slider_text_fre']),
						':button_text_fre'=>stripcleantohtml($_POST['button_text_fre']),
						':slider_text_man'=>stripcleantohtml($_POST['slider_text_man']),
						':button_text_man'=>stripcleantohtml($_POST['button_text_man']),
						':slider_text_can'=>stripcleantohtml($_POST['slider_text_can']),
						':button_text_can'=>stripcleantohtml($_POST['button_text_can']),
						':slider_text_ger'=>stripcleantohtml($_POST['slider_text_ger']),
						':button_text_ger'=>stripcleantohtml($_POST['button_text_ger']),
						':slider_text_per'=>stripcleantohtml($_POST['slider_text_per']),
						':button_text_per'=>stripcleantohtml($_POST['button_text_per']),
						':slider_text_kor'=>stripcleantohtml($_POST['slider_text_kor']),
						':button_text_kor'=>stripcleantohtml($_POST['button_text_kor']),
						':slider_text_mal'=>stripcleantohtml($_POST['slider_text_mal']),
						':button_text_mal'=>stripcleantohtml($_POST['button_text_mal']),
						':slider_text_ind'=>stripcleantohtml($_POST['slider_text_ind']),
						':button_text_ind'=>stripcleantohtml($_POST['button_text_ind']),
						':slider_text_ara'=>stripcleantohtml($_POST['slider_text_ara']),
						':button_text_ara'=>stripcleantohtml($_POST['button_text_ara']),
						':slider_text_jap'=>stripcleantohtml($_POST['slider_text_jap']),
						':button_text_jap'=>stripcleantohtml($_POST['button_text_jap']),
						':button_link'=>stripcleantohtml($_POST['button_link']),
						':sl_no'=>stripcleantohtml($_POST['sl_no'])
						);
		$where='WHERE id='.base64_decode($_GET['id']).'';
		update(HOME_SLIDER, $field, $where, $execute);

		if($gallery_photo!='')
		{
			if($photo_details = find('first', HOME_SLIDER, 'slider_image', "WHERE id = ".base64_decode($_GET['id'])."", array()))
			{
				if($photo_details['slider_image']!='' && file_exists('../images/slideshow/'.$photo_details['slider_image']))
				{
					unlink('../images/slideshow/'.$photo_details['slider_image']);
				}
			}

			$image_name = base64_decode($_GET['id']).'_'.$gallery_photo;
			move_uploaded_file($_FILES['slider_image']['tmp_name'], '../images/slideshow/'.$image_name);
			update(HOME_SLIDER, 'slider_image=:slider_image', 'WHERE id='.base64_decode($_GET['id']).'', array(':slider_image'=>$image_name));
		}
		
		header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_slider.php?mode=update');
	}
}

$photo = find('first', HOME_SLIDER, '*', "WHERE id = ".base64_decode($_GET['id'])."", array());
$sl_list = "";

if($slider_list = find('all', HOME_SLIDER, 'sl_no', "WHERE 1 ORDER BY sl_no ASC ", array()))
{
	foreach($slider_list AS $ls)
	{
		$sl_list.= $ls['sl_no'].",";
	}
	$sl_list = rtrim($sl_list,',');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Homepage Slider | Add New Slider</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#add-form").validate({
						rules: {
							heading1_tu: "required",
							heading2_tu: "required",
							heading3_tu: "required",
							button_text_tu: "required",
							button_link: "required",
							sl_no: "required"
						},
						messages: {
							heading1_tu: "<font color = 'red'><b>Please enter heading 1</b></font>",
							heading2_tu: "<font color = 'red'><b>Please enter heading 2</b></font>",
							heading3_tu: "<font color = 'red'><b>Please enter heading 3</b></font>",
							button_text_tu: "<font color = 'red'><b>Please enter button text</b></font>",
							button_link: "<font color = 'red'><b>Please enter button link</b></font>",
							sl_no: "<font color = 'red'><b>Please enter serial numner</b></font>",
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Homepage Slider</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active"><a href="list_slider.php">Homepage Slider</a></li>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Edit Slider</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
										<div class="panel panel-grey">
										<div class="panel-heading">Edit Slider</div>
											<div class="panel-body">
												<div class="panel-body pan">
													<form name = "add-form" id = "add-form" action = "" method = "POST" enctype = "multipart/form-data">
														<div class="row">
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (English)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_eng" id = "slider_text_eng" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_eng']) && $_POST['slider_text_eng']!='' ? $_POST['slider_text_eng'] : $photo['slider_text_eng'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (English)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_eng" id = "button_text_eng" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_eng']) && $_POST['button_text_eng']!='' ? $_POST['button_text_eng'] : $photo['button_text_eng'] ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (Russian)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_rus" id = "slider_text_rus" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_rus']) && $_POST['slider_text_rus']!='' ? $_POST['slider_text_rus'] : $photo['slider_text_rus'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (Russian)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_rus" id = "button_text_rus" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_rus']) && $_POST['button_text_rus']!='' ? $_POST['button_text_rus'] : $photo['button_text_rus'] ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (Turkish)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_tur" id = "slider_text_tur" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_tur']) && $_POST['slider_text_tur']!='' ? $_POST['slider_text_tur'] : $photo['slider_text_tur'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (Turkish)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_tur" id = "button_text_tur" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_tur']) && $_POST['button_text_tur']!='' ? $_POST['button_text_tur'] : $photo['button_text_tur'] ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (Spanish)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_spa" id = "slider_text_spa" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_spa']) && $_POST['slider_text_spa']!='' ? $_POST['slider_text_spa'] : $photo['slider_text_spa'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (Spanish)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_spa" id = "button_text_spa" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_spa']) && $_POST['button_text_spa']!='' ? $_POST['button_text_spa'] : $photo['button_text_spa'] ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (French)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_fre" id = "slider_text_fre" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_fre']) && $_POST['slider_text_fre']!='' ? $_POST['slider_text_fre'] : $photo['slider_text_fre'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (French)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_fre" id = "button_text_fre" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_fre']) && $_POST['button_text_fre']!='' ? $_POST['button_text_fre'] : $photo['button_text_fre'] ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (Mandarin)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_man" id = "slider_text_man" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_man']) && $_POST['slider_text_man']!='' ? $_POST['slider_text_man'] : $photo['slider_text_man'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (Mandarin)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_man" id = "button_text_man" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_man']) && $_POST['button_text_man']!='' ? $_POST['button_text_man'] : $photo['button_text_man'] ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (Cantonese)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_can" id = "slider_text_can" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_can']) && $_POST['slider_text_can']!='' ? $_POST['slider_text_can'] : $photo['slider_text_can'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (Cantonese)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_can" id = "button_text_can" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_can']) && $_POST['button_text_can']!='' ? $_POST['button_text_can'] : $photo['button_text_can'] ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (German)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_ger" id = "slider_text_ger" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_ger']) && $_POST['slider_text_ger']!='' ? $_POST['slider_text_ger'] : $photo['slider_text_ger'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (German)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_ger" id = "button_text_ger" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_ger']) && $_POST['button_text_ger']!='' ? $_POST['button_text_ger'] : $photo['button_text_ger'] ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (Persian)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_per" id = "slider_text_per" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_per']) && $_POST['slider_text_per']!='' ? $_POST['slider_text_per'] : $photo['slider_text_per'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (Persian)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_per" id = "button_text_per" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_per']) && $_POST['button_text_per']!='' ? $_POST['button_text_per'] : $photo['button_text_per'] ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (Korean)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_kor" id = "slider_text_kor" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_kor']) && $_POST['slider_text_kor']!='' ? $_POST['slider_text_kor'] : $photo['slider_text_kor'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (Korean)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_kor" id = "button_text_kor" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_kor']) && $_POST['button_text_kor']!='' ? $_POST['button_text_kor'] : $photo['button_text_kor'] ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (Malay)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_mal" id = "slider_text_mal" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_mal']) && $_POST['slider_text_mal']!='' ? $_POST['slider_text_mal'] : $photo['slider_text_mal'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (Malay)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_mal" id = "button_text_mal" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_mal']) && $_POST['button_text_mal']!='' ? $_POST['button_text_mal'] : $photo['button_text_mal'] ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (Indonesian)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_ind" id = "slider_text_ind" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_ind']) && $_POST['slider_text_ind']!='' ? $_POST['slider_text_ind'] : $photo['slider_text_ind'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (Indonesian)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_ind" id = "button_text_ind" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_ind']) && $_POST['button_text_ind']!='' ? $_POST['button_text_ind'] : $photo['button_text_ind'] ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (Arabic)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_ara" id = "slider_text_ara" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_ara']) && $_POST['slider_text_ara']!='' ? $_POST['slider_text_ara'] : $photo['slider_text_ara'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (Arabic)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_ara" id = "button_text_ara" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_ara']) && $_POST['button_text_ara']!='' ? $_POST['button_text_ara'] : $photo['button_text_ara'] ?>"/>
																	</div>
																</div>
															</div>
															<div class="col-md-6">																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Slider text (Japanese)
																	</label>
																	<div class="input-icon right">
																		<textarea name = "slider_text_jap" id = "slider_text_jap" class="form-control" style="resize:none;"><?php echo isset($_POST['slider_text_jap']) && $_POST['slider_text_jap']!='' ? $_POST['slider_text_jap'] : $photo['slider_text_jap'] ?></textarea>
																	</div>
																</div>																
																<div class="form-group">
																	<label for="inputEmail" class="control-label">
																		Button Text (Japanese)
																	</label>
																	<div class="input-icon right">
																		<input name = "button_text_jap" id = "button_text_jap" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['button_text_jap']) && $_POST['button_text_jap']!='' ? $_POST['button_text_jap'] : $photo['button_text_jap'] ?>"/>
																	</div>
																</div>
															</div>															
															<div class="col-md-6">	
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Button Link
																	</label>
																	<div class="input-icon right">
																		<textarea name = "button_link" id = "button_link" class="form-control"><?php echo isset($_POST['button_link']) && $_POST['button_link']!='' ? $_POST['button_link'] : $photo['button_link'] ?></textarea>
																	</div>
																</div>
																</div>
																<div class="col-md-6">	
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Serial Number
																	</label>
																	<div class="input-icon right">
																		<input name = "sl_no" id = "sl_no" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['sl_no']) && $_POST['sl_no']!='' ? $_POST['sl_no'] : $photo['sl_no'] ?>"/>
																		<div><small style="color:#ff0000">Available SL No: <?php echo($sl_list!='' ? $sl_list : 'N/A');?></small></div>
																	</div>
																</div>																
															</div>														
															<div class="col-md-12">
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Upload Photo
																	</label>
																	<div class="input-icon right" style="margin-left: -14px;">
																		<input name = "slider_image" id = "slider_image" placeholder="Inlcude some file" type="file" onchange="ValidateFileUpload()"/>
																		<div><small style="color:#ff0000;padding-left: 13px;">Image dimension: 1920px by 950px</small></div>
																	</div>
																</div>
																<div class="form-group">
																	<?php
																		$exist_file = "../images/slideshow/".$photo['slider_image'];
																			if (file_exists($exist_file))
																				{
																	?>
																					<img src="../images/slideshow/<?php echo $photo['slider_image'] ;?>" style="width:100px;" id="blah"><br><br>
																	<?php
																				}
																				else
																				{
																	?>
																					<img src="../images/misc/no_image.jpg" style="width:auto; height:100px;" id="blah"><br><br>
																	<?php
																				}
																	?>	
																</div>
															</div>
														</div>													
														<div class="form-actions text-right pal">
															<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Update</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>
	<script>
     	function ValidateFileUpload() {
			var fuData = document.getElementById('slider_image');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('slider_image').value = '';
            }
    }
</script>
