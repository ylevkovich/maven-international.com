<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
$page_serial = 11;
include_once('includes/permission.php');

$where_clause = "WHERE 1";

$count_result = find("first", ENQUIRIES, "count(id) as total_count", $where_clause, array());

$total_result = $count_result['total_count'];

$record_no = PAGELIMIT;
$no_of_page = ceil($total_result / $record_no);

if(isset($_GET['page']))
{
	$page = $_GET['page'];
	if(($page > $no_of_page) && $no_of_page!=0)
	{
		$page=$no_of_page;
	}
	else
	{
		$page=$_GET['page'];
	}
}
else
{
	$page=1;
}

$offset = ($page-1) * $record_no;

$enquiry_list = find('all', ENQUIRIES, '*', "".$where_clause." ORDER BY id DESC LIMIT ".$offset.",".$record_no, array());

$total_search_result_main = count($enquiry_list);

$page_link = 5;
$mid_link = ceil($page_link/2);
if($page_link%2==0)
{
	$st_link = $mid_link;
	$end_link = $mid_link-1;
}
else
{
	$st_link = $mid_link-1;
	$end_link = $mid_link-1;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Enquiries</title>
    <?php include_once('includes/scripts.php')?>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>  
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Enquiries</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Enquiries</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
				<div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
								<div class="col-md-12">
									<div id="area-chart-spline" style="width: 100%; height: 300px; display: none;"></div>
								</div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>

                            <div class="col-lg-12">
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-grey">
											<div class="panel-heading">Lists Of Enquiries <span style="float:right;margin-top:-5px;"><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/export_csv.php?type=enquiries" class="label-success" style="padding:10px 15px; color:#ffffff;font-size:13px;">&darr; Export Data In CSV</a></span></div>
											<div class="panel-body">
												<table class="table table-hover">
													<thead>
													<tr>
														<th>#</th>
														<th>Full Name</th>
														<th>Email</th>
														<th>Mobile</th>
														<th>Date</th>
														<th>Company</th>
														<th>Message</th>
														<th>Status</th>
														<th>Action</th>
													</tr>
													</thead>
													<tbody>
													<?php
													if($enquiry_list)
													{
														$sl_number = 1;
														foreach($enquiry_list AS $enquiry)
														{
															$temp_date = explode(':-:', $enquiry['date']);
															$display_date = end($temp_date);
															$temp = explode(':-:', $enquiry['message']);
															$message = $temp[0];
													?>
													<tr>
														<td><?php echo($sl_number);?></td>
														<td><?php echo($enquiry['full_name']);?></td>
														<td><?php echo($enquiry['email_address']);?></td>
														<td><?php echo($enquiry['mobile_number']);?></td>
														<td><?php echo($display_date);?></td>
														<td><?php echo($enquiry['company_name']);?></td>
														<td><?php echo($message);?></td>
														<td>
														<?php
														if($enquiry['status'] == 'Y')
														{
														?>
														<span class="label label-sm label-success">Read</span>
														<?php
														}
														else
														{
														?>
														<span class="label label-sm label-warning">Unread</span>
														<?php
														}
														?>
														</td>
														<td><a href = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/reply.php?id=<?php echo(base64_encode($enquiry['id']));?>" title = "Send Reply" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/reply.png" border = "0" alt = "reply" /></a></td>
													</tr>
													<?php
														$sl_number++;
														}
													}
													else
													{
													?>
													<tr>
														<td colspan = "7" align = "center"><font color = "red"><b>No Record Found!</b></td>
													</tr>
													<?php
													}
													?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<?php
								if($total_result>1)
								{
									if($total_search_result_main == $total_result)
									{
										//DO NOTHING
									} 
									else
									{
								?>
								<div class="row">
									<div class="col-lg-12">
										<ul data-hover="" class="pagination mtm mbm">
											<?php 
											if($page > 1)
											{
											?>
											<li><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_enquiries.php?page=<?php echo($page-1);?>">«</a></li>
											<?php
											}
											if($no_of_page < $page_link)
											{
												for($l=1; $l<=$no_of_page; $l++)
												{
											?>
											<li <?php echo($page==$l ? 'class="active"' : '');?> ><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_enquiries.php?page=<?php echo $l;?>"><?php echo $l;?></a></li>
											<?php
													if($l==$no_of_page)
													{
														break;
													}
												}
											}
											else if($page>$no_of_page-$mid_link)
											{
												for($l=$no_of_page-$page_link+1;$l<=$no_of_page;$l++)
												{
											?>
											<li <?php echo($page==$l ? 'class="active"' : '');?> ><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_enquiries.php?page=<?php echo $l;?>"><?php echo $l;?></a></li>
											<?php
													if($l==$no_of_page)
													{
														break;
													} 
												}
											}
											else if($page>$mid_link)
											{ 
												for($l=$page-$st_link;$l<=$page+$end_link;$l++)
												{
											?>
											<li <?php echo($page==$l ? 'class="active"' : '');?> ><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_enquiries.php?page=<?php echo $l;?>"><?php echo $l;?></a></li>
											<?php
													if($l==$no_of_page)
													{
														break;
													} 
												}
											}
											else
											{
												for($l=1;$l<=$page_link;$l++)
												{
											?>
											<li <?php echo($page==$l ? 'class="active"' : '');?> ><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_enquiries.php?page=<?php echo $l;?>"><?php echo $l;?></a></li>
											<?php
													if($l==$no_of_page)
													{
														break;
													}
												}
											}
											if($page!=$no_of_page) 
											{
											?>
											<li><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_enquiries.php?page=<?php echo($page+1);?>">»</a></li>
											<?php
											}
											?>
										</ul>
									 </div>
								</div>
								<?php
									}
								}
								?>
                            </div>
                        </div>
                    </div>
                </div>

                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>