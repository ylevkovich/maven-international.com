<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
$about_us_page_content = find('first', STATIC_PAGE_ABOUT_US, '*', "WHERE id = 1", array());
if(isset($_POST['btn_submit']))
{
	//print_r($_POST);exit;
		
	if(isset($_FILES['photo']['name']) && $_FILES['photo']['name']!="")
		{
			$file1 = "../images/about_us/".$about_us_page_content['photo'];
	
			$v_image1=$_FILES['photo']['name'];
			$path1="../images/about_us/";
			$v_pic1=rand(0,200000)."_".$v_image1;
			if (file_exists($file1))
				{
					@unlink("../images/about_us/".$about_us_page_content["photo"]);
				}
			move_uploaded_file($_FILES['photo']['tmp_name'],$path1.$v_pic1);
		}
		else
		{
			$v_pic1=$about_us_page_content['photo'];
		}
			
		$value = 'photo=:photo, page_content_eng=:page_content_eng, page_content_rus=:page_content_rus, page_content_tur=:page_content_tur, page_content_spa=:page_content_spa, page_content_fre=:page_content_fre, page_content_man=:page_content_man, page_content_can=:page_content_can, page_content_ger=:page_content_ger, page_content_per=:page_content_per, page_content_kor=:page_content_kor, page_content_mal=:page_content_mal, page_content_ind=:page_content_ind, page_content_ara=:page_content_ara, page_content_jap=:page_content_jap';
		$execute = array(
						':photo'=>$v_pic1,
						':page_content_eng'=>$_POST['page_content_eng'],
						':page_content_rus'=>$_POST['page_content_rus'],
						':page_content_tur'=>$_POST['page_content_tur'],
						':page_content_spa'=>$_POST['page_content_spa'],
						':page_content_fre'=>$_POST['page_content_fre'],
						':page_content_man'=>$_POST['page_content_man'],
						':page_content_can'=>$_POST['page_content_can'],
						':page_content_ger'=>$_POST['page_content_ger'],
						':page_content_per'=>$_POST['page_content_per'],
						':page_content_kor'=>$_POST['page_content_kor'],
						':page_content_mal'=>$_POST['page_content_mal'],
						':page_content_ind'=>$_POST['page_content_ind'],
						':page_content_ara'=>$_POST['page_content_ara'],
						':page_content_jap'=>$_POST['page_content_jap']
						);	
		$update=update(STATIC_PAGE_ABOUT_US, $value, 'WHERE id=1',$execute);
		if($update)
		{
			$_SESSION['SET_TYPE'] = 'success';
			$_SESSION['SET_FLASH'] = 'About content successfully updated';
			header('location:admin/edit_about_us.php');
			exit;
		}
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Static Pages | Manage About Us</title>
    <?php include_once('includes/scripts.php')?>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Static Pages</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
						<li>&nbsp;Static Pages&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">About Us</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
										<div class="panel panel-grey">
											<div class="panel-heading">Edit About Us</div>
											<div class="panel-body">
												<div class="panel-body pan">
													<form name = "edit-form" id = "edit-form" action="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_about_us.php" method = "POST" enctype = "multipart/form-data">
														<div class="col-lg-12">
														<div class="form-group">
																	<label for="inputMessage" class="control-label">
																		Add Photo
																	</label>
																	<input name = "photo" id = "photo" type="file" placeholder="" onChange="ValidateFileUpload()"/>
																<div><small style="color:#ff0000">Image dimension: 700px by Auto</small></div>
																</div>
																<div class="form-group">
																	<?php
																		$exist_file = "../images/about_us/".$about_us_page_content['photo'];
																			if (file_exists($exist_file) && $about_us_page_content['photo']!='')
																				{
																	?>
																					<img src="../images/about_us/<?php echo $about_us_page_content['photo'] ;?>" style="width:150px;" id="blah2">
																	<?php
																				}
																				else
																				{
																	?>
																					<img src="../images/misc/no_image.jpg" style="width:150px;" id="blah2">
																	<?php
																				}
																	?>	
																</div>
															</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (Eng)
																</label>
																<textarea name = "page_content_eng" id = "page_content_eng" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_eng']); ?> </textarea>
															</div>
															</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (Russian)
																</label>
																<textarea name = "page_content_rus" id = "page_content_rus" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_rus']); ?> </textarea>
															</div>
															</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (Turkish)
																</label>
																<textarea name = "page_content_tur" id = "page_content_tur" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_tur']); ?> </textarea>
															</div>
														</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (Spanish)
																</label>
																<textarea name = "page_content_spa" id = "page_content_spa" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_spa']); ?> </textarea>
															</div>
														</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (French)
																</label>
																<textarea name = "page_content_fre" id = "page_content_fre" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_fre']); ?> </textarea>
															</div>
															</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (Mandarin)
																</label>
																<textarea name = "page_content_man" id = "page_content_man" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_man']); ?> </textarea>
															</div>
															</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (Cantonese)
																</label>
																<textarea name = "page_content_can" id = "page_content_can" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_can']); ?> </textarea>
															</div>
															</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (German)
																</label>
																<textarea name ="page_content_ger" id = "page_content_ger" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_ger']); ?> </textarea>
															</div>
															</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (Persian)
																</label>
																<textarea name = "page_content_per" id = "page_content_per" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_per']); ?> </textarea>
															</div>
															</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (Korean)
																</label>
																<textarea name = "page_content_kor" id = "page_content_kor" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_kor']); ?> </textarea>
															</div>
															</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (Mal)
																</label>
																<textarea name = "page_content_mal" id = "page_content_mal" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_mal']); ?> </textarea>
															</div>
															</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (Indonesian)
																</label>
																<textarea name = "page_content_ind" id = "page_content_ind" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_ind']); ?> </textarea>
															</div>
															</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (Arabic)
																</label>
																<textarea name = "page_content_ara" id = "page_content_ara" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_ara']); ?> </textarea>
															</div>
															</div>
														<div class="col-lg-12">
															<div class="form-group">
																<label for="inputMessage" class="control-label">
																	Page Content (Japanese)
																</label>
																<textarea name = "page_content_jap" id = "page_content_jap" rows="5" class="form-control"> <?php echo($about_us_page_content['page_content_jap']); ?> </textarea>
															</div>
															</div>
														<div class="col-lg-12">
															<div class="form-actions text-right pal">
																<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Update</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<script>
	CKEDITOR.replace('page_content_eng',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	CKEDITOR.replace('page_content_rus',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	CKEDITOR.replace('page_content_tur',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	CKEDITOR.replace('page_content_spa',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	CKEDITOR.replace('page_content_fre',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	CKEDITOR.replace('page_content_man',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	CKEDITOR.replace('page_content_can',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	CKEDITOR.replace('page_content_ger',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	CKEDITOR.replace('page_content_per',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	CKEDITOR.replace('page_content_kor',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	CKEDITOR.replace('page_content_mal',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	CKEDITOR.replace('page_content_ind',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	CKEDITOR.replace('page_content_ara',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	CKEDITOR.replace('page_content_jap',{
		extraAllowedContent:'u;span{color}',
		filebrowserBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php',
		filebrowserImageBrowseUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/ckfinder.php?Type=Images',
		filebrowserUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?=DOMAIN_NAME_PATH_ADMIN?>admin/script/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	
	height: '450px'
	});
	
</script>

<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>
	<script>
     	function ValidateFileUpload() {
			var fuData = document.getElementById('photo');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah2').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('photo').value = '';
            }
    }
</script>
