<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
if(isset($_GET['mode']) && $_GET['mode'] == 'delete')
{
	$_SESSION['SET_TYPE'] = 'success';
	$_SESSION['SET_FLASH'] = 'Supporter have been deleted successfully';
}
if(isset($_GET['mode']) && $_GET['mode'] == 'add')
{
	$_SESSION['SET_TYPE'] = 'success';
	$_SESSION['SET_FLASH'] = 'Supporter have been uploaded successfully';
}
if(isset($_GET['mode']) && $_GET['mode'] == 'update')
{
	$_SESSION['SET_TYPE'] = 'success';
	$_SESSION['SET_FLASH'] = 'Supporter have been updated successfully';
}
if(!isset($_GET['page']))
{
	unset($_SESSION['SEARCH_TITLE']);
}
if(isset($_GET['id']) && $_GET['id']!='')
{
	$image_path = find("first", SUPPORTER, "*", "WHERE id = ".$_GET['id']."", array());
	if($image_path['image']!='' && file_exists('../images/image_support/'.$image_path['image']))
	{
		unlink('../images/image_support/'.$image_path['image']);
	}
	delete(SUPPORTER, 'WHERE id=:id', array(':id'=>$_GET['id']));
	header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_supporters.php?mode=delete');
}
if(isset($_POST['btn_search']))
{
	$_SESSION['SEARCH_TITLE'] = $_POST['search_title'];
}
else
{
	//DO NOTHING
}
$execution=array();
$where_clause = "WHERE ";
if(@$_SESSION['SEARCH_TITLE']!='')
{
	$where_clause.= "(name_en LIKE :search_drop_val)  AND ";
	$execution[':search_drop_val'] = "%".stripcleantohtml($_SESSION['SEARCH_TITLE'])."%";
}
if(@$_SESSION['SEARCH_TITLE'] == '')
{
	$where_clause.= '1';
}

$where_clause = rtrim($where_clause, 'AND ');

$count_result = find("first", SUPPORTER, "count(id) as total_count", $where_clause, $execution);

$total_result = $count_result['total_count'];

$record_no = PAGELIMIT;
$no_of_page = ceil($total_result / $record_no);

if(isset($_GET['page']))
{
	$page = $_GET['page'];
	if(($page > $no_of_page) && $no_of_page!=0)
	{
		$page=$no_of_page;
	}
	else
	{
		$page=$_GET['page'];
	}
}
else
{
	$page=1;
}

$offset = ($page-1) * $record_no;

$supporter_list = find('all', SUPPORTER, '*', "".$where_clause." ORDER BY id DESC LIMIT ".$offset.",".$record_no, $execution);

$total_search_result_main = count($supporter_list);

$page_link = 5;
$mid_link = ceil($page_link/2);
if($page_link%2==0)
{
	$st_link = $mid_link;
	$end_link = $mid_link-1;
}
else
{
	$st_link = $mid_link-1;
	$end_link = $mid_link-1;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Manage Supporter | Lists Of Supporters</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		function delete_record(id)
		{
			if(confirm('Are you sure you wish to delete this record?'))
			{
				window.location.href = '<?php echo(DOMAIN_NAME_PATH_ADMIN)?>list_supporters.php?id='+id;
			}
		}
	//-->
	</script>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Manage Supporter</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Lists Of Supporters</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
				<div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
								<div class="col-md-12">
									<div id="area-chart-spline" style="width: 100%; height: 300px; display: none;"></div>
								</div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-grey">
											<div class="panel-heading">Lists Of Supporters <span style="float:right;margin-top:-5px;"><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/add_supporter.php" class="label-success" style="padding:10px 15px; color:#ffffff;font-size:13px;">+ Add New Supporter</a></div>
											<div class="panel-body">
												

												<!-- <div>
													<nav role="navigation" class="navbar navbar-default">
														<div >
															<div id="bs-example-navbar-collapse-1">
																<form class="navbar-form" name = "search-form" id = "search-form" action = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>list_supporters.php" method = "POST">
																	<div class="col-md-3">
																		<div class="form-group">
																			<input type="text" name = "search_title" id = "search_title" placeholder="Name" class="form-control" value = "<?php echo(@$_SESSION['SEARCH_TITLE']);?>" />
																		</div>
																	</div>
																	<div class="col-md-3">
																		<div class="form-group">
																			<button type="submit" class="btn btn-green" name = "btn_search" id = "btn_search">Search</button>
																		</div>
																	</div>
																</form>
															</div>
														</div>
													</nav>
												</div> -->

												<table class="table table-hover">
													<thead>
													<tr>
														<!-- <th>Title (<span style="color:#0000FF">English</span>)</th> -->
														<th>Image</th>
														<th>Website Link</th>
														<th>Action</th>
													</tr>
													</thead>
													<tbody>
													<?php
													if($supporter_list)
													{
														foreach($supporter_list AS $supporter_listing)
														{
													?>
													<tr>
														<!-- <td><?php echo($supporter_listing['name_en']);?></td> -->
														<td>
															<div style="width:200px;">
																<?php
																	$exist_file = "../images/image_support/".$supporter_listing['image'];
																		if (file_exists($exist_file))
																			{
																?>
																				<img src="../images/image_support/<?php echo $supporter_listing['image'] ;?>" style="width:100px;">
																<?php
																			}
																			else
																			{
																?>
																				<img src="../images/misc/no_image.jpg" style="width:100px;">
																<?php
																			}
																?>	
															</div>
														</td>
														<td><?php echo($supporter_listing['website_link'] ? $supporter_listing['website_link'] : 'N/A');?></td>
														<td><a href = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/edit_supporter.php?id=<?php echo(base64_encode($supporter_listing['id']));?>" title = "Edit Supporter" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/edit.png" border = "0" alt = "Edit Supporter" /></a>&nbsp;&nbsp;&nbsp;<a href = "javascript:void(0);" onclick = "delete_record('<?php echo($supporter_listing['id']);?>');" title = "Delete Supporter" data-toggle="tooltip"><img src = "<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/images/icons/delete.png" border = "0" alt = "Delete Supporter" /></a></td>
													</tr>
													<?php
														}
													}
													else
													{
													?>
													<tr>
														<td colspan = "3" align = "center"><font color = "red"><b>No Record Found!</b></td>
													</tr>
													<?php
													}
													?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

								<?php
								if($total_result>1)
								{
									if($total_search_result_main == $total_result)
									{
										//DO NOTHING
									} 
									else
									{
								?>
								<div class="row">
									<div class="col-lg-12">
										<ul data-hover="" class="pagination mtm mbm">
											<?php 
											if($page > 1)
											{
											?>
											<li><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_supporters.php?page=<?php echo($page-1);?>">«</a></li>
											<?php
											}
											if($no_of_page < $page_link)
											{
												for($l=1; $l<=$no_of_page; $l++)
												{
											?>
											<li <?php echo($page==$l ? 'class="active"' : '');?> ><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_supporters.php?page=<?php echo $l;?>"><?php echo $l;?></a></li>
											<?php
													if($l==$no_of_page)
													{
														break;
													}
												}
											}
											else if($page>$no_of_page-$mid_link)
											{
												for($l=$no_of_page-$page_link+1;$l<=$no_of_page;$l++)
												{
											?>
											<li <?php echo($page==$l ? 'class="active"' : '');?> ><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_supporters.php?page=<?php echo $l;?>"><?php echo $l;?></a></li>
											<?php
													if($l==$no_of_page)
													{
														break;
													} 
												}
											}
											else if($page>$mid_link)
											{ 
												for($l=$page-$st_link;$l<=$page+$end_link;$l++)
												{
											?>
											<li <?php echo($page==$l ? 'class="active"' : '');?> ><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_supporters.php?page=<?php echo $l;?>"><?php echo $l;?></a></li>
											<?php
													if($l==$no_of_page)
													{
														break;
													} 
												}
											}
											else
											{
												for($l=1;$l<=$page_link;$l++)
												{
											?>
											<li <?php echo($page==$l ? 'class="active"' : '');?> ><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_supporters.php?page=<?php echo $l;?>"><?php echo $l;?></a></li>
											<?php
													if($l==$no_of_page)
													{
														break;
													}
												}
											}
											if($page!=$no_of_page) 
											{
											?>
											<li><a href="<?php echo(DOMAIN_NAME_PATH_ADMIN);?>admin/list_supporters.php?page=<?php echo($page+1);?>">»</a></li>
											<?php
											}
											?>
										</ul>
									 </div>
								</div>
								<?php
									}
								}
								?>
                            </div>
                        </div>
                    </div>
                </div>

                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>