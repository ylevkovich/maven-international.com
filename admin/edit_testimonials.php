<?php
include_once('../init.php');
validation_check($_SESSION['admin_id'], 'admin/index.php');
permission_allowed($_SESSION['role']);
if(!isset($_GET['id']) || (isset($_GET['id']) && $_GET['id'] ==''))
{
	header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_testimonials.php');
}
$flag_status = true;
if(isset($_POST['btn_submit']))
{
	if($_FILES['testimonials_photo']['name']!='')
	{
		$explode_data = explode('.', $_FILES['testimonials_photo']['name']);
		$extension = end($explode_data);
		if($extension == 'jpg' OR $extension == 'JPG' OR $extension == 'gif' OR $extension == 'GIF' OR $extension == 'png' OR $extension == 'PNG' OR $extension == 'JPEG' OR $extension == 'jpeg')
		{
			$gallery_photo = $_FILES['testimonials_photo']['name'];
		}
		else
		{
			$flag_status = false;
			$_SESSION['SET_TYPE'] = 'error';
			$_SESSION['SET_FLASH'] = 'Invalid extension. Please uplaod .jpg or .jpeg or .gif or .png image';
		}
	}
	if($_FILES['testimonials_icon']['name']!='')
	{
		$explode_data2 = explode('.', $_FILES['testimonials_icon']['name']);
		$extension = end($explode_data2);
		if($extension == 'jpg' OR $extension == 'JPG' OR $extension == 'gif' OR $extension == 'GIF' OR $extension == 'png' OR $extension == 'PNG' OR $extension == 'JPEG' OR $extension == 'jpeg')
		{
			$gallery_photo2 = $_FILES['testimonials_icon']['name'];
		}
		else
		{
			$flag_status = false;
			$_SESSION['SET_TYPE'] = 'error';
			$_SESSION['SET_FLASH'] = 'Invalid extension. Please uplaod .jpg or .jpeg or .gif or .png image';
		}
	}

	if($flag_status)
	{	

		$field=' description_eng=:description_eng, description_rus=:description_rus, description_tur=:description_tur, description_spa=:description_spa, description_fre=:description_fre, description_man=:description_man, description_can=:description_can, description_ger=:description_ger, description_per=:description_per, description_kor=:description_kor, description_mal=:description_mal, description_ind=:description_ind, description_ara=:description_ara, description_jap=:description_jap, client_name_eng=:client_name_eng, client_name_rus=:client_name_rus, client_name_tur=:client_name_tur, client_name_spa=:client_name_spa, client_name_fre=:client_name_fre, client_name_man=:client_name_man, client_name_can=:client_name_can, client_name_ger=:client_name_ger, client_name_per=:client_name_per, client_name_kor=:client_name_kor, client_name_mal=:client_name_mal, client_name_ind=:client_name_ind, client_name_ara=:client_name_ara, client_name_jap=:client_name_jap, client_description_eng=:client_description_eng, client_description_rus=:client_description_rus, client_description_tur=:client_description_tur, client_description_spa=:client_description_spa, client_description_fre=:client_description_fre, client_description_man=:client_description_man, client_description_can=:client_description_can, client_description_ger=:client_description_ger, client_description_per=:client_description_per, client_description_kor=:client_description_kor, client_description_mal=:client_description_mal, client_description_ind=:client_description_ind, client_description_ara=:client_description_ara, client_description_jap=:client_description_jap';
		$execute=array(
						':description_eng'=>$_POST['description_eng'],
						':description_rus'=>$_POST['description_rus'],
						':description_tur'=>$_POST['description_tur'],
						':description_spa'=>$_POST['description_spa'],
						':description_fre'=>$_POST['description_fre'],
						':description_man'=>$_POST['description_man'],
						':description_can'=>$_POST['description_can'],
						':description_ger'=>$_POST['description_ger'],
						':description_per'=>$_POST['description_per'],
						':description_kor'=>$_POST['description_kor'],
						':description_mal'=>$_POST['description_mal'],
						':description_ind'=>$_POST['description_ind'],
						':description_ara'=>$_POST['description_ara'],
						':description_jap'=>$_POST['description_jap'],
						':client_name_eng'=>stripcleantohtml($_POST['client_name_eng']),
						':client_name_rus'=>stripcleantohtml($_POST['client_name_rus']),
						':client_name_tur'=>stripcleantohtml($_POST['client_name_tur']),
						':client_name_spa'=>stripcleantohtml($_POST['client_name_spa']),
						':client_name_fre'=>stripcleantohtml($_POST['client_name_fre']),
						':client_name_man'=>stripcleantohtml($_POST['client_name_man']),
						':client_name_can'=>stripcleantohtml($_POST['client_name_can']),
						':client_name_ger'=>stripcleantohtml($_POST['client_name_ger']),
						':client_name_per'=>stripcleantohtml($_POST['client_name_per']),
						':client_name_kor'=>stripcleantohtml($_POST['client_name_kor']),
						':client_name_mal'=>stripcleantohtml($_POST['client_name_mal']),
						':client_name_ind'=>stripcleantohtml($_POST['client_name_ind']),
						':client_name_ara'=>stripcleantohtml($_POST['client_name_ara']),
						':client_name_jap'=>stripcleantohtml($_POST['client_name_jap']),
						':client_description_eng'=>stripcleantohtml($_POST['client_description_eng']),
						':client_description_rus'=>stripcleantohtml($_POST['client_description_rus']),
						':client_description_tur'=>stripcleantohtml($_POST['client_description_tur']),
						':client_description_spa'=>stripcleantohtml($_POST['client_description_spa']),
						':client_description_fre'=>stripcleantohtml($_POST['client_description_fre']),
						':client_description_man'=>stripcleantohtml($_POST['client_description_man']),
						':client_description_can'=>stripcleantohtml($_POST['client_description_can']),
						':client_description_ger'=>stripcleantohtml($_POST['client_description_ger']),
						':client_description_per'=>stripcleantohtml($_POST['client_description_per']),
						':client_description_kor'=>stripcleantohtml($_POST['client_description_kor']),
						':client_description_mal'=>stripcleantohtml($_POST['client_description_mal']),
						':client_description_ind'=>stripcleantohtml($_POST['client_description_ind']),
						':client_description_ara'=>stripcleantohtml($_POST['client_description_ara']),
						':client_description_jap'=>stripcleantohtml($_POST['client_description_jap'])
						);
		$where='WHERE id='.base64_decode($_GET['id']).'';
		update(MANAGE_TESTIMONIALS, $field, $where, $execute);

		if($gallery_photo!='')
		{
			if($photo_details = find('first', MANAGE_TESTIMONIALS, 'testimonials_image', "WHERE id = ".base64_decode($_GET['id'])."", array()))
			{
				if($photo_details['testimonials_image']!='' && file_exists('../images/testmonials/'.$photo_details['testimonials_image']))
				{
					unlink('../images/testmonials/'.$photo_details['testimonials_image']);
				}
			}

			$image_name = base64_decode($_GET['id']).'_'.$gallery_photo;
			move_uploaded_file($_FILES['testimonials_photo']['tmp_name'], '../images/testmonials/'.$image_name);
			update(MANAGE_TESTIMONIALS, 'testimonials_image=:testimonials_image', 'WHERE id='.base64_decode($_GET['id']).'', array(':testimonials_image'=>$image_name));
		}
		if($gallery_photo2!='')
		{
			if($photo_details2 = find('first', MANAGE_TESTIMONIALS, 'testimonials_icon', "WHERE id = ".base64_decode($_GET['id'])."", array()))
			{
				if($photo_details2['testimonials_icon']!='' && file_exists('../images/testmonials/'.$photo_details2['testimonials_icon']))
				{
					unlink('../images/testmonials/'.$photo_details2['testimonials_icon']);
				}
			}

			$image_name2 = base64_decode($_GET['id']).'_'.$gallery_photo2;
			move_uploaded_file($_FILES['testimonials_icon']['tmp_name'], '../images/testmonials/'.$image_name2);
			update(MANAGE_TESTIMONIALS, 'testimonials_icon=:testimonials_icon', 'WHERE id='.base64_decode($_GET['id']).'', array(':testimonials_icon'=>$image_name2));
		}
		
		header('location:'.DOMAIN_NAME_PATH_ADMIN.'admin/list_testimonials.php?mode=update');
	}
}
$photo = find('first', MANAGE_TESTIMONIALS, '*', "WHERE id = ".base64_decode($_GET['id'])."", array());


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo ADMIN_PAGE_TITLE?> | Homepage Slider | Add New Slider</title>
    <?php include_once('includes/scripts.php')?>

	<script type="text/javascript">
	<!--
		(function($,W,D)
		{
			var JQUERY4U = {};

			JQUERY4U.UTIL =
			{
				setupFormValidation: function()
				{
					//form validation rules
					$("#add-form").validate({
						rules: {
							description_eng: "required",
							description_rus: "required", 
							description_tur: "required", 
							description_spa: "required", 
							description_fre: "required", 
							description_man: "required", 
							description_can: "required", 
							description_ger: "required", 
							description_per: "required", 
							description_kor: "required", 
							description_mal: "required", 
							description_ind: "required", 
							description_ara: "required", 
							description_jap: "required", 
							client_name_eng: "required", 
							client_name_rus: "required", 
							client_name_tur: "required", 
							client_name_spa: "required", 
							client_name_fre: "required", 
							client_name_man: "required", 
							client_name_can: "required", 
							client_name_ger: "required", 
							client_name_per: "required", 
							client_name_kor: "required", 
							client_name_mal: "required", 
							client_name_ind: "required", 
							client_name_ara: "required", 
							client_name_jap: "required",
							client_description_eng: "required",
							client_description_rus: "required", 
							client_description_tur: "required", 
							client_description_spa: "required", 
							client_description_fre: "required", 
							client_description_man: "required", 
							client_description_can: "required", 
							client_description_ger: "required", 
							client_description_per: "required", 
							client_description_kor: "required", 
							client_description_mal: "required", 
							client_description_ind: "required", 
							client_description_ara: "required", 
							client_description_jap: "required",
						},
						messages: {
							description_eng: "<font color = 'red'><b>Please enteer testimonial (English)</b></font>",
							description_rus: "<font color = 'red'><b>Please enteer testimonial (Russian)</b></font>", 
							description_tur: "<font color = 'red'><b>Please enteer testimonial (Turkish)</b></font>", 
							description_spa: "<font color = 'red'><b>Please enteer testimonial (Spanish)</b></font>", 
							description_fre: "<font color = 'red'><b>Please enteer testimonial (French)</b></font>", 
							description_man: "<font color = 'red'><b>Please enteer testimonial (Mandarin)</b></font>", 
							description_can: "<font color = 'red'><b>Please enteer testimonial (Cantonese)</b></font>", 
							description_ger: "<font color = 'red'><b>Please enteer testimonial (German)</b></font>", 
							description_per: "<font color = 'red'><b>Please enteer testimonial (Persian)</b></font>", 
							description_kor: "<font color = 'red'><b>Please enteer testimonial (Korean)</b></font>", 
							description_mal: "<font color = 'red'><b>Please enteer testimonial (Malay)</b></font>", 
							description_ind: "<font color = 'red'><b>Please enteer testimonial (Indonesian)</b></font>", 
							description_ara: "<font color = 'red'><b>Please enteer testimonial (Arabic)</b></font>", 
							description_jap: "<font color = 'red'><b>Please enteer testimonial (Japanese)</b></font>",
							client_name_eng: "<font color = 'red'><b>Please enteer client name (English)</b></font>",
							client_name_rus: "<font color = 'red'><b>Please enteer client name (Russian)</b></font>", 
							client_name_tur: "<font color = 'red'><b>Please enteer client name (Turkish)</b></font>", 
							client_name_spa: "<font color = 'red'><b>Please enteer client name (Spanish)</b></font>", 
							client_name_fre: "<font color = 'red'><b>Please enteer client name (French)</b></font>", 
							client_name_man: "<font color = 'red'><b>Please enteer client name (Mandarin)</b></font>", 
							client_name_can: "<font color = 'red'><b>Please enteer client name (Cantonese)</b></font>", 
							client_name_ger: "<font color = 'red'><b>Please enteer client name (German)</b></font>", 
							client_name_per: "<font color = 'red'><b>Please enteer client name (Persian)</b></font>", 
							client_name_kor: "<font color = 'red'><b>Please enteer client name (Korean)</b></font>", 
							client_name_mal: "<font color = 'red'><b>Please enteer client name (Malay)</b></font>", 
							client_name_ind: "<font color = 'red'><b>Please enteer client name (Indonesian)</b></font>", 
							client_name_ara: "<font color = 'red'><b>Please enteer client name (Arabic)</b></font>", 
							client_name_jap: "<font color = 'red'><b>Please enteer client name (Japanese)</b></font>",
							client_description_eng: "<font color = 'red'><b>Please enteer client description (English)</b></font>",
							client_description_rus: "<font color = 'red'><b>Please enteer client description (Russian)</b></font>", 
							client_description_tur: "<font color = 'red'><b>Please enteer client description (Turkish)</b></font>", 
							client_description_spa: "<font color = 'red'><b>Please enteer client description (Spanish)</b></font>", 
							client_description_fre: "<font color = 'red'><b>Please enteer client description (French)</b></font>", 
							client_description_man: "<font color = 'red'><b>Please enteer client description (Mandarin)</b></font>", 
							client_description_can: "<font color = 'red'><b>Please enteer client description (Cantonese)</b></font>", 
							client_description_ger: "<font color = 'red'><b>Please enteer client description (German)</b></font>", 
							client_description_per: "<font color = 'red'><b>Please enteer client description (Persian)</b></font>", 
							client_description_kor: "<font color = 'red'><b>Please enteer client description (Korean)</b></font>", 
							client_description_mal: "<font color = 'red'><b>Please enteer client description (Malay)</b></font>", 
							client_description_ind: "<font color = 'red'><b>Please enteer client description (Indonesian)</b></font>", 
							client_description_ara: "<font color = 'red'><b>Please enteer client description (Arabic)</b></font>", 
							client_description_jap: "<font color = 'red'><b>Please enteer client description (Japanese)</b></font>",
						},
						submitHandler: function(form) {
							form.submit();
						}
					});
				}
			}

			//when the dom has loaded setup form validation rules
			$(D).ready(function($) {
				JQUERY4U.UTIL.setupFormValidation();
			});

		})(jQuery, window, document);
	//-->
	</script>
	<style>.pad_mar_bod {background-color: #eee;padding: 5px 0px;margin: 0px 0px 10px 0px;}</style>
</head>
<body>
    <div>       
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <?php include_once('includes/admin_header.php')?>
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <?php include_once('includes/left_navigation.php');?>
            <!--END SIDEBAR MENU-->
       
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">Testimonials</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active"><a href="list_testimonials.php">Testimonials</a></li>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Edit Testimonial</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->

                <!--BEGIN CONTENT-->
               <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-12">
								<div id="notify_msg_div"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
										<div class="panel panel-grey">
										<div class="panel-heading">Edit Testimonial</div>
											<div class="panel-body">
												<div class="panel-body pan">
													<form name = "add-form" id = "add-form" action = "" method = "POST" enctype = "multipart/form-data">
														<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="inputName" class="control-label">
																	Upload Photo
																</label>
																<div class="input-icon right">
																	<input name = "testimonials_photo" id = "testimonials_photo" placeholder="Inlcude some file" type="file" onChange="ValidateFileUpload()"/>
																</div>
															</div>
															<div class="form-group">
																<?php
																	$exist_file = "../images/testmonials/".$photo['testimonials_image'];
																		if (file_exists($exist_file))
																			{
																?>
																				<img src="../images/testmonials/<?php echo $photo['testimonials_image'] ;?>" style="height:70px;" id="blah"><br><br>
																<?php
																			}
																			else
																			{
																?>
																				<img src="../images/misc/no_image.jpg" style="width:auto; height:100px;" id="blah"><br><br>
																<?php
																			}
																?>	
															</div>	
														</div>	
														<div class="col-md-6">														
																<div class="form-group">
																	<label for="inputName" class="control-label">
																		Upload Icon
																	</label>
																	<div class="input-icon right">
																		<input name = "testimonials_icon" id = "testimonials_icon" placeholder="Inlcude some file" type="file" onChange="ValidateFileUpload()"/>
																	</div>
																</div>
																<div class="form-group">
																	<?php
																		$exist_file = "../images/testmonials/".$photo['testimonials_icon'];
																			if (file_exists($exist_file))
																				{
																	?>
																					<img src="../images/testmonials/<?php echo $photo['testimonials_icon'] ;?>" style="width:80px;" id="blah"><br><br>
																	<?php
																				}
																				else
																				{
																	?>
																					<img src="../images/misc/no_image.jpg" style="width:auto; height:80px;" id="blah"><br><br>
																	<?php
																				}
																	?>	
																</div>
															</div>
										<!--+++++++++++++++++++++++++++++++++++++++++++++++++++++-->
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (English)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_eng" id = "description_eng" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_eng']) && $_POST['description_eng']!='' ? $_POST['description_eng'] : $photo['description_eng'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (English)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_eng" id = "client_name_eng" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_eng']) && $_POST['client_name_eng']!='' ? $_POST['client_name_eng'] : $photo['client_name_eng'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (English)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_eng" id = "client_description_eng" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_eng']) && $_POST['client_description_eng']!='' ? $_POST['client_description_eng'] : $photo['client_description_eng'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (Russian)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_rus" id = "description_rus" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_rus']) && $_POST['description_rus']!='' ? $_POST['description_rus'] : $photo['description_rus'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (Russian)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_rus" id = "client_name_rus" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_rus']) && $_POST['client_name_rus']!='' ? $_POST['client_name_rus'] : $photo['client_name_rus'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (Russian)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_rus" id = "client_description_rus" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_rus']) && $_POST['client_description_rus']!='' ? $_POST['client_description_rus'] : $photo['client_description_rus'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (Turkish)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_tur" id = "description_tur" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_tur']) && $_POST['description_tur']!='' ? $_POST['description_tur'] : $photo['description_tur'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (Turkish)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_tur" id = "client_name_tur" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_tur']) && $_POST['client_name_tur']!='' ? $_POST['client_name_tur'] : $photo['client_name_tur'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (Turkish)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_tur" id = "client_description_tur" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_tur']) && $_POST['client_description_tur']!='' ? $_POST['client_description_tur'] : $photo['client_description_tur'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (Spanish)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_spa" id = "description_spa" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_spa']) && $_POST['description_spa']!='' ? $_POST['description_spa'] : $photo['description_spa'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (Spanish)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_spa" id = "client_name_spa" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_spa']) && $_POST['client_name_spa']!='' ? $_POST['client_name_spa'] : $photo['client_name_spa'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (Spanish)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_spa" id = "client_description_spa" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_spa']) && $_POST['client_description_spa']!='' ? $_POST['client_description_spa'] : $photo['client_description_spa'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (French)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_fre" id = "description_fre" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_fre']) && $_POST['description_fre']!='' ? $_POST['description_fre'] : $photo['description_fre'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (French)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_fre" id = "client_name_fre" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_fre']) && $_POST['client_name_fre']!='' ? $_POST['client_name_fre'] : $photo['client_name_fre'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (French)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_fre" id = "client_description_fre" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_fre']) && $_POST['client_description_fre']!='' ? $_POST['client_description_fre'] : $photo['client_description_fre'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (Mandarin)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_man" id = "description_man" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_man']) && $_POST['description_man']!='' ? $_POST['description_man'] : $photo['description_man'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (Mandarin)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_man" id = "client_name_man" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_man']) && $_POST['client_name_man']!='' ? $_POST['client_name_man'] : $photo['client_name_man'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (Mandarin)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_man" id = "client_description_man" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_man']) && $_POST['client_description_man']!='' ? $_POST['client_description_man'] : $photo['client_description_man'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (Cantonese)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_can" id = "description_can" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_can']) && $_POST['description_can']!='' ? $_POST['description_can'] : $photo['description_can'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (Cantonese)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_can" id = "client_name_can" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_can']) && $_POST['client_name_can']!='' ? $_POST['client_name_can'] : $photo['client_name_can'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (Cantonese)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_can" id = "client_description_can" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_can']) && $_POST['client_description_can']!='' ? $_POST['client_description_can'] : $photo['client_description_can'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (German)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_ger" id = "description_ger" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_ger']) && $_POST['description_ger']!='' ? $_POST['description_ger'] : $photo['description_ger'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (German)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_ger" id = "client_name_ger" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_ger']) && $_POST['client_name_ger']!='' ? $_POST['client_name_ger'] : $photo['client_name_ger'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (German)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_ger" id = "client_description_ger" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_ger']) && $_POST['client_description_ger']!='' ? $_POST['client_description_ger'] : $photo['client_description_ger'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (Persian)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_per" id = "description_per" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_per']) && $_POST['description_per']!='' ? $_POST['description_per'] : $photo['description_per'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (Persian)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_per" id = "client_name_per" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_per']) && $_POST['client_name_per']!='' ? $_POST['client_name_per'] : $photo['client_name_per'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (Persian)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_per" id = "client_description_per" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_per']) && $_POST['client_description_per']!='' ? $_POST['client_description_per'] : $photo['client_description_per'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (Korean)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_kor" id = "description_kor" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_kor']) && $_POST['description_kor']!='' ? $_POST['description_kor'] : $photo['description_kor'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (Korean)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_kor" id = "client_name_kor" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_kor']) && $_POST['client_name_kor']!='' ? $_POST['client_name_kor'] : $photo['client_name_kor'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (Korean)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_kor" id = "client_description_kor" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_kor']) && $_POST['client_description_kor']!='' ? $_POST['client_description_kor'] : $photo['client_description_kor'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (Malay)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_mal" id = "description_mal" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_mal']) && $_POST['description_mal']!='' ? $_POST['description_mal'] : $photo['description_mal'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (Malay)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_mal" id = "client_name_mal" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_mal']) && $_POST['client_name_mal']!='' ? $_POST['client_name_mal'] : $photo['client_name_mal'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (Malay)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_mal" id = "client_description_mal" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_mal']) && $_POST['client_description_mal']!='' ? $_POST['client_description_mal'] : $photo['client_description_mal'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (Indonesian)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_ind" id = "description_ind" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_ind']) && $_POST['description_ind']!='' ? $_POST['description_ind'] : $photo['description_ind'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (Indonesian)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_ind" id = "client_name_ind" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_ind']) && $_POST['client_name_ind']!='' ? $_POST['client_name_ind'] : $photo['client_name_ind'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (Indonesian)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_ind" id = "client_description_ind" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_ind']) && $_POST['client_description_ind']!='' ? $_POST['client_description_ind'] : $photo['client_description_ind'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (Arabic)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_ara" id = "description_ara" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_ara']) && $_POST['description_ara']!='' ? $_POST['description_ara'] : $photo['description_ara'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (Arabic)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_ara" id = "client_name_ara" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_ara']) && $_POST['client_name_ara']!='' ? $_POST['client_name_ara'] : $photo['client_name_ara'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (Arabic)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_ara" id = "client_description_ara" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_ara']) && $_POST['client_description_ara']!='' ? $_POST['client_description_ara'] : $photo['client_description_ara'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-12 pad_mar_bod ">	
																<div class="col-md-6">	
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Testimonial (Japanese)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "description_jap" id = "description_jap" class="form-control" style="resize:none;height: 145px;"><?php echo isset($_POST['description_jap']) && $_POST['description_jap']!='' ? $_POST['description_jap'] : $photo['description_jap'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Name (Japanese)
																		</label>
																		<div class="input-icon right">
																			<input name = "client_name_jap" id = "client_name_jap" type="text" placeholder="" class="form-control" value="<?php echo isset($_POST['client_name_jap']) && $_POST['client_name_jap']!='' ? $_POST['client_name_jap'] : $photo['client_name_jap'] ?>"/>
																		</div>
																	</div>
																	<div class="form-group">
																		<label for="inputEmail" class="control-label">
																			Client Description (Japanese)
																		</label>
																		<div class="input-icon right">
																			<textarea name = "client_description_jap" id = "client_description_jap" class="form-control" style="resize:none;"><?php echo isset($_POST['client_description_jap']) && $_POST['client_description_jap']!='' ? $_POST['client_description_jap'] : $photo['client_description_jap'] ?></textarea>
																		</div>
																	</div>
																</div>
															</div>
										<!--------------------++++++++++++--------------------------->
																													

															
														</div>
													
														<div class="form-actions text-right pal">
															<button type="submit" class="btn btn-primary" name = "btn_submit" id = "btn_submit">Update</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <?php include_once('includes/admin_footer.php');?>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   </body>
</html>
<?php
if(isset($_SESSION['SET_FLASH']))
{
	if($_SESSION['SET_TYPE']=='error')
	{
		echo "<script type='text/javascript'>showError('".$_SESSION['SET_FLASH']."');</script>";
	}
	if($_SESSION['SET_TYPE']=='success')
	{
		echo "<script type='text/javascript'>showSuccess('".$_SESSION['SET_FLASH']."');</script>";
	}
}
unset($_SESSION['SET_FLASH']);
unset($_SESSION['SET_TYPE']);
$db=NULL;
?>
	<script>
     	function ValidateFileUpload() {
			var fuData = document.getElementById('slider_image');
			var FileUploadPath = fuData.value;

//To check if user upload any file
       
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

		if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

// To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

//The file upload is NOT an image
		else {
             	   alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
					document.getElementById('slider_image').value = '';
            }
    }
</script>
