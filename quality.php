<?php

include('init.php');

$about_content = find('first', STATIC_PAGE_ABOUT_US, '*', "WHERE id = 1", array());

?>

<!DOCTYPE html>

<html lang="en">

<head>

	<title>Our quality</title>

	<meta name="description" content="<?=$about_content['meta_description'];?>">

	<meta name="keywords" content="<?=$about_content['meta_keyward'];?>">

	<link rel='shortcut icon' type='image/x-icon' href='<?php echo(DOMAIN_NAME_PATH);?>images/misc/<?=$general_settings['favicon'];?>' />

	<?php include('meta.php');?>

</head>

<body>		

	<?php include('mobile_menu.php');?>

	<?php include('topbar.php');?>

	<?php include('inner_page_header.php');?>	 	 

	<div class="inner_section">

		<div class="container">

			<div class="row">

				<div class="col-md-12 lpd">

					<div class="">

						<div class="col-xs-12 npd">

							<h1 class="con-hd con-hdbig">Our quality</h1>

						</div>

						<div class="clearfix"></div>						

					</div>

					

					<div class="sp-body">

						<div class="col-xs-12 col-md-5 npd">

							<!-- 	 <?=$about_content['page_content_'.$_SESSION['lan']];?>  -->

							<p class="text-center"><strong>Maven International conforms to ISO 17100:2015 quality standard for translation services</strong></p>		
							<p><strong>Why ISO 17100:2015</strong></p>				
							<p class="text-justify">ISO 17100:2015 is the only quality standard specifically developed for the translation industry. Other standards used for translation have little to no relevance. 												
							</p> <br> 
							<p><strong>Scope</strong></p>							
							<p class="text-justify">ISO 17100:2015 covers the entire translation process, from service agreements and quoting to the qualification criteria of assigned translators and revisers. The primary focus, however, is on the translation process and confidentiality. Projects must be assigned to at least two separate linguists: a translator and a reviser. Both would have to meet the minimum criteria required (specialized academic qualifications or experience).</p> <br>
							<p><strong>How does ISO 17100:2015 benefit you</strong></p>
							<p class="text-justify">ISO complied translations benefit you in many ways. Firstly, you’ll be confident that your document is in the right hands. Only translators who meet the required academic qualifications, experience, and language skills will be assigned to our translation projects. The 2-step translation process, performed by separate linguists, significantly minimizes chances of mistranslation.</p> <br> 
							<p class="text-justify">In terms of confidentiality, clients specify the exact period of time in which their files may remain in our cloud (including emails and physical storage facilities). Clients may opt for files to be deleted immediately after the translation process is concluded, or a certain number of days after the project.</p>
						</div>
						
						<div class="col-md-6 col-md-offset-1 npd">

							<img src="<?php echo(DOMAIN_NAME_PATH);?>images/about_us/quality.jpg" onerror="this.src='<?=DOMAIN_NAME_PATH;?>images/misc/no_image.jpg';" width="100%" height="auto" border="0" alt="">

						</div>

						<div class="clearfix"></div>						

					</div>

				</div>

				<!-- <div class="col-md-4 rpd hidden-xs">

					<div class="sp-body" >

						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='<?php echo(DOMAIN_NAME_PATH);?>faq.php'"><?php echo(LANG_7);?></h1>

						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='<?php echo(DOMAIN_NAME_PATH);?>contactos.php'"><?php echo(LANG_6);?></h1>

						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='http://grtechdemo.com/maven/privacy.php'"><?php echo(LANG_77);?></h1>

					</div>

				</div> -->

				<div class="clearfix"></div>

			</div>

		</div>

	</div>

	<?php include('footer.php');?>

	<?php include('script.php');?>

</body>

</html>