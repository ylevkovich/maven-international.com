<meta charset="utf-8">
<meta name="msvalidate.01" content="AD0174A8D2D88BA6FF6005A68DEEE8B1" />
<!-- <meta charset="UTF-8"> -->
<meta name="google-site-verification" content="rcNpJUISDzOZ58PsBDhcSe-ThL6EhtB7NmjvJ1aVDWU" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" charset="utf8">

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="css/mdp.css">

<link href="css/bootstrap.css" rel="stylesheet">

<link href="css/style.css?v=1.0.0" rel="stylesheet">

<link href="css/responsive.css" rel="stylesheet">

<link rel="stylesheet" href="css/swiper.css">

<link rel="stylesheet" type="text/css" href="css/plugins.css" />

<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>

<link rel="stylesheet" href="css/notifyBar.css" type="text/css"/>

<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<link href="css/select2.min.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

<!--[if lt IE 9]>

<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

<![endif]-->



<script type="text/javascript">

	function set_status(val)

		{

			$.post("set_language.php?id="+val,function(data){

				window.location.reload();

			});

		}

</script>
