
<form class="form-horizontal cform cform-res" class="contactform" id="contactform" method = "POST" enctype = "multipart/form-data">
	<ul class="nav nav-tabs hidden-xs">
		<li class="active col-sm-6 pik-lang lround tabone" id="tabone">
			<a  href="javascript:void(0)"><?php echo(LANG_26);?></a>
		</li>		
		<li class="pik-lang rround col-sm-6 tabtwo" id="tabtwo">
			<a  href="javascript:void(0)"><?php echo(LANG_84);?></a>
		</li>
	</ul>

<!--    Consecutive Interpretation form-->
	<div class="tab-content">
		<div id="open_one" class="tab-pane active open_one">
		    <div style="height:50px"></div>
		    <div class="clearfix"></div>
		    <div id="login" class="animate form">
			    <h2 style="border:0px solid #000; padding: 0px"><?php echo(LANG_29);?></h2>
			    <div id="lang_pair_<?php echo $id; ?>">
				    <!-- lang A -->
						<div class="form-group">
							<label class="control-label col-xs-4" for ="source-language"><?php echo(LANG_79);?></label>
							<div class="col-xs-8 " >
							  	<select name="source-language[]" dir="ltr" class="form-control cval one_<?php echo $id; ?> validate[required]">
									<option value="" ><?php echo(LANG_17);?></option>
									<?php

										if(!empty($languages['right']))
										{
											foreach($languages['right'] as $lan_key => $lan_value)
											{
									?>
									<option value="<?=$lan_value['name'];?>" <?php echo(isset($_SESSION['source_language']) && $_SESSION['source_language'] == $lan_value['name'] ? 'selected':'' )?>><?=$lan_value['name'];?></option>
									<?php
											}
										}
										else
										{
									?>
									<option value=""><?php echo(LANG_14);?></option>
									<?php
										}
									?>
							  	</select>
							  	<div class="error errorone errorone_<?php echo $id; ?>" id="errorone"><?php echo(LANG_105);?></div>
							</div>						
						</div>	
						<!-- lang B	-->
						<div class="form-group">
							<label class="control-label col-xs-4" for="translate_to"><?php echo(LANG_80);?></label>
							<div class="col-xs-8 mlts"> 	
								<select  class="form-control cval two1_<?php echo $id; ?>" name="translate_to[]" dir="ltr">
								  	<option value="" ><?php echo(LANG_17);?></option>
									<?php
										if(!empty($languages['right']))
										{
											foreach($languages['right'] as $lan_key => $lan_value)
											{
									?>
									<option value="<?=$lan_value['name'];?>" <?php echo(isset($_SESSION['translate_to']) && $_SESSION['translate_to'] == $lan_value['name'] ? 'selected':'' )?>><?=$lan_value['name'];?></option>
									<?php
											}
										}
										else
										{
									?>
									<option value=""><?php echo(LANG_14);?></option>
									<?php
										}
									?>
							  	</select>
							   	<div class="error errortwo1 errortwo1_<?php echo $id; ?>" id="errortwo1"><?php echo(LANG_106);?></div>
							</div>
						</div>	
						<!-- date			  	 -->
					  	<div class="form-group">
							<label class="control-label col-xs-4 tsub" ><?php echo(LANG_32);?> *<br class="hidden-xs"><?php echo(LANG_33);?></label>
							
							<div class="col-sm-8" >
								
								<input type="text" class="form-control multiDatespicker deadline_<?php echo $id; ?>" name="date_required[]" value="<?php echo(isset($_SESSION['deadline']) && $_SESSION['deadline'] !='' ? $_SESSION['deadline'] : '')?>" placeholder="<?php echo(LANG_21);?>" />
							   	<div class="error errorthree" id="errorthree"><?php echo(LANG_107);?></div>
							   	<div class="clearfix"></div>
							   	<div style="margin:10px 0px " class="text-right"></div>
							</div>
					  	</div>

                    <?php
                    if($id!='Ng'){
                        ?>
                        <!-- interpreter -->
                        <div class="form-group" >
                            <label class="control-label col-xs-4 tsub" ><?php echo(LANG_37);?>
                                <br>
                                <?php

                                if($id!='OA'){	?>
                                    <p id="min">(<?php echo(LANG_38);?>)</p>
                                <?php	}	?>
                            </label>
                            <div class="col-sm-8" dir="ltr">

                                <input type="text" class="form-control p20" name="minimum_required" onKeyPress="return number(event)"  >
                                <div class="error" id=""><?php echo(LANG_15);?></div>

                            </div>
                        </div>
                    <?php	}	?>

					  <!-- add more	button -->
					  	<h2 style="border:0px solid #000;padding-top:0px; margin-top:-23px !important">
					  		<div id="remove_<?php echo $id; ?>" class="small" onclick="removeText_2('<?php echo $id; ?>')" style="display: none;"><?php echo(LANG_81);?></div>
					  		<div class="small" onclick="appendText_2('<?php echo $id; ?>')"><?php echo(LANG_30);?></div>
							<div class="clearfix"></div>
					  	</h2>
				</div>				  
				<hr>
				

				<!-- where -->
				<!-- <div class="form-group" >
					<label class="control-label col-xs-4 tsub" ><?php echo(LANG_39);?></label>
					<div class="col-sm-8 ">
					
						<input type="text" class="form-control p20" name="require_service" placeholder="<?php echo(LANG_120);?>" ><
					   	<div class="error" id=""><?php echo(LANG_15);?></div>
					   
					</div>
				</div>
				<hr>	 -->				
				<input type="hidden" name="id" class="form_id" value="<?php echo $id; ?>" />
				<div class="form-group " style="margin-bottom:0px">
					<div class="col-md-12 text-right">				  
						<button class="watch text-b bttm_gp piklan next-two" id="add_text2" type="button" style="background:#7197b6"><?php echo(LANG_22);?></button>
					</div>
				</div>
			</div>
		</div>
		<div id="open_two" class="tab-pane open_two">
			<div style="height:50px" class="hidden-xs"></div>
			<div id="login" class="animate form">
			<?php if($id!='OA'){?>
				<!-- Event topic -->
				<div class="form-group">
					<label class="control-label col-xs-4" ><?php echo(LANG_41);?></label>
					<div class="col-xs-8">
					<!-- LANG_121 -->
						<input type="text" class="form-control validate[required] " name="event_topic" placeholder="<?php echo(LANG_121);?>" value="" data-errormessage-value-missing="<?php echo(LANG_108);?>">
					</div>
				</div>				
				<!-- event address -->
				<div class="form-group">
					<label class="control-label col-xs-4 tsub" ><?php echo(LANG_44);?></label>
					<div class="col-sm-8">
						<!-- <textarea name="event_address" rows="" cols="" class="form-control validate[required] p20" style="resize:none" data-errormessage-value-missing="<?php echo(LANG_15);?>"></textarea> -->
						<textarea name="event_address" rows="" cols="" class="form-control p20" placeholder="<?php echo(LANG_120);?>" style="resize:none"></textarea><!-- LANG_120 -->
					</div>
				</div>
                <!-- equipment -->
                <div id="radio_<?php echo $id; ?>" class="form-group">
                    <label class="control-label col-xs-4 tsub" >
                        Do you require Simultaneous Interpretation Equipment?
                    </label>
                    <div class="col-sm-8 text-left tsen">
                        <input 
                            type="radio" 
                            name="equipment_request" 
                            id="equipment_yes" 
                            value="1" />
                        <label for="equipment_yes">Yes</label>
                        <input type="radio" name="equipment_request" id="equipment_no" value="0">
                        <label for="equipment_no">No</label>

                        <div class = "form-inline equipment_input" hidden="hidden">
                            <label class="control-label col-xs-3"><?php echo(LANG_90);?> </label>
                            <input type="text" name="type_cert" id="type_cert" class="form-control  " /> <!-- LANG_119 -->
                            <div class="col-xs-12" style="padding-top: 5px; font-size: smaller;">
                                <p>If yes please specify the number of attendants who require interpretation.We need to know this number in order to quote for the number of headsets.</p>
                            </div>
                        </div>

                        <input type="hidden" name="type_equipment" id="type_equipment" disabled="disabled" value="" readonly  onfocus="this.value='';" class="form-control"  /><!-- LANG_119-->
                    </div>
                </div>
			<?php }else{?>
				<!-- court address -->
				<div class="form-group">
					<label class="control-label col-xs-4 tsub" ><?php echo(LANG_123);?></label>
					<div class="col-sm-8">
						<!-- <textarea name="event_address" rows="" cols="" class="form-control validate[required] p20" style="resize:none" data-errormessage-value-missing="<?php echo(LANG_15);?>"></textarea> -->
						<textarea name="event_address" rows="" cols="" class="form-control p20" placeholder="<?php echo(LANG_120);?>" style="resize:none"></textarea><!-- LANG_120 -->
					</div>
				</div>
			<?php }?>
				<!-- request type -->
				<div id="radio_<?php echo $id; ?>" class="form-group">
                                    <label class="control-label col-xs-4 tsub" ><?php echo(LANG_86);?></label>
                                    <div class="col-sm-8 text-left tsen">
                                        <input 
                                            type="radio" 
                                            name="corporate_request" 
                                            class="validate[required]" 
                                            value="Private" 
                                            data-errormessage-value-missing="<?php echo(LANG_99);?>" 
                                            data-prompt-position="centerTop: 180,46" />
                                            <?php echo(LANG_46);?>&nbsp;&nbsp;
                                        <input type="radio"  name="corporate_request" value="Corporate"> <?php echo(LANG_47);?>&nbsp;&nbsp;
                                    </div>
				</div>
				<!-- name -->
				<div class="form-group">
					<label class="control-label col-xs-4" ><?php echo(LANG_48);?></label>
					<div class="col-xs-8">
						<input type="text" class="form-control validate[required]" name="name" data-errormessage-value-missing="<?php echo(LANG_100);?>" placeholder="<?php echo(LANG_115);?>"><!-- LANG_115 -->
					</div>
				</div>
		  	<!-- email -->
		  	<div class="form-group" >
		  		<label class="control-label col-xs-4" ><?php echo(LANG_49);?></label>
		  		<div class="col-xs-8">
		  			<input type="text" class="form-control validate[required],custom[email]" name="email_address" data-errormessage-value-missing="<?php echo(LANG_101);?>" placeholder="<?php echo(LANG_116);?>"><!-- LANG_116 -->
		  		</div>
		  	</div>	
		  	<!-- phone	-->
		  	<div class="form-group">
		  		<label class="control-label col-xs-4" ><?php echo(LANG_50);?></label>
		  		<div class="col-xs-8">
		  			<input type="text" class="form-control validate[required, custom[phone]]" name="phone_number" data-errormessage-value-missing="<?php echo(LANG_102);?>" placeholder="<?php echo(LANG_117);?>"><!-- LANG_117 -->
		  		</div>
		  	</div>
		  	<!-- company -->
		  	<div class="form-group" id="company_filed1"> 
		  	<label class="control-label col-xs-4 comp1" id="comp1"><?php echo(LANG_51);?></label>
		  		<div class="col-xs-8">
		  			<input type="text" class="form-control validate[required]" name="company" data-errormessage-value-missing="<?php echo(LANG_103);?>" placeholder="<?php echo(LANG_118);?>"><!-- LANG_118 -->
		  		</div>
		  	</div>
		  	<!-- additional info -->
		  	<div class="form-group"> 
			  	<label class="control-label col-xs-4"><?php echo(LANG_82);?></label>
			  	<div class="col-xs-8">
			  		<textarea name="additional_info" rows="" cols="" class="form-control p20" style="resize:none" ></textarea>
			  	</div>
		   	</div>
		   	<!-- captcha -->
		  	<div class="form-group">
                            <div class="g-recaptcha"></div>
                        </div>
		  	
		  	<div class="form-group " style="margin-bottom:0px">
				<div class="col-md-12 text-right">
				   	<button class="watch text-b bttm_gp piklan prev"   type="button"  id="pre_one" style="background:#7197b6"><?php echo(LANG_52);?></button>
				  	<button class="watch text-b bttm_gp piklan"  name="btnsubmit_form_two" type="submit"  style="background:#7197b6"><?php echo(LANG_83);?></button>
				</div>
		  	</div>
		</div>
	</div>
</div>
</form>
<!-- for add more -->
<script type="text/javascript">
function appendText_2(id) {
	var app = "#lang_pair_"+id;
    $(app).append(tmpHTML);
    var rem = "#remove_"+id;
    $(rem).show();

    var get = "lang_child_"+id;
	var all = app+" #"+get;
	var len = $(all).length;
	var all_len = len+1;
	// console.log(all_len);

	//var tmpHTML = $('#lang_pair').html();
	//var tmpHTML = "<input type=\"file\" name=\"file1\" onchange=\"changed()\">"; 
	var tmpHTML = "<div id='lang_child_"+id+"' style=\"padding:10px; margin-bottom:10px\">";
			//language A
			tmpHTML += "<div class=\"form-group\">";
				tmpHTML += "<label class=\"control-label col-xs-4\" ><?php echo(LANG_79);?></label>";
				tmpHTML += "<div class=\"col-xs-8 \" >";
				  	tmpHTML += "<select name=\"source-language[]\" dir=\"ltr\" class='form-control cval validate[required] one_"+id+"' >";
				  	tmpHTML += "<option value=\"\"><?php echo(LANG_17);?></option>";
						<?php
							if(!empty($languages['right']))
							{
								foreach($languages['right'] as $lan_key => $lan_value)
								{
						?>
						tmpHTML += "<option value=\"<?=$lan_value['name'];?>\"><?=$lan_value['name'];?></option>";
						<?php
								}
							}
							else
							{
						?>
						tmpHTML += "<option value=\"\"><?php echo(LANG_17);?></option>";
						<?php
							}
						?>
				  	tmpHTML += "</select>";
				  	tmpHTML += "<div class='error errorone errorone_"+id+"' id=\"errorone\"><?php echo(LANG_105);?></div>";
				tmpHTML += "</div>";				
		  	tmpHTML += "</div>";
		  	//language B						  
		  	tmpHTML += "<div class=\"form-group\" style=\"margin-bottom:0px\">";
				tmpHTML += "<label class='control-label col-xs-4' ><?php echo(LANG_80);?></label>";
				tmpHTML += "<div class='col-xs-8 mlts'> ";	
				  	tmpHTML += "<select  class='form-control cval two1_"+id+"' dir='ltr' name=\"translate_to[]\" required>";
						tmpHTML += "<option value=\"\"><?php echo(LANG_17);?></option>";
						<?php
							 if(!empty($languages['right']))
							 {
								 foreach($languages['right'] as $lan_key => $lan_value)
								 {
						?>
						tmpHTML += "<option value='<?=$lan_value['name'];?>'><?=$lan_value['name'];?></option>";
								<?php
								 }
							 }
							 else
							{
						 ?>
						tmpHTML += "<option value=''><?php echo(LANG_17);?></option>";
						<?php
							 }
						?>
				  	tmpHTML += "</select>";
				  	tmpHTML += "<div class='error errortwo1 errortwo1_"+id+"' id='errortwo1'><?php echo(LANG_106);?></div>";
				tmpHTML += "</div>";
		 	tmpHTML += " </div>";
		 	// date
		 	tmpHTML += "<br/><div class=\"form-group\" style=\"margin-bottom:0px\">";
				tmpHTML += "<label class='control-label col-xs-4 tsub' ><?php echo(LANG_32);?> <br class='hidden-xs'><?php echo(LANG_33);?></label>";
				tmpHTML += "<div class='col-sm-8'> ";	
				  	tmpHTML += "<input type='text' class='form-control multiDatespicker1 deadline_"+id+"' name=\"date_required[]\" required value='<?php echo(isset($_SESSION['deadline']) && $_SESSION['deadline'] !='' ? $_SESSION['deadline'] : '')?>'>";
				  	tmpHTML += "<div class='error errorthree1' id='errorthree'><?php echo(LANG_107);?></div>";
				  	tmpHTML += "<div class='clearfix'></div>";
				  	tmpHTML += "<div style=\"margin:10px 0px\" class='text-right'></div>";
				tmpHTML += "</div>";
		 	tmpHTML += " </div>";
		 	// tmpHTML += "<div class=\"form-group\" style=\"margin-bottom:0px\">";
				// tmpHTML += "<label class='control-label col-xs-4 tsub' ></label>";
				// tmpHTML += "<div class='clearfix hidden-sm hidden-md hidden-lg'></div>";	
				// tmpHTML += "<div class='col-sm-8 ad_padi'>";
				//   	tmpHTML += "<input type='radio' name=\"select_day["+all_len+"]\" value='Full Day' class='show_hour day_val' checked required > <?php echo(LANG_34);?>&nbsp;&nbsp;/&nbsp;&nbsp;<input type='radio' value='Half Day' name=\"select_day["+all_len+"]\" class='show_hour half_day day_val'> <?php echo(LANG_35);?>&nbsp;&nbsp;/&nbsp;&nbsp;<input type='radio' value='Show Hour' name=\"select_day["+all_len+"]\" class='attrInputs day_val'>&nbsp;&nbsp;";
				//   	tmpHTML += "<span class=''><input type='text' class='form-control five' name=\"select_hours[]\" onKeyPress=\"return number(event)\" style=\"border-radius:5px !important; width:70px; display:inline-block;\"> <?php echo(LANG_36);?><div class='error errorfive1' id='errorfive'><?php echo(LANG_96);?></div></span>";
				//   	tmpHTML += "<div class='error errorfour1' id='errorfour'><?php echo(LANG_95);?></div>";
				// tmpHTML += "</div>";
		 	// tmpHTML += " </div>";
		tmpHTML += " </div>";

	// alert(id);
	var app = "#lang_pair_"+id;
    $(app).append(tmpHTML);
    var rem = "#remove_"+id;
    $(rem).show();

    $('.multiDatespicker1').each(function(){
	    $(this).multiDatesPicker({ minDate: 0});
	});
	
    //onfocus=\"myFunction(this)\"
}
function removeText_2(id) {
	var app = "#lang_pair_"+id;
	var get = "lang_child_"+id;
	var elem = document.getElementById(get);
	var all = app+" #"+get;
	var len = $(all).length;
	var rem = "#remove_"+id;
	// alert(len);

	if (len == 1) {
		elem.remove();
		$(rem).hide();
	}
	else {
		elem.remove();
	}
	// console.log($("#lang_pair #lang_child").length);
}
</script>

<!-- for Private or Corporate Request* -->
<script type="text/javascript">
	$('input[name=corporate_request]').click(function () {
		var req = $("input[name='corporate_request']:checked").val();

		if (req == "Private") {
			$("input[name='company']").removeClass("validate[required]");
			$("input[name='company']").removeAttr( "data-errormessage-value-missing" );
			$(".comp1").empty();
			$(".comp1").append("<?php echo(LANG_85);?>");
		}
		else {
			$("input[name='company']").addClass("validate[required]");
			$("input[name='company']").attr( "data-errormessage-value-missing", "<?php echo(LANG_15);?>" );
			$(".comp1").empty();
			$(".comp1").append("<?php echo(LANG_51);?>");
		}
	// alert(req);
	});


    $('input[name=equipment_request]').click(function () {
        var x1 = $("input[name='equipment_request']:checked").val();
        if (x1 == "1") {
            $(".equipment_input").show();
        }
        else{
            $(".equipment_input").hide();
        }

    });
</script>


