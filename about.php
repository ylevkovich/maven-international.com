<?php

include('init.php');

$about_content = find('first', STATIC_PAGE_ABOUT_US, '*', "WHERE id = 1", array());

?>

<!DOCTYPE html>

<html lang="en">

<head>

	<title><?=$about_content['page_title'];?></title>

	<meta name="description" content="<?=$about_content['meta_description'];?>">

	<meta name="keywords" content="<?=$about_content['meta_keyward'];?>">

	<link rel='shortcut icon' type='image/x-icon' href='<?php echo(DOMAIN_NAME_PATH);?>images/misc/<?=$general_settings['favicon'];?>' />

	<?php include('meta.php');?>

</head>

<body>		

	<?php include('mobile_menu.php');?>

	<?php include('topbar.php');?>

	<?php include('inner_page_header.php');?>	 	 

	<div class="inner_section">

		<div class="container">

			<div class="row">

				<div class="col-md-12 lpd">

					<div class="">

						<div class="col-xs-12 npd">

							<h1 class="con-hd con-hdbig"><?php echo(LANG_24);?></h1>

						</div>

						<div class="clearfix"></div>						

					</div>

					

					<div class="sp-body">

						<div class="col-xs-12 col-md-5 npd">

							<!-- 	 <?=$about_content['page_content_'.$_SESSION['lan']];?>  -->

								
							<p class="text-justify">Headquartered in Kuala Lumpur, Malaysia, Maven International is a fast growing company offering esteemed, cost-effective language solutions. With over a decade of experience in managing language-related projects, we have partnered with top organizations worldwide, providing professional translation and interpretation services in over 100 languages. Our team of linguists has helped world leading brands overcome cultural barriers and embrace globalization. We take pride in our services and the resulting satisfaction. <br> <br>			
								With offices in Malaysia, Canada, and Turkey, we are geared and ready to serve the international community. Thanks to our skilled team of highly dedicated and experienced linguists, we strive to provide state-of-the-art language solutions capable of meeting various needs across a diverse group of industries. We are determined to excel and innovate in order to keep pace with a vastly expanding translation industry. Our research and development efforts aim to combine the expertise of human translators and the latest technology with the objective of providing efficient, customer-oriented language solutions. Reaching a global audience has never been easier!						
							</p> <br> 

							<p class="text-center"><strong>We provide translation and interpretation services in over 100 languages across three continents</strong></p>
							<p>Malaysia is considered a haven of diversity and multiculturalism, which makes it a perfect platform to excel and innovate in the art of breaking language barriers.
								In the early 2000s, a group of professional translators and interpreters based in Kuala Lumpur, Malaysia, collaborated with the objective of setting up a professional language service provider (LSP). 
								</p> <br> 
							<p><strong>The mission </strong></p>
							<p>То provide customer-oriented solutions with an emphasis on objectively assessable quality control methods based on internationally recognized standards. 
								Since then, Maven International has become one of the leading LSPs in South-East Asia. Quality has been the underlying factor leading to our success, with several government and private organizations relying on our services for nearly a decade. Our growth led to a breakthrough in the international market; with offices in Kuala Lumpur, Ottawa and Istanbul, we have now partnered with top organizations worldwide, providing state-of-the-art translation and interpretation services in over 100 languages. 
							</p>
						</div>
						
						<div class="col-md-6 col-md-offset-1 npd">

							<img src="<?php echo(DOMAIN_NAME_PATH);?>images/about_us/<?php echo($about_content['photo']);?>" onerror="this.src='<?=DOMAIN_NAME_PATH;?>images/misc/no_image.jpg';" width="100%" height="auto" border="0" alt="">

						</div>

						<div class="clearfix"></div>						

					</div>

				</div>

				<!-- <div class="col-md-4 rpd hidden-xs">

					<div class="sp-body" >

						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='<?php echo(DOMAIN_NAME_PATH);?>faq.php'"><?php echo(LANG_7);?></h1>

						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='<?php echo(DOMAIN_NAME_PATH);?>contactos.php'"><?php echo(LANG_6);?></h1>

						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='http://grtechdemo.com/maven/privacy.php'"><?php echo(LANG_77);?></h1>

					</div>

				</div> -->

				<div class="clearfix"></div>

			</div>

		</div>

	</div>

	<?php include('footer.php');?>

	<?php include('script.php');?>

</body>

</html>