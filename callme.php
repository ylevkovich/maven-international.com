<?php

if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST['name']) && !empty($_POST['phone'])) {
    require_once('init.php');
    require_once('classes/Email.php');
    require_once('classes/Telegram.php');
    $mail_subject = 'Call back';
    $mail_Body = "Dear Administrator,<br/><br/>
        User want call back! His details:<br/><br/>
        <b>Name: </b>" . $_POST['name'] . "<br/> 
        <b>Company: </b>" . $_POST['company'] . "<br/>
        <b>Language: </b>" . $_POST['corporate'] . "<br/>
        <b>phone: </b>" . $_POST['phone'] . "<br/><br/><br/>
        Regards,<br>Administrator,<br>MAVEN";
    Telegram::sendLetter($mail_Body);
    (new Email())->sendLetter(SERVICE_EMAIL, $mail_subject, $mail_Body);
} else {
    header("location: /");
}
?>
