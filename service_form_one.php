<form class="form-horizontal cform cform-res contactform1" id="contactform1" method="POST"
      enctype="multipart/form-data">
    <input type="hidden" name="MAX_FILE_SIZE" value="10000000">
    <ul class="nav nav-tabs hidden-xs">
        <li class="active col-sm-4 pik-lang lround tabone" id="tabone">
            <a href="javascript:void(0)"><?php echo(LANG_26); ?></a>
        </li>
        <li class="col-sm-4 pik-lang tabtwo" id="tabtwo">
            <a href="javascript:void(0)"><?php echo(LANG_27); ?></a>
        </li>
        <li class="pik-lang rround col-sm-4" id="tabthree">
            <a href="javascript:void(0)"><?php echo(LANG_28); ?></a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="open_one" class="tab-pane active open_one">
            <div style="height:50px"></div>
            <div id="login" class="animate form">
                <div id="lang_pair_1_<?php echo $id; ?>">
                    <div class="form-group">
                        <label class="control-label col-xs-4"><?php echo(LANG_16); ?></label>
                        <div class="col-xs-8 ">
                            <select name="source-language[]" dir="ltr" class="form-control"
                                    id="source_<?php echo $id; ?>">
                                <option value=""><?php echo(LANG_17); ?></option>
                                <?php
                                if (!empty($languages['left'])) {
                                    foreach ($languages['left'] as $lan_key => $lan_value) {
                                        ?>
                                        <option value="<?= $lan_value['name']; ?>" <?php echo(isset($_SESSION['source_language']) && $_SESSION['source_language'] == $lan_value['name'] ? 'selected' : '') ?>><?= $lan_value['name']; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value=""><?php echo(LANG_14); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <div class="error errorone_<?php echo $id; ?>" id="errorone"><?php echo(LANG_15); ?></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4" for="translate_to"><?php echo(LANG_92); ?></label>
                        <div class="col-xs-8 mlts" dir="ltr">
                            <input type="hidden" name="translate_to" id="translate_to" class="trans" dir="ltr">
                            <select name="target-language[]" class="form-control" id="target_<?php echo $id; ?>">
                                <option value=""><?php echo(LANG_17); ?></option>
                                <?php
                                if (!empty($languages['left'])) {
                                    foreach ($languages['left'] as $lan_key => $lan_value) {
                                        ?>
                                        <option value="<?= $lan_value['name']; ?>" <?php echo(isset($_SESSION['translate_to']) && $_SESSION['translate_to'] == $lan_value['name'] ? 'selected' : '') ?>><?= $lan_value['name']; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value=""><?php echo(LANG_14); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <div class="error errortwo_<?php echo $id; ?>" id="errortwo"><?php echo(LANG_93); ?></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-4"><?php echo(LANG_20); ?></label>
                        <div class="col-xs-8 ">
                            <input type="text" dir="ltr" name="deadline[]"
                                   class="form-control datepicker deadline_<?php echo $id; ?>"
                                   value="<?php echo(isset($_SESSION['deadline']) && $_SESSION['deadline'] != '' ? $_SESSION['deadline'] : '') ?>"
                                   placeholder="<?php echo(LANG_21); ?>" readonly>
                            <div class="error errorthree_<?php echo $id; ?>"
                                 id="errorthree"><?php echo(LANG_94); ?></div>
                        </div>
                    </div>

                    <h2 style="border:0px solid #000;padding-top:0px; margin-top:-23px !important">
                        <div id="remove_1_<?php echo $id; ?>" class="small" onclick="removeText('<?php echo $id; ?>')"
                             style="display: none;"><?php echo(LANG_81); ?></div>
                        <div class="small" onclick="appendText('<?php echo $id; ?>')"><?php echo(LANG_30); ?></div>
                        <div class="clearfix"></div>
                    </h2>
                </div>

                <input type="hidden" name="id" class="form_one" value="<?php echo $id; ?>">
                <div class="form-group " style="margin-bottom:0px">
                    <div class="col-md-12 text-right">
                        <button class="watch text-b bttm_gp piklan next" id="add_text" type="button"
                                style="background:#7197b6"><?php echo(LANG_22); ?></button>
                    </div>
                </div>
            </div>
        </div>
        <div id="open_two" class="tab-pane open_two">
            <div style="height:50px"></div>
            <div id="login" class="animate form">
                <div class="form-group">
                    <p class="control-label col-xs-12"><?php //echo(LANG_53);?></p>
                    <div class="form-group">
                        <div class="col-sm-offset-0 col-sm-12">
                            <h4>You can attach several documents</h4>
                        </div>
                    </div>
                    <div class="box_<?php echo $id; ?>">
                        <div class="box has-advanced-upload" style="padding:0px">
                            <div class="box__input ">
                                <input name="files[]" id="file" class="box__file file"
                                       data-multiple-caption="{count} files selected" multiple type="file"
                                       style="opacity:0">
                                <label for="file"
                                       style="text-align:center !important; max-width: 100% !important; width: 100% !important;"><strong><?php echo(LANG_54); ?>
                                        <br><br></strong><span
                                            class="box__dragndrop"><?php echo(LANG_91); ?></span></label>
                            </div>
                            <div class="box__uploading">Uploading&hellip;</div>
                            <div class="box__success">Done!</div>
                            <div class="box__error">Error! <span></span>.</div>
                        </div>

                        <br/>
                        <p>If your file size exceeds the <b>10 MB</b>, please send your request via email:
                            info@maven-international.com, or insert the number of pages/words to receive a cost
                            estimate.</p>

                    </div>
                </div>
                <div class="form-group " style="margin-bottom:0px">
                    <div class="col-md-12 text-right">
                        <button class="watch text-b bttm_gp piklan prev-one" type="button" id="pre_one"
                                style="background:#7197b6"><?php echo(LANG_52); ?></button>
                        <button class="watch text-b bttm_gp piklan next-one" id="next_one" type="button"
                                style="background:#7197b6"><?php echo(LANG_22); ?></button>
                    </div>
                </div>
            </div>
        </div>
        <div id="open_three" class="tab-pane">
            <div style="height:50px" class="hidden-xs"></div>
            <div class="clearfix"></div>
            <div id="login" class="animate form">
                <div class="no_words" style="display: block;" id="txs">
                    <?php if ($id != 'OQ') { ?>
                        <!-- text subject -->
                        <div class="form-group">
                            <label class="control-label col-xs-4 tsub"><?php echo(LANG_57); ?></label>
                            <div class="col-sm-8 text-subject">
                                <input name="text-subject"
                                       class="form-control validate[required] p20 "
                                       style="resize:none"
                                       data-errormessage-value-missing="<?php echo(LANG_97); ?>"
                                       placeholder="<?php echo(LANG_113); ?>"
                                />
                                <!-- LANG_113-->
                            </div>
                        </div>
                    <?php } ?>
                    <!-- no. word -->
                    <div class="form-group">
                        <label class="control-label col-sm-4 tsub"><?php echo(LANG_58); ?><br>(<?php echo(LANG_59); ?>)</label>
                        <div class="col-sm-8 no_word">
                            <input type="text" class="form-control validate[required] p20" name="no_word" value=""
                                   data-errormessage-value-missing="<?php echo(LANG_98); ?>"
                                   placeholder="<?php echo(LANG_114); ?>"><!-- LANG_114-->
                        </div>
                    </div>
                </div>
                <!-- request -->
                <div class="form-group">
                    <label class="control-label col-xs-4 tsub"><?php echo(LANG_86); ?></label>
                    <div class="col-sm-8 ad_padi">
                        <input
                                type="radio"
                                class=" validate[required] "
                                value="Private"
                                name="private"
                                data-errormessage-value-missing="<?php echo(LANG_99); ?>"
                                data-prompt-position="centerTop: 180,47">
                        <?php echo(LANG_46); ?>&nbsp;&nbsp;/&nbsp;&nbsp;
                        <input type="radio" name="private" value="Corporate"> <?php echo(LANG_47); ?>
                        <!-- data-errormessage-value-missing="<?php //echo(LANG_99);?> -->
                    </div>
                </div>
                <!-- name -->
                <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo(LANG_48); ?></label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control validate[required]" name="name"
                               data-errormessage-value-missing="<?php echo(LANG_100); ?>"
                               placeholder="<?php echo(LANG_115); ?>"><!-- LANG_115 -->
                    </div>
                </div>
                <!-- email -->
                <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo(LANG_49); ?></label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control validate[required],custom[email]" name="email_address"
                               data-errormessage-value-missing="<?php echo(LANG_101); ?>"
                               placeholder="<?php echo(LANG_116); ?>"> <!-- LANG_116 -->
                    </div>
                </div>
                <!-- phone -->
                <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo(LANG_50); ?></label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control validate[required, custom[phone]]" name="phone_number"
                               data-errormessage-value-missing="<?php echo(LANG_102); ?>"
                               placeholder="<?php echo(LANG_117); ?>"> <!-- LANG_117 -->
                    </div>
                </div>
                <!-- company -->
                <div class="form-group" id="company_filed">
                    <label class="control-label col-xs-4 comp1" id="comp1"><?php echo(LANG_51); ?></label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control validate[required]" name="company"
                               data-errormessage-value-missing="<?php echo(LANG_103); ?>"
                               placeholder="<?php echo(LANG_118); ?>"><!-- LANG_118 -->
                    </div>
                </div>

                <!-- additional info -->
                <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo(LANG_82); ?></label>
                    <div class="col-xs-8">
                        <textarea name="additional_info" rows="" cols="" class="form-control p20"
                                  style="resize:none"></textarea>
                    </div>
                </div>
                <!-- cert -->
                <div class="form-group">
                    <label class="control-label col-xs-4" for="cert"><?php echo(LANG_87); ?></label>
                    <div class=" col-xs-8 mlts text-left">
                        <input
                                type="radio"
                                name="cert"
                                id="certYes"
                                value="Yes"
                                class="validate[required]"
                                data-errormessage-value-missing="<?php echo(LANG_104); ?>"
                                data-prompt-position="centerTop: 100,47"/>&nbsp;&nbsp;
                        <label for="certYes"><?php echo(LANG_88); ?></label>

                        <input type="radio" name="cert" id="certNo" value="No"/>
                        <label for="certNo"><?php echo(LANG_89); ?></label>

                        <div class="form-inline cert_input" hidden="hidden">
                            <label class="control-label col-xs-3"><?php echo(LANG_90); ?> </label>
                            <input type="text" name="type_cert" id="type_cert" placeholder="<?php echo(LANG_119); ?>"
                                   class="form-control  "/> <!-- LANG_119 -->
                            <div class="col-xs-12" style="padding-top: 5px; font-size: smaller;">
                                <p>Don’t know what type of certification your document requires? </p>
                                <p>We can help! </p>
                                <p>Please provide as much detail as possible with regard to the purpose of this
                                    translation</p>
                                <p>(for example: translating Canadian high school certificate for university application
                                    in Sweden).</p>
                            </div>
                        </div>

                        <input type="hidden" name="type_cert" id="type_cert" disabled="disabled" value=""
                               placeholder="<?php echo(LANG_119); ?>" readonly onfocus="this.value='';"
                               class="form-control"/><!-- LANG_119-->

                    </div>
                </div>
                <!-- captcha -->
                <div id="g-recaptcha-contact" class="g-recaptcha" data-sitekey="6Le6vTcUAAAAAFK2nL_oKcCap7a7EU4Tu8iUtL_3"></div>

                <!-- button -->
                <div class="form-group " style="margin-bottom:0px">
                    <div class="col-md-12 text-right">
                        <button class="watch text-b bttm_gp piklan prev-two" id="pre_two" type="submit"
                                style="background:#7197b6"><?php echo(LANG_52); ?></button>

                        <button id="btn_callMe" class="watch text-b bttm_gp piklan submit-one" name="btnsubmit"
                                style="background:#7197b6"><?php echo(LANG_83); ?></button>
                    </div>
                </div>

                <script type="text/javascript">
                    var widgetCont = null;
                    var onloadCallback = function() {
                        widgetCont = grecaptcha.render('g-recaptcha-contact', {
                            'sitekey' : '6Le6vTcUAAAAAFK2nL_oKcCap7a7EU4Tu8iUtL_3'
                        });
                    };

                    var btn = document.getElementById('btn_callMe');
                    var html_element = document.getElementById('g-recaptcha-contact');
                    var div = document.createElement('div');
                    div.innerHTML = "<span>Заполните капчу</span>";

                    $('#contactform1, #contactform').submit(function () {
                        if(!grecaptcha.getResponse(widgetCont)){
                            html_element.appendChild(div);
                            return false;
                        }
                    });

                </script>
            </div>
        </div>
    </div>

</form>

<!-- for the add more button -->
<script type="text/javascript">
    function appendText(id) {
        var app = "#lang_pair_1_" + id;
        $(app).append(tmpHTML);
        var rem = "#remove_1_" + id;
        $(rem).show();

        var get = "lang_child_1_" + id;
        var all = app + " #" + get;
        var len = $(all).length;
        var all_len = len + 1;
        // console.log(all_len);

        //var tmpHTML = $('#lang_pair').html();
        //var tmpHTML = "<input type=\"file\" name=\"file1\" onchange=\"changed()\">";
        var tmpHTML = "<div id='lang_child_1_" + id + "' style=\"padding:10px; margin-bottom:10px\">";
        //language A
        tmpHTML += "<div class=\"form-group\">";
        tmpHTML += "<label class=\"control-label col-xs-4\" ><?php echo(LANG_16);?></label>";
        tmpHTML += "<div class=\"col-xs-8 \" >";
        tmpHTML += "<select name=\"source-language[]\" dir=\"ltr\" class='form-control cval validate[required] one_" + id + "' >";
        tmpHTML += "<option value=\"\"><?php echo(LANG_17);?></option>"
        <?php
        if(!empty($languages['left']))
        {
        foreach($languages['left'] as $lan_key => $lan_value)
        {
        ?>
        tmpHTML += "<option value=\"<?=$lan_value['name'];?>\"><?=$lan_value['name'];?></option>";
        <?php
        }
        }
        else
        {
        ?>
        tmpHTML += "<option value=\"\"><?php echo(LANG_14);?></option>";
        <?php
        }
        ?>
        tmpHTML += "</select>";
        tmpHTML += "<div class='error errorone errorone_" + id + "' id=\"errorone\"><?php echo(LANG_15);?></div>";
        tmpHTML += "</div>";
        tmpHTML += "</div>";
        //language B
        tmpHTML += "<div class=\"form-group\" style=\"margin-bottom:0px\">";
        tmpHTML += "<label class='control-label col-xs-4' ><?php echo(LANG_92);?></label>";
        tmpHTML += "<div class='col-xs-8 mlts'> ";
        tmpHTML += "<select  class='form-control cval two1_" + id + "' dir='ltr' name=\"target-language[]\" required>";
        tmpHTML += "<option value=\"\"><?php echo(LANG_17);?></option>"
        <?php
        if(!empty($languages['left']))
        {
        foreach($languages['left'] as $lan_key => $lan_value)
        {
        ?>
        tmpHTML += "<option value='<?=$lan_value['name'];?>'><?=$lan_value['name'];?></option>";
        <?php
        }
        }
        else
        {
        ?>
        tmpHTML += "<option value=''><?php echo(LANG_14);?></option>";
        <?php
        }
        ?>
        tmpHTML += "</select>";
        tmpHTML += "<div class='error errortwo1 errortwo1_" + id + "' id='errortwo1'><?php echo(LANG_93);?></div>";
        tmpHTML += "</div>";
        tmpHTML += " </div>";
        // date
        tmpHTML += "<br/><div class=\"form-group\" style=\"margin-bottom:0px\">";
        tmpHTML += "<label class='control-label col-xs-4 tsub' ><?php echo(LANG_20);?> </label>";
        tmpHTML += "<div class='col-sm-8'> ";
        tmpHTML += "<input type='text' class='form-control datepicker deadline_" + id + "' name=\"deadline[]\" required value='<?php echo(isset($_SESSION['deadline']) && $_SESSION['deadline'] != '' ? $_SESSION['deadline'] : '')?>'>";
        tmpHTML += "<div class='error errorthree1' id='errorthree'><?php echo(LANG_94);?></div>";
        tmpHTML += "<div class='clearfix'></div>";
        tmpHTML += "<div style=\"margin:10px 0px\" class='text-right'></div>";
        tmpHTML += "</div>";
        tmpHTML += " </div>";
        tmpHTML += " </div>";

        // alert(id);
        var app = "#lang_pair_1_" + id;
        $(app).append(tmpHTML);
        var rem = "#remove_1_" + id;
        $(rem).show();

        $('.datepicker').each(function () {
            $(this).datepicker({minDate: 0});
        });

        //onfocus=\"myFunction(this)\"
    }
    function removeText(id) {
        var app = "#lang_pair_1_" + id;
        var get = "lang_child_1_" + id;
        var elem = document.getElementById(get);
        var all = app + " #" + get;
        var len = $(all).length;
        var rem = "#remove_1_" + id;
        // alert(len);

        if (len == 1) {
            elem.remove();
            $(rem).hide();
        }
        else {
            elem.remove();
        }
        // console.log($("#lang_pair #lang_child").length);
    }
</script>
<!-- for Private or Corporate Request* -->
<script type="text/javascript">
    $('input[name=private]').click(function () {
        var req = $("input[name='private']:checked").val();

        if (req == "Private") {
            $("input[name='company']").removeClass("validate[required]");
            $("input[name='company']").removeAttr("data-errormessage-value-missing");
            $(".comp1").empty();
            $(".comp1").append("<?php echo(LANG_85);?>");
        }
        else {
            $("input[name='company']").addClass("validate[required]");
            $("input[name='company']").attr("data-errormessage-value-missing", "<?php echo(LANG_15);?>");
            $(".comp1").empty();
            $(".comp1").append("<?php echo(LANG_51);?>");
        }
        // alert(req);
    });


</script>

<!-- for certification -->
<script type="text/javascript">
    $('input[name=cert]').click(function () {
        var x = $("input[name='cert']:checked").val();
        if (x == "Yes") {
            $(".cert_input").show();
        }
        else {
            $(".cert_input").hide();
        }

    });
</script>


<style type="text/css">
    .js .box__file + label {
        max-width: 100%;
        text-overflow: ellipsis;
        text-align: left;
        white-space: nowrap;
        cursor: pointer;
        display: inline-block;
        overflow: hidden;
        width: 100%;
        font-size: 25px;
        color: #999;
        padding: 30px;
    }
</style>