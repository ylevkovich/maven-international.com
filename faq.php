<?php
include('init.php');
$faq_content = find('first', STATIC_PAGE_FAQ, '*', "WHERE id = 1", array());
$faq_list = find('all', MANAGE_FAQ, '*', "WHERE 1", array());
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <title><?=$faq_content['page_title'];?></title>
	  <meta name="description" content="<?=$faq_content['meta_description'];?>">
	  <meta name="keywords" content="<?=$faq_content['meta_keyward'];?>">
	  <link rel='shortcut icon' type='image/x-icon' href='<?php echo(DOMAIN_NAME_PATH);?>images/misc/<?=$general_settings['favicon'];?>' />
	  <?php include('meta.php');?>
   </head>
   <body>		
	 <?php include('mobile_menu.php');?>
	 <?php include('topbar.php');?>
     <?php include('inner_page_header.php');?>	 
	  <div class="inner_section">
		<div class="container">
			<div class="row">
				<div class="col-md-8 lpd">
					<div class="sp-body">
						<div class="col-xs-12 npd">
							<h1 class="con-hd con-hdbig"><?php echo(LANG_7);?></h1>
							<table class="ftable">
								<?php
								if(!empty($faq_list))
								{
									foreach($faq_list as $faq_key => $faq_value)
									{
								?>
								<tr>
									<td><h1 class="con-hd con-hdsmsall2"><?=$faq_key+1?> </h1></td>
									<td>
										<h1 class="con-hd con-hdsmsall2"><?=$faq_value['question_'.$_SESSION['lan']];?></h1>
										<p class="con-des fd"><?=nl2br($faq_value['answer_'.$_SESSION['lan']]);?></p>
									</td>
								</tr>
								<?php
									}
								}
								else
								{
								?>
								<tr>
									<td colspan="2" align="cemter">
										<p class="con-des fd"><?php echo('No record found');?></p>
									</td>
								</tr>
								<?php
								}
								?>								
							</table>
						</div>
						<div class="clearfix"></div>						
					</div>
				</div>
				<!-- <div class="col-md-4 rpd hidden-xs">
					<div class="sp-body" >
						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='<?php echo(DOMAIN_NAME_PATH);?>faq.php'"><?php echo(LANG_7);?></h1>
						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='<?php echo(DOMAIN_NAME_PATH);?>contactos.php'"><?php echo(LANG_6);?></h1>
					</div>
				</div> -->
				<div class="clearfix"></div>
			</div>
		</div>
	  </div>
	  <?php include('footer.php');?>
      <?php include('script.php');?>
   </body>
</html>

