<?php

if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST['sub_name']) && !empty($_POST['sub_email'])) {
    require_once('init.php');
    require_once('classes/Email.php');
    require_once ('classes/Telegram.php');
    
    $recaptchaValidator = new RecaptchaValidator();
    if (!$recaptchaValidator->isValid($_POST['g-recaptcha-response'])) {
        exit;
    }
    
    $mail_subject = 'New subscriber';
    $mail_Body = "Dear Administrator,<br/><br/>A you have a new subscriber! His details.<br/><br/><b>New subscriber name: </b>" . $_POST['sub_name'] . "<br/> <b>Email: </b>" . $_POST['sub_email'] . "<br/><br/><br/>Regards,<br>Administrator,<br>MAVEN";
    Telegram::sendLetter($mail_Body);
    (new Email())->sendLetter(SERVICE_EMAIL, $mail_subject, $mail_Body);
} else {
    header("location: /");
}
