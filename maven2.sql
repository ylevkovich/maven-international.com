-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Oct 29, 2017 at 10:09 AM
-- Server version: 5.5.42
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `maven2`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_master`
--

CREATE TABLE `admin_master` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` int(11) NOT NULL COMMENT '1-admin,2-editor,3-authors,4-contributor',
  `status` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_master`
--

INSERT INTO `admin_master` (`id`, `full_name`, `email_address`, `password`, `role`, `status`) VALUES
(1, 'Maven International', 'admin@gmail.com', 'd1f0d9f9fb06c8e34dc9ba0eacde3fb1', 1, 'Y'),
(14, 'Avik Mallick', 'coderz.neo@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 'Y'),
(15, 'Sudipto Lahiri', 'career.sudipto@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 3, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

CREATE TABLE `email_template` (
  `id` int(11) NOT NULL,
  `subject_eng` text NOT NULL,
  `body_eng` text NOT NULL,
  `subject_rus` text NOT NULL,
  `subject_spa` text NOT NULL,
  `subject_fre` text NOT NULL,
  `subject_man` text NOT NULL,
  `subject_can` text NOT NULL,
  `subject_ger` text NOT NULL,
  `subject_per` text NOT NULL,
  `subject_kor` text NOT NULL,
  `subject_mal` text NOT NULL,
  `subject_ind` text NOT NULL,
  `subject_ara` text NOT NULL,
  `subject_jap` text NOT NULL,
  `body_rus` text NOT NULL,
  `body_spa` text NOT NULL,
  `body_fre` text NOT NULL,
  `body_man` text NOT NULL,
  `body_can` text NOT NULL,
  `body_ger` text NOT NULL,
  `body_per` text NOT NULL,
  `body_kor` text NOT NULL,
  `body_mal` text NOT NULL,
  `body_ind` text NOT NULL,
  `body_ara` text NOT NULL,
  `body_jap` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_template`
--

INSERT INTO `email_template` (`id`, `subject_eng`, `body_eng`, `subject_rus`, `subject_spa`, `subject_fre`, `subject_man`, `subject_can`, `subject_ger`, `subject_per`, `subject_kor`, `subject_mal`, `subject_ind`, `subject_ara`, `subject_jap`, `body_rus`, `body_spa`, `body_fre`, `body_man`, `body_can`, `body_ger`, `body_per`, `body_kor`, `body_mal`, `body_ind`, `body_ara`, `body_jap`) VALUES
(1, 'Thank you for submitting your quote request.', 'Our team is currently reviewing your request and will send a quote within one hour. If your assignment requires more time to price, our sales team will notify you.\n\n', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(20) DEFAULT NULL,
  `message` text,
  `quote_ref` varchar(20) DEFAULT NULL,
  `date` text,
  `subject` text,
  `corporate_request` varchar(255) DEFAULT NULL,
  `add_document` text,
  `company_name` varchar(225) DEFAULT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `full_name`, `email_address`, `mobile_number`, `message`, `quote_ref`, `date`, `subject`, `corporate_request`, `add_document`, `company_name`, `status`) VALUES
(1, 'qwdq', 'stp.kostyuk@gmail.com', '123123', 'qdwdqwd', '1451439793', '16 Aug 2017, 12:58 PM', NULL, 'Private', NULL, 'qwdqwd', 'N'),
(2, 'vdvdf', 'vvn@test.ua', '3355553', 'vfvxc', '434146902', '21 Aug 2017, 01:43 PM', NULL, 'Private', NULL, 'vcxv', 'N'),
(3, 'Russel', 'rasilgaleev@gmail.com', '012345678', 'test', '645500051', '26 Aug 2017, 10:48 AM', NULL, 'Private', NULL, 'Maven', 'N'),
(4, 'vvn', 'vvn@test.ua', '334455', 'test mail', '1523510141', '04 Sep 2017, 06:59 AM', NULL, 'Private', NULL, '11233', 'N'),
(5, 'bdb', 'vvn@test.ua', '334455', 'fdgdf', '1402604261', '08 Sep 2017, 11:10 AM', NULL, 'Private', NULL, 'fdf', 'N'),
(6, 'vvn', 'vvn@test.ua', '334455', 'test mail', '1267816364', '12 Sep 2017, 06:54 AM', NULL, 'Private', NULL, 'Test', 'N'),
(7, 'qqq', 'nahirnujjaroslaw@gmail.com', '0987654321', 'vghjbkjn', '1069686100', '12 Sep 2017, 07:02 PM', NULL, 'Corporate', NULL, 'pnu', 'N'),
(8, 'vvn', 'vvn@test.ua', '223344', 'test mail', '413589596', '13 Sep 2017, 09:06 AM', NULL, 'Private', NULL, '22334', 'N'),
(9, 'Gilles Fabre', 'gilles.fabre@worldrugby.org', '00353879068419', 'To whom it may concern\r\n\r\nHi, I am contacting you as we have an event scheduled in Kuala Lumpur on 27-28-29 October and we are looking to rent a tour-guide system (Infoport) with 3 mikes and a number of headsets (number to be confirmed)\r\n\r\nI would be very grateful if you could let me know if you are in a position to rent this equipment\r\n\r\nRegards\r\n\r\nGilles Fabre\r\nWorld Rugby', '47660471', '13 Sep 2017, 10:12 AM', NULL, 'Corporate', NULL, 'World Rugby (Dublin, Ireland)', 'N'),
(10, '1', 'yu.destroyer.ok@gmail.com', '11111111', '111', '1035140823', '17 Oct 2017, 10:26 PM', NULL, 'Private', NULL, '1', 'N'),
(11, '1', 'yu.destroyer.ok@gmail.com', '111111', '11', '1521888897', '17 Oct 2017, 10:28 PM', NULL, 'Private', NULL, '111', 'N'),
(12, '1', 'yu.destroyer.ok@gmail.com', '11111', '1111', '1035858614', '18 Oct 2017, 07:13 AM', NULL, 'Private', NULL, '1', 'N'),
(13, '1', 'yu.destroyer.ok@gmail.com', '11111', '1111', '584553476', '18 Oct 2017, 07:13 AM', NULL, 'Private', NULL, '1', 'N'),
(14, '1', 'yu.destroyer.ok@gmail.com', '11111', '1111', '2019831000', '18 Oct 2017, 07:13 AM', NULL, 'Private', NULL, '1', 'N'),
(15, '1', 'yu.destroyer.ok@gmail.com', '11111', '1111', '754195193', '18 Oct 2017, 07:13 AM', NULL, 'Private', NULL, '1', 'N'),
(16, '1', 'yu.destroyer.ok@gmail.com', '11111', '1111', '1293942256', '18 Oct 2017, 07:14 AM', NULL, 'Private', NULL, '1', 'N'),
(17, '1', 'yu.destroyer.ok@gmail.com', '11111', '1111', '1279392196', '18 Oct 2017, 07:14 AM', NULL, 'Private', NULL, '1', 'N'),
(18, '1', 'yu.destroyer.ok@gmail.com', '11111', '1111', '918608363', '18 Oct 2017, 07:14 AM', NULL, 'Private', NULL, '1', 'N'),
(19, '1', 'yu.destroyer.ok@gmail.com', '11111', '1111', '285807500', '18 Oct 2017, 07:14 AM', NULL, 'Private', NULL, '1', 'N'),
(20, '1', 'yu.destroyer.ok@gmail.com', '11111', '1111', '949703247', '18 Oct 2017, 07:23 AM', NULL, 'Private', NULL, '1', 'N'),
(21, '1', 'youra.l2@mail.ru', '1111', '111', '1818141837', '18 Oct 2017, 09:57 AM', NULL, 'Private', NULL, '1', 'N'),
(22, '1', 'youra.l2@mail.ru', '1111', '111', '1934657377', '18 Oct 2017, 09:57 AM', NULL, 'Private', NULL, '1', 'N'),
(23, '1', 'youra.l2@mail.ru', '1111', '111', '587072149', '18 Oct 2017, 09:58 AM', NULL, 'Private', NULL, '1', 'N'),
(24, '1', 'youra.l2@mail.ru', '1111', '111', '631819674', '18 Oct 2017, 09:59 AM', NULL, 'Private', NULL, '1', 'N'),
(25, 'youra.l2@mail.ru', 'youra.l2@mail.ru', '11111', '1111', '622106639', '18 Oct 2017, 05:10 PM', NULL, 'Private', NULL, 'youra.l2@mail.ru', 'N'),
(26, 'youra.l2@mail.ru', 'youra.l2@mail.ru', '11111', '1111', '1105748636', '18 Oct 2017, 05:11 PM', NULL, 'Private', NULL, 'youra.l2@mail.ru', 'N'),
(27, 'youra.l2@mail.ru', 'youra.l2@mail.ru', '11111', '1111', '1926855148', '18 Oct 2017, 05:12 PM', NULL, 'Private', NULL, 'youra.l2@mail.ru', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `home_slider`
--

CREATE TABLE `home_slider` (
  `id` int(11) NOT NULL,
  `slider_image` varchar(255) NOT NULL,
  `slider_text_eng` varchar(255) NOT NULL,
  `button_text_eng` varchar(255) NOT NULL,
  `slider_text_rus` varchar(255) NOT NULL,
  `button_text_rus` varchar(255) NOT NULL,
  `slider_text_tur` varchar(255) NOT NULL,
  `button_text_tur` varchar(255) NOT NULL,
  `slider_text_spa` varchar(255) NOT NULL,
  `button_text_spa` varchar(255) NOT NULL,
  `slider_text_fre` varchar(255) NOT NULL,
  `button_text_fre` varchar(255) NOT NULL,
  `slider_text_man` varchar(255) NOT NULL,
  `button_text_man` varchar(255) NOT NULL,
  `slider_text_can` varchar(255) NOT NULL,
  `button_text_can` varchar(255) NOT NULL,
  `slider_text_ger` varchar(255) NOT NULL,
  `button_text_ger` varchar(255) NOT NULL,
  `slider_text_per` varchar(255) NOT NULL,
  `button_text_per` varchar(255) NOT NULL,
  `slider_text_kor` varchar(255) NOT NULL,
  `button_text_kor` varchar(255) NOT NULL,
  `slider_text_mal` varchar(255) NOT NULL,
  `button_text_mal` varchar(255) NOT NULL,
  `slider_text_ind` varchar(255) NOT NULL,
  `button_text_ind` varchar(255) NOT NULL,
  `slider_text_ara` varchar(255) NOT NULL,
  `button_text_ara` varchar(255) NOT NULL,
  `slider_text_jap` varchar(255) NOT NULL,
  `button_text_jap` varchar(255) NOT NULL,
  `button_link` text NOT NULL,
  `sl_no` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_slider`
--

INSERT INTO `home_slider` (`id`, `slider_image`, `slider_text_eng`, `button_text_eng`, `slider_text_rus`, `button_text_rus`, `slider_text_tur`, `button_text_tur`, `slider_text_spa`, `button_text_spa`, `slider_text_fre`, `button_text_fre`, `slider_text_man`, `button_text_man`, `slider_text_can`, `button_text_can`, `slider_text_ger`, `button_text_ger`, `slider_text_per`, `button_text_per`, `slider_text_kor`, `button_text_kor`, `slider_text_mal`, `button_text_mal`, `slider_text_ind`, `button_text_ind`, `slider_text_ara`, `button_text_ara`, `slider_text_jap`, `button_text_jap`, `button_link`, `sl_no`) VALUES
(12, '12_globus.jpg', 'We speak your language. We speak over 100 other languages too.', 'watch video', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '#', 1);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`) VALUES
(1, 'Arabic'),
(2, 'Mandarin'),
(3, 'Cantonese'),
(4, 'English'),
(5, 'English (USA)'),
(6, 'French (France)'),
(7, 'German'),
(8, 'Italian'),
(9, 'Japanese'),
(10, 'Spanish (Spain)'),
(11, 'Spanish (Latin America)'),
(12, 'Afrikaans'),
(13, 'Albanian'),
(14, 'Amharic'),
(15, 'Armenian'),
(16, 'Assyrian'),
(17, 'Basque'),
(18, 'Belarusian'),
(19, 'Bengali'),
(20, 'Bosnian'),
(21, 'Bulgarian'),
(22, 'Burmese'),
(23, 'Catalan'),
(24, 'Cebuano ( Binisaya/Visayan )'),
(25, 'Mandarin'),
(26, 'Cantonese'),
(27, 'Classical Greek'),
(28, 'Cook Island Maori'),
(29, 'Croatian'),
(30, 'Czech'),
(31, 'Danish'),
(32, 'Dari'),
(33, 'Dinka'),
(34, 'Dutch'),
(35, 'Dutch (Belgium)'),
(36, 'English (USA)'),
(37, 'Esperanto'),
(38, 'Estonian'),
(39, 'Faroese'),
(40, 'Farsi'),
(41, 'Finnish'),
(42, 'French (Canadian Québécois)'),
(43, 'French (France)'),
(44, 'Georgian'),
(45, 'German'),
(46, 'German (Switzerland)'),
(47, 'Greek'),
(48, 'Gujarati'),
(49, 'Haitian Creole French'),
(50, 'Hawaiian'),
(51, 'Hebrew'),
(52, 'Hindi'),
(53, 'Hungarian'),
(54, 'Icelandic'),
(55, 'Indonesian'),
(56, 'Irish Gaelic'),
(57, 'Italian'),
(58, 'Jamaican Creole English'),
(59, 'Japanese'),
(60, 'Javanese'),
(61, 'Kazakh'),
(62, 'Khmer'),
(63, 'Kirundi'),
(64, 'Korean'),
(65, 'Kurdish Kurmanji (Latin)'),
(66, 'Kurdish Sorani (Arab)'),
(67, 'Lao'),
(68, 'Latin'),
(69, 'Latvian'),
(70, 'Lithuanian'),
(71, 'Luxembourgish'),
(72, 'Macedonian'),
(73, 'Malay'),
(74, 'Maltese'),
(75, 'Maori'),
(76, 'Marathi'),
(77, 'Mongolian'),
(78, 'Nepali'),
(79, 'Norwegian'),
(80, 'Pashto'),
(81, 'Persian'),
(82, 'Polish'),
(83, 'Portuguese (Brazil)'),
(84, 'Portuguese (Portugal)'),
(85, 'Punjabi'),
(86, 'Quechua'),
(87, 'Romanian'),
(88, 'Russian'),
(89, 'Samoan'),
(90, 'Serbian'),
(91, 'Sesotho'),
(92, 'Shona'),
(93, 'Sinhala'),
(94, 'Slovak'),
(95, 'Slovenian'),
(96, 'Somali'),
(97, 'Spanish (Latin America)'),
(98, 'Spanish (Spain)'),
(99, 'Spanish (US)'),
(100, 'Sudanese Arabic'),
(101, 'Sundanese'),
(102, 'Swahili'),
(103, 'Swedish'),
(104, 'Tagalog'),
(105, 'Tajik'),
(106, 'Tamil'),
(107, 'Thai'),
(108, 'Tibetan'),
(109, 'Tigrinya'),
(110, 'Tokelauan'),
(111, 'Tongan'),
(112, 'Turkish'),
(113, 'Ukrainian'),
(114, 'Urdu'),
(115, 'Uzbek'),
(116, 'Vietnamese'),
(117, 'Welsh'),
(118, 'Chinese Simplified');

-- --------------------------------------------------------

--
-- Table structure for table `manage_address`
--

CREATE TABLE `manage_address` (
  `id` int(11) NOT NULL,
  `address_eng` text NOT NULL,
  `address_rus` text NOT NULL,
  `address_tur` text NOT NULL,
  `address_spa` text NOT NULL,
  `address_fre` text NOT NULL,
  `address_man` text NOT NULL,
  `address_can` text NOT NULL,
  `address_ger` text NOT NULL,
  `address_per` text NOT NULL,
  `address_kor` text NOT NULL,
  `address_mal` text NOT NULL,
  `address_ind` text NOT NULL,
  `address_ara` text NOT NULL,
  `address_jap` text NOT NULL,
  `phone_number` varchar(225) NOT NULL,
  `company_name_eng` varchar(225) NOT NULL,
  `company_name_rus` varchar(255) NOT NULL,
  `company_name_tur` varchar(255) NOT NULL,
  `company_name_spa` varchar(255) NOT NULL,
  `company_name_fre` varchar(255) NOT NULL,
  `company_name_man` varchar(255) NOT NULL,
  `company_name_can` varchar(255) NOT NULL,
  `company_name_ger` varchar(255) NOT NULL,
  `company_name_per` varchar(255) NOT NULL,
  `company_name_kor` varchar(255) NOT NULL,
  `company_name_mal` varchar(255) NOT NULL,
  `company_name_ind` varchar(255) NOT NULL,
  `company_name_ara` varchar(255) NOT NULL,
  `company_name_jap` varchar(255) NOT NULL,
  `country_eng` varchar(50) NOT NULL,
  `country_rus` varchar(50) NOT NULL,
  `country_tur` varchar(50) NOT NULL,
  `country_spa` varchar(50) NOT NULL,
  `country_fre` varchar(50) NOT NULL,
  `country_man` varchar(50) NOT NULL,
  `country_can` varchar(50) NOT NULL,
  `country_ger` varchar(50) NOT NULL,
  `country_per` varchar(50) NOT NULL,
  `country_kor` varchar(50) NOT NULL,
  `country_mal` varchar(50) NOT NULL,
  `country_ind` varchar(50) NOT NULL,
  `country_ara` varchar(50) NOT NULL,
  `country_jap` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manage_address`
--

INSERT INTO `manage_address` (`id`, `address_eng`, `address_rus`, `address_tur`, `address_spa`, `address_fre`, `address_man`, `address_can`, `address_ger`, `address_per`, `address_kor`, `address_mal`, `address_ind`, `address_ara`, `address_jap`, `phone_number`, `company_name_eng`, `company_name_rus`, `company_name_tur`, `company_name_spa`, `company_name_fre`, `company_name_man`, `company_name_can`, `company_name_ger`, `company_name_per`, `company_name_kor`, `company_name_mal`, `company_name_ind`, `company_name_ara`, `company_name_jap`, `country_eng`, `country_rus`, `country_tur`, `country_spa`, `country_fre`, `country_man`, `country_can`, `country_ger`, `country_per`, `country_kor`, `country_mal`, `country_ind`, `country_ara`, `country_jap`) VALUES
(3, '35-3A BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.\r\n<br>\r\ninfo@maven-international.com', '17-09 BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.', '17-09 BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.', '17-09 BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.', '17-09 BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.', '17-09 BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.', '17-09 BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.', '17-09 BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.', '17-09 BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.', '17-09 BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.', '17-09 BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.', '17-09 BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.', '17-09 BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.', '17-09 BINJAI 8 TOWER, LORONG BINJAI,\r\n50450 KUALA LUMPUR, MALAYSIA.', '(+6)03-2181-5771', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'MAVEN INTERNATIONAL SDN BHD (1031478-D)', 'KUALA LUMPUR, MALAYSIA', 'KUALA LUMPUR, MALAYSIA', 'KUALA LUMPUR, MALAYSIA', 'KUALA LUMPUR, MALAYSIA', 'KUALA LUMPUR, MALAYSIA', 'KUALA LUMPUR, MALAYSIA', 'KUALA LUMPUR, MALAYSIA', 'KUALA LUMPUR, MALAYSIA', 'KUALA LUMPUR, MALAYSIA', 'KUALA LUMPUR, MALAYSIA', 'KUALA LUMPUR, MALAYSIA', 'KUALA LUMPUR, MALAYSIA', 'KUALA LUMPUR, MALAYSIA', 'KUALA LUMPUR, MALAYSIA'),
(4, 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY\r\n<br>\r\ninfo@maven-international.com', 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY', 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY', 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY', 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY', 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY', 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY', 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY', 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY', 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY', 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY', 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY', 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY', 'MERKEZ MAH.HASAT SOK.NO:52/1, 34381 ŞIŞLI, İSTANBUL, TURKEY', '(+9)0531-7458425', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'MAVEN İNTERNATIONAL DANIŞMANLIK LIMITED ŞIRKETI (040785-5)', 'ISTANBUL, TURKEY', 'ISTANBUL, TURKEY', 'ISTANBUL, TURKEY', 'ISTANBUL, TURKEY', 'ISTANBUL, TURKEY', 'ISTANBUL, TURKEY', 'ISTANBUL, TURKEY', 'ISTANBUL, TURKEY', 'ISTANBUL, TURKEY', 'ISTANBUL, TURKEY', 'ISTANBUL, TURKEY', 'ISTANBUL, TURKEY', 'ISTANBUL, TURKEY', 'ISTANBUL, TURKEY'),
(5, '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n<br>\r\ninfo@maven-international.ca', '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n', '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n', '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n', '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n', '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n', '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n', '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n', '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n', '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n', '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n', '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n', '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n', '100 GLOUCESTER STREET, SUITE 481, ON K2P 0A4\r\nOTTAWA, CANADA\r\n', '(+1)6136997975', 'MAVEN INTERNATIONAL INC (974862-8)', 'MAVEN INTERNATIONAL INC (974862-8)', 'MAVEN INTERNATIONAL INC (974862-8)', 'MAVEN INTERNATIONAL INC (974862-8)', 'MAVEN INTERNATIONAL INC (974862-8)', 'MAVEN INTERNATIONAL INC (974862-8)', 'MAVEN INTERNATIONAL INC (974862-8)', 'MAVEN INTERNATIONAL INC (974862-8)', 'MAVEN INTERNATIONAL INC (974862-8)', 'MAVEN INTERNATIONAL INC (974862-8)', 'MAVEN INTERNATIONAL INC (974862-8)', 'MAVEN INTERNATIONAL INC (974862-8)', 'MAVEN INTERNATIONAL INC (974862-8)', 'MAVEN INTERNATIONAL INC (974862-8)', 'OTTAWA, CANADA', 'OTTAWA, CANADA', 'OTTAWA, CANADA', 'OTTAWA, CANADA', 'OTTAWA, CANADA', 'OTTAWA, CANADA', 'OTTAWA, CANADA', 'OTTAWA, CANADA', 'OTTAWA, CANADA', 'OTTAWA, CANADA', 'OTTAWA, CANADA', 'OTTAWA, CANADA', 'OTTAWA, CANADA', 'OTTAWA, CANADA');

-- --------------------------------------------------------

--
-- Table structure for table `manage_enquery_document`
--

CREATE TABLE `manage_enquery_document` (
  `id` int(11) NOT NULL,
  `enquery_id` int(11) DEFAULT NULL,
  `image_name` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manage_enquery_document`
--

INSERT INTO `manage_enquery_document` (`id`, `enquery_id`, `image_name`) VALUES
(1, 1, '39195_test_by_avik.docx'),
(2, 2, '50142_KM-12FN20-motor.pdf'),
(3, 3, '5804_'),
(4, 4, '125245_'),
(5, 5, '165336_'),
(6, 6, '97698_clapton 2017 resume (1).pdf'),
(7, 7, '108770_'),
(8, 8, '173142_'),
(9, 9, '108581_HTML tag Add a meta tag to your site.docx'),
(10, 10, '13487_HTML tag Add a meta tag to your site.docx'),
(11, 11, '2501_'),
(12, 12, '103144_baloon.png'),
(13, 13, '100746_Maven website content (EN) V-2017.docx'),
(14, 14, '131931_'),
(15, 15, '189282_Mansor CV .pdf'),
(16, 16, '101614_Application for PROZ JOB 1315495.docx'),
(17, 16, '7090_CV - Lisette Martineau - 2017.pdf'),
(18, 16, '101758_uottranslator certificate.jpg'),
(19, 17, '125427_'),
(20, 18, '59363_CV_Gurgen Matosyan.docx'),
(21, 19, '107489_power of attorney - 2.docx'),
(22, 20, '60366_power of attorney - 2.docx'),
(23, 21, '47248_power of attorney - 2.docx'),
(0, 0, '35726_'),
(0, 0, '150098_'),
(0, 1, '120202_'),
(0, 11, '127850_'),
(0, 20, '54058_'),
(0, 24, '68146_');

-- --------------------------------------------------------

--
-- Table structure for table `manage_faq`
--

CREATE TABLE `manage_faq` (
  `id` int(11) NOT NULL,
  `question_eng` text NOT NULL,
  `question_rus` text NOT NULL,
  `question_tur` text NOT NULL,
  `question_spa` text NOT NULL,
  `question_fre` text NOT NULL,
  `question_man` text NOT NULL,
  `question_can` text NOT NULL,
  `question_ger` text NOT NULL,
  `question_per` text NOT NULL,
  `question_kor` text NOT NULL,
  `question_mal` text NOT NULL,
  `question_ind` text NOT NULL,
  `question_ara` text NOT NULL,
  `question_jap` text NOT NULL,
  `answer_eng` text NOT NULL,
  `answer_rus` text NOT NULL,
  `answer_tur` text NOT NULL,
  `answer_spa` text NOT NULL,
  `answer_fre` text NOT NULL,
  `answer_man` text NOT NULL,
  `answer_can` text NOT NULL,
  `answer_ger` text NOT NULL,
  `answer_per` text NOT NULL,
  `answer_kor` text NOT NULL,
  `answer_mal` text NOT NULL,
  `answer_ind` text NOT NULL,
  `answer_ara` text NOT NULL,
  `answer_jap` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manage_faq`
--

INSERT INTO `manage_faq` (`id`, `question_eng`, `question_rus`, `question_tur`, `question_spa`, `question_fre`, `question_man`, `question_can`, `question_ger`, `question_per`, `question_kor`, `question_mal`, `question_ind`, `question_ara`, `question_jap`, `answer_eng`, `answer_rus`, `answer_tur`, `answer_spa`, `answer_fre`, `answer_man`, `answer_can`, `answer_ger`, `answer_per`, `answer_kor`, `answer_mal`, `answer_ind`, `answer_ara`, `answer_jap`) VALUES
(3, 'What is an interpreter?', '', '', '', '', '', '', '', '', '', '', '', '', '', 'An interpreter is a person who orally communicates information from one language to another. ', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 'What is language interpretation?', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Language interpretation is the facilitation of dialogue between two or more parties using different languages.', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `manage_quotes`
--

CREATE TABLE `manage_quotes` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(20) DEFAULT NULL,
  `message` text,
  `date` text,
  `subject` text,
  `source_language` text,
  `translate_language` text,
  `deadline` text,
  `corporate_request` varchar(225) DEFAULT NULL,
  `text_subject` text,
  `no_word` varchar(225) DEFAULT NULL,
  `date_required` text,
  `select_day` varchar(225) DEFAULT NULL,
  `select_hours` varchar(255) DEFAULT NULL,
  `minimum_required` varchar(225) DEFAULT NULL,
  `require_service` varchar(225) DEFAULT NULL,
  `event_topic` varchar(225) DEFAULT NULL,
  `event_start_date` varchar(225) DEFAULT NULL,
  `event_end_date` varchar(225) DEFAULT NULL,
  `event_address` text,
  `company` varchar(225) DEFAULT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manage_quotes`
--

INSERT INTO `manage_quotes` (`id`, `service_id`, `full_name`, `email_address`, `mobile_number`, `message`, `date`, `subject`, `source_language`, `translate_language`, `deadline`, `corporate_request`, `text_subject`, `no_word`, `date_required`, `select_day`, `select_hours`, `minimum_required`, `require_service`, `event_topic`, `event_start_date`, `event_end_date`, `event_address`, `company`, `status`) VALUES
(1, 4, 'Avik Mallick', 'avikmallick87@gmail.com', '+91 8013334177', ':-:test 123:-:test 456:-:test 456789', '15 Dec 2016, 06:47 AM:-:15 Dec 2016, 02:54 PM:-:15 Dec 2016, 03:00 PM:-:15 Dec 2016, 03:03 PM', 'test:-:test:-:test 123', 'Afrikaans', 'Arabic:-:English:-:Albanian:-:', '22 Dec 2016', 'Corporate', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y'),
(2, 4, 'moaz korena', 'moazkorena@gmail.com', '0060136331948', ':-:moaz', '15 Dec 2016, 02:47 PM:-:15 Dec 2016, 02:47 PM', 'Response for your inquiry', 'Arabic', 'Chinese Simplified:-:Chinese Traditional:-:English:-:', '17 Dec 2016', 'Private', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y'),
(3, 4, 'moaz korena', 'moazkorena@gmail.com', '0060136331948', ':-:korena', '15 Dec 2016, 03:01 PM:-:15 Dec 2016, 03:02 PM', 'Response for your inquiry', 'Japanese', 'Arabic:-:Chinese Simplified:-:Chinese Traditional:-:', '17 Dec 2016', 'Private', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y'),
(4, 4, 'moaz korena', 'moazkorena@gmail.com', '0060136331948', NULL, '20 Dec 2016, 02:20 PM', NULL, 'English', '', '23 Dec 2016', 'Private', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(5, 4, 'mohamed', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 01:57 AM', NULL, 'Chinese Traditional', '', '31 Dec 2016', 'Private', 'test', '111', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(6, 4, 'mohamed', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 02:03 AM', NULL, 'Chinese Traditional', '', '31 Dec 2016', 'Private', 'test', '111', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(7, 4, 'ttetst', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 02:04 AM', NULL, 'Chinese Traditional', '', '31 Dec 2016', 'Private', 'tt', '7878778', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(8, 4, 'ttetst', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 02:11 AM', NULL, 'Chinese Traditional', '', '31 Dec 2016', 'Private', 'tt', '7878778', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(9, 4, 'ttetst', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 02:17 AM', NULL, 'Chinese Traditional', '', '31 Dec 2016', 'Private', 'tt', '7878778', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(10, 4, 'ttetst', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 02:21 AM', NULL, 'Chinese Traditional', '', '31 Dec 2016', 'Private', 'tt', '7878778', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(11, 4, 'ttetst', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 02:23 AM', NULL, 'Chinese Traditional', '', '31 Dec 2016', 'Private', 'tt', '7878778', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(12, 4, 'ttetst', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 02:24 AM', NULL, 'Chinese Traditional', '', '31 Dec 2016', 'Private', 'tt', '7878778', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(13, 4, 'ttetst', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 02:26 AM', NULL, 'Chinese Traditional', '', '31 Dec 2016', 'Private', 'tt', '7878778', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(14, 4, 'ttetst', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 03:10 AM', NULL, 'Chinese Traditional', '', '31 Dec 2016', 'Private', 'tt', '7878778', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(15, 4, 'Mohamed Ahmed Elgharabawy', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 03:12 AM', NULL, 'French (France)', 'Arabic:-:Chinese Simplified:-:Chinese Traditional:-:English:-:English (USA):-:French (France):-:German:-:Italian:-:Japanese:-:Spanish (Spain):-:Spanish (Latin America):-:Afrikaans:-:Albanian:-:Amharic:-:Armenian:-:Assyrian:-:Basque:-:Belarusian:-:Bengali:-:Bosnian:-:Bulgarian:-:Burmese:-:Catalan:-:Cebuano ( Binisaya/Visayan ):-:Classical Greek:-:Cook Island Maori:-:Croatian:-:Czech:-:Danish:-:Dari:-:Dinka:-:Dutch:-:Dutch (Belgium):-:Esperanto:-:Estonian:-:Faroese:-:Farsi:-:Finnish:-:French (Canadian Quï¿½bï¿½cois):-:Georgian:-:German (Switzerland):-:Greek:-:Gujarati:-:Haitian Creole French:-:Hawaiian:-:Hebrew:-:Hindi:-:Hungarian:-:Icelandic:-:Indonesian:-:Irish Gaelic:-:Jamaican Creole English:-:Javanese:-:Kazakh:-:Khmer:-:Kirundi:-:Korean:-:Kurdish Kurmanji (Latin):-:Kurdish Sorani (Arab):-:Lao:-:Latin:-:Latvian:-:Lithuanian:-:Luxembourgish:-:Macedonian:-:Malay:-:Maltese:-:Maori:-:Marathi:-:Mongolian:-:Nepali:-:Norwegian:-:Pashto:-:Persian:-:Polish:-:Portuguese (Brazil):-:Portuguese (Portugal):-:Punjabi:-:Quechua:-:Romanian:-:Russian:-:Samoan:-:Serbian:-:Sesotho:-:Shona:-:Sinhala:-:Slovak:-:Slovenian:-:Somali:-:Spanish (US):-:Sudanese Arabic:-:Sundanese:-:Swahili:-:Swedish:-:Tagalog:-:Tajik:-:Tamil:-:Thai:-:Tibetan:-:Tigrinya:-:Tokelauan:-:Tongan:-:Turkish:-:Ukrainian:-:Urdu:-:Uzbek:-:Vietnamese:-:Welsh:-:', '31 Dec 2016', 'Private', 'WWWSS', '09009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(16, 4, 'Mohamed Ahmed Elgharabawy', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 03:15 AM', NULL, 'Chinese Traditional', 'Arabic:-:Chinese Simplified:-:Chinese Traditional:-:English:-:English (USA):-:French (France):-:German:-:Italian:-:Japanese:-:Spanish (Spain):-:Spanish (Latin America):-:Afrikaans:-:Albanian:-:Amharic:-:Armenian:-:Assyrian:-:Basque:-:Belarusian:-:Bengali:-:Bosnian:-:Bulgarian:-:Burmese:-:Catalan:-:Cebuano ( Binisaya/Visayan ):-:Classical Greek:-:Cook Island Maori:-:Croatian:-:Czech:-:Danish:-:Dari:-:Dinka:-:Dutch:-:Dutch (Belgium):-:Esperanto:-:Estonian:-:Faroese:-:Farsi:-:Finnish:-:French (Canadian Quï¿½bï¿½cois):-:Georgian:-:German (Switzerland):-:Greek:-:Gujarati:-:Haitian Creole French:-:Hawaiian:-:Hebrew:-:Hindi:-:Hungarian:-:Icelandic:-:Indonesian:-:Irish Gaelic:-:Jamaican Creole English:-:Javanese:-:Kazakh:-:Khmer:-:Kirundi:-:Korean:-:Kurdish Kurmanji (Latin):-:Kurdish Sorani (Arab):-:Lao:-:Latin:-:Latvian:-:Lithuanian:-:Luxembourgish:-:Macedonian:-:Malay:-:Maltese:-:Maori:-:Marathi:-:Mongolian:-:Nepali:-:Norwegian:-:Pashto:-:Persian:-:Polish:-:Portuguese (Brazil):-:Portuguese (Portugal):-:Punjabi:-:Quechua:-:Romanian:-:Russian:-:Samoan:-:Serbian:-:Sesotho:-:Shona:-:Sinhala:-:Slovak:-:Slovenian:-:Somali:-:Spanish (US):-:Sudanese Arabic:-:Sundanese:-:Swahili:-:Swedish:-:Tagalog:-:Tajik:-:Tamil:-:Thai:-:Tibetan:-:Tigrinya:-:Tokelauan:-:Tongan:-:Turkish:-:Ukrainian:-:Urdu:-:Uzbek:-:Vietnamese:-:Welsh:-:', '31 Dec 2016', 'Private', 'eee', '112', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(17, 4, 'QW', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 03:17 AM', NULL, 'Chinese Traditional', 'Arabic:-:', '31 Dec 2016', 'Private', 'test', '121212', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(18, 4, 'qw', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 03:22 AM', NULL, 'Chinese Simplified', 'Chinese Simplified:-:', '31 Dec 2016', 'Private', 'test', '1000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y'),
(19, 4, 'qw', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 03:24 AM', NULL, 'Chinese Simplified', 'Chinese Simplified:-:', '31 Dec 2016', 'Private', 'test', '1000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N'),
(20, 4, 'Mohamed Ahmed Elgharabawy', 'mohamed.ghr@gmail.com', '0182324932', NULL, '21 Dec 2016, 03:28 AM', NULL, 'Chinese Simplified', 'Arabic:-:', '31 Dec 2016', 'Private', 'test', '7000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y'),
(21, 4, 'ngbfvcd', 'gfvcd@jkhgfd.cun', '876543', NULL, '02 Jan 2017, 11:21 AM', NULL, 'English (USA)', '', '20 Jan 2017', 'Corporate', '[;lkjhgfd', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y'),
(22, 4, 'dw', 'edw@dsfs.com', '3243', NULL, '07 Feb 2017, 02:48 PM', NULL, '', '', '', 'Private', 'sd', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y'),
(23, 7, 'dfdsf', '', 'fdsf', NULL, '02 Mar 2017, 03:16 PM', NULL, '', '', NULL, '', NULL, NULL, '', '', '', '2', '', 'dsfd', '', '', 'fdsfdf', '', 'N'),
(24, 6, '', '', '', NULL, '07 Mar 2017, 08:25 AM', NULL, 'Chinese Traditional', 'Chinese Simplified', NULL, '', NULL, NULL, '', 'Half Day', '', '2', '', '', '', '', '', '', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `manage_quote_document`
--

CREATE TABLE `manage_quote_document` (
  `id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `quote_id` int(11) DEFAULT NULL,
  `image_name` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manage_quote_document`
--

INSERT INTO `manage_quote_document` (`id`, `service_id`, `quote_id`, `image_name`) VALUES
(1, 4, 1, '125023_test_by_avik.docx'),
(2, 4, 2, '126251_KM-12FN20-motor.pdf'),
(3, 4, 3, '152221_KM-12FN20-motor.pdf'),
(4, 4, 14, '28322_'),
(5, 4, 15, '13170_'),
(6, 4, 16, '101643_'),
(7, 4, 17, '194468_'),
(8, 4, 18, '88944_'),
(9, 4, 19, '124649_'),
(10, 4, 20, '111212_'),
(11, 4, 21, '105403_');

-- --------------------------------------------------------

--
-- Table structure for table `manage_service`
--

CREATE TABLE `manage_service` (
  `id` int(11) NOT NULL,
  `service_icon` text NOT NULL,
  `service_title_eng` varchar(255) NOT NULL,
  `service_title_rus` varchar(255) NOT NULL,
  `service_title_tur` varchar(255) NOT NULL,
  `service_title_spa` varchar(255) NOT NULL,
  `service_title_fra` varchar(255) NOT NULL,
  `service_title_man` varchar(255) NOT NULL,
  `service_title_can` varchar(255) NOT NULL,
  `service_title_ger` varchar(255) NOT NULL,
  `service_title_per` varchar(255) NOT NULL,
  `service_title_kor` varchar(255) NOT NULL,
  `service_title_mal` varchar(255) NOT NULL,
  `service_title_ind` varchar(255) NOT NULL,
  `service_title_ara` varchar(255) NOT NULL,
  `service_title_jap` varchar(255) NOT NULL,
  `service_description_eng` text NOT NULL,
  `service_description_rus` text NOT NULL,
  `service_description_tur` text NOT NULL,
  `service_description_spa` text NOT NULL,
  `service_description_fre` text NOT NULL,
  `service_description_man` text NOT NULL,
  `service_description_can` text NOT NULL,
  `service_description_ger` text NOT NULL,
  `service_description_per` text NOT NULL,
  `service_description_kor` text NOT NULL,
  `service_description_mal` text NOT NULL,
  `service_description_ind` text NOT NULL,
  `service_description_ara` text NOT NULL,
  `service_description_jap` text NOT NULL,
  `attachment_document` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manage_service`
--

INSERT INTO `manage_service` (`id`, `service_icon`, `service_title_eng`, `service_title_rus`, `service_title_tur`, `service_title_spa`, `service_title_fra`, `service_title_man`, `service_title_can`, `service_title_ger`, `service_title_per`, `service_title_kor`, `service_title_mal`, `service_title_ind`, `service_title_ara`, `service_title_jap`, `service_description_eng`, `service_description_rus`, `service_description_tur`, `service_description_spa`, `service_description_fre`, `service_description_man`, `service_description_can`, `service_description_ger`, `service_description_per`, `service_description_kor`, `service_description_mal`, `service_description_ind`, `service_description_ara`, `service_description_jap`, `attachment_document`) VALUES
(4, '4_56541_translation2.png', 'Translation', 'перевод', '&ccedil;eviri', 'Traducci&oacute;n', 'Traduction', 'орчуулга', '翻譯', '&Uuml;bersetzung', 'ترجمه', '번역', 'Terjemahan', 'Terjemahan', 'ترجمة', '翻訳', 'Maven International conforms to ISO 17100: 2015 standard for translation services \r\nMaven International provides quality translation services in over 100 languages. Our services range from the translation of legal documents such as contracts, certificates, certified translation of documents and licenses, to financial, technical, and business documents. We also specialize in translating marketing content such as brochures, slogans, and websites. We work closely with our clients to ensure they receive exceptional services and uncompromised results. Contact us for a quote today!', 'Maven International стремится обеспечить качественную переводческую и локализационную помощь, адаптированную к вашим конкретным требованиям. Мы работаем в тесном контакте с нашими клиентами из различных отраслей, чтобы обеспечить им исключительные услуги и бескомпромиссные результаты. Мы предлагаем несколько услуг, связанных с переводом, охватывающих широкий спектр типов документов и технических областей, которые все предоставляются опытными носителями языка с упором на точность, конфиденциальность и точность. Наши услуги по переводу включают редактирование, корректуру и форматирование. Свяжитесь с нами сегодня для цитаты.', 'Maven International, &ouml;zel gereksinimlerinize g&ouml;re kaliteli &ccedil;eviri ve yerelleştirme yardımı sağlamaya &ccedil;alışmaktadır. İstisnai hizmetler ve uzlaşmaz sonu&ccedil;lar elde etmek i&ccedil;in &ccedil;eşitli sekt&ouml;rlerden m&uuml;şterilerimiz ile yakın işbirliği i&ccedil;erisindeyiz. Doğruluk, gizlilik ve hassasiyet &uuml;zerine odaklanan deneyimli yerli-konuşmacıların sunduğu &ccedil;ok &ccedil;eşitli belge t&uuml;r&uuml; ve teknik alanı kapsayan &ccedil;ok sayıda &ccedil;eviri hizmetleri sunuyoruz. &Ccedil;eviri hizmetlerimiz d&uuml;zenleme, d&uuml;zeltme ve bi&ccedil;imlendirme dahildir. Bug&uuml;n bir teklif i&ccedil;in bizimle iletişime ge&ccedil;in.', 'Maven International se esfuerza por proporcionar asistencia de traducci&oacute;n y localizaci&oacute;n de calidad adaptada a sus necesidades espec&iacute;ficas. Trabajamos estrechamente con nuestros clientes de diversas industrias para garantizar que reciban servicios excepcionales y resultados sin compromisos. Ofrecemos m&uacute;ltiples servicios relacionados con la traducci&oacute;n que abarcan una amplia gama de tipos de documentos y campos t&eacute;cnicos que son prestados por hablantes nativos con experiencia centr&aacute;ndose en la precisi&oacute;n, confidencialidad y precisi&oacute;n. Nuestros servicios de traducci&oacute;n incluyen la edici&oacute;n, correcci&oacute;n y formato. P&oacute;ngase en contacto con nosotros hoy para una cotizaci&oacute;n.', 'Maven International s''efforce de fournir une assistance de traduction et de localisation de qualit&eacute; adapt&eacute;e &agrave; vos besoins sp&eacute;cifiques. Nous travaillons en &eacute;troite collaboration avec nos clients de diverses industries afin de s''assurer qu''ils re&ccedil;oivent des services exceptionnels et des r&eacute;sultats sans compromis. Nous offrons de multiples services li&eacute;s &agrave; la traduction couvrant un large &eacute;ventail de types de documents et de domaines techniques qui sont tous rendus par des natifs exp&eacute;riment&eacute;s en se concentrant sur la pr&eacute;cision, la confidentialit&eacute; et la pr&eacute;cision. Nos services de traduction incluent l''&eacute;dition, la correction d''&eacute;preuves et la mise en forme. Contactez-nous d&egrave;s aujourd''hui pour un devis.', 'Жинхэнэ хэлэлцээр хийгч мэргэжилтэн Олон улсын чанарын орчуулгыг, таны тусгай шаардлагад тохируулан нутагшуулах туслалцаа үзүүлэх зорьдог. Бид тэд онцгой үйлчилгээг uncompromised үр дүнг хүлээн хангахын тулд янз бүрийн салбарт манай үйлчлүүлэгч нягт хамтран ажиллаж байна. Бид баримт бичиг төрөл, бүх үнэн зөв, нууцлал, нарийн анхаарч туршлагатай эх чанга яригч үзүүлсэн техникийн салбарт өргөн хүрээг хамарсан олон орчуулга холбоотой үйлчилгээг санал болгож байна. Манай орчуулгын үйлчилгээ засварлах, нотолгоо, мөн форматлах багтаасан байна. ишлэл өнөөдөр бидэнтэй холбоо барина уу.', 'Maven International致力於根據您的特定要求提供優質的翻譯和本地化服務。我們與來自不同行業的客戶緊密合作，確保他們獲得卓越的服務和不妥協的結果。我們提供多種翻譯相關服務，涵蓋廣泛的文檔類型和技術領域，所有這些都是由經驗豐富的母語人士提供，專注於準確性，保密性和精確性。我們的翻譯服務包括編輯，校對和格式化。聯繫我們今天報價。', 'Maven International bem&uuml;ht sich um eine qualitativ hochwertige &Uuml;bersetzung und Lokalisierung, die auf Ihre spezifischen Anforderungen zugeschnitten ist. Wir arbeiten eng mit unseren Kunden aus verschiedenen Branchen zusammen, um sicherzustellen, dass sie au&szlig;ergew&ouml;hnliche Dienstleistungen und kompromisslose Ergebnisse erhalten. Wir bieten Ihnen mehrere &Uuml;bersetzungsdienste an, die eine breite Palette von Dokumenttypen und technischen Fachgebieten abdecken, die alle von erfahrenen Muttersprachlern mit Genauigkeit, Vertraulichkeit und Pr&auml;zision vermittelt werden. Unsere &Uuml;bersetzungsdienste beinhalten die Bearbeitung, Korrekturlesen und Formatierung. Kontaktieren Sie uns heute f&uuml;r ein Angebot.', 'MAVEN بین المللی در تلاش برای ترجمه با کیفیت و کمک های محلی سازی متناسب با نیازهای خاص خود را ارائه. ما به همکاری نزدیک با مشتریان خود از صنایع مختلف برای اطمینان از آنها خدمات استثنایی و نتایج غیر قابل انتظار دریافت خواهید کرد. ما ارائه می دهیم متعدد خدمات مربوط به ترجمه پوشش طیف گسترده ای از انواع سند و زمینه های فنی که همه با تجربه بومی زبانان با تمرکز بر دقت، محرمانه بودن و دقت ارائه شده است. خدمات ترجمه ما شامل ویرایش، غلط گیری، و قالب بندی می باشد. تماس با ما امروز برای نقل قول.', 'Maven International은 고객의 특정 요구 사항에 맞는 고품질의 번역 및 현지화 지원을 제공하기 위해 노력합니다. 우리는 다양한 산업 분야의 고객들과 긴밀히 협력하여 탁월한 서비스와 타협없는 결과를 보장합니다. 당사는 정확성, 기밀성 및 정밀성에 중점을 둔 숙련 된 원주민이 제공하는 광범위한 문서 유형 및 기술 분야를 포괄하는 여러 번역 관련 서비스를 제공합니다. 당사의 번역 서비스에는 편집, 교정 및 서식이 포함됩니다. 따옴표를 위해 저희에게 오늘 연락하십시오.', 'sdsds', 'sdsd', 'dadad', 'Maven Internationalは、お客様の特定の要件に合わせた質の高い翻訳とローカリゼーションの支援を提供するよう努めています。当社は様々な業界のお客様と緊密に連携し、優れたサービスと妥協のない結果を確実に提供します。私たちは、経験豊かなネイティブスピーカーが正確性、機密性、正確性に焦点を当てたさまざまな種類の文書や技術分野を網羅した、複数の翻訳関連サービスを提供しています。当社の翻訳サービスには、編集、校正、書式設定が含まれます。見積もりについては、今すぐお問い合わせください。', 'Y'),
(5, '5_transcreation_2.png', 'Transcreation', 'Транскреация', 'İletim', 'Transcreación', 'Transcription', 'Transcreation', '創造', 'Transkreation', 'Transcreation', '트랜스 크 레이션', 'Transkreasi', 'Transkreasi', 'Transcreation', '転造', 'Need assistance in promoting your ideas, products, and brands across linguistic divide to establish an international presence? Our team believes in the coalescence of translation and creative process. The building of your brand for worldwide consumption has never been easier. Contact us for a quote today!', 'Нужна помощь в продвижении ваших идей, продуктов и брендов через языковой барьер и создании международного присутствия? Наша команда в Maven International верит в объединение процесса перевода и творчества. Мы готовы сотрудничать с вами, чтобы успешно производить работу, которая, несомненно, вызовет все правильные эмоции посредством систематического перевода, лингвистической точности и стимулирования творчества. Строительство вашего бренда для мирового потребления никогда не было проще. Свяжитесь с нами для цитаты сегодня!', 'Need assistance in promoting your ideas, products, and brands across linguistic divide and establish an international presence? Our team at Maven International believes in the coalescence of translation and creative process. We are ready to collaborate with you to successfully produce work that will surely evoke all the right emotions through systematic translation, linguistic accuracy, and stimulating creativity. The building of your brand for worldwide consumption has never been easier. Contact us for a quote today!', 'Need assistance in promoting your ideas, products, and brands across linguistic divide and establish an international presence? Our team at Maven International believes in the coalescence of translation and creative process. We are ready to collaborate with you to successfully produce work that will surely evoke all the right emotions through systematic translation, linguistic accuracy, and stimulating creativity. The building of your brand for worldwide consumption has never been easier. Contact us for a quote today!', 'Need assistance in promoting your ideas, products, and brands across linguistic divide and establish an international presence? Our team at Maven International believes in the coalescence of translation and creative process. We are ready to collaborate with you to successfully produce work that will surely evoke all the right emotions through systematic translation, linguistic accuracy, and stimulating creativity. The building of your brand for worldwide consumption has never been easier. Contact us for a quote today!', 'хэлний хуваагдал даяар таны санаа, бүтээгдэхүүн, брэндийг дэмжих туслалцаа хэрэгтэй, олон улсын оролцоог бий? Жинхэнэ хэлэлцээр хийгч мэргэжилтэн International манай багийнхан орчуулга, бүтээлч үйл явцын coalescence итгэдэг. Бид амжилттай заавал системтэй орчуулгын хэл, үнэн зөв,, үр дүнтэй бүтээлч дамжуулан бүх зөв сэтгэл хөдлөлөө дурсан болно ажлыг үйлдвэрлэх та бүхэнтэй хамтран ажиллахад бэлэн байна. Дэлхий даяар хэрэглээний таны брэндийн барилгын хялбар байгаагүй юм. Өнөөдөр хэлсэн нь биднийг Холбоо барих!', 'Need assistance in promoting your ideas, products, and brands across linguistic divide and establish an international presence? Our team at Maven International believes in the coalescence of translation and creative process. We are ready to collaborate with you to successfully produce work that will surely evoke all the right emotions through systematic translation, linguistic accuracy, and stimulating creativity. The building of your brand for worldwide consumption has never been easier. Contact us for a quote today!', 'Need assistance in promoting your ideas, products, and brands across linguistic divide and establish an international presence? Our team at Maven International believes in the coalescence of translation and creative process. We are ready to collaborate with you to successfully produce work that will surely evoke all the right emotions through systematic translation, linguistic accuracy, and stimulating creativity. The building of your brand for worldwide consumption has never been easier. Contact us for a quote today!', 'Need assistance in promoting your ideas, products, and brands across linguistic divide and establish an international presence? Our team at Maven International believes in the coalescence of translation and creative process. We are ready to collaborate with you to successfully produce work that will surely evoke all the right emotions through systematic translation, linguistic accuracy, and stimulating creativity. The building of your brand for worldwide consumption has never been easier. Contact us for a quote today!', 'Need assistance in promoting your ideas, products, and brands across linguistic divide and establish an international presence? Our team at Maven International believes in the coalescence of translation and creative process. We are ready to collaborate with you to successfully produce work that will surely evoke all the right emotions through systematic translation, linguistic accuracy, and stimulating creativity. The building of your brand for worldwide consumption has never been easier. Contact us for a quote today!', 'Need assistance in promoting your ideas, products, and brands across linguistic divide and establish an international presence? Our team at Maven International believes in the coalescence of translation and creative process. We are ready to collaborate with you to successfully produce work that will surely evoke all the right emotions through systematic translation, linguistic accuracy, and stimulating creativity. The building of your brand for worldwide consumption has never been easier. Contact us for a quote today!', 'Need assistance in promoting your ideas, products, and brands across linguistic divide and establish an international presence? Our team at Maven International believes in the coalescence of translation and creative process. We are ready to collaborate with you to successfully produce work that will surely evoke all the right emotions through systematic translation, linguistic accuracy, and stimulating creativity. The building of your brand for worldwide consumption has never been easier. Contact us for a quote today!', 'Need assistance in promoting your ideas, products, and brands across linguistic divide and establish an international presence? Our team at Maven International believes in the coalescence of translation and creative process. We are ready to collaborate with you to successfully produce work that will surely evoke all the right emotions through systematic translation, linguistic accuracy, and stimulating creativity. The building of your brand for worldwide consumption has never been easier. Contact us for a quote today!', 'Need assistance in promoting your ideas, products, and brands across linguistic divide and establish an international presence? Our team at Maven International believes in the coalescence of translation and creative process. We are ready to collaborate with you to successfully produce work that will surely evoke all the right emotions through systematic translation, linguistic accuracy, and stimulating creativity. The building of your brand for worldwide consumption has never been easier. Contact us for a quote today!', 'Y'),
(6, '6_Proofreading.png', 'Simultaneous Interpretation', 'Синхронный перевод', 'Simultane Terc&uuml;me', 'Interpretaci&oacute;n simult&aacute;nea', 'Interpr&eacute;tation simultan&eacute;e', 'Нэгэн зэрэг тайлбар', '同聲傳譯', 'Simultane Interpretation', 'ترجمه همزمان', '동시 통역', 'Tafsiran Serentak', 'Interpretasi Simultan', 'الترجمة الفورية', '同時通訳', 'Simultaneous interpretation (SI) is a specialised practice that demands an accurate oral translation done concurrently with the source speaker. Commonly used in large conferences and meetings, SI is a more complex and arduous process than consecutive interpretation. With our global network and years of experience in serving numerous front-page organisations, Maven International assures optimum reliability and cost efficiency in SI services. \r\n\r\nEQUIPMENT AND TECH SUPPORT. \r\n\r\n Maven International provides equipment necessary for carrying out simultaneous interpretation, as well as a full oversight and management of equipment provision, set-up scheduling, and continuous on-site technical support and back-up.  \r\n\r\nThe equipment includes: \r\n\r\n- Wireless headsets for the reception of real-time interpretation\r\n\r\n- Portable Simultaneous Interpretation System (SIS)\r\n\r\n- Sound-proof booth for the occupancy of the interpreter, equipped with microphones and interpreter desks\r\n\r\n- Technical crew to set, monitor, overlook seamless interpreting and troubleshoot any glitches that may occur in the transmission\r\n\r\n- Other Audio Visual solutions\r\n\r\nMore information is available upon request.', 'Синхронный перевод (SI) - это специализированная практика, требующая тщательного и точного устного перевода, выполняемого одновременно с источником оратора как в темпе, так и в содержании. Обычно используемый в больших конференциях, встречах и презентациях, синхронный перевод является более сложным и трудным процессом, чем последовательный перевод. Сотрудничая с некоторыми авторитетными организациями мира, Maven International гарантирует вам высокое качество надежных, систематических и экономичных услуг СИ. Наша команда выбирается на основе профессиональной квалификации, образовательных полномочий, опыта и знания языка. Мы готовы помочь вам в достижении ваших целей, в координации с организационными комитетами и в конечном итоге предоставить вам оптимальные решения. Запрос цитатой сегодня.', 'Eşzamanlı terc&uuml;me (SI), hem konuşmacıya hem de i&ccedil;eriğe eşzamanlı olarak yapılan kapsamlı ve doğru s&ouml;zl&uuml; &ccedil;eviri talep eden uzmanlaşmış bir uygulamadır. B&uuml;y&uuml;k konferanslarda, toplantılarda ve sunumlarda yaygın olarak kullanılan sim&uuml;ltane terc&uuml;me, ardışık terc&uuml;meden &ccedil;ok daha karmaşık ve zahmetli bir s&uuml;re&ccedil;tir. D&uuml;nyanın saygın kuruluşlarından bazılarıyla işbirliği i&ccedil;inde olan Maven International, en y&uuml;ksek kalite, g&uuml;venilir, sistematik ve d&uuml;ş&uuml;k maliyetli SI hizmetleri sağladığını garanti ediyor. Ekibimiz mesleki niteliklere, eğitim kimlik bilgilerine, tecr&uuml;beye ve dil yeterliliğine dayanarak se&ccedil;ilir. Hedeflerinize ulaşmanız, organizasyon komiteleriyle koordinasyon halinde bulunmanız konusunda size yardımcı olmaya hazırız ve eninde sonunda en iyi &ouml;z&uuml;mleri sunmaktayız. Bug&uuml;n teklif isteyin.', 'La interpretaci&oacute;n simult&aacute;nea (SI) es una pr&aacute;ctica especializada que exige una traducci&oacute;n oral completa y exacta realizada simult&aacute;neamente con el orador de origen, tanto en el ritmo como en el contenido. Com&uacute;nmente utilizado en grandes conferencias, reuniones y presentaciones, la interpretaci&oacute;n simult&aacute;nea es un proceso m&aacute;s complejo y arduo que la interpretaci&oacute;n consecutiva. Despu&eacute;s de haber colaborado con algunas de las organizaciones de renombre mundial, Maven International asegura que se le proporciona la m&aacute;s alta calidad de servicios SI fiables, sistem&aacute;ticos y rentables. Nuestro equipo se selecciona en base a calificaciones profesionales, credenciales educativas, experiencia y competencia ling&uuml;&iacute;stica. Estamos dispuestos a ayudarle en el logro de sus objetivos, la coordinaci&oacute;n con los comit&eacute;s organizadores y, en &uacute;ltima instancia, ofrecerle soluciones &oacute;ptimas. Solicite una cotizaci&oacute;n hoy.', 'L''interpr&eacute;tation simultan&eacute;e (SI) est une pratique sp&eacute;cialis&eacute;e qui exige une traduction orale approfondie et exacte faite simultan&eacute;ment avec le locuteur source &agrave; la fois dans le rythme et le contenu. G&eacute;n&eacute;ralement utilis&eacute; dans les grandes conf&eacute;rences, r&eacute;unions et pr&eacute;sentations, l''interpr&eacute;tation simultan&eacute;e est un processus plus complexe et plus ardu que l''interpr&eacute;tation cons&eacute;cutive. Ayant collabor&eacute; avec certaines des organisations les plus r&eacute;put&eacute;es au monde, Maven International vous assure que vous disposez de la plus haute qualit&eacute; de services SI fiables, syst&eacute;matiques et &eacute;conomiques. Notre &eacute;quipe est s&eacute;lectionn&eacute;e en fonction des qualifications professionnelles, des dipl&ocirc;mes, de l''exp&eacute;rience et des comp&eacute;tences linguistiques. Nous sommes pr&ecirc;ts &agrave; vous aider &agrave; atteindre vos objectifs, &agrave; vous coordonner avec les comit&eacute;s organisateurs et &agrave; vous fournir des solutions optimales. Demandez un devis d&egrave;s aujourd''hui.', 'Нэгэн зэрэг тайлбар (SI) бүрэн гүйцэд, үнэн зөв аман орчуулга хурдтай, агуулга аль аль нь эх хэлэгч нь нэгэн зэрэг хийж шаарддаг тусгайлсан арга юм. том бага хурал, уулзалт, илтгэл-д түгээмэл хэрэглэгддэг нэгэн зэрэг тайлбар тайлбар дараалан илүү төвөгтэй, хэцүү үйл явц юм. Дэлхийн хамгийн нэр хүндтэй байгууллагуудын зарим нь хамтран ажиллаж дараа, жинхэнэ хэлэлцээр хийгч мэргэжилтэн Олон улсын чи, найдвартай системтэй, зардал, үр ашигтай SI үйлчилгээний хамгийн өндөр чанар олгож байна баталгаажуулдаг. Манай хамт олон мэргэжлийн ур чадвар, боловсролын итгэмжлэлүүдтэй, туршлага, хэл хэлний үндсэн дээр сонгосон юм. Бид таны зорилгодоо хүрэх зохион байгуулах хороонд нь зохицуулах танд туслахад бэлэн байна, эцэст нь оновчтой шийдэл таныг хангах. Өнөөдөр эшлэлийг хүсч байна.', '同聲傳譯（SI）是一種專業實踐，需要與源說話者在速度和內容上同時進行徹底和準確的口頭翻譯。通常在大型會議，會議和演示中使用，同聲傳譯比連續解釋更複雜和艱鉅。 Maven International與一些世界著名的組織合作，確保您獲得最高質量的可靠，系統和具有成本效益的SI服務。我們的團隊是基於專業資格，教育憑證，經驗和語言能力。我們隨時準備幫助您實現您的目標，與組織委員會協調，最終為您提供最佳的解決方案。今天請求報價。', 'Simultandolmetschen (SI) ist eine spezialisierte Praxis, die eine gr&uuml;ndliche und genaue m&uuml;ndliche &Uuml;bersetzung erfordert, die gleichzeitig mit dem Quellsprecher sowohl in Tempo als auch in Inhalt durchgef&uuml;hrt wird. H&auml;ufig in gro&szlig;en Konferenzen, Treffen und Pr&auml;sentationen verwendet, ist die gleichzeitige Interpretation ein komplexer und m&uuml;hsamer Prozess als konsekutiver Interpretation. Mit der Zusammenarbeit mit einigen der renommierten Organisationen der Welt versichert Maven International, dass Sie mit der h&ouml;chsten Qualit&auml;t zuverl&auml;ssiger, systematischer und kosteng&uuml;nstiger SI-Services ausgestattet sind. Unser Team wird ausgew&auml;hlt auf berufliche Qualifikationen, p&auml;dagogische Anmeldeinformationen, Erfahrung und Sprachkenntnisse. Wir sind bereit, Ihnen bei der Erreichung Ihrer Ziele zu helfen, mit den Organisationskomitees zu koordinieren und Ihnen letztlich optimale L&ouml;sungen zu bieten. Fordern Sie heute ein Angebot an.', 'ترجمه همزمان (SI) یک عمل تخصصی است که ترجمه شفاهی کامل و دقیق همزمان با بلندگو منبع هر دو در سرعت و محتوا انجام خواسته است. معمولا در کنفرانس بزرگ، جلسات، و ارائه استفاده می شود، ترجمه همزمان، یک فرآیند پیچیده تر و دشوار تر از تفسیر متوالی است. پس از همکاری با برخی از سازمان های معتبر جهان، انباره های بین المللی اطمینان شما با بالاترین کیفیت از خدمات قابل اعتماد، منظم و هزینه SI کارآمد ارائه شده است. تیم ما بر اساس مدارک حرفه ای، مدارک آموزشی، تجربه و مهارت در زبان انتخاب شده است. ما آماده کمک به شما در دستیابی به اهداف خود را، هماهنگی با کمیته سازماندهی هستند، و در نهایت شما با راه حل های مطلوب ارائه. درخواست یک نقل قول امروز.', '동시 통역 (SI)은 속도와 내용 모두에서 원어민과 동시에 철저하고 정확한 구두 번역을 요구하는 전문적인 관행입니다. 대규모 회의, 회의 및 발표에서 일반적으로 사용되는 동시 통역은 연속 통역보다 더 복잡하고 힘든 과정입니다. Maven International은 세계의 평판 좋은 조직과 협력하여 신뢰할 수 있고 체계적이고 비용 효율적인 SI 서비스를 제공합니다. 우리 팀은 전문 자격, 교육 자격 증명, 경험 및 언어 숙련도에 따라 선정됩니다. 우리는 목표를 달성하고, 조직위원회와 협조하며 궁극적으로 최적의 솔루션을 제공 할 수 있도록 귀하를 지원할 준비가되어 있습니다. 오늘 견적을 요청하십시오.', '##', '##', '##', '##', 'N'),
(7, '7_maven_icon-14.png', 'Consecutive Interpretation', 'Последовательный перевод', 'Ardıl Terc&uuml;me', 'Interpretaci&oacute;n Consecutiva', 'Interpr&eacute;tation cons&eacute;cutive', 'дараалсан тайлбар', '連續解釋', 'Konsekutivinterpretation', 'تفسیر انتقال پی در پی', '순차 통역', 'Tafsiran Berturut-turut', 'Interpretasi Berturut-turut', 'تفسير التوالي', '逐次通訳', 'Consecutive interpretation (CI) usually occurs in smaller settings such as court hearings, interviews, conference calls, or business meetings. Different from simultaneous interpretation, consecutive interpreters render the translation after the source speaker has concluded the speech or between pauses. Our consecutive interpreters are periodically trained to deliver accurate and meticulous interpretation while preserving technical jargons or semantics. Request a quote today!\r\n', 'Последовательный устный перевод обычно происходит в небольших условиях, таких как судебные слушания, интервью, телефонные конференции или деловые встречи. В отличие от синхронного перевода, последовательные переводчики выполняют перевод после того, как исходный оратор завершил речь или между паузами. Наши последовательные устные переводчики периодически проходят подготовку для точной и тщательной интерпретации при сохранении технических жаргонов или семантики. Запрос цитатой сегодня.', 'Ardıl yorum genellikle mahkeme duruşmaları, m&uuml;lakatlar, konferans g&ouml;r&uuml;şmeleri veya iş toplantıları gibi daha k&uuml;&ccedil;&uuml;k ortamlarda ger&ccedil;ekleşir. Eşzamanlı terc&uuml;meden farklı olarak ardışık terc&uuml;manlar, konuşmacı konuşmayı tamamladıktan sonra veya duraklar arasında &ccedil;evirir. Ardıl &ccedil;evirmenlerimiz, teknik jargonları veya anlambilimlerini koruyarak doğru ve titiz bir yorum sunmak i&ccedil;in periyodik olarak eğitilirler. Bug&uuml;n teklif isteyin.', 'La interpretaci&oacute;n consecutiva usualmente ocurre en entornos m&aacute;s peque&ntilde;os, como audiencias judiciales, entrevistas, conferencias telef&oacute;nicas o reuniones de negocios. Diferente de la interpretaci&oacute;n simult&aacute;nea, los int&eacute;rpretes consecutivos hacen la traducci&oacute;n despu&eacute;s de que el orador fuente haya concluido el discurso o entre pausas. Nuestros int&eacute;rpretes consecutivos son entrenados peri&oacute;dicamente para ofrecer una interpretaci&oacute;n precisa y meticulosa, preservando al mismo tiempo las jergas t&eacute;cnicas o sem&aacute;nticas. Solicite una cotizaci&oacute;n hoy.', 'L''interpr&eacute;tation cons&eacute;cutive se produit habituellement dans des contextes plus restreints tels que les audiences du tribunal, les entrevues, les conf&eacute;rences t&eacute;l&eacute;phoniques ou les r&eacute;unions d''affaires. Diff&eacute;rent de l''interpr&eacute;tation simultan&eacute;e, interpr&egrave;tes cons&eacute;cutifs rendre la traduction apr&egrave;s que le locuteur source a conclu le discours ou entre les pauses. Nos interpr&egrave;tes cons&eacute;cutifs sont p&eacute;riodiquement form&eacute;s pour fournir une interpr&eacute;tation pr&eacute;cise et m&eacute;ticuleuse tout en pr&eacute;servant les jargons techniques ou la s&eacute;mantique. Demandez un devis d&egrave;s aujourd''hui.', 'Дараалсан тайлбар нь ихэвчлэн тухайн шүүх хуралдаанд, ярилцлага, бага хурал дуудлага, эсвэл бизнесийн хурал цуглаан гэх мэт бага тохиргоо тохиолддог. нэгэн зэрэг тайлбарлах өөр дараалсан хэлмэрч дараа эх үүсвэр яригч илтгэл байгуулсан, эсхүл түр зогсооно хооронд орчуулгыг үзүүлэх. Бидний дараалан хэлмэрч үе үе техникийн jargons буюу семантиксийг үрэхгүйгээр үнэн зөв, нямбай тайлбарыг хүргэх бэлтгэгдсэн байна. Өнөөдөр эшлэлийг хүсч байна.', '連續解釋通常發生在較小的場合，例如法庭聽證，面試，電話會議或商務會議。與同時解釋不同，連續解釋器在源說話者結束語音之後或在暫停之間呈現翻譯。我們的連續翻譯定期接受培訓，提供準確和細緻的解釋，同時保留技術術語或語義。今天請求報價。', 'Konsekutive Interpretation tritt in der Regel in kleineren Einstellungen wie Gericht Anh&ouml;rungen, Interviews, Konferenzgespr&auml;che oder Gesch&auml;ftstreffen. Anders als bei gleichzeitiger Interpretation machen aufeinander folgende Dolmetscher die &Uuml;bersetzung, nachdem der Quellsprecher die Sprache oder zwischen Pausen abgeschlossen hat. Unsere konsekutiven Dolmetscher werden regelm&auml;&szlig;ig geschult, um eine genaue und sorgf&auml;ltige Interpretation unter Beibehaltung der Fachjargons oder Semantik zu liefern. Fordern Sie heute ein Angebot an.', 'تفسیر انتقال پی در پی معمولا در تنظیمات کوچکتر مانند جلسات دادگاه، مصاحبه ها، کنفرانس تلفنی، یا جلسات کسب و کار رخ می دهد. متفاوت از آنچه به طور همزمان، مترجمان شفاهی متوالی ارائه ترجمه بعد از سخنران منبع سخنرانی این نتیجه رسیده است و یا بین مکث. مفسران متوالی ما به صورت دوره ای آموزش دیده برای ارائه تفسیر دقیق و دقیق در حالی که حفظ از jargons فنی یا معناشناسی. درخواست یک نقل قول امروز.', '순차적 인 해석은 법원 청문회, 면담, 전화 회의 또는 비즈니스 미팅과 같은 작은 환경에서 주로 발생합니다. 동시 통역과 달리, 연속 통역사는 원어 화자가 말을 끝내거나 일시 중지 사이에 번역을 렌더링합니다. 우리의 연속 통역사는 기술 전문 용어 또는 의미를 보존하면서 정확하고 세심한 해석을 제공하도록 정기적으로 교육을 받았습니다. 오늘 견적을 요청하십시오.', 'ss', 'ss', 'ss', 'ss', 'N'),
(8, '8_maven_icon-16.png', 'Court Interpretation', 'Судебное расследование', 'Mahkeme Yorumu', 'Interpretaci&oacute;n del Tribunal', 'Interpr&eacute;tation de la Cour', 'Шүүхийн тайлбар', '法院解釋', 'Gerichtsauslegung', 'تفسیر دادگاه', '법원 통역', 'Tafsiran Mahkamah', 'Interpretasi Pengadilan', 'تفسير المحكمة', '裁判所の解釈', 'Court interpretation is a translation field brimming with extensive legal terminologies and procedures. Being aware of the need for professional assistance in court interpretation, we provide highly qualified, experienced interpreters who are intimately familiar with legal protocols and are capable of rendering smooth uninterrupted interpretation with precision and reliability. Court interpretation service is provided in Malaysia (Kuala Lumpur), Canada (Ottawa) and Turkey (Istanbul).', 'Толкование судом является полем перевода, наполненным обширными юридическими терминологиями и процедурами. Осознавая необходимость профессиональной помощи в толковании судебных решений, мы предоставляем высококвалифицированных, опытных переводчиков, которые хорошо знакомы с юридическими протоколами и способны обеспечивать плавную бесперебойную интерпретацию с точностью и надежностью.', 'Mahkeme yorumu, kapsamlı yasal terminolojilere ve prosed&uuml;rlere sahip bir &ccedil;evirinin alanıdır. Mahkeme yorumunda mesleki yardıma ihtiya&ccedil; duyulduğunun bilincinde olarak, yasal protokolleri yakından tanıyan ve hassas ve g&uuml;venilir bir şekilde kesintisiz ve kesintisiz bir şekilde yorumlama yeteneğine sahip nitelikli, deneyimli terc&uuml;manlar sunmaktayız.', 'La interpretaci&oacute;n de la corte es un campo de la traducci&oacute;n lleno de extensas terminolog&iacute;as y procedimientos legales. Al ser conscientes de la necesidad de asistencia profesional en la interpretaci&oacute;n judicial, ofrecemos int&eacute;rpretes altamente calificados y experimentados que est&aacute;n &iacute;ntimamente familiarizados con los protocolos legales y son capaces de hacer una interpretaci&oacute;n suave y sin interrupciones con precisi&oacute;n y fiabilidad.', 'L''interpr&eacute;tation des tribunaux est un domaine de la traduction qui regorge de terminologies et de proc&eacute;dures juridiques &eacute;tendues. &Eacute;tant conscients de la n&eacute;cessit&eacute; d''une assistance professionnelle dans l''interpr&eacute;tation des tribunaux, nous fournissons des interpr&egrave;tes exp&eacute;riment&eacute;s et hautement qualifi&eacute;s qui connaissent parfaitement les protocoles juridiques et sont capables de rendre l''interpr&eacute;tation sans interruption avec pr&eacute;cision et fiabilit&eacute;.', 'Шүүхийн тайлбар өргөн цар хүрээтэй хууль эрх зүйн нэр томъёоны, журмын brimming орчуулга талбар юм. шүүхийн тайлбар мэргэжлийн туслалцаа үзүүлэх шаардлагатай талаар мэдлэгтэй байх нь, бид хууль эрх зүйн протокол нь дотно танил бөгөөд нарийвчлал, найдвартай байдал нь гөлгөр тасралтгүй тайлбарлах үзүүлэх чадвартай өндөр чадвартай, туршлагатай орчуулагч өгнө.', '法院解釋是一個翻譯領域，有廣泛的法律術語和程序。意識到需要專業的法律解釋協助，我們提供高度合格，經驗豐富的口譯，他們非常熟悉法律協議，並能夠準確，可靠地進行平穩不間斷的解釋。', 'Gerichtsinterpretation ist ein &Uuml;bersetzungsfeld mit umfangreichen rechtlichen Terminologien und Verfahren. Wenn wir uns der professionellen Unterst&uuml;tzung bei der Gerichtsinterpretation bewusst sind, stellen wir hochqualifizierte, erfahrene Dolmetscher zur Verf&uuml;gung, die mit den rechtlichen Protokollen vertraut sind und in der Lage sind, eine reibungslose, ununterbrochene Interpretation mit Pr&auml;zision und Zuverl&auml;ssigkeit zu erm&ouml;glichen.', 'تفسیر دادگاه یک میدان پر از ترجمه اصطلاحات حقوقی گسترده و روش است. بودن از نیاز به کمک های حرفه ای در تفسیر دادگاه آگاه، ما شما را مجرب، مترجمان با تجربه که از نزدیک با پروتکل های قانونی آشنا هستند و قادر به ارائه تفسیر بدون وقفه صاف با دقت و قابلیت اطمینان می باشد.', '법원 통역은 광범위한 법적 용어 및 절차로 가득 찬 번역 분야입니다. 법원 통역에서 전문적인 도움이 필요하다는 것을 알고 있으므로, 우리는 법적 절차에 정통한 정확하고 믿을 수있는 원활한 통역을 제공 할 수있는 우수한 자격을 갖춘 경험 많은 통역사를 제공합니다.', 'ss', 'ss', 's', 's', 'N'),
(9, '9_Linguistic validation 2.png', 'Linguistic Validation', 'Лингвистическая проверка', 'Dilbilim Validasyonu', 'Validación lingüística', 'Validation linguistique', 'хэлний Баталгаажуулалт', '語言驗證', 'Linguistische Validierung', 'اعتبار سنجی زبانی', '언어 검증', 'Pengesahan Linguistik', 'Validasi linguistik', 'التحقق اللغوي', '言語検証', 'Linguistic validation focuses on the objectives of maintaining the reliability, conceptual equivalence, and content validity of a certain document. We know how important it is to provide our clients with culturally accurate results. We approach this service by thoroughly reviewing the accuracy of already translated documents and validating a final review using steps such as retranslation and back translation. Request a quote today!', 'Лингвистическое обоснование фокусируется на задачах поддержания надежности, концептуальной эквивалентности и достоверности содержания определенного документа. Мы знаем, как важно предоставлять нашим клиентам точные в культуре результаты. Мы всегда уделяем особое внимание обеспечению того, чтобы ваши документы обрабатывались профессионалами, обладающими обширными знаниями грамматики, семантики и идиом из культур исходного и целевого языков. Мы подходим к этой услуге, тщательно анализируя точность уже переведенных документов и проверяя окончательный обзор, используя такие шаги, как ретрансляция и обратный перевод. Результат будет отражать отличное качество в конечном результате вашего документа. Запрос цитатой сегодня!', 'Dilbilim doğrulaması, belirli bir belgenin güvenilirliğini, kavramsal eşdeğerliğini ve içeriğin geçerliliğini korumak hedeflerine odaklanmaktadır. Müşterilerimize kültürel açıdan doğru sonuçlar vermenin ne kadar önemli olduğunu biliyoruz. Belgelerinizin dilbilgisi, anlambilim ve deyimler hakkında kaynak ve hedef dillerin kültürlerinden kapsamlı bilgiye sahip profesyoneller tarafından işlenmesini sağlamak için daima ekstra yol kat ederiz. Zaten çevrilmiş belgelerin doğruluğunu iyice gözden geçirerek ve yeniden çevirme ve geri çeviri gibi adımları kullanarak nihai bir incelemeyi doğrulayarak bu hizmete yaklaşıyoruz. Sonuç belgenizin nihai sonucunda mükemmel kaliteyi yansıtacaktır. Bugün teklif isteyin!', 'La validación lingüística se centra en los objetivos de mantener la fiabilidad, la equivalencia conceptual y la validez del contenido de un determinado documento. Sabemos lo importante que es proporcionar a nuestros clientes resultados culturalmente precisos. Siempre nos esforzamos por asegurar que sus documentos sean manejados por profesionales que poseen un amplio conocimiento de gramática, semántica y expresiones idiomáticas de culturas tanto del idioma de origen como del idioma de destino. Nos acercamos a este servicio revisando detenidamente la exactitud de los documentos ya traducidos y validando una revisión final usando pasos como la retradución y la retrotraducción. El resultado reflejará una excelente calidad en el resultado final de su documento. Solicite una cotización hoy!', 'La validation linguistique se concentre sur les objectifs de maintien de la fiabilité, de l''équivalence conceptuelle et de la validité d''un certain document. Nous savons combien il est important de fournir à nos clients des résultats culturellement précis. Nous faisons toujours un effort supplémentaire pour vous assurer que vos documents sont gérés par des professionnels qui possèdent une connaissance approfondie de la grammaire, la sémantique et les idiomes de cultures de la source et les langues cibles. Nous approchons ce service en examinant minutieusement l''exactitude des documents déjà traduits et en validant un examen final en utilisant des étapes telles que la traduction et la traduction arrière. Le résultat reflètera une excellente qualité dans le résultat final de votre document. Demandez un devis dès aujourd''hui!', 'Хэл баталгаажуулалтын тодорхой баримт найдвартай, үзэл баримтлалын эквивалент, агуулга төгөлдөр байлгах зорилт тал дээр анхаарна. Бид соёл үнэн зөв үр дүнг манай үйлчлүүлэгч хангах нь ямар чухал болохыг мэдэж байгаа. Бид үргэлж таны бичиг баримт эх үүсвэр болон зорилтот хэл аль алиныг нь соёл дүрэм, утга өргөн мэдлэг эзэмшсэн мэргэжлийн болон Idioms зохицуулагддаг гэдгийг баталгаажуулахын тулд нэмэлт бээр явж байна. Бид сайтар аль хэдийн орчуулсан баримт бичиг үнэн зөв хянаж, тухайлбал дахин орчуулахыг буцаж орчуулга зэрэг алхмуудыг ашиглан эцсийн хянан баталгаажуулах нь энэ үйлчилгээг ханддаг. үр дүн таны хайсан баримт бичгийн эцсийн үр дүнг нь маш сайн чанарын тусгасан болно. Өнөөдөр эшлэлийг хүсэх!', '語言驗證的重點是保持某一文檔的可靠性，概念等同性和內容有效性的目標。我們知道為客戶提供文化上準確的結果有多麼重要。我們總是多走一英里，以確保您的文檔由具有源語言和目標語言文化的語法，語義和習語的廣泛知識的專業人員處理。我們通過徹底審查已翻譯文檔的準確性並使用重新翻譯和反向翻譯等步驟驗證最終審核，來處理此服務。結果將在您的文檔的最終結果中反映出優秀的質量。立即索取報價！', 'Die sprachliche Validierung konzentriert sich auf die Ziele der Erhaltung der Zuverlässigkeit, der konzeptionellen Äquivalenz und der Inhaltsgültigkeit eines bestimmten Dokuments. Wir wissen, wie wichtig es ist, unseren Kunden kulturell genaue Ergebnisse zu vermitteln. Wir gehen immer die extra Meile, um sicherzustellen, dass Ihre Dokumente von Fachleuten behandelt werden, die umfangreiche Kenntnisse der Grammatik, Semantik und Idiome aus Kulturen sowohl der Quelle als auch der Zielsprachen besitzen. Wir nähern uns diesem Service, indem wir die Genauigkeit der bereits übersetzten Dokumente sorgfältig überprüfen und eine abschließende Überprüfung mit Schritten wie Rückübersetzung und Rückübersetzung überprüfen. Das Ergebnis spiegelt hervorragende Qualität im endgültigen Ergebnis Ihres Dokuments wider. Fordern Sie noch heute ein Angebot an!', 'اعتبار سنجی زبانی در اهداف حفظ قابلیت اطمینان، هم ارزی مفهومی، و اعتبار محتوای یک سند خاص متمرکز است. ما می دانیم چقدر مهم است برای ارائه به مشتریان با نتایج فرهنگی دقیق است. ما همیشه به مایل اضافی به اطمینان حاصل شود که اسناد خود را توسط حرفه ای که دارای دانش گسترده ای از دستور زبان، معناشناسی و اصطلاحات از فرهنگ هر دو منبع و زبان هدف به کار گرفته. رویکرد ما به این سرویس توسط به طور کامل بررسی دقت و صحت اسناد در حال حاضر ترجمه و اعتبار از بررسی نهایی با استفاده از مراحل مانند تبادله و ترجمه. نتیجه این خواهد کیفیت عالی در نتیجه نهایی از سند خود را منعکس کند. درخواست یک نقل قول امروز!', '언어 유효성 검사는 특정 문서의 안정성, 개념적 동등성 및 내용 유효성을 유지하는 목적에 중점을 둡니다. 문화적으로 정확한 결과를 고객에게 제공하는 것이 얼마나 중요한지 잘 알고 있습니다. 문법, 의미 및 원본 및 대상 언어의 문화에 대한 폭 넓은 지식을 가진 전문가가 귀하의 문서를 처리 할 수 ​​있도록 항상 여분의 노력을 기울입니다. Google은 이미 번역 된 문서의 정확성을 철저히 검토하고 재 번역 및 역 번역과 같은 단계를 사용하여 최종 검토를 검증함으로써이 서비스에 접근합니다. 결과는 문서의 최종 결과에서 우수한 품질을 반영합니다. 오늘 견적을 요청하십시오!', '', '', '', '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `manage_testimonials`
--

CREATE TABLE `manage_testimonials` (
  `id` int(11) NOT NULL,
  `testimonials_image` text NOT NULL,
  `testimonials_icon` text NOT NULL,
  `description_eng` text NOT NULL,
  `description_rus` text NOT NULL,
  `description_tur` text NOT NULL,
  `description_spa` text NOT NULL,
  `description_fre` text NOT NULL,
  `description_man` text NOT NULL,
  `description_can` text NOT NULL,
  `description_ger` text NOT NULL,
  `description_per` text NOT NULL,
  `description_kor` text NOT NULL,
  `description_mal` text NOT NULL,
  `description_ind` text NOT NULL,
  `description_ara` text NOT NULL,
  `description_jap` text NOT NULL,
  `client_name_eng` varchar(225) NOT NULL,
  `client_name_rus` varchar(225) NOT NULL,
  `client_name_tur` varchar(225) NOT NULL,
  `client_name_spa` varchar(225) NOT NULL,
  `client_name_fre` varchar(225) NOT NULL,
  `client_name_man` varchar(225) NOT NULL,
  `client_name_can` varchar(225) NOT NULL,
  `client_name_ger` varchar(225) NOT NULL,
  `client_name_per` varchar(225) NOT NULL,
  `client_name_kor` varchar(225) NOT NULL,
  `client_name_mal` varchar(225) NOT NULL,
  `client_name_ind` varchar(225) NOT NULL,
  `client_name_ara` varchar(225) NOT NULL,
  `client_name_jap` varchar(225) NOT NULL,
  `client_description_eng` text NOT NULL,
  `client_description_rus` text NOT NULL,
  `client_description_tur` text NOT NULL,
  `client_description_spa` text NOT NULL,
  `client_description_fre` text NOT NULL,
  `client_description_man` text NOT NULL,
  `client_description_can` text NOT NULL,
  `client_description_ger` text NOT NULL,
  `client_description_per` text NOT NULL,
  `client_description_kor` text NOT NULL,
  `client_description_mal` text NOT NULL,
  `client_description_ind` text NOT NULL,
  `client_description_ara` text NOT NULL,
  `client_description_jap` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manage_testimonials`
--

INSERT INTO `manage_testimonials` (`id`, `testimonials_image`, `testimonials_icon`, `description_eng`, `description_rus`, `description_tur`, `description_spa`, `description_fre`, `description_man`, `description_can`, `description_ger`, `description_per`, `description_kor`, `description_mal`, `description_ind`, `description_ara`, `description_jap`, `client_name_eng`, `client_name_rus`, `client_name_tur`, `client_name_spa`, `client_name_fre`, `client_name_man`, `client_name_can`, `client_name_ger`, `client_name_per`, `client_name_kor`, `client_name_mal`, `client_name_ind`, `client_name_ara`, `client_name_jap`, `client_description_eng`, `client_description_rus`, `client_description_tur`, `client_description_spa`, `client_description_fre`, `client_description_man`, `client_description_can`, `client_description_ger`, `client_description_per`, `client_description_kor`, `client_description_mal`, `client_description_ind`, `client_description_ara`, `client_description_jap`) VALUES
(3, '162274_cprofile1.jpg', '4_clogo1.png', 'Maven has a long-standing relationship with the AFC. In that time, their consultants have provided thousands of pages of translations in a number of different global languages. Maven are entrusted with translating highly-sensitive and commercially-confident documents, and have done so in a timely and professional manner. I highly recommend them. ', '', '', '', '', '', '', '', '', '', '', '', '', '', 'James Kitching', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Asian Football Confederation', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, '162274_cprofile1.jpg', '4_clogo1.png', 'Thanks to Maven International, our Asia-Pacific region training conference was a huge success. It was great to work with the friendly and helpful project managers, technicians and interpreters who made a usually complicated process extremely easy. Maven International has outdone the majority of interpretation organization that we have worked with in the past. Thank you for helping us with our conference.', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Katherin Chi', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Kiwanis International', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(5, '162274_cprofile1.jpg', '4_clogo1.png', 'On behalf of organizers of the World Islamic Economic Forum (WIEF) we would like to extend to Maven International our sincere gratefulness for the quality services you provided following your employment for professional interpretation services for the World Islamic Economic Forum of 2010 and 2012; held d in Kuala Lumpur and Johor Bahru respectively.', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Fazil Irwan', '', '', '', '', '', '', '', '', '', '', '', '', '', 'World Islamic Economic Forum', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6, '162274_cprofile1.jpg', '4_clogo1.png', 'The working relationships between AFI & Maven had so far been pleasant. With the biggest project involving bringing a group of interpreters to our flagship event in Fiji in 2016. Rasil being the main point of contact had been extremely helpful in meeting our needs. Business service team (e.g. Abdul Aziz, Rasil etc) provides swift response on our requests and prompt delivery on all translation works.', '', '', '', '', '', '', '', '', '', '', '', '', '', 'David Lee', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Alliance For Financial Inclusion', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7, '155292_cprofile2.jpg', '67773_cprofile2.jpg', 'At short notice, arrangements were made for us to go directly to the hostels and speak with groups of workers from several countries. The Maven team were very professional, flexible as needed and worked well with us auditors. The team from Maven contributed significantly to the success of the audit an nd both my colleague and I are very pleased and grateful with the level of service given.\r\n', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Thivakaran Narayanan', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Det Norske Veritas AS', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8, '162653_cprofile3.jpg', '51355_cprofile3.jpg', 'The Maven team has proven themselves to be efficient and proactive, particularly in meeting deadlines and their added value support on event management issues. Maven international exercises just the right amount of balance between being a flexible, easy to deal with organization yet having all the key strengths, attributes and rigor necessary to support any large project. It was a pleasant experience working with such a professional team.', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Sohaib Ali Jassim\nJakarta Bureau Chief', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Aljazeera Channel', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9, '162653_cprofile3.jpg', '51355_cprofile3.jpg', 'My special thanks to you and your team for the great job and effort was made in the transcreation and translation projects we have accomplished with you. The extra time and effort you put was highly appreciated.', '', '', '', '', '', '', '', '', '', '', '', '', '', '29 Content Creation', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Moazir Salah', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `footer_logo` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `site_logo` varchar(255) NOT NULL,
  `opening_time_eng` text NOT NULL,
  `opening_time_rus` text NOT NULL,
  `opening_time_tur` text NOT NULL,
  `opening_time_spa` text NOT NULL,
  `opening_time_fre` text NOT NULL,
  `opening_time_man` text NOT NULL,
  `opening_time_can` text NOT NULL,
  `opening_time_ger` text NOT NULL,
  `opening_time_per` text NOT NULL,
  `opening_time_kor` text NOT NULL,
  `opening_time_mal` text NOT NULL,
  `opening_time_ind` text NOT NULL,
  `opening_time_ara` text NOT NULL,
  `opening_time_jap` text NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `google_analytics_script` text NOT NULL,
  `facebook_link` varchar(255) NOT NULL,
  `twitter_link` varchar(255) NOT NULL,
  `linkedin_link` varchar(255) NOT NULL,
  `google_plus_link` varchar(255) NOT NULL,
  `favicon` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `footer_logo`, `phone_number`, `site_logo`, `opening_time_eng`, `opening_time_rus`, `opening_time_tur`, `opening_time_spa`, `opening_time_fre`, `opening_time_man`, `opening_time_can`, `opening_time_ger`, `opening_time_per`, `opening_time_kor`, `opening_time_mal`, `opening_time_ind`, `opening_time_ara`, `opening_time_jap`, `email_address`, `google_analytics_script`, `facebook_link`, `twitter_link`, `linkedin_link`, `google_plus_link`, `favicon`) VALUES
(1, '54834_footer-logo.png', '+185550 MAVEN (62836)', 'logo-01.png', 'Monday-Friday, 24 hour customer service!', 'Mon-Thurs: 8:30 - 16:30\r\nFr: 8:30 - 16:00 ', 'Mon-Thurs: 8:30 - 16:30\r\nFr: 8:30 - 16:00 ', 'Mon-Thurs: 8:30 - 16:30\r\nFr: 8:30 - 16:00 ', 'Mon-Thurs: 8:30 - 16:30\r\nFr: 8:30 - 16:00 ', 'Mon-Thurs: 8:30 - 16:30\r\nFr: 8:30 - 16:00 ', 'Mon-Thurs: 8:30 - 16:30\r\nFr: 8:30 - 16:00 ', 'Mon-Thurs: 8:30 - 16:30\r\nFr: 8:30 - 16:00 ', 'Mon-Thurs: 8:30 - 16:30\r\nFr: 8:30 - 16:00 ', 'Mon-Thurs: 8:30 - 16:30\r\nFr: 8:30 - 16:00 ', 'Mon-Thurs: 8:30 - 16:30\r\nFr: 8:30 - 16:00 ', 'Mon-Thurs: 8:30 - 16:30\r\nFr: 8:30 - 16:00 ', 'Mon-Thurs: 8:30 - 16:30\r\nFr: 8:30 - 16:00 ', 'Mon-Thurs: 8:30 - 16:30\r\nFr: 8:30 - 16:00 ', 'info@maven-international.com', '', '#', '#', '#', '#', '179535_Maven logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `static_lang`
--

CREATE TABLE `static_lang` (
  `id` int(11) NOT NULL,
  `title_eng` text NOT NULL,
  `title_rus` text NOT NULL,
  `title_tur` text NOT NULL,
  `title_spa` text NOT NULL,
  `title_fre` text NOT NULL,
  `title_man` text NOT NULL,
  `title_can` text NOT NULL,
  `title_ger` text NOT NULL,
  `title_per` text NOT NULL,
  `title_kor` text NOT NULL,
  `title_mal` text NOT NULL,
  `title_ind` text NOT NULL,
  `title_ara` text NOT NULL,
  `title_jap` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_lang`
--

INSERT INTO `static_lang` (`id`, `title_eng`, `title_rus`, `title_tur`, `title_spa`, `title_fre`, `title_man`, `title_can`, `title_ger`, `title_per`, `title_kor`, `title_mal`, `title_ind`, `title_ara`, `title_jap`) VALUES
(1, 'Home', 'Домашняя страница', 'Ana Sayfa', 'Casa', 'Accueil', '家', '家', 'Zuhause', 'بطرف خانه', '홈', 'Laman', 'Rumah', 'الصفحة الرئيسية', '自宅'),
(2, 'About Us', 'О нас', 'Hakkımızda', 'Sobre nosotros', '&Agrave; propos de nous', '關於我們', '关于我们', '&Uuml;ber uns', 'دربارهی ما', '회사 소개', 'Tentang kita', 'Tentang kami', 'معلومات عنا', '私たちに関しては'),
(3, 'Services', 'Услуги', 'Hizmetler', 'Servicios', 'Prestations de service', '服務', '服务', 'Dienstleistungen', 'خدمات', '서비스', 'perkhidmatan', 'Jasa', 'خدمات', 'サービス'),
(4, 'Clientele', 'Наши клиенты', 'M&uuml;şteriler', 'Clientele', 'client&egrave;le', '客戶', '客户', 'Kundschaft', 'ارباب رجوع', '소송의뢰인', 'pelanggan', 'klien', 'الزبائن', '顧客'),
(5, 'Join Our Team', 'Присоединитесь к нашей команде', 'Bize Katılın', '&Uacute;nase a nuestro equipo', 'Rejoignez notre &eacute;quipe', '加入我們的團隊', '加入我们的团队', 'Join Our Team', 'به تیم ما بپیوند', '우리의 팀에 가입', 'Sertai pasukan kami', 'Bergabung Tim Kami', 'انضمام إلى فريق عملنا', '私たちのチームに参加する'),
(6, 'Contact Us', 'Свяжитесь с нами', 'İletişim', 'cont&aacute;ctenos', 'Contactez-nous', '聯繫我們', '联系我们', 'Kontaktieren Sie uns', 'تماس با ما', '문의', 'Hubungi Kami', 'Hubungi Kami', 'اتصل بنا', 'お問い合わせ'),
(7, 'FAQ', 'FAQ', 'SSS', 'Preguntas frecuentes', 'FAQ', '常問問題', '常问问题', 'FAQ', 'پرسش و پاسخ', '자주 묻는 질문', 'FAQ', 'FAQ', 'التعليمات', 'よくある質問'),
(8, 'For translators', 'Для переводчиков', '&ccedil;evirmenler i&ccedil;in', 'para traductores', 'pour les traducteurs', '翻譯', '翻译', 'f&uuml;r &Uuml;bersetzer', 'برای مترجمان', '번역에 대한', 'untuk penterjemah', 'penerjemah', 'للمترجمين', '翻訳者の場合'),
(9, 'Login', 'Войти', 'giri&Aring;&Yuml;', 'login', 'login', '登錄', '注册', 'Login', 'ورود', '로그인', 'login', 'login', 'دخول', 'ログイン'),
(10, 'Facebook', 'Facebook', 'Facebook', 'Facebook', 'Facebook', 'Facebook', 'Facebook', 'Facebook', 'فیس بوک', '페이스 북', 'Facebook', 'Facebook', 'فيس بوك', 'フェイスブック'),
(11, 'Request a quote', 'Получить предложение', 'Hızlı teklifler', 'Necesidad r&aacute;pida oferta?', 'Besoin d''offre rapide?', '需要快速報價？', '需要快速报价？', 'Ben&ouml;tigen Sie eine schnelle Angebot?', 'نیاز ارائه سریع؟', '빠른 서비스를 필요하십니까?', 'Perlu tawaran cepat?', 'Butuh Penawaran cepat?', 'تحتاج عرض سريع؟', 'クイックオファーが必要ですか？'),
(12, 'Task type', 'Выберите услугу', 'G&ouml;rev t&uuml;r&uuml;', 'tipo de tarea', 'Type de t&acirc;che', '任務類型', '任务类型', 'Aufgabentyp', 'نوع کار', '작업 유형', 'Jenis tugas', 'Jenis tugas', 'نوع العمل', 'タスクタイプ'),
(13, 'Select Task type', 'Выберите услугу', 'G&ouml;rev Tipi Se&ccedil;iniz', 'Seleccione el Tipo de Tarea', 'S&eacute;lectionnez le type de t&acirc;ches', '選擇任務類型', '选择任务类型', 'W&auml;hlen Sie Aufgabentyp', 'نوع کار را انتخاب کنید', '작업 유형을 선택', 'Pilih jenis Task', 'Pilih jenis Task', 'اختر نوع العمل', 'タスクタイプを選択'),
(14, 'No record found', 'Записей не найдено', 'Kay&Auml;&plusmn;t bulunamad&Auml;&plusmn;', 'No se encontr&oacute; el registro', 'Aucun enregistrement trouv&eacute;', '沒有找到記錄', '没有找到记录', 'Kein Eintrag gefunden', 'هیچ سابقه ای پیدا نشد', '기록 없음', 'Rekod tidak dijumpai', 'Tidak ada catatan yang ditemukan', 'لا يوجد سجلات', 'レコードが見つかりません'),
(15, 'Please select a language', 'Выберите язык', 'Dil Se&ccedil;in', 'este campo es requerido', 'Ce champ est obligatoire', '這是必填欄', '这是必填栏', 'Dieses Feld wird ben&ouml;tigt,', 'این زمینه مورد نیاز است', '이 필드는 필수 항목입니다', 'Bidang ini diperlukan', 'Bidang ini diperlukan', 'هذه الخانة مطلوبه', 'この項目は必須です'),
(16, 'Source Language *', 'Язык оригинала', 'Kaynak dil', 'Lenguaje fuente', 'Langue originelle', '源語言', '源语言', 'Ausgangssprache', 'زبان منبع', '원어', 'bahasa sumber', 'Bahasa sumber', 'لغة مصدر', 'ソース言語'),
(17, 'Choose Language', 'Выбрать язык', 'Dilleri se&ccedil;in', 'Elija los idiomas', 'Choisissez les langues', '選擇語言', '选择语言', 'W&auml;hlen Sie die Sprachen', 'انتخاب زبان', '언어 선택', 'Pilih bahasa', 'Pilih bahasa', 'اختر اللغات', '言語を選択する'),
(18, 'Translate to', 'Язык перевода', 'E &ccedil;evir', 'Traducir a', 'Traduire en', '翻譯為', '翻译成', '&Uuml;bersetzen ins', 'ترجمه به', '번역 대상', 'terjemah ke', 'Terjemahkan ke', 'ترجمة الى', '翻訳する'),
(20, 'Deadline *', 'Срок исполнения', 'Teslim tarihi', 'Fecha tope', 'Date limite', '截止日期', '截止日期', 'Frist', 'ضرب الاجل', '마감 시간', 'Tarikh akhir', 'Batas waktu', 'الموعد النهائي', '締め切り'),
(21, 'Choose the date of deadline', 'Выбрать срок исполнения', 'Son ba&Aring;&Yuml;vuru tarihini se&ccedil;in', 'Elegir la fecha l&iacute;mite', 'Choisir la date limite', '選擇截止日期', '选择截止日期', 'W&auml;hlen Sie das Abreisedatum', 'تاریخ مهلت انتخاب', '마감일을 선택하십시오', 'Pilih tarikh tarikh akhir', 'Pilih tanggal batas waktu', 'اختيار تاريخ الموعد النهائي', '締め切り日を選択してください'),
(22, 'Next', 'Далее', 'Sonraki', 'Siguiente', 'Prochain', '下一個', '下一个', 'N&auml;chster', 'بعد', '다음 것', 'Seterusnya', 'Berikutnya', 'التالى', '次'),
(23, 'Read more testimonials', 'Читать другие отзывы', 'Referansları okuyunuz', 'Leer m&aacute;s testimonios', 'En savoir plus', '閱讀更多評價', '阅读更多评价', 'Lesen Sie mehr testimonials', 'خواندن توصیفات بیشتر', '더 많은 사용 후기 읽기', 'Baca lebih lanjut testimoni', 'Baca lebih banyak testimonial', 'قراءة المزيد من الشهادات', 'より多くの声を読む'),
(24, 'About Maven International', 'О Maven International', 'Maven International hakk&Auml;&plusmn;nda', 'Acerca de Maven International', '&Agrave; propos de Maven International', '關於Maven國際', '关于Maven国际', '&Uuml;ber Maven International', 'درباره Maven را بین المللی', '메이븐 국제 회사 소개', 'Mengenai Maven Antarabangsa', 'Tentang Maven Internasional', 'حول مخضرم الدولية', 'Maven Internationalについて'),
(25, 'Get a quote for', 'Получить предложение на', 'I&ccedil;in fiyat teklifi al', 'Obtenga un presupuesto para', 'Obtenez un devis pour', '獲取報價', '获取报价', 'Holen Sie sich ein Angebot f&uuml;r', 'یک نقل قول برای', '견적 받기', 'Dapatkan sebut harga untuk', 'Mendapatkan penawaran untuk', 'الحصول على أسعار لل', '見積もりを入手する'),
(26, '1. pick language', '1. Выберите язык', '1.Dil Se&ccedil;in', '1. escoger el idioma', '1. choisir la langue', '1. 選擇語言', '1. 选择语言', '1. Sprache w&auml;hlen', '1. انتخاب زبان', '1. 언어 선택', 'bahasa 1. pick', 'bahasa 1. pick', '1. اختيار اللغة', '1. 言語を選ぶ'),
(27, '2. Attach document', '2. Приложите документ', '2. Dosyanızı ekleyin', '2. Adjuntar documento', '2. Joindre le document', '2. 附加文檔', '2. 附加文档', '2. Dokument anh&auml;ngen', '2. ضمیمه کردن یک سند', '2.문서 첨부', '2. Lampirkan dokumen', '2. Melampirkan dokumen', '2. إرفاق المستندات', '2. 添付文書'),
(28, '3. Get a quote', '3.Получить предложение', '3. Fiyat teklifi alın', '3. Obtenga una cotizaci&oacute;n', '3. Obtenez un devis', '3. 獲取報價', '3. 获取报价', '3. Holen Sie sich ein Angebot', '3. یک نقل قول', '3. 견적', '3. Dapatkan sebut harga', '3. Dapatkan kutipan', '3. إقتبس', '3. 見積もりを取得'),
(29, 'Language Pair', 'Языковая пара', 'Dil &ccedil;ifti', 'Par de idiomas', 'Paire de langues', '語言對', '语言对', 'Sprachpaare', 'زوج زبانی', '언어 쌍', 'Pasangan bahasa', 'bahasa Pasangan', 'الزوج اللغوي', '言語ペア'),
(30, 'Add More', 'Добавить языковую пару', 'Daha ekle', 'A&ntilde;adir m&aacute;s', 'Ajouter plus', '添加更多', '添加更多', 'Mehr hinzuf&uuml;gen', 'اضافه کردن بیشتر', '더 추가', 'Tambah Lagi', 'Tambah Lagi', 'أضف المزيد', 'さらに追加'),
(32, 'Date required', 'Дата (ы)', 'Tarih gerekli', 'Fecha requerida', 'Date requise', '需要日期', '需要日期', 'Datum erforderlich', 'تاریخ مورد نیاز', '필요한 날짜', 'tarikh diperlukan', 'Tanggal dibutuhkan', 'تاريخ مطلوبة', '日付が必要'),
(33, '(You can select more than one day) .', 'больше одного дня', 'Bir g&uuml;nden fazla', 'M&aacute;s de un d&iacute;a', 'Plus d''un jour', '超過一天', '超过一天', 'Mehr als einen Tag', 'بیش از یک روز', '하루 이상', 'Lebih daripada satu hari', 'Meira en einn dag', 'أكثر من يوم واحد', '1日以上'),
(34, 'Full Day', 'полный день', 'Tam g&uuml;n', 'D&iacute;a completo', 'Journ&eacute;e compl&egrave;te', '全日', '全日', 'Ganztags', 'کل روز', '하루 종일', 'Sepanjang hari', 'Sehari penuh', 'يوم كامل', '丸1日'),
(35, 'Half Day', 'полдня', 'Yarım g&uuml;n', 'Medio d&iacute;a', 'Demi-journ&eacute;e', '半天', '半天', 'Halber Tag', 'نیم روز', '반나절', 'Setengah hari', 'Setengah hari', 'نصف يوم', '半日'),
(36, 'Select Hours', 'отборные часы', 'Saat Se&ccedil;iniz', 'Seleccione Horas', 'S&eacute;lectionnez Heures', '選擇小時', '选择小时', 'W&auml;hlen Sie Stunden aus', 'انتخاب ساعت', '시간 선택', 'Pilih Waktu', 'Pilih Jam', 'حدد ساعات', '時間の選択'),
(37, 'How many interpreters do you need', 'Сколько переводчиков требуется?', 'Ka&ccedil; terc&uuml;mana ihtiyacınız var?', '&iquest;Cu&aacute;ntos int&eacute;rpretes necesitas?', 'Combien d''interpr&egrave;tes avez-vous besoin', '你需要多少口譯員', '你需要多少口译员', 'Wie viele Dolmetscher brauchen Sie?', 'چگونه بسیاری از مفسران، شما نیاز به', '얼마나 많은 통역이 필요한가요?', 'Berapa banyak penterjemah yang anda perlukan', 'Berapa banyak penafsir yang Anda butuhkan', 'كم عدد المترجمين هل تحتاج', '何人の通訳が必要ですか？'),
(38, 'Minimum 2 required', 'Минимальное количество переводчиков на одну языковую пару - 2', 'En az 2 gerekli', 'Se requiere un m&iacute;nimo de 2', 'Minimum 2 requis', '最少需要2個', '最少需要2个', 'Minimum 2 erforderlich', 'حداقل 2 مورد نیاز', '최소 2 명 필요', 'Minimum 2 diperlukan', 'Minimum 2 diperlukan', '2 الحد الأدنى المطلوب', '必要最低限2'),
(39, 'Where do you require this service?', 'Где требуется услуга? (Например Стамбул)', 'Bu hizmete nerede ihtiyacınız var?', '&iquest;D&oacute;nde necesita este servicio?', 'Mist&auml; tarvitset t&auml;t&auml; palvelua?', '您在哪裡需要此服務？', '您在哪里需要此服务？', 'Wo ben&ouml;tigen Sie diesen Service?', 'که در آن شما نیاز این سرویس؟', '이 서비스는 어디에서 필요합니까?', 'Di manakah anda memerlukan perkhidmatan ini?', 'Di mana Anda membutuhkan layanan ini?', 'أين تتطلب هذه الخدمة؟', 'このサービスはどこに必要ですか？'),
(40, 'Dont have access to this information yet? No problem. Next', 'Не имеют доступ к этой информации пока нет? Нет проблем. следующий', 'Bu bilgilere hen&uuml;z eriÅŸemiyor musunuz? Sorun deÄŸil. Sonraki', '&iquest;No tienes acceso a esta informaci&oacute;n todav&iacute;a? No hay problema. Siguiente', 'Vous n''avez pas encore acc&egrave;s &agrave; cette information? Pas de probl&egrave;me. Prochain', '無法訪問此信息嗎？ 沒問題。 下一個', '无法访问此信息吗？ 没问题。 下一个', 'Sie haben noch keinen Zugang zu diesen Informationen? Kein Problem. N&auml;chster', 'اصلا دسترسی به این اطلاعات هنوز؟ مشکلی نیست. بعد', '이 정보에 아직 액세스 권한이 없습니까? 문제 없어. 다음 것', 'Dont mempunyai akses kepada maklumat ini lagi? Tiada masalah. Seterusnya', 'Dont memiliki akses ke informasi ini belum? Tidak masalah. Berikutnya', 'ليس لدينا إمكانية الوصول إلى هذه المعلومات حتى الآن؟ ليس هناك أى مشكلة. التالى', 'この情報へのアクセスはまだありませんか？ 問題ない。 次'),
(41, 'Event Topic*', 'Тематика*', 'Etkinlik konusu *', 'Tema del evento *', 'Sujet de l''&eacute;v&eacute;nement *', '活動主題*', '活动主题*', 'Veranstaltungsthema *', 'رویداد موضوع *', '이벤트 주제 *', 'Event Topic *', 'Acara Topik *', 'الحدث الموضوع *', 'イベントトピック*'),
(42, 'Event Start date*', 'Событие Дата начала*', 'Etkinlik baÅŸlangÄ±&ccedil; tarihi *', 'Evento Fecha de inicio *', 'Date de d&eacute;but de l''&eacute;v&eacute;nement *', '活動開始日期*', '活动开始日期*', 'Veranstaltung Startdatum *', 'تاریخ شروع رویداد *', '이벤트 시작 날짜 *', 'Event Mula Tarikh *', 'Acara Tanggal mulai *', 'الحدث تاريخ البدء *', 'イベント開始日*'),
(43, 'Event End date*', 'дата окончания события*', 'Etkinlik BitiÅŸ tarihi *', 'Fecha de finalizaci&oacute;n del evento *', 'Date de fin d''&eacute;v&eacute;nement *', '事件結束日期*', '事件结束日期*', 'Veranstaltung Enddatum *', 'رویداد تاریخ پایان *', '이벤트 종료 날짜 *', 'Acara Akhir Tarikh *', 'Acara End date *', 'حدث نهاية التاريخ *', 'イベント終了日*'),
(44, 'Event Address', 'Где требуется услуга?', 'Etkinlik adresi', 'Direcci&oacute;n del evento', 'Adresse de l''&eacute;v&eacute;nement', '事件地址', '事件地址', 'Veranstaltungsadresse', 'آدرس واقعه', '이벤트 주소', 'Alamat Event', 'Alamat Event', 'عنوان الحدث', 'イベントアドレス'),
(45, 'Private or Corporate Request :', 'Частное или юридическое лицо', '&Ouml;zel veya kurumsal talep', 'Privada o corporativa Solicitud:', 'Priv&eacute; ou entreprise Demande:', '私人或公司請求：', '私人或公司请求：', 'Private oder gesch&auml;ftliche Anfragen:', 'خصوصی و یا شرکت درخواست پاسخ به:', '개인 또는 기업 요청 :', 'Persendirian atau korporat Permintaan:', 'Pribadi atau Perusahaan Permintaan:', 'الخاص أو الشركات طلب:', '私的または企業の要求：'),
(46, 'Private', 'Частнoe лицо', '&ouml;zel', 'privado', 'priv&eacute;', '私人的', '私营', 'privat', 'خصوصی', '은밀한', 'swasta', 'pribadi', 'خاص', 'プライベート'),
(47, 'Corporate', 'Юридическое лицо', 'Kurumsal talep', 'cc', 'сс', 'сс', 'сс', 'сс', 'сс', 'ссс', 'сс', 'сс', 'сс', 'コーポレート'),
(48, 'Name*', 'Имя', 'İsim', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', '名*'),
(49, 'Email Address*', 'Имейл', 'E-posta', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', '電子メールアドレス'),
(50, 'Phone number (with country code)*', 'Телефон*', 'Telefon*', 'CC', 'СС', 'СС', 'СС', 'СС', 'СС', 'СС', 'СС', 'СС', 'СС', 'СС'),
(51, 'Company*', 'Компания', 'Şirket', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', '会社*'),
(52, 'Previous', 'Предыдущее окно', '&Ouml;nceki', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', '前'),
(53, 'Add Document', 'Выберите файл', 'Dosya se&ccedil;in', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс'),
(54, 'Click to upload document here', 'перетащите файлы сюда', 'D&ouml;k&uuml;manı buraya s&uuml;r&uuml;kleyip bırakın', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'ここにドキュメントをドラッグ＆ドロップする'),
(55, 'OR', 'или', 'veya', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'または'),
(56, 'Choose file (File not available? Next)', 'Выберите файл (Файл не доступен? Далее)', 'Dosya se&ccedil;in (Dosya kullanılamıyor mu? Sonraki)', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс'),
(57, 'Text Subject*', 'Тематика контента (текст)', 'Metin konusu (metin)', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'テキスト件名*'),
(58, 'No. of words/pages *', 'Количество слов/страниц', 'Kelime sayısı/sayfalar', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', '単語数/ページ数'),
(59, '1 standard page = 250words', '(1 стандартная страница = 250 слов)', '(1 standart sayfa = 250 kelime)', 'сс', 'СС', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', '1標準ページ= 250語'),
(60, 'Testimonials and clients', 'zz', 'zz', 'zz', 'zz', 'zz', 'zz', 'zz', 'zz', 'zz', 'zz', 'zz', 'zz', 'お客様の声'),
(61, 'Contact us', 'Свяжитесь с нами', 'İletişim', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'お問い合わせ'),
(62, 'Additional information', 'Дополнительная информация', 'Ek bilgi', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'コメント'),
(63, 'Send', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', '送信'),
(64, 'E.g.  John Doe', '', '', '', '', '', '', '', '', '', '', '', '', '例えば。 ジョン・ドウ'),
(65, 'E.g. your.mail@mail.com', '', '', '', '', '', '', '', '', '', '', '', '', '例えば。 your.mail@mail.com'),
(66, 'E.g.  Corporation Ltd', '', '', '', '', '', '', '', '', '', '', '', '', '例えば。 株式会社'),
(67, 'E.g.  01234-55555', '', '', '', '', '', '', '', '', '', '', '', '', '例えば。 01234-55555'),
(68, 'Call us', '', '', '', '', '', '', '', '', '', '', '', '', 'お電話ください'),
(69, 'Write to us', 'Напишите нам:', 'Bize e-posta ile de ulaşabilirsiniz:', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', '私たちに書いてください'),
(70, 'We will respond within 1 hour', '', '', '', '', '', '', '', '', '', '', '', '', '1時間以内に対応いたします'),
(71, 'Read more', '', '', '', '', '', '', '', '', '', '', '', '', '続きを読む'),
(72, 'Contact Maven International', '', '', '', '', '', '', '', '', '', '', '', '', 'Maven Internationalに連絡する'),
(73, 'Click Here!', '', '', '', '', '', '', '', '', '', '', '', '', 'ここをクリック！'),
(74, 'OUR CUSTOMERS TALK', '', '', '', '', '', '', '', '', '', '', '', '', '私たちの顧客は話します'),
(75, 'Thank you for contacting us. We will get back to you within next 1 hours', '', '', '', '', '', '', '', '', '', '', '', '', 'お問い合わせいただきありがとうございます。 24時間以内にご連絡させていただきます'),
(76, 'Invalid extension. Please upload .jpg or .jpeg or .gif or .png image', '', '', '', '', '', '', '', '', '', '', '', '', '無効な拡張機能です。 .jpgまたは.jpegまたは.gifまたは.png画像をアップロードしてください'),
(77, ' Terms & Conditions', '', '', '', '', '', '', '', '', '', '', '', '', '期間と条件'),
(78, 'Privacy', '', '', '', '', '', '', '', '', '', '', '', '', 'プライバシー'),
(79, 'Language A *', 'Язык А*', 'Dil A *', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', '言語A'),
(80, 'Language B *', 'Язык Б*', 'Dil B*', 'ss', 'ss', 's', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss'),
(81, 'Remove', 'убрать', 'Sil', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss'),
(82, 'Additional Information', 'Дополнительная информация', 'Ek bilgi', 'сс', 'с', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс'),
(83, 'Get A Quote', 'Получить предложение', 'Fiyat teklifi alınız', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс', 'сс'),
(84, '2. Get a quote', '2. Получить предложение', '2. Fiyat teklifi alınız', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss'),
(85, 'Company', 'Компания', 'Şirket', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'sss', 'sss', 'ss', 'ss'),
(86, 'Private or Corporate Request*', 'Частное или юридическое лицо', '&Ouml;zel veya kurumsal talep', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss', 'ss'),
(87, 'Certification (if any?) *', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(88, 'Yes', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(89, 'No', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(90, '[If Yes]', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(91, 'If no attachment, then simply click <b>Next</b>', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(92, 'Target Language *', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(93, 'Please select target languages', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(94, 'Please select a date', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(95, 'Please select a duration', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(96, 'Please enter hour', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(97, 'Please enter text subject', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(98, 'Please enter no of words/pages', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(99, 'Please select request type', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(100, 'Please enter your name', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(101, 'Please enter a valid email address', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(102, 'Please enter phone number (with country code). Example +902129094097', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(103, 'Please enter company name', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(104, 'Please select certification', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(105, 'Please select Language A', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(106, 'Please select Language B', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(107, 'Please select date(s) required', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(108, 'Please enter event topic', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(109, 'Choose the services', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(110, 'Choose Service', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(111, 'If no attachment, then kindly proceed to <b>Step 3</b>', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(112, '+185550 MAVEN (62836)', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(113, 'e.g. Medical, Legal, Technical', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(114, 'Enter no of words/pages', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(115, 'Enter your name', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(116, 'Enter a valid email address', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(117, 'Please enter phone number (with country code). Example +902129094097', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(118, 'Enter company name', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(119, 'Type of Certificate', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(120, 'e.g. Istanbul', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(121, 'E.g. sports, finance..etc ', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(122, 'Your quote reference: ', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(123, 'Court address ', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(124, 'Do you require Simultaneous Interpretation Equipment', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(125, 'Choose File (If file not available, please click Send). ', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(126, 'Translation', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(127, 'Transcreation', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(128, 'Simultaneous Interpretation', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(129, 'Consecutive Interpretation', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(130, 'Court Interpretation', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(131, 'Linguistic Validation', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(132, 'Transcreation', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(133, 'Simultaneous Interpretation', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(134, 'Consecutive Interpretation', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(135, 'Court Interpretation', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(136, 'Linguistic Validation', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `static_page_about_us`
--

CREATE TABLE `static_page_about_us` (
  `id` int(11) NOT NULL,
  `page_content_eng` text NOT NULL,
  `page_content_rus` text NOT NULL,
  `page_content_tur` text NOT NULL,
  `page_content_spa` text NOT NULL,
  `page_content_fre` text NOT NULL,
  `page_content_man` text NOT NULL,
  `page_content_can` text NOT NULL,
  `page_content_ger` text NOT NULL,
  `page_content_per` text NOT NULL,
  `page_content_kor` text NOT NULL,
  `page_content_mal` text NOT NULL,
  `page_content_ind` text NOT NULL,
  `page_content_ara` text NOT NULL,
  `page_content_jap` text NOT NULL,
  `photo` text NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_keyward` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page_about_us`
--

INSERT INTO `static_page_about_us` (`id`, `page_content_eng`, `page_content_rus`, `page_content_tur`, `page_content_spa`, `page_content_fre`, `page_content_man`, `page_content_can`, `page_content_ger`, `page_content_per`, `page_content_kor`, `page_content_mal`, `page_content_ind`, `page_content_ara`, `page_content_jap`, `photo`, `page_title`, `meta_keyward`, `meta_description`) VALUES
(1, '<p style="text-align:center"><strong>Maven International conforms to ISO 17100: 2015 and ISO 13611: 2014 standards for translation and interpretation services</strong></p>\r\n\r\n<p style="text-align:justify">Headquartered in Ottawa, Canada, Maven International is a language service provider&nbsp;offering esteemed, cost-effective language solutions. With over a decade of experience in managing language-related projects, we have partnered with top organizations worldwide, providing professional translation and interpretation services in over 100 languages. Our team of linguists has helped world leading brands overcome cultural barriers and embrace globalization. We take pride in our services and the resulting satisfaction.</p>\r\n\r\n<p style="text-align:justify">&nbsp;</p>\r\n\r\n<p style="text-align:justify">With offices in Malaysia&nbsp;and Turkey, we are geared and ready to serve the international community. Thanks to our skilled team of highly dedicated and experienced linguists, we strive to provide state-of-the-art language solutions capable of meeting various needs across a diverse group of industries. We are determined to excel and innovate in order to keep pace with the vastly expanding translation industry. Our research and development efforts are aimed at combining the sharpest creativity with the latest technology towards providing customer-oriented, quality language services. Reaching the global audience has never been simpler and more cost-efficient!</p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p>Bridging communication gaps is no longer an option, but a necessity. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, editors, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n', '<p>Bridging communication gaps is no longer an option, but a necessity. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, editors, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n', '<p>Bridging communication gaps is no longer an option, but a necessity. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, editors, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n', '<p>Bridging communication gaps is no longer an option, but a necessity. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, editors, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n', '<p>Bridging communication gaps is no longer an option, but a necessity. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, editors, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n', '<p>Bridging communication gaps is no longer an option, but a necessity. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, editors, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n', '<p>Bridging communication gaps is no longer an option, but a necessity. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, editors, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n', '<p>Bridging communication gaps is no longer an option, but a necessity. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, editors, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n', '<p>Bridging communication gaps is no longer an option, but a necessity. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, editors, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n', '<p>Bridging communication gaps is no longer an option, but a necessity. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, editors, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n', '<p>Bridging communication gaps is no longer an option, but a necessity. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, editors, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n', '<p>Bridging communication gaps is no longer an option, but a necessity. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, editors, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n', '<p>Bridging communication gaps is no longer an option, but a necessity. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, editors, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n', 'shutterstock.jpg', 'About Us', 'About Us', 'Best Interpretation and Translation Services in Malaysia, Turkey, Canada. Maven International also provide Solution.');

-- --------------------------------------------------------

--
-- Table structure for table `static_page_contact_us`
--

CREATE TABLE `static_page_contact_us` (
  `id` int(11) NOT NULL,
  `page_content` text NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_keyward` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page_contact_us`
--

INSERT INTO `static_page_contact_us` (`id`, `page_content`, `page_title`, `meta_keyward`, `meta_description`) VALUES
(1, '', 'Contact Us | Contacts of Translation company', 'certificated, professional, fast, cheap, vip, premium', 'Call the Translation company in Malaysia, Canada, Turkey. Located in Kuala Lumpur, Ottawa, Istanbul');

-- --------------------------------------------------------

--
-- Table structure for table `static_page_faq`
--

CREATE TABLE `static_page_faq` (
  `id` int(11) NOT NULL,
  `page_content` text NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_keyward` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page_faq`
--

INSERT INTO `static_page_faq` (`id`, `page_content`, `page_title`, `meta_keyward`, `meta_description`) VALUES
(1, '', 'FAQ', 'FAQ', 'FAQ');

-- --------------------------------------------------------

--
-- Table structure for table `static_page_home`
--

CREATE TABLE `static_page_home` (
  `id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_keyward` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page_home`
--

INSERT INTO `static_page_home` (`id`, `page_title`, `meta_keyward`, `meta_description`) VALUES
(1, 'Professional translation and interpretation services in Malaysia, Turkey and Canada', 'Translation, Transcreation, Simultaneous Interpretation, Consecutive Interpretation, Linguistic Validation, Court Interpretation', 'Provides Translation also Simultaneous, Consecutive, Court Interpretations and Linguistic Validation Services in Kuala Lumpur (Malaysia), in Istanbul (Turkey), and in Ottawa (Canada)');

-- --------------------------------------------------------

--
-- Table structure for table `static_page_home_first_block_four`
--

CREATE TABLE `static_page_home_first_block_four` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `link` text NOT NULL,
  `title_eng` text NOT NULL,
  `description_eng` varchar(255) NOT NULL,
  `title_rus` text NOT NULL,
  `description_rus` varchar(255) NOT NULL,
  `title_tur` text NOT NULL,
  `description_tur` varchar(255) NOT NULL,
  `title_spa` text NOT NULL,
  `description_spa` varchar(255) NOT NULL,
  `title_fre` text NOT NULL,
  `description_fre` varchar(255) NOT NULL,
  `title_man` text NOT NULL,
  `description_man` varchar(255) NOT NULL,
  `title_can` text NOT NULL,
  `description_can` varchar(255) NOT NULL,
  `title_ger` text NOT NULL,
  `description_ger` varchar(255) NOT NULL,
  `title_per` text NOT NULL,
  `description_per` varchar(255) NOT NULL,
  `title_kor` text NOT NULL,
  `description_kor` varchar(255) NOT NULL,
  `title_mal` text NOT NULL,
  `description_mal` varchar(255) NOT NULL,
  `title_ind` text NOT NULL,
  `description_ind` varchar(255) NOT NULL,
  `title_ara` text NOT NULL,
  `description_ara` varchar(255) NOT NULL,
  `title_jap` text NOT NULL,
  `description_jap` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page_home_first_block_four`
--

INSERT INTO `static_page_home_first_block_four` (`id`, `image`, `link`, `title_eng`, `description_eng`, `title_rus`, `description_rus`, `title_tur`, `description_tur`, `title_spa`, `description_spa`, `title_fre`, `description_fre`, `title_man`, `description_man`, `title_can`, `description_can`, `title_ger`, `description_ger`, `title_per`, `description_per`, `title_kor`, `description_kor`, `title_mal`, `description_mal`, `title_ind`, `description_ind`, `title_ara`, `description_ara`, `title_jap`, `description_jap`) VALUES
(1, '32104_linguistic_validation_2.png', '', 'Linguistic Validation', 'Investigate the reliability of a translated document through our Linguistic Validation process.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `static_page_home_first_block_one`
--

CREATE TABLE `static_page_home_first_block_one` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `link` text NOT NULL,
  `title_eng` text NOT NULL,
  `description_eng` varchar(255) NOT NULL,
  `title_rus` text NOT NULL,
  `description_rus` varchar(255) NOT NULL,
  `title_tur` text NOT NULL,
  `description_tur` varchar(255) NOT NULL,
  `title_spa` text NOT NULL,
  `description_spa` varchar(255) NOT NULL,
  `title_fre` text NOT NULL,
  `description_fre` varchar(255) NOT NULL,
  `title_man` text NOT NULL,
  `description_man` varchar(255) NOT NULL,
  `title_can` text NOT NULL,
  `description_can` varchar(255) NOT NULL,
  `title_ger` text NOT NULL,
  `description_ger` varchar(255) NOT NULL,
  `title_per` text NOT NULL,
  `description_per` varchar(255) NOT NULL,
  `title_kor` text NOT NULL,
  `description_kor` varchar(255) NOT NULL,
  `title_mal` text NOT NULL,
  `description_mal` varchar(255) NOT NULL,
  `title_ind` text NOT NULL,
  `description_ind` varchar(255) NOT NULL,
  `title_ara` text NOT NULL,
  `description_ara` varchar(255) NOT NULL,
  `title_jap` text NOT NULL,
  `description_jap` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page_home_first_block_one`
--

INSERT INTO `static_page_home_first_block_one` (`id`, `image`, `link`, `title_eng`, `description_eng`, `title_rus`, `description_rus`, `title_tur`, `description_tur`, `title_spa`, `description_spa`, `title_fre`, `description_fre`, `title_man`, `description_man`, `title_can`, `description_can`, `title_ger`, `description_ger`, `title_per`, `description_per`, `title_kor`, `description_kor`, `title_mal`, `description_mal`, `title_ind`, `description_ind`, `title_ara`, `description_ara`, `title_jap`, `description_jap`) VALUES
(1, '56541_translation2.png', '', 'Translation', 'A combination of technology and quality translators gives you faster services at lower prices.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `static_page_home_first_block_three`
--

CREATE TABLE `static_page_home_first_block_three` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `link` text NOT NULL,
  `title_eng` text NOT NULL,
  `description_eng` varchar(255) NOT NULL,
  `title_rus` text NOT NULL,
  `description_rus` varchar(255) NOT NULL,
  `title_tur` text NOT NULL,
  `description_tur` varchar(255) NOT NULL,
  `title_spa` text NOT NULL,
  `description_spa` varchar(255) NOT NULL,
  `title_fre` text NOT NULL,
  `description_fre` varchar(255) NOT NULL,
  `title_man` text NOT NULL,
  `description_man` varchar(255) NOT NULL,
  `title_can` text NOT NULL,
  `description_can` varchar(255) NOT NULL,
  `title_ger` text NOT NULL,
  `description_ger` varchar(255) NOT NULL,
  `title_per` text NOT NULL,
  `description_per` varchar(255) NOT NULL,
  `title_kor` text NOT NULL,
  `description_kor` varchar(255) NOT NULL,
  `title_mal` text NOT NULL,
  `description_mal` varchar(255) NOT NULL,
  `title_ind` text NOT NULL,
  `description_ind` varchar(255) NOT NULL,
  `title_ara` text NOT NULL,
  `description_ara` varchar(255) NOT NULL,
  `title_jap` text NOT NULL,
  `description_jap` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page_home_first_block_three`
--

INSERT INTO `static_page_home_first_block_three` (`id`, `image`, `link`, `title_eng`, `description_eng`, `title_rus`, `description_rus`, `title_tur`, `description_tur`, `title_spa`, `description_spa`, `title_fre`, `description_fre`, `title_man`, `description_man`, `title_can`, `description_can`, `title_ger`, `description_ger`, `title_per`, `description_per`, `title_kor`, `description_kor`, `title_mal`, `description_mal`, `title_ind`, `description_ind`, `title_ara`, `description_ara`, `title_jap`, `description_jap`) VALUES
(1, '17299_maven_icon-14.png', '', 'Consecutive Interpretation ', 'Consecutive interpretation (CI) usually occurs in smaller settings such as court hearings, interviews, conference calls, or business meetings.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `static_page_home_first_block_two`
--

CREATE TABLE `static_page_home_first_block_two` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `link` text NOT NULL,
  `title_eng` text NOT NULL,
  `description_eng` varchar(255) NOT NULL,
  `title_rus` text NOT NULL,
  `description_rus` varchar(255) NOT NULL,
  `title_tur` text NOT NULL,
  `description_tur` varchar(255) NOT NULL,
  `title_spa` text NOT NULL,
  `description_spa` varchar(255) NOT NULL,
  `title_fre` text NOT NULL,
  `description_fre` varchar(255) NOT NULL,
  `title_man` text NOT NULL,
  `description_man` varchar(255) NOT NULL,
  `title_can` text NOT NULL,
  `description_can` varchar(255) NOT NULL,
  `title_ger` text NOT NULL,
  `description_ger` varchar(255) NOT NULL,
  `title_per` text NOT NULL,
  `description_per` varchar(255) NOT NULL,
  `title_kor` text NOT NULL,
  `description_kor` varchar(255) NOT NULL,
  `title_mal` text NOT NULL,
  `description_mal` varchar(255) NOT NULL,
  `title_ind` text NOT NULL,
  `description_ind` varchar(255) NOT NULL,
  `title_ara` text NOT NULL,
  `description_ara` varchar(255) NOT NULL,
  `title_jap` text NOT NULL,
  `description_jap` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page_home_first_block_two`
--

INSERT INTO `static_page_home_first_block_two` (`id`, `image`, `link`, `title_eng`, `description_eng`, `title_rus`, `description_rus`, `title_tur`, `description_tur`, `title_spa`, `description_spa`, `title_fre`, `description_fre`, `title_man`, `description_man`, `title_can`, `description_can`, `title_ger`, `description_ger`, `title_per`, `description_per`, `title_kor`, `description_kor`, `title_mal`, `description_mal`, `title_ind`, `description_ind`, `title_ara`, `description_ara`, `title_jap`, `description_jap`) VALUES
(1, '16758_simultaneous_interpretation_2.png', '', 'Simultaneous Interpretation', 'Simultaneous interpretation (SI) is a specialized practice that demands an accurate. oral translation done concurrently with the source speaker. ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `static_page_home_second_block_four`
--

CREATE TABLE `static_page_home_second_block_four` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `link` text NOT NULL,
  `title_eng` text NOT NULL,
  `description_eng` varchar(255) NOT NULL,
  `title_rus` text NOT NULL,
  `description_rus` varchar(255) NOT NULL,
  `title_tur` text NOT NULL,
  `description_tur` varchar(255) NOT NULL,
  `title_spa` text NOT NULL,
  `description_spa` varchar(255) NOT NULL,
  `title_fre` text NOT NULL,
  `description_fre` varchar(255) NOT NULL,
  `title_man` text NOT NULL,
  `description_man` varchar(255) NOT NULL,
  `title_can` text NOT NULL,
  `description_can` varchar(255) NOT NULL,
  `title_ger` text NOT NULL,
  `description_ger` varchar(255) NOT NULL,
  `title_per` text NOT NULL,
  `description_per` varchar(255) NOT NULL,
  `title_kor` text NOT NULL,
  `description_kor` varchar(255) NOT NULL,
  `title_mal` text NOT NULL,
  `description_mal` varchar(255) NOT NULL,
  `title_ind` text NOT NULL,
  `description_ind` varchar(255) NOT NULL,
  `title_ara` text NOT NULL,
  `description_ara` varchar(255) NOT NULL,
  `title_jap` text NOT NULL,
  `description_jap` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `static_page_home_second_block_one`
--

CREATE TABLE `static_page_home_second_block_one` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `link` text NOT NULL,
  `title_eng` text NOT NULL,
  `description_eng` varchar(255) NOT NULL,
  `title_rus` text NOT NULL,
  `description_rus` varchar(255) NOT NULL,
  `title_tur` text NOT NULL,
  `description_tur` varchar(255) NOT NULL,
  `title_spa` text NOT NULL,
  `description_spa` varchar(255) NOT NULL,
  `title_fre` text NOT NULL,
  `description_fre` varchar(255) NOT NULL,
  `title_man` text NOT NULL,
  `description_man` varchar(255) NOT NULL,
  `title_can` text NOT NULL,
  `description_can` varchar(255) NOT NULL,
  `title_ger` text NOT NULL,
  `description_ger` varchar(255) NOT NULL,
  `title_per` text NOT NULL,
  `description_per` varchar(255) NOT NULL,
  `title_kor` text NOT NULL,
  `description_kor` varchar(255) NOT NULL,
  `title_mal` text NOT NULL,
  `description_mal` varchar(255) NOT NULL,
  `title_ind` text NOT NULL,
  `description_ind` varchar(255) NOT NULL,
  `title_ara` text NOT NULL,
  `description_ara` varchar(255) NOT NULL,
  `title_jap` text NOT NULL,
  `description_jap` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `static_page_home_second_block_three`
--

CREATE TABLE `static_page_home_second_block_three` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `link` text NOT NULL,
  `title_eng` text NOT NULL,
  `description_eng` varchar(255) NOT NULL,
  `title_rus` text NOT NULL,
  `description_rus` varchar(255) NOT NULL,
  `title_tur` text NOT NULL,
  `description_tur` varchar(255) NOT NULL,
  `title_spa` text NOT NULL,
  `description_spa` varchar(255) NOT NULL,
  `title_fre` text NOT NULL,
  `description_fre` varchar(255) NOT NULL,
  `title_man` text NOT NULL,
  `description_man` varchar(255) NOT NULL,
  `title_can` text NOT NULL,
  `description_can` varchar(255) NOT NULL,
  `title_ger` text NOT NULL,
  `description_ger` varchar(255) NOT NULL,
  `title_per` text NOT NULL,
  `description_per` varchar(255) NOT NULL,
  `title_kor` text NOT NULL,
  `description_kor` varchar(255) NOT NULL,
  `title_mal` text NOT NULL,
  `description_mal` varchar(255) NOT NULL,
  `title_ind` text NOT NULL,
  `description_ind` varchar(255) NOT NULL,
  `title_ara` text NOT NULL,
  `description_ara` varchar(255) NOT NULL,
  `title_jap` text NOT NULL,
  `description_jap` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `static_page_home_second_block_two`
--

CREATE TABLE `static_page_home_second_block_two` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `link` text NOT NULL,
  `title_eng` text NOT NULL,
  `description_eng` varchar(255) NOT NULL,
  `title_rus` text NOT NULL,
  `description_rus` varchar(255) NOT NULL,
  `title_tur` text NOT NULL,
  `description_tur` varchar(255) NOT NULL,
  `title_spa` text NOT NULL,
  `description_spa` varchar(255) NOT NULL,
  `title_fre` text NOT NULL,
  `description_fre` varchar(255) NOT NULL,
  `title_man` text NOT NULL,
  `description_man` varchar(255) NOT NULL,
  `title_can` text NOT NULL,
  `description_can` varchar(255) NOT NULL,
  `title_ger` text NOT NULL,
  `description_ger` varchar(255) NOT NULL,
  `title_per` text NOT NULL,
  `description_per` varchar(255) NOT NULL,
  `title_kor` text NOT NULL,
  `description_kor` varchar(255) NOT NULL,
  `title_mal` text NOT NULL,
  `description_mal` varchar(255) NOT NULL,
  `title_ind` text NOT NULL,
  `description_ind` varchar(255) NOT NULL,
  `title_ara` text NOT NULL,
  `description_ara` varchar(255) NOT NULL,
  `title_jap` text NOT NULL,
  `description_jap` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `static_page_home_translation_agency`
--

CREATE TABLE `static_page_home_translation_agency` (
  `id` int(11) NOT NULL,
  `translation_agency_eng` longtext NOT NULL,
  `translation_agency_rus` longtext NOT NULL,
  `translation_agency_tur` longtext NOT NULL,
  `translation_agency_spa` longtext NOT NULL,
  `translation_agency_fre` longtext NOT NULL,
  `translation_agency_man` longtext NOT NULL,
  `translation_agency_can` longtext NOT NULL,
  `translation_agency_ger` longtext NOT NULL,
  `translation_agency_per` longtext NOT NULL,
  `translation_agency_kor` longtext NOT NULL,
  `translation_agency_mal` longtext NOT NULL,
  `translation_agency_ind` longtext NOT NULL,
  `translation_agency_ara` longtext NOT NULL,
  `translation_agency_jap` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page_home_translation_agency`
--

INSERT INTO `static_page_home_translation_agency` (`id`, `translation_agency_eng`, `translation_agency_rus`, `translation_agency_tur`, `translation_agency_spa`, `translation_agency_fre`, `translation_agency_man`, `translation_agency_can`, `translation_agency_ger`, `translation_agency_per`, `translation_agency_kor`, `translation_agency_mal`, `translation_agency_ind`, `translation_agency_ara`, `translation_agency_jap`) VALUES
(1, '<p>Headquartered in Kuala Lumpur, Malaysia, Maven International is a language service provider offering esteemed, cost-effective solutions. With over a decade of experience in managing language-related projects, we have partnered with top organizations worldwide, providing professional translation and interpretation services in over 100 languages. Our team of linguists has helped world leading brands overcome cultural barriers and embrace globalization. We take pride in our services and the resulting satisfaction. With offices in Malaysia, Canada and Turkey, we are geared and ready to serve the international community.</p>\r\n', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `static_page_join_team`
--

CREATE TABLE `static_page_join_team` (
  `id` int(11) NOT NULL,
  `page_content_eng` longtext NOT NULL,
  `page_content_rus` text NOT NULL,
  `page_content_tur` text NOT NULL,
  `page_content_spa` text NOT NULL,
  `page_content_fre` text NOT NULL,
  `page_content_man` text NOT NULL,
  `page_content_can` text NOT NULL,
  `page_content_ger` text NOT NULL,
  `page_content_per` text NOT NULL,
  `page_content_kor` text NOT NULL,
  `page_content_mal` text NOT NULL,
  `page_content_ind` text NOT NULL,
  `page_content_ara` text NOT NULL,
  `page_content_jap` text NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_keyward` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page_join_team`
--

INSERT INTO `static_page_join_team` (`id`, `page_content_eng`, `page_content_rus`, `page_content_tur`, `page_content_spa`, `page_content_fre`, `page_content_man`, `page_content_can`, `page_content_ger`, `page_content_per`, `page_content_kor`, `page_content_mal`, `page_content_ind`, `page_content_ara`, `page_content_jap`, `page_title`, `meta_keyward`, `meta_description`) VALUES
(1, '<p style="margin-left:.5in">Bridging communication gaps is no longer an option. The language industry is rapidly expanding and so are we. At Maven International, we rely on a set of highly specialized translators, interpreters, revisers, and language consultants under the management of our experienced team, and in strict conformity with internationally recognized standards.</p>\r\n\r\n<p style="margin-left:.5in">&nbsp;</p>\r\n\r\n<p style="margin-left:.5in"><strong>Submit your application</strong></p>\r\n\r\n<p style="margin-left:.5in">Expanding our team is always an objective. If you are an experienced translator or interpreter with qualifying credentials, we would gladly welcome you into our pool of linguists. Send us your application today!</p>\r\n\r\n<p style="margin-left:.5in">&nbsp;</p>\r\n\r\n<p style="margin-left:.5in"><strong>A qualifying translator must meet one of the following requirements:</strong></p>\r\n\r\n<ul>\r\n	<li>Advanced translation studies (recognized qualification)</li>\r\n	<li>Equivalent qualification in another specialization plus a minimum of two years documented experience in translation.</li>\r\n	<li>At least five years of documented professional experience in translation.</li>\r\n</ul>\r\n\r\n<p style="margin-left:.5in"><strong>A qualifying interpreter must meet one of the following requirements:</strong></p>\r\n\r\n<ul>\r\n	<li>A recognized degree (BA., MA. or Ph.D.) in interpretation from an institution of higher education, or a recognized educational certificate in the field of interpretation</li>\r\n	<li>A recognized degree in any other field from an institution of higher education plus two years of continuous interpretation experience;</li>\r\n	<li>An attestation of competence in interpretation awarded by an appropriate government body, government-accredited body, or recognized professional organization in the field of interpretation, and proof of other equivalent qualifications or interpretation experience</li>\r\n	<li>Membership in an existing nationwide register of interpreters with clear qualification criteria</li>\r\n	<li>Five years of continuous interpretation experience</li>\r\n	<li>A certificate of attendance in a recognized interpretation training program</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Shortlisted candidates will be contacted for further screening, which includes a detailed interview as well as a practical language proficiency and translation test. Send your applications to <a href="mailto:hr@maven-international.com">hr@maven-international.com</a> or contact any of our offices for further information.</p>\r\n', '', '', '', '', '', '', '', '<p>a</p>\r\n', '', '<p>c</p>\r\n', '', '', '', 'Join Our Team', 'Join Our Team', 'Join Our Team');

-- --------------------------------------------------------

--
-- Table structure for table `static_page_our_client`
--

CREATE TABLE `static_page_our_client` (
  `id` int(11) NOT NULL,
  `page_content` text NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_keyward` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page_our_client`
--

INSERT INTO `static_page_our_client` (`id`, `page_content`, `page_title`, `meta_keyward`, `meta_description`) VALUES
(1, '', 'Our Clients', 'gazprom, fifa, afc, ioc, unhcr, reuters, aljazeera, lukoil, petronas, embassy of sweden, malay chamber', 'Our Clients are Sport Organizations, Embassies, Oil &amp; Gas, Banks, News, Finance, Education, Development, Manufacturing, Chambers of commerce');

-- --------------------------------------------------------

--
-- Table structure for table `static_page_our_service`
--

CREATE TABLE `static_page_our_service` (
  `id` int(11) NOT NULL,
  `page_content` text NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_keyward` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page_our_service`
--

INSERT INTO `static_page_our_service` (`id`, `page_content`, `page_title`, `meta_keyward`, `meta_description`) VALUES
(1, '', 'Our Service', 'Our Service', 'Our Service');

-- --------------------------------------------------------

--
-- Table structure for table `static_page_privacy`
--

CREATE TABLE `static_page_privacy` (
  `id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_keyward` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `page_content_eng` text NOT NULL,
  `page_content_rus` text NOT NULL,
  `page_content_tur` text NOT NULL,
  `page_content_spa` text NOT NULL,
  `page_content_fre` text NOT NULL,
  `page_content_man` text NOT NULL,
  `page_content_can` text NOT NULL,
  `page_content_ger` text NOT NULL,
  `page_content_per` text NOT NULL,
  `page_content_kor` text NOT NULL,
  `page_content_mal` text NOT NULL,
  `page_content_ind` text NOT NULL,
  `page_content_ara` text NOT NULL,
  `page_content_jap` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `static_page_privacy`
--

INSERT INTO `static_page_privacy` (`id`, `page_title`, `meta_keyward`, `meta_description`, `page_content_eng`, `page_content_rus`, `page_content_tur`, `page_content_spa`, `page_content_fre`, `page_content_man`, `page_content_can`, `page_content_ger`, `page_content_per`, `page_content_kor`, `page_content_mal`, `page_content_ind`, `page_content_ara`, `page_content_jap`) VALUES
(1, 'Privacy', 'Privacy', 'Privacy', '<p><strong>1.1. Your Privacy</span></strong></p>\r\n<p>This page explains our privacy policy which includes the use and protection of any information submitted by visitors. If you choose to register and transact using our website or send an e-mail which provides personally identifiable data, we will use it so as to serve you in the most efficient and effective manner. We will not share this information with third party without having your explicit agreement.</p>\r\n<p><strong>1.2. Information Collected</strong></p>\r\n<p>No personally identifiable information is gathered during the browsing of the UniMAP website except for information given by you via e-mails, or web forms.</p>\r\n<p><strong>1.3. Links to other websites</strong></p>\r\n<p>Maven International&nbsp;website contains links to other websites, both Government and Non-Government organisations. This privacy policy applies only to our site and you should be aware that other sites linked by the website may have different privacy policies and we highly recommend that you read and understand the privacy statements of each site.</p>\r\n<p><strong>1.4. Changes to this Policy</strong></p>\r\n<p>If this privacy policy changes in any way, it will be updated on this page. Regularly reviewing this page ensures you are updated on the information which is collected, how it is used and under what circumstances, if any, it is shared with other parties.</p>\r\n<p><strong>1.5. Queries or Complaints</strong></p>\r\n<p>If you have any queries or complaints about this privacy policy or about the website, you can contact us.</p>\r\n<p><strong>2. Legal Disclaimer</strong></p>\r\n<p><strong>2.1. General Notice</strong></p>\r\n<p>Maven International&nbsp;makes no warranties with respect to the document, or any part thereof, including any warrantees of title, noninfringement of copyright or patent rights of others, merchantability, or fitness or suitability for any purpose.</p>\r\n<p><strong>2.2. Limitation and exclusion of warranties and liability</strong></p>\r\n<p>Whilst we endeavour to ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we commit to ensuring that the website remains  available or that the material on the website is kept up-to-date.<br>	To the maximum extent permitted by applicable law we exclude all representations, warranties and conditions relating to this website and the use of this website (including, without limitation, any warranties implied by law of satisfactory quality, fitness for purpose and/or the use of reasonable care and skill).<br>We will not be liable to you in respect of any loss or corruption of any data, database or software.</p>\r\n<p><strong>2.3. Entire agreement</strong></p>\r\n<p>This disclaimer, together with our privacy policy, constitutes the entire agreement between you and us in relation to your use of our website, and supersedes all previous agreements in respect of your use of this website.</p>\r\n<p><strong>2.4. Copyright</strong></p>\r\n<p>All aspects concerning this website which include designs, texts, graphics, applications, software and code sources are the copyrights of Maven International unless otherwise mentioned.<br>No part of the contents or materials available on this website may be reproduced, licensed, sold, published, transmitted, modified, adapted, publicly displayed, broadcast (including storage in any medium by electronic means whether or not transiently for any purpose saved as permitted herein) without the prior written permission of&nbsp;Maven International></p>', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `supporters`
--

CREATE TABLE `supporters` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `supp_name` text NOT NULL,
  `website_link` varchar(255) NOT NULL,
  `description` varchar(32) DEFAULT NULL,
  `slide_position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supporters`
--

INSERT INTO `supporters` (`id`, `image`, `supp_name`, `website_link`, `description`, `slide_position`) VALUES
(1, 'fifa.png', 'THE FÉDÉRATION INTERNATIONALE DE FOOTBALL ASSOCIATION (FIFA)', '', 'sport', 1),
(2, 'afc.png', 'ASIAN FOOTBAL CONFEDERATION (AFC)', '', 'sport', 12),
(3, 'ipc.gif', 'INTERNATIONAL PARALYMPIC COMMITTEE (IPC)', '', 'sport', 9),
(4, 'ihf.png', 'INTERNATIONAL HOCKEY FEDERATION (FIH)', '', 'sport', 0),
(5, 'ocm.png', 'MALAYSIA OLYMPIC COMMITTEE', '', 'sport', 0),
(6, 'ioc.png', 'INTERNATIONAL OLYMPIC COMMITTEE', '', 'sport', 5),
(7, 'aus.jpg', 'EMBASSY OF AUSTRALIA', '', 'embassie', 0),
(9, 'foreign-commonwealth.png', 'EMBASSY OF THE UNITED KINGDOM', '', 'embassie', 0),
(10, 'jordan.jpg', 'EMBASSY OF JORDAN', '', 'embassie', 0),
(11, 'kazak.jpg', 'EMBASSY OF THE REPUBLIC OF KAZAKHSTAN', '', 'embassie', 0),
(15, 'swed.png', 'EMBASSY OF SWEDEN', '', 'embassie', 0),
(17, 'interpol.jpg', 'INTERPOL', '', 'non-profit', 3),
(22, 'unhcr.jpg', 'THE OFFICE OF THE UNITED NATIONS HIGH COMMISSIONER FOR REFUGEES (UNHCR)', '', 'non-profit', 8),
(23, 'irc.png', 'INTERNATIONAL RESCUE COMMITTEE (IRC)', '', 'non-profit', 10),
(24, 'un.jpg', 'UNITED NATIONS', '', 'non-profit', 4),
(25, 'icrc.png', 'INTERNATIONAL COMMITTEE OF THE RED CROSS', '', 'non-profit', 13),
(26, 'kiwanis.jpg', 'KIWANIS INTERNATIONAL', '', 'non-profit', 0),
(27, 'mercy.jpg', 'MERCY MISSION', '', 'non-profit', 0),
(28, 'iom.png', 'INTERNATIONAL ORGANIZATION FOR MIGRATION', '', 'non-profit', 0),
(29, 'ti.jpg', 'TRANSPARENCY INTERNATIONAL', '', 'non-profit', 0),
(44, 'lukoil.png', 'LUKOIL', '', 'oil-gas', 0),
(45, 'gazprom.png', 'GAZPROM', '', 'oil-gas', 14),
(46, 'petronas.jpg', 'PETRONAS', '', 'oil-gas', 0),
(47, 'dnvgl.png', 'DNV GL', '', 'oil-gas', 0),
(48, 'wief.jpg', 'WORLD ISLAMIC ECONOMIC FORUM (WIEF)', '', 'bank-fin', 11),
(49, 'bankaljazeera.jpg', 'BANK ALJAZIRA', '', 'bank-fin', 0),
(50, 'faa.png', 'FINANCE ACCREDITATION AGENCY (FAA)', '', 'bank-fin', 0),
(51, 'hsbc.jpg', 'HSBC AMANAH', '', 'bank-fin', 0),
(52, 'redmoney.png', 'RED MONEY GROUP', '', 'bank-fin', 0),
(53, 'afi.png', 'ALLIANCE FOR FINANCIAL INCLUSION (AIF)', '', 'bank-fin', 0),
(54, 'rhb.png', 'RHB BANK', '', 'bank-fin', 0),
(55, 'br.jpg', 'BRITISH COUNCIL', '', 'others', 7),
(56, 'aljazeera.jpg', 'AL JAZEERA', '', 'others', 2),
(57, 'uem.jpg', 'UEM SUNRISE', '', 'others', 0),
(58, 'suriaklcc.png', 'SURIA KLCC', '', 'others', 0),
(59, 'kumon.png', 'KUMON EDUCATION', '', 'others', 0),
(60, 'burve.png', 'BUREAU VERITAS', '', 'others', 15),
(61, 'thomson.jpg', 'THOMSON REUTERS', '', 'others', 6),
(62, 'scope.png', 'SCOPE GROUP', '', 'others', 0),
(63, 'smiths.png', 'SMITH DETECTION', '', 'others', 0),
(64, 'lcwalkiki.png', 'LC WAIKIKI', '', 'others', 0),
(65, 'sher.png', 'SHERATON HOTELS AND RESORTS', '', 'others', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;