<?php

include('init.php');

$faq_content = find('first', STATIC_PAGE_JOIN_TEAM, '*', "WHERE id = 1", array());

?>

<!DOCTYPE html>

<html lang="en">

   <head>

      <title><?=$faq_content['page_title'];?></title>

	  <meta name="description" content="<?=$faq_content['meta_description'];?>">

	  <meta name="keywords" content="<?=$faq_content['meta_keyward'];?>">

	  <link rel='shortcut icon' type='image/x-icon' href='<?php echo(DOMAIN_NAME_PATH);?>images/misc/<?=$general_settings['favicon'];?>' />

	  <?php include('meta.php');?>

   </head>

   <body>		

	 <?php include('mobile_menu.php');?>

	 <?php include('topbar.php');?>

     <?php include('inner_page_header.php');?>	 

	  <div class="inner_section">

		<div class="container">

			<div class="row">

				<div class="col-md-8 lpd">

					<div class="">

						<div class="col-xs-12 npd">

							<h1 class="con-hd con-hdbig"><?php echo(LANG_5);?></h1>

							<div class="">

								<div class="col-xs-12 npd">

									<?=$faq_content['page_content_'.$_SESSION['lan']];?>

								</div>

								<div class="clearfix"></div>						

							</div>

						</div>

						<div class="clearfix"></div>						

					</div>

				</div>

				<!-- <div class="col-md-4 rpd hidden-xs">

					<div class="sp-body" >

						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='<?php echo(DOMAIN_NAME_PATH);?>faq.php'"><?php echo(LANG_7);?></h1>

						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='<?php echo(DOMAIN_NAME_PATH);?>contactos.php'"><?php echo(LANG_6);?></h1>

						<h1 class="con-hd" style="cursor:pointer;" onclick="window.location.href='http://grtechdemo.com/maven/privacy.php'"><?php echo(LANG_77);?></h1>

					</div>

				</div> -->

				<div class="clearfix"></div>

			</div>

		</div>

	  </div>

	  <?php include('footer.php');?>

      <?php include('script.php');?>

   </body>

</html>



